-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th7 20, 2017 lúc 02:38 PM
-- Phiên bản máy phục vụ: 10.0.29-MariaDB-cll-lve
-- Phiên bản PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `kcuhbrue_dhms`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `value`) VALUES
(10, 'Kích thước', ''),
(11, 'Màu sắc', 'xanh'),
(12, 'Màu sắc', 'do'),
(13, 'Kích thước', '24'),
(14, 'Kích thước', '25'),
(15, 'Kích thước', '26'),
(16, 'Kiểu dáng', 'thon'),
(17, '', 'XL'),
(18, '', 'L'),
(19, '', 'M'),
(20, '', 'S'),
(21, 'Kích thước', 'M'),
(22, 'Kích thước', 'X'),
(23, 'Kích thước', 'L'),
(24, 'Màu sắc', 'Đỏ'),
(25, 'Kích thước', '39'),
(26, 'Kích thước', '40'),
(27, 'Kích thước', '41'),
(28, 'Kích thước', '42'),
(29, 'Kích thước', '43'),
(30, 'Màu sắc', 'Tím'),
(31, 'Màu sắc', 'Vàng'),
(32, 'Chất liệu', 'Inox'),
(33, 'Chất liệu', 'Thép'),
(34, 'Chất liệu', 'Nhựa'),
(35, 'Tiêu đề', 'Default Title'),
(36, 'Kích thước', 'XL'),
(37, 'Kích thước', '44'),
(38, '', 'Caro'),
(39, 'Tiêu đề', 'Default Title'),
(40, 'Màu sắc', '345345'),
(41, 'Màu sắc', '3453456445'),
(42, 'Kích thước', '345345'),
(43, 'Kích thước', '3453445665'),
(44, 'Tiêu đề', 'Default Title'),
(45, 'Tiêu đề', 'Default Title'),
(46, 'Tiêu đề', 'Default Title'),
(47, 'Tiêu đề', 'Default Title'),
(48, 'Tiêu đề', 'Default Title'),
(49, 'Tiêu đề', 'Default Title'),
(50, 'Tiêu đề', 'Default Title'),
(51, 'Tiêu đề', 'Default Title'),
(52, 'Tiêu đề', 'Default Title'),
(53, 'Tiêu đề', 'Default Title'),
(54, 'Tiêu đề', 'Default Title'),
(55, 'Tiêu đề', 'Default Title'),
(56, 'Tiêu đề', 'Default Title'),
(57, 'Tiêu đề', 'Default Title'),
(58, 'Tiêu đề', 'Default Title'),
(59, 'Tiêu đề', 'Default Title'),
(60, 'Tiêu đề', 'Default Title'),
(61, 'Tiêu đề', 'Default Title'),
(62, 'Tiêu đề', 'Default Title'),
(63, 'Tiêu đề', 'Default Title'),
(64, 'Tiêu đề', 'Default Title'),
(65, 'Tiêu đề', 'Default Title'),
(66, 'Tiêu đề', 'Default Title'),
(67, 'Tiêu đề', 'Default Title'),
(68, 'Tiêu đề', 'Default Title'),
(69, 'Tiêu đề', 'Default Title'),
(70, 'Tiêu đề', 'Default Title'),
(71, 'Tiêu đề', 'Default Title'),
(72, 'Tiêu đề', 'Default Title'),
(73, 'Tiêu đề', 'Default Title'),
(74, 'Tiêu đề', 'Default Title'),
(75, 'Tiêu đề', 'Default Title'),
(76, 'Tiêu đề', 'Default Title'),
(77, 'Tiêu đề', 'Default Title'),
(78, 'Tiêu đề', 'Default Title'),
(79, 'Tiêu đề', 'Default Title'),
(80, 'Tiêu đề', 'Default Title'),
(81, 'Tiêu đề', 'Default Title'),
(82, 'Tiêu đề', 'Default Title'),
(83, 'Tiêu đề', 'Default Title'),
(84, 'Tiêu đề', 'Default Title'),
(85, 'Tiêu đề', 'Default Title'),
(86, 'Tiêu đề', 'Default Title'),
(87, 'Tiêu đề', 'Default Title'),
(88, 'Tiêu đề', 'Default Title'),
(89, 'Tiêu đề', 'Default Title'),
(90, 'Tiêu đề', 'Default Title'),
(91, 'Tiêu đề', 'Default Title'),
(92, 'Tiêu đề', 'Default Title'),
(93, 'Tiêu đề', 'Default Title'),
(94, 'Tiêu đề', 'Default Title'),
(95, 'Tiêu đề', 'Default Title'),
(96, 'Tiêu đề', 'Default Title'),
(97, 'Tiêu đề', 'Default Title'),
(98, 'Tiêu đề', 'Default Title'),
(99, 'Tiêu đề', 'Default Title'),
(100, 'Tiêu đề', 'Default Title'),
(101, 'Tiêu đề', 'Default Title'),
(102, 'Tiêu đề', 'Default Title'),
(103, 'Tiêu đề', 'Default Title'),
(104, 'Tiêu đề', 'Default Title'),
(105, 'Tiêu đề', 'Default Title'),
(106, 'Tiêu đề', 'Default Title'),
(107, 'Tiêu đề', 'Default Title'),
(108, 'Tiêu đề', 'Default Title'),
(109, 'Tiêu đề', 'Default Title'),
(110, 'Tiêu đề', 'Default Title'),
(111, 'Tiêu đề', 'Default Title'),
(112, 'Tiêu đề', 'Default Title'),
(113, 'Tiêu đề', 'Default Title'),
(114, 'Tiêu đề', 'Default Title'),
(115, 'Tiêu đề', 'Default Title'),
(116, 'Tiêu đề', 'Default Title'),
(117, 'Tiêu đề', 'Default Title'),
(118, 'Tiêu đề', 'Default Title'),
(119, 'Tiêu đề', 'Default Title'),
(120, 'Tiêu đề', 'Default Title'),
(121, 'Tiêu đề', 'Default Title'),
(122, 'Tiêu đề', 'Default Title'),
(123, 'Tiêu đề', 'Default Title'),
(124, 'Tiêu đề', 'Default Title'),
(125, 'Tiêu đề', 'Default Title'),
(126, 'Tiêu đề', 'Default Title'),
(127, 'Tiêu đề', 'Default Title'),
(128, 'Tiêu đề', 'Default Title'),
(129, 'Tiêu đề', 'Default Title'),
(130, 'Tiêu đề', 'Default Title'),
(131, 'Tiêu đề', 'Default Title'),
(132, 'Tiêu đề', 'Default Title'),
(133, 'Tiêu đề', 'Default Title'),
(134, 'Tiêu đề', 'Default Title'),
(135, 'Tiêu đề', 'Default Title'),
(136, 'Tiêu đề', 'Default Title'),
(137, 'Tiêu đề', 'Default Title'),
(138, 'Tiêu đề', 'Default Title'),
(139, 'Tiêu đề', 'Default Title'),
(140, 'Tiêu đề', 'Default Title'),
(141, 'Tiêu đề', 'Default Title'),
(142, 'Tiêu đề', 'Default Title'),
(143, 'Tiêu đề', 'Default Title'),
(144, 'Tiêu đề', 'Default Title'),
(145, 'Tiêu đề', 'Default Title'),
(146, 'Tiêu đề', 'Default Title'),
(147, 'Tiêu đề', 'Default Title'),
(148, 'Tiêu đề', 'Default Title'),
(149, 'Tiêu đề', 'Default Title'),
(150, 'Tiêu đề', 'Default Title'),
(151, 'Tiêu đề', 'Default Title'),
(152, 'Tiêu đề', 'Default Title'),
(153, 'Tiêu đề', 'Default Title'),
(154, 'Tiêu đề', 'Default Title'),
(155, 'Tiêu đề', 'Default Title'),
(156, 'Tiêu đề', 'Default Title'),
(157, 'Tiêu đề', 'Default Title'),
(158, 'Tiêu đề', 'Default Title'),
(159, 'Tiêu đề', 'Default Title'),
(160, 'Tiêu đề', 'Default Title'),
(161, 'Tiêu đề', 'Default Title'),
(162, 'Tiêu đề', 'Default Title'),
(163, 'Tiêu đề', 'Default Title'),
(164, 'Tiêu đề', 'Default Title'),
(165, 'Tiêu đề', 'Default Title'),
(166, 'Tiêu đề', 'Default Title'),
(167, 'Tiêu đề', 'Default Title'),
(168, 'Tiêu đề', 'Default Title'),
(169, 'Tiêu đề', 'Default Title'),
(170, 'Tiêu đề', 'Default Title'),
(171, 'Tiêu đề', 'Default Title'),
(172, 'Tiêu đề', 'Default Title'),
(173, 'Tiêu đề', 'Default Title'),
(174, 'Tiêu đề', 'Default Title'),
(175, 'Tiêu đề', 'Default Title'),
(176, 'Tiêu đề', 'Default Title'),
(177, 'Tiêu đề', 'Default Title'),
(178, 'Tiêu đề', 'Default Title'),
(179, 'Tiêu đề', 'Default Title'),
(180, 'Tiêu đề', 'Default Title'),
(181, 'Tiêu đề', 'Default Title'),
(182, 'Tiêu đề', 'Default Title'),
(183, 'Tiêu đề', 'Default Title'),
(184, 'Tiêu đề', 'Default Title'),
(185, 'Tiêu đề', 'Default Title'),
(186, 'Tiêu đề', 'Default Title'),
(187, 'Tiêu đề', 'Default Title'),
(188, 'Tiêu đề', 'Default Title'),
(189, 'Tiêu đề', 'Default Title'),
(190, 'Tiêu đề', 'Default Title'),
(191, 'Tiêu đề', 'Default Title'),
(192, 'Tiêu đề', 'Default Title'),
(193, 'Tiêu đề', 'Default Title'),
(194, 'Tiêu đề', 'Default Title'),
(195, 'Tiêu đề', 'Default Title'),
(196, 'Tiêu đề', 'Default Title'),
(197, 'Tiêu đề', 'Default Title'),
(198, 'Tiêu đề', 'Default Title'),
(199, 'Tiêu đề', 'Default Title'),
(200, 'Tiêu đề', 'Default Title'),
(201, 'Tiêu đề', 'Default Title'),
(202, 'Tiêu đề', 'Default Title'),
(203, 'Tiêu đề', 'Default Title'),
(204, 'Tiêu đề', 'Default Title'),
(205, 'Tiêu đề', 'Default Title'),
(206, 'Tiêu đề', 'Default Title'),
(207, 'Tiêu đề', 'Default Title'),
(208, 'Tiêu đề', 'Default Title'),
(209, 'Tiêu đề', 'Default Title'),
(210, 'Tiêu đề', 'Default Title'),
(211, 'Tiêu đề', 'Default Title'),
(212, 'Tiêu đề', 'Default Title'),
(213, 'Tiêu đề', 'Default Title'),
(214, 'Tiêu đề', 'Default Title'),
(215, 'Tiêu đề', 'Default Title'),
(216, 'Tiêu đề', 'Default Title'),
(217, 'Tiêu đề', 'Default Title'),
(218, 'Tiêu đề', 'Default Title'),
(219, 'Tiêu đề', 'Default Title'),
(220, 'Tiêu đề', 'Default Title'),
(221, 'Tiêu đề', 'Default Title'),
(222, 'Tiêu đề', 'Default Title'),
(223, 'Tiêu đề', 'Default Title'),
(224, 'Tiêu đề', 'Default Title'),
(225, 'Tiêu đề', 'Default Title'),
(226, 'Tiêu đề', 'Default Title'),
(227, 'Tiêu đề', 'Default Title'),
(228, 'Tiêu đề', 'Default Title'),
(229, 'Tiêu đề', 'Default Title'),
(230, 'Tiêu đề', 'Default Title'),
(231, 'Tiêu đề', 'Default Title'),
(232, 'Tiêu đề', 'Default Title'),
(233, 'Tiêu đề', 'Default Title'),
(234, 'Tiêu đề', 'Default Title'),
(235, 'Tiêu đề', 'Default Title'),
(236, 'Tiêu đề', 'Default Title'),
(237, 'Tiêu đề', 'Default Title'),
(238, 'Tiêu đề', 'Default Title'),
(239, 'Tiêu đề', 'Default Title'),
(240, 'Tiêu đề', 'Default Title'),
(241, 'Tiêu đề', 'Default Title');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `slug` varchar(500) NOT NULL,
  `cover_photo` varchar(128) NOT NULL,
  `short_description` varchar(500) NOT NULL,
  `full_description` text NOT NULL,
  `status` int(11) NOT NULL,
  `meta_keyword` varchar(128) NOT NULL,
  `meta_description` varchar(128) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `slug`, `cover_photo`, `short_description`, `full_description`, `status`, `meta_keyword`, `meta_description`, `added_date`, `updated_date`, `updated_by`) VALUES
(1, 'Juno đồng giá giày dép 250.000 đồng tại cửa hàng Huỳnh Tấn Phát', 'juno-dong-gia-giay-dep-250-000-dong-tai-cua-hang-huynh-tan-phat', '2015-11-11-14-22-13-21728515-Chrysanthemum.jpg', 'Từ ngày 16/10/2015 – 18/10/2015, duy nhất tại cửa hàng 352 Huỳnh Tấn Phát, Q7, Tp.HCM, Juno đồng giá tất cả giày dép với giá chỉ còn 250.000 đồng một đôi, chào mừng cửa hàng đã hoàn tất xong quá trình sửa chữa và nâng cấp.', '<p>Từ ng&agrave;y 16/10/2015 &ndash; 18/10/2015, duy nhất tại cửa h&agrave;ng 352 Huỳnh Tấn Ph&aacute;t, Q7, Tp.HCM, Juno đồng gi&aacute; tất cả gi&agrave;y d&eacute;p với gi&aacute; chỉ c&ograve;n 250.000 đồng một đ&ocirc;i, ch&agrave;o mừng cửa h&agrave;ng đ&atilde; ho&agrave;n tất xong qu&aacute; tr&igrave;nh sửa chữa v&agrave; n&acirc;ng cấp.</p>\r\n\r\n<p>Cửa h&agrave;ng Juno Huỳnh Tấn Ph&aacute;t, toạ lạc tại địa chỉ 352 Huỳnh Tấn Ph&aacute;t, Q.7, Tp.HCM sau một thời gian sửa chữa v&agrave; n&acirc;ng cấp để phục vụ kh&aacute;ch h&agrave;ng ng&agrave;y một chu đ&aacute;o, chuy&ecirc;n nghiệp hơn đ&atilde; đi v&agrave;o hoạt động. Nh&acirc;n dịp n&agrave;y, Juno muốn gửi lời cảm ơn s&acirc;u sắc đến c&aacute;c kh&aacute;ch h&agrave;ng đ&atilde; lu&ocirc;n y&ecirc;u mến v&agrave; ủng hộ cửa h&agrave;ng trong suốt thời gian qua bằng chương tr&igrave;nh ưu đ&atilde;i cực lớn &quot;Đồng gi&aacute; 250,000 đồng tất cả gi&agrave;y d&eacute;p&rdquo; duy nhất tại cửa h&agrave;ng Huỳnh Tấn Ph&aacute;t. Chương tr&igrave;nh bắt đầu từ 16/10/2015 &ndash; 18/10/2015. Đ&acirc;y được xem l&agrave; một cơ hội v&agrave;ng để ph&aacute;i đẹp sở hữu những sản phẩm gi&agrave;y d&eacute;p thời trang ch&iacute;nh h&atilde;ng với gi&aacute; cực sốc.</p>\r\n\r\n<p>Lưu &yacute;: - Chương tr&igrave;nh đồng gi&aacute; 250.000 đồng kh&ocirc;ng &aacute;p dụng cho sản phẩm Boots, t&uacute;i/v&iacute; v&agrave; phụ kiện - Chỉ &aacute;p dụng tại cửa h&agrave;ng Huỳnh Tấn Ph&aacute;t H&agrave;ng trăm đ&ocirc;i gi&agrave;y với nhiều kiểu d&aacute;ng, mẫu m&atilde; thời trang, chất lượng với gi&aacute; si&ecirc;u tiết kiệm từ gi&agrave;y xăng đan, gi&agrave;y cao g&oacute;t, gi&agrave;y b&uacute;p b&ecirc; đang chờ đ&oacute;n bạn. Nếu muốn thay đổi lại tủ gi&agrave;y của m&igrave;nh hay d&agrave;nh tặng một m&oacute;n qu&agrave; bất ngờ cho những người th&acirc;n y&ecirc;u v&agrave;o những ng&agrave;y đặc biệt sắp tới th&igrave; đ&acirc;y l&agrave; cơ hội v&agrave;ng cho bạn đấy, h&atilde;y nhanh ch&acirc;n l&ecirc;n n&agrave;o, cơ hội chỉ d&agrave;nh cho bạn trong 3 ng&agrave;y th&ocirc;i.</p>\r\n', 0, 'aaa', 'bbbb', '2015-11-11 14:22:13', '2015-11-11 14:22:13', 1),
(2, 'Sắc trắng rạng ngời ngày đông – bạn có dám thử?', 'sac-trang-rang-ngoi-ngay-dong---ban-co-dam-thu?', '2015-11-11-14-31-18-599914550-Jellyfish.jpg', 'Bước sang mùa đông, tủ quần áo của cô nàng nào hầu như cũng có xu hướng trầm màu hơn cho những chiếc áo khoác, cardigan, áo len dày dặn ấm áp. Và những gam màu tươi sáng sẽ chịu chung “số phận” đó là bị các nàng xếp vào một xó. Tuy nhiên, sắc trắng lại không xứng đáng bị vứt bỏ chút nào khỏi tủ đồ của các nàng trong mùa đông lạnh giá này. Trái lại, hãy yêu và tận dụng tông màu trung tính hoàn hảo này để biến ngày đông của bạn thêm phần rạng rỡ.', '<p>Bước sang m&ugrave;a đ&ocirc;ng, tủ quần &aacute;o của c&ocirc; n&agrave;ng n&agrave;o hầu như cũng c&oacute; xu hướng trầm m&agrave;u hơn cho những chiếc &aacute;o kho&aacute;c, cardigan, &aacute;o len d&agrave;y dặn ấm &aacute;p. V&agrave; những gam m&agrave;u tươi s&aacute;ng sẽ chịu chung &ldquo;số phận&rdquo; đ&oacute; l&agrave; bị c&aacute;c n&agrave;ng xếp v&agrave;o một x&oacute;. Tuy nhi&ecirc;n, sắc trắng lại kh&ocirc;ng xứng đ&aacute;ng bị vứt bỏ ch&uacute;t n&agrave;o khỏi tủ đồ của c&aacute;c n&agrave;ng trong m&ugrave;a đ&ocirc;ng lạnh gi&aacute; n&agrave;y. Tr&aacute;i lại, h&atilde;y y&ecirc;u v&agrave; tận dụng t&ocirc;ng m&agrave;u trung t&iacute;nh ho&agrave;n hảo n&agrave;y để biến ng&agrave;y đ&ocirc;ng của bạn th&ecirc;m phần rạng rỡ. Chỉ cần một ch&uacute;t tinh tế trong việc lựa chọn chất vải, một ch&uacute;t kh&eacute;o l&eacute;o khi phối đồ, bạn sẽ chẳng c&ograve;n phải lo sắc trắng sẽ khiến bạn tr&ocirc;ng lạnh hơn trong những ng&agrave;y m&ugrave;a đ&ocirc;ng sắp tới đấy. Đồng thời với gam m&agrave;u trắng tinh kh&ocirc;i, bạn sẽ chẳng bao giờ lo bị lỗi mốt đ&acirc;u. H&atilde;y c&ugrave;ng kh&aacute;m ph&aacute; để l&yacute; giải được tại sao gam m&agrave;u n&agrave;y lại khiến m&ugrave;a đ&ocirc;ng trở n&ecirc;n rạng rỡ hơn nh&eacute;.</p>\r\n', 0, '', 'Bước sang mùa đông, tủ quần áo của cô nàng nào hầu như cũng có xu hướng trầm màu hơn cho những chiếc áo khoác, cardigan, áo len ', '2015-11-11 14:31:18', '2015-11-11 14:31:18', 1),
(3, 'Chọn phụ kiện chuẩn xu hướng thu đông 2015?', 'chon-phu-kien-chuan-xu-huong-thu-dong-2015?', '2015-11-11-14-31-55-384460449-Tulips.jpg', 'Bước sang mùa đông, tủ quần áo của cô nàng nào hầu như cũng có xu hướng trầm màu hơn cho những chiếc áo khoác, cardigan, áo len dày dặn ấm áp. Và những gam màu tươi sáng sẽ chịu chung “số phận” đó là bị các nàng xếp vào một xó. Tuy nhiên, sắc trắng lại không xứng đáng bị vứt bỏ chút nào khỏi tủ đồ của các nàng trong mùa đông lạnh giá này. Trái lại, hãy yêu và tận dụng tông màu trung tính hoàn hảo này để biến ngày đông của bạn thêm phần rạng rỡ.', '<p>Bước sang m&ugrave;a đ&ocirc;ng, tủ quần &aacute;o của c&ocirc; n&agrave;ng n&agrave;o hầu như cũng c&oacute; xu hướng trầm m&agrave;u hơn cho những chiếc &aacute;o kho&aacute;c, cardigan, &aacute;o len d&agrave;y dặn ấm &aacute;p. V&agrave; những gam m&agrave;u tươi s&aacute;ng sẽ chịu chung &ldquo;số phận&rdquo; đ&oacute; l&agrave; bị c&aacute;c n&agrave;ng xếp v&agrave;o một x&oacute;. Tuy nhi&ecirc;n, sắc trắng lại kh&ocirc;ng xứng đ&aacute;ng bị vứt bỏ ch&uacute;t n&agrave;o khỏi tủ đồ của c&aacute;c n&agrave;ng trong m&ugrave;a đ&ocirc;ng lạnh gi&aacute; n&agrave;y. Tr&aacute;i lại, h&atilde;y y&ecirc;u v&agrave; tận dụng t&ocirc;ng m&agrave;u trung t&iacute;nh ho&agrave;n hảo n&agrave;y để biến ng&agrave;y đ&ocirc;ng của bạn th&ecirc;m phần rạng rỡ. Chỉ cần một ch&uacute;t tinh tế trong việc lựa chọn chất vải, một ch&uacute;t kh&eacute;o l&eacute;o khi phối đồ, bạn sẽ chẳng c&ograve;n phải lo sắc trắng sẽ khiến bạn tr&ocirc;ng lạnh hơn trong những ng&agrave;y m&ugrave;a đ&ocirc;ng sắp tới đấy. Đồng thời với gam m&agrave;u trắng tinh kh&ocirc;i, bạn sẽ chẳng bao giờ lo bị lỗi mốt đ&acirc;u. H&atilde;y c&ugrave;ng kh&aacute;m ph&aacute; để l&yacute; giải được tại sao gam m&agrave;u n&agrave;y lại khiến m&ugrave;a đ&ocirc;ng trở n&ecirc;n rạng rỡ hơn nh&eacute;.</p>\r\n', 0, '', '', '2015-11-11 14:31:55', '2015-11-11 14:31:55', 1),
(4, 'Jacket da không chỉ dành cho những cô nàng cá tính', 'jacket-da-khong-chi-danh-cho-nhung-co-nang-ca-tinh', '2015-11-11-14-32-23-861175537-Lighthouse.jpg', 'Bước sang mùa đông, tủ quần áo của cô nàng nào hầu như cũng có xu hướng trầm màu hơn cho những chiếc áo khoác, cardigan, áo len dày dặn ấm áp. Và những gam màu tươi sáng sẽ chịu chung “số phận” đó là bị các nàng xếp vào một xó. Tuy nhiên, sắc trắng lại không xứng đáng bị vứt bỏ chút nào khỏi tủ đồ của các nàng trong mùa đông lạnh giá này. Trái lại, hãy yêu và tận dụng tông màu trung tính hoàn hảo này để biến ngày đông của bạn thêm phần rạng rỡ.', '<p>Bước sang m&ugrave;a đ&ocirc;ng, tủ quần &aacute;o của c&ocirc; n&agrave;ng n&agrave;o hầu như cũng c&oacute; xu hướng trầm m&agrave;u hơn cho những chiếc &aacute;o kho&aacute;c, cardigan, &aacute;o len d&agrave;y dặn ấm &aacute;p. V&agrave; những gam m&agrave;u tươi s&aacute;ng sẽ chịu chung &ldquo;số phận&rdquo; đ&oacute; l&agrave; bị c&aacute;c n&agrave;ng xếp v&agrave;o một x&oacute;. Tuy nhi&ecirc;n, sắc trắng lại kh&ocirc;ng xứng đ&aacute;ng bị vứt bỏ ch&uacute;t n&agrave;o khỏi tủ đồ của c&aacute;c n&agrave;ng trong m&ugrave;a đ&ocirc;ng lạnh gi&aacute; n&agrave;y. Tr&aacute;i lại, h&atilde;y y&ecirc;u v&agrave; tận dụng t&ocirc;ng m&agrave;u trung t&iacute;nh ho&agrave;n hảo n&agrave;y để biến ng&agrave;y đ&ocirc;ng của bạn th&ecirc;m phần rạng rỡ. Chỉ cần một ch&uacute;t tinh tế trong việc lựa chọn chất vải, một ch&uacute;t kh&eacute;o l&eacute;o khi phối đồ, bạn sẽ chẳng c&ograve;n phải lo sắc trắng sẽ khiến bạn tr&ocirc;ng lạnh hơn trong những ng&agrave;y m&ugrave;a đ&ocirc;ng sắp tới đấy. Đồng thời với gam m&agrave;u trắng tinh kh&ocirc;i, bạn sẽ chẳng bao giờ lo bị lỗi mốt đ&acirc;u. H&atilde;y c&ugrave;ng kh&aacute;m ph&aacute; để l&yacute; giải được tại sao gam m&agrave;u n&agrave;y lại khiến m&ugrave;a đ&ocirc;ng trở n&ecirc;n rạng rỡ hơn nh&eacute;.</p>\r\n', 0, '', '', '2015-11-11 14:32:23', '2015-11-11 14:32:23', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `display_order` int(11) NOT NULL,
  `slug` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `size` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `class_css` varchar(128) NOT NULL,
  `meta_title` varchar(70) NOT NULL,
  `meta_keyword` varchar(256) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `no_of_homepage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `display_order`, `slug`, `description`, `size`, `color`, `class_css`, `meta_title`, `meta_keyword`, `meta_description`, `status`, `added_date`, `updated_date`, `updated_by`, `no_of_homepage`) VALUES
(1, 0, 'Giày Cao Gót', 1, 'giay-cao-got', '<p>Nước hoa hiệu cho ph&aacute;i nữ</p>\r\n', 1, 1, 'cao_got', 'Giày Cao Gót', 'giay cao got', 'Nước hoa hiệu cho phái nữ', 0, '2015-10-14 22:34:22', '2017-01-16 09:06:06', 1, 16),
(2, 0, 'GIÀY SANDAL', 2, 'giay-sandal', '<p>Nước hoa cho ph&aacute;i mạnh</p>\r\n', 0, 0, '', 'GIÀY SANDAL', '', 'Nước hoa nam giới', 0, '2015-10-14 22:39:37', '2017-01-05 23:45:28', 1, 16),
(3, 0, 'GIÀY BÚP BÊ', 3, 'giay-bup-be', '<p>NƯỚC HOA PHỔ TH&Ocirc;NG NỮ</p>\r\n', 0, 1, 'bup_be', 'GIÀY BÚP BÊ', '', 'FILE HỒ SƠ - BẢNG VĂN PHÒNG', 0, '2015-10-31 22:31:44', '2017-01-16 09:06:52', 1, 16),
(4, 0, 'GIÀY BOOT', 4, 'giay-boot', '<p>NƯỚC HOA PHỔ TH&Ocirc;NG NAM</p>\r\n', 1, 1, 'bot', 'GIÀY BOOT', '', 'Nước hoa phổ thông nam', 0, '2015-10-31 22:31:58', '2017-01-17 14:22:18', 1, 0),
(5, 0, 'SP chuẩn bị về VN', 5, 'sp-chuan-bi-v-vn', '<p>LĂN KHỬ M&Ugrave;I NỮ</p>\r\n', 1, 1, 'Lăn khử mùi nữ', 'SP chuẩn bị về VN', 'khu mui,lan khu mui,khu mui nu,lan khu mui nu', 'Lăn khử mùi nữ', 0, '2015-10-31 22:32:15', '2016-03-16 22:08:47', 1, 0),
(6, 0, 'Lăn khử mùi nam', 6, 'lan-khu-mui-nam', '<p>LĂN KHỬ M&Ugrave;I NAM</p>\r\n', 1, 1, 'Lăn khử mùi nam', 'Lăn khử mùi nam', 'khu mui nam,khu mui,lan khu mui,lan khu mui cho nam', 'Lăn khử mùi nam', 0, '2015-10-31 22:32:32', '2016-03-09 20:19:15', 1, 0),
(7, 0, 'Thực phẩm chức năng ', 7, 'thuc-pham-chuc-nang', '<p>Thực phẩm phục hồi chức năng</p>\r\n', 1, 1, 'Thực phẩm chức năng ', 'Thực phẩm chức năng ', 'thuc pham nhat,thuc pham chưc nang,thưc pham phục hoi chuc nang', 'Thực phẩm phục hồi chức năng', 0, '2015-10-31 22:32:48', '2016-03-09 20:30:10', 1, 0),
(8, 0, 'Nước hoa mini', 8, 'nuoc-hoa-mini', '<p>NƯỚC HOA MINI</p>\r\n', 0, 0, 'Nước hoa mini', 'Nước hoa mini', '', 'Nước hoa mini', 0, '2015-10-31 22:32:59', '2017-01-05 23:47:28', 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cms`
--

CREATE TABLE `cms` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `display_order` int(11) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `type` enum('about','help') NOT NULL,
  `description` text NOT NULL,
  `meta_keyword` varchar(128) NOT NULL,
  `meta_title` varchar(128) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `cms`
--

INSERT INTO `cms` (`id`, `name`, `display_order`, `slug`, `type`, `description`, `meta_keyword`, `meta_title`, `meta_description`, `status`, `added_date`, `updated_date`, `updated_by`) VALUES
(1, 'Giới thiệu chung', 1, 'gioi-thieu-chung', 'about', '<p>Nội dung giới thiệu</p>\r\n', 'gioi thieu ve diem hen mua sam', '', 'Trung tam mua sam', 0, '2015-11-06 20:47:57', '2016-03-09 14:01:19', 1),
(2, 'Giao hàng', 2, 'giao-hang', 'about', '<p>Giao h&agrave;ng</p>\r\n', '', '', '', 0, '2015-11-06 20:49:27', '2015-11-06 21:08:25', 1),
(3, 'Đổi trả', 3, 'doi-tra', 'about', '<p>Nội dung&nbsp;Đổi trả</p>\r\n', '', '', '', 0, '2015-11-06 20:50:49', '2015-11-06 21:08:29', 1),
(4, 'Chính sách bảo mật', 4, 'chinh-sach-bao-mat', 'about', '<p>Ch&iacute;nh s&aacute;ch bảo mật</p>\r\n', '', '', '', 0, '2015-11-06 20:51:05', '2015-11-06 21:08:33', 1),
(5, 'Tuyển dụng', 5, 'tuyen-dung', 'about', '<p>Tuyển dụng</p>\r\n', '', '', '', 0, '2015-11-06 20:51:17', '2015-11-06 21:08:37', 1),
(6, 'Hướng dẫn thanh toán', 1, 'huong-dan-thanh-toan', 'help', '<p>Hướng dẫn thanh to&aacute;n</p>\r\n', '', '', '', 0, '2015-11-06 20:51:42', '2015-11-06 21:08:18', 1),
(7, 'Hướng dẫn mua hàng', 2, 'huong-dan-mua-hang', 'help', '<p>Hướng dẫn mua h&agrave;ng</p>\r\n', '', '', '', 0, '2015-11-06 20:52:25', '2015-11-06 21:08:45', 1),
(8, 'Tài khoản giao dịch', 3, 'tai-khoan-giao-dich', 'help', '<p><a href=\"gioithieu.html\">T&agrave;i khoản giao dịch</a></p>\r\n', '', '', '', 0, '2015-11-06 20:52:35', '2015-11-06 21:08:48', 1),
(9, 'Quy định thảo luận', 4, 'quy-dinh-thao-luan', 'help', '<p>Quy định thảo luận</p>\r\n', '', '', '', 0, '2015-11-06 20:52:50', '2015-11-06 21:08:51', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `color`
--

CREATE TABLE `color` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `color`
--

INSERT INTO `color` (`id`, `name`, `status`) VALUES
(1, 'Màu xanh', 0),
(2, 'Màu đỏ', 0),
(3, 'Màu vàng', 0),
(4, 'Màu tím', 0),
(5, 'Màu hồng', 0),
(6, 'Màu bạc', 0),
(7, 'Màu trắng', 0),
(8, 'Màu đen', 0),
(9, 'Chấm bi', 0),
(11, 'Màu xám', 0),
(12, 'Màu kem', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` text,
  `masb` varchar(255) NOT NULL,
  `catid` int(11) DEFAULT NULL,
  `loai` tinyint(1) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `block` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `district`
--

INSERT INTO `district` (`id`, `name`, `masb`, `catid`, `loai`, `ordering`, `block`) VALUES
(1, 'Quận 1', 'SGN', 2, 3, 1, 0),
(2, 'Quận 2', 'HAN', 2, 3, 2, 0),
(3, 'Quận 3', 'DAD', 2, 3, 3, 0),
(5, 'Quận 4', 'NHA', 2, 3, 4, 0),
(8, 'Quận 5', 'BMV', 2, 3, 5, 0),
(11, 'Quận 6', 'DLI', 2, 3, 6, 0),
(12, 'Quận 7', 'PQC', 2, 3, 7, 0),
(13, 'Quận 8', 'VCL', 2, 3, 8, 0),
(14, 'Quận 9', 'UIH', 2, 3, 9, 0),
(15, 'Quận 10', 'VCA', 2, 3, 10, 0),
(16, 'Quận 11', 'VCS', 2, 3, 11, 0),
(17, 'Quận 12', 'VKG', 2, 3, 12, 0),
(18, 'Quận Bình Tân ', 'CAH', 2, 3, 13, 0),
(19, 'Quận Bình Thạnh ', 'HPH', 2, 3, 14, 0),
(20, 'Quận Gò Vấp ', 'DIN', 2, 3, NULL, 0),
(21, 'Quận Phú Nhuận ', 'VII', 2, 3, NULL, 0),
(22, 'Quận Tân Bình ', 'HUI', 2, 3, NULL, 0),
(23, 'Quận Tân Phú ', 'VDH', 2, 3, NULL, 0),
(24, 'Quận Thủ Đức ', 'PXU', 2, 3, NULL, 0),
(25, 'Huyện Bình Chánh ', 'TBB', 2, 3, NULL, 0),
(26, 'Huyện Cần Giờ ', '', 2, 3, NULL, 0),
(27, 'Huyện Củ Chi ', '', 2, 3, NULL, 0),
(28, 'Huyện Hóc Môn ', '', 2, 3, NULL, 0),
(29, 'Huyện Nhà Bè ', '', 2, 3, NULL, 0),
(30, 'Quận Ba Đình', '', 3, 1, NULL, 0),
(31, 'Quận Hoàn Kiếm', '', 3, 1, NULL, 0),
(32, 'Quận Tây Hồ', '', 3, 1, NULL, 0),
(33, 'Quận Long Biên', '', 3, 1, NULL, 0),
(34, 'Quận Cầu Giấy', '', 3, 1, NULL, 0),
(35, 'Quận Đống Đa', '', 3, 1, NULL, 0),
(36, 'Quận Hai Bà Trưng', '', 3, 1, NULL, 0),
(37, 'Quận Hoàng Mai', '', 3, 1, NULL, 0),
(38, 'Quận Thanh Xuân', '', 3, 0, NULL, 0),
(39, 'Quận Hà Đông', '', 3, 1, NULL, 0),
(40, 'Huyện Sóc Sơn', '', 3, 1, NULL, 0),
(41, 'Huyện Đông Anh', '', 3, 1, NULL, 0),
(42, 'Huyện Gia Lâm', '', 3, 1, NULL, 0),
(43, 'Huyện Từ Liêm', '', 3, 1, NULL, 0),
(44, 'Huyện Thanh Trì', '', 3, 1, NULL, 0),
(45, 'Thị Xã Sơn Tây', '', 3, 1, NULL, 0),
(46, 'Huyện Ba Vì', '', 3, 1, NULL, 0),
(47, 'Huyện Phúc Thọ', '', 3, 1, NULL, 0),
(48, 'Huyện Đan Phượng', '', 3, 1, NULL, 0),
(49, 'Huyện Hoài Đức', '', 3, 1, NULL, 0),
(50, 'Huyện Quốc Oai', '', 3, 1, NULL, 0),
(51, 'Huyện Thạch Thất', '', 3, 1, NULL, 0),
(52, 'Huyện Chương Mỹ', '', 3, 1, NULL, 0),
(53, 'Huyện Thanh Oai', '', 3, 1, NULL, 0),
(54, 'Huyện Thường Tín', '', 3, 1, NULL, 0),
(55, 'Huyện Phú Xuyên', '', 3, 1, NULL, 0),
(56, 'Huyện Ứng Hòa', '', 3, 1, NULL, 0),
(57, 'Huyện Mỹ Đức', '', 3, 1, NULL, 0),
(58, 'Huyện Mê Linh', '', 3, 1, NULL, 0),
(59, 'Tp. Biên Hòa', '', 9, 3, NULL, 0),
(60, 'Thị Xã Long Khánh', '', 9, 1, NULL, 0),
(61, 'Huyện Tân Phú', '', 9, 1, NULL, 0),
(62, 'Huyện Vĩnh Cửu', '', 9, 0, NULL, 0),
(63, 'Huyện Định Quán', '', 9, 1, NULL, 0),
(64, 'Huyện Trảng Bom ', '', 9, 1, NULL, 0),
(65, 'Huyện Thống Nhất', '', 9, 1, NULL, 0),
(66, 'Huyện Long Thành', '', 9, 1, NULL, 0),
(67, 'Huyện Xuân Lộc', '', 9, 1, NULL, 0),
(68, 'Huyện Nhơn Trạch', '', 9, 1, NULL, 0),
(69, 'Huyện Cẩm Mỹ', '', 9, 1, NULL, 0),
(70, 'Quận Liên Chiểu', '', 4, 1, NULL, 0),
(71, 'Quận Thanh Khê', '', 4, 1, NULL, 0),
(72, 'Quận Hải Châu', '', 4, 1, NULL, 0),
(73, 'Quận Sơn Trà', '', 4, 1, NULL, 0),
(74, 'Quận Ngũ Hành Sơn', '', 4, 1, NULL, 0),
(75, 'Huyện Hòa Vang', '', 4, 1, NULL, 0),
(76, 'Huyện Hoàng Sa', '', 4, 1, NULL, 0),
(77, 'Tp. Thủ Dầu Một', '', 6, 1, NULL, 0),
(78, 'Huyện Dầu Tiếng', '', 6, 1, NULL, 0),
(79, 'Huyện Bến Cát', '', 6, 1, NULL, 0),
(80, 'Huyện Phú Giáo', '', 6, 1, NULL, 0),
(81, 'Huyện Tân Uyên', '', 6, 1, NULL, 0),
(82, 'Thị Xã Dĩ An', '', 6, 1, NULL, 0),
(83, 'Thị Xã Thuận An', '', 6, 1, NULL, 0),
(84, 'Quận Ninh Kiều', '', 5, 1, NULL, 0),
(85, 'Quận Ô Môn', '', 5, 1, NULL, 0),
(86, 'Quận Bình Thủy', '', 5, 1, NULL, 0),
(87, 'Quận Cái Răng', '', 5, 1, NULL, 0),
(88, 'Huyện Thốt Nốt', '', 5, 1, NULL, 0),
(89, 'Huyện Vĩnh Thạnh', '', 5, 1, NULL, 0),
(90, 'Huyện Cờ Đỏ', '', 5, 1, NULL, 0),
(91, 'Huyện Phong Điền', '', 5, 1, NULL, 0),
(92, 'Tp. Tân An', '', 7, 1, NULL, 0),
(93, 'Huyện Tân Hưng', '', 7, 1, NULL, 0),
(94, 'Huyện Vĩnh Hưng', '', 7, 1, NULL, 0),
(95, 'Huyện Mộc Hóa', '', 7, 1, NULL, 0),
(96, 'Huyện Tân Thạnh', '', 7, 1, NULL, 0),
(97, 'Huyện Thạnh Hóa', '', 7, 1, NULL, 0),
(98, 'Huyện Đức Hòa', '', 7, 1, NULL, 0),
(99, 'Huyện Đức Huệ', '', 7, 1, NULL, 0),
(100, 'Huyện Bến Lức ', '', 7, 1, NULL, 0),
(101, 'Huyện Thủ Thừa', '', 7, 1, NULL, 0),
(102, 'Huyện Tân Trụ', '', 7, 1, NULL, 0),
(103, 'Huyện Cần Đước ', '', 7, 1, NULL, 0),
(104, 'Huyện Cần Giuộc', '', 7, 1, NULL, 0),
(105, 'Huyện Châu Thành', '', 7, 1, NULL, 0),
(106, 'Tp. Mỹ Tho', '', 8, 1, NULL, 0),
(107, 'Thị Xã Gò Công', '', 8, 1, NULL, 0),
(108, 'Huyện Tân Phước', '', 8, 1, NULL, 0),
(109, 'Huyện Cái Bè', '', 8, 1, NULL, 0),
(110, 'Huyện Cai Lậy', '', 8, 1, NULL, 0),
(111, 'Huyện Châu Thành', '', 8, 1, NULL, 0),
(112, 'Huyện Chợ Gạo', '', 8, 1, NULL, 0),
(113, 'Huyện Gò Công Đông', '', 8, 1, NULL, 0),
(114, 'Huyện Gò Công Tây', '', 0, 1, NULL, 0),
(115, 'Huyện Gò Công Tây', '', 8, 1, NULL, 0),
(116, 'Tp. Vũng Tàu', '', 11, 1, NULL, 0),
(117, 'Tp. Mỹ Tho', '', 8, 1, NULL, 0),
(118, 'Thị Xã Bà Rịa', '', 11, 1, NULL, 0),
(119, 'Huyện Châu Đức', '', 11, 1, NULL, 0),
(120, 'Huyện Xuyên Mộc', '', 11, 1, NULL, 0),
(121, 'Huyện Long Điền', '', 11, 1, NULL, 0),
(122, 'Huyện Đất Đỏ', '', 11, 1, NULL, 0),
(123, 'Huyện Tân Thành', '', 11, 1, NULL, 0),
(124, 'Huyện Côn Đảo', '', 11, 1, NULL, 0),
(125, 'Thị Xã Tây Ninh', '', 10, 1, NULL, 0),
(126, 'Huyện Tân Biên', '', 10, 1, NULL, 0),
(127, 'Huyện Tân Châu', '', 10, 1, NULL, 0),
(128, 'Huyện Dương Minh Châu', '', 10, 1, NULL, 0),
(129, 'Huyện Châu Thành', '', 10, 1, NULL, 0),
(130, 'Huyện Hòa Thành', '', 10, 1, NULL, 0),
(131, 'Huyện Gò Dầu', '', 10, 1, NULL, 0),
(132, 'Huyện Bến Cầu', '', 10, 1, NULL, 0),
(133, 'Huyện Trảng Bàng', '', 10, 1, NULL, 0),
(134, 'Tp. Bến Tre', '', 12, 1, NULL, 0),
(135, 'Huyện Châu Thành', '', 12, 1, NULL, 0),
(136, 'Huyện Chợ Lách', '', 12, 1, NULL, 0),
(137, 'Huyện Mỏ Cày Bắc', '', 12, 0, NULL, 0),
(138, 'Huyện Giồng Trôm', '', 12, 1, NULL, 0),
(139, 'Huyện Bình Đại', '', 12, 1, NULL, 0),
(140, 'Huyện Ba Tri', '', 12, 1, NULL, 0),
(141, 'Huyện Thạnh Phú', '', 12, 1, NULL, 0),
(142, 'Huyện Mỏ Cày Nam', '', 12, 1, NULL, 0),
(143, 'Tp. Đà Lạt', '', 23, 1, NULL, 0),
(144, 'Tp. Bảo Lộc', '', 23, 1, NULL, 0),
(145, 'Huyện Lạc Dương', '', 23, 1, NULL, 0),
(146, 'Huyện Lâm Hà', '', 23, 1, NULL, 0),
(147, 'Huyện Đơn Dương', '', 23, 1, NULL, 0),
(148, 'Huyện Đức Trọng', '', 23, 1, NULL, 0),
(149, 'Huyện Di Linh', '', 23, 1, NULL, 0),
(150, 'Huyện Bảo Lâm', '', 23, 1, NULL, 0),
(151, 'Huyện Cát Tiên', '', 23, 1, NULL, 0),
(152, 'huyện Đạ Huoai', '', 23, 1, NULL, 0),
(153, 'Huyện Đạ Tẻh', '', 23, 1, NULL, 0),
(154, 'Tp. Phan Thiết', '', 46, 1, NULL, 0),
(155, 'Huyện Tuy Phong', '', 46, 1, NULL, 0),
(156, 'Huyện Bắc Bình', '', 46, 1, NULL, 0),
(157, 'Huyện Hàm Thuận Nam', '', 46, 1, NULL, 0),
(158, 'Huyện Hàm Thuận Bắc', '', 46, 1, NULL, 0),
(159, 'Huyện Tánh Linh', '', 46, 1, NULL, 0),
(160, 'Huyện Đức Linh', '', 46, 1, NULL, 0),
(161, 'Huyện Phú Qúy', '', 46, 1, NULL, 0),
(162, 'Huyện Hàm Tân', '', 46, 1, NULL, 0),
(163, 'Thị Xã Đồng Xoài', '', 47, 1, NULL, 0),
(164, 'Huyện Phước Long', '', 47, 1, NULL, 0),
(165, 'Huyện Lộc Ninh', '', 47, 1, NULL, 0),
(166, 'Huyện Bù Đốp', '', 47, 1, NULL, 0),
(167, 'Huyện Bình Long', '', 47, 1, NULL, 0),
(168, 'Huyện Đồng Phú', '', 47, 1, NULL, 0),
(169, 'Huyện Bù Đăng', '', 47, 1, NULL, 0),
(170, 'Huyện Chơn Thành', '', 47, 1, NULL, 0),
(171, 'Tp. Long Xuyên', '', 53, 1, NULL, 0),
(172, 'Thị Xã Châu Đốc', '', 53, 1, NULL, 0),
(173, 'Huyện An Phú', '', 53, 0, NULL, 0),
(174, 'Huyện Châu Phú', '', 53, 0, NULL, 0),
(175, 'Huyện Châu Thành', '', 53, 0, NULL, 0),
(176, 'Huyện Chợ Mới', '', 53, 1, NULL, 0),
(177, 'Huyện Phú Tân', '', 53, 1, NULL, 0),
(178, 'Huyện Thoại Sơn', '', 53, 1, NULL, 0),
(179, 'Huyện Tịnh Biên', '', 53, 1, NULL, 0),
(180, 'Huyện Tri Tôn', '', 53, 1, NULL, 0),
(181, 'Huyện Tân Châu', '', 53, 1, NULL, 0),
(182, 'Tp. Trà Vinh', '', 73, 1, NULL, 0),
(183, 'Huyện Càng Long', '', 73, 1, NULL, 0),
(184, 'Huyện Cầu Kè', '', 73, 1, NULL, 0),
(185, 'Huyện Cầu Ngang', '', 73, 1, NULL, 0),
(186, 'Huyện Châu Thành', '', 73, 1, NULL, 0),
(187, 'Huyện Duyên Hải', '', 73, 1, NULL, 0),
(188, 'Huyện Tiểu Cần', '', 73, 1, NULL, 0),
(189, 'Huyện Trà Cú', '', 73, 1, NULL, 0),
(190, 'Tp. Sóc Trăng', '', 74, 1, NULL, 0),
(191, 'Thị Xã Vĩnh Châu', '', 74, 1, NULL, 0),
(192, 'Huyện Châu Thành', '', 74, 1, NULL, 0),
(193, 'Huyện Cù Lao Dung', '', 74, 1, NULL, 0),
(194, 'Huyện Kế Sách', '', 74, 1, NULL, 0),
(195, 'Huyện Long Phú', '', 74, 1, NULL, 0),
(196, 'Huyện Mỹ Tú', '', 74, 1, NULL, 0),
(197, 'Huyện Mỹ Xuyên', '', 74, 1, NULL, 0),
(198, 'Huyện Ngã Năm', '', 74, 1, NULL, 0),
(199, 'Huyện Thạnh Trị', '', 74, 1, NULL, 0),
(200, 'Huyện Trần Đề', '', 74, 1, NULL, 0),
(201, 'Quận Dương Kinh', '', 33, 1, NULL, 0),
(202, 'Quận Đồ Sơn', '', 33, 1, NULL, 0),
(203, 'Quận Hải An', '', 33, 1, NULL, 0),
(204, 'Quận Hồng Bàng', '', 33, 1, NULL, 0),
(205, 'Quận Kiến An', '', 33, 1, NULL, 0),
(206, 'Quận Lê Chân', '', 33, 1, NULL, 0),
(207, 'Quận Ngô Quyền', '', 33, 1, NULL, 0),
(208, 'Huyện An Dương ', '', 33, 1, NULL, 0),
(209, 'Huyện An Lão', '', 33, 1, NULL, 0),
(210, 'Huyện Bạch Long Vĩ', '', 33, 1, NULL, 0),
(211, 'Huyện Cát Hải', '', 33, 1, NULL, 0),
(212, 'Huyện Kiến Thụy', '', 33, 1, NULL, 0),
(213, 'Huyện Thủy Nguyên', '', 33, 1, NULL, 0),
(214, 'Huyện Tiên Lãng', '', 33, 1, NULL, 0),
(215, 'Huyện Vĩnh Bảo', '', 33, 1, NULL, 0),
(216, 'Tp. Nha Trang', '', 29, 1, NULL, 0),
(217, 'Tp. Cam Ranh', '', 29, 1, NULL, 0),
(218, 'Thị Xã Ninh Hòa', '', 29, 1, NULL, 0),
(219, 'Huyện Cam Lâm', '', 29, 1, NULL, 0),
(220, 'Huyện Diên Khánh', '', 29, 1, NULL, 0),
(221, 'Huyện Khánh Sơn', '', 29, 1, NULL, 0),
(222, 'Huyện Khánh Vĩnh', '', 29, 1, NULL, 0),
(223, 'Huyện Trường Sa', '', 29, 1, NULL, 0),
(224, 'Huyện Vạn Ninh', '', 29, 1, NULL, 0),
(225, 'Tp. Cao Lãnh', '', 40, 1, NULL, 0),
(226, 'Thị Xã Sa Đéc', '', 40, 1, NULL, 0),
(227, 'Thị Xã Hồng Ngự', '', 40, 1, NULL, 0),
(228, 'Huyện Cao Lãnh', '', 40, 1, NULL, 0),
(229, 'Huyện Châu Thành', '', 40, 1, NULL, 0),
(230, 'Huyện Hồng Ngự', '', 40, 1, NULL, 0),
(231, 'Huyện Lai Vung', '', 40, 1, NULL, 0),
(232, 'Huyện Lấp Vò', '', 40, 1, NULL, 0),
(233, 'Huyện Tam Nông', '', 40, 1, NULL, 0),
(234, 'Huyện Tân Hồng', '', 40, 1, NULL, 0),
(235, 'Huyện Thanh Bình', '', 40, 1, NULL, 0),
(236, 'Huyện Tháp Mười', '', 0, 1, NULL, 0),
(237, 'Huyện Tháp Mười', '', 40, 1, NULL, 0),
(238, 'Tp. Rạch Gía', '', 28, 1, NULL, 0),
(239, 'Thị Xã Hà Tiên', '', 28, 1, NULL, 0),
(240, 'Huyện An Biên', '', 28, 1, NULL, 0),
(241, 'Huyện An Minh', '', 28, 1, NULL, 0),
(242, 'Huyện Châu Thành', '', 28, 1, NULL, 0),
(243, 'Huyện Giồng Riềng ', '', 28, 1, NULL, 0),
(244, 'Huyện Gò Quao', '', 28, 1, NULL, 0),
(245, 'Huyện Hòn Đất', '', 28, 1, NULL, 0),
(246, 'Huyện Kiên Hải', '', 28, 1, NULL, 0),
(247, 'Huyện Kiên Lương', '', 28, 1, NULL, 0),
(248, 'Huyện Phú Quốc', '', 28, 1, NULL, 0),
(249, 'Huyện Tân Hiệp', '', 28, 1, NULL, 0),
(250, 'Huyện Vĩnh Thuận', '', 28, 1, NULL, 0),
(251, 'Huyện U Minh Thượng', '', 28, 1, NULL, 0),
(252, 'Huyện Giang Thành', '', 28, 1, NULL, 0),
(253, 'Tp.Vinh', '', 21, 1, NULL, 0),
(254, 'Thị Xã Cửa Lò', '', 21, 1, NULL, 0),
(255, 'Thị Xã Thái Hòa', '', 21, 1, NULL, 0),
(256, 'Huyện Anh Sơn', '', 21, 1, NULL, 0),
(257, 'Huyện Con Cuông', '', 21, 1, NULL, 0),
(258, 'Huyện Diễn Châu', '', 21, 1, NULL, 0),
(259, 'Huyện Đô Lương', '', 21, 1, NULL, 0),
(260, 'Huyện Hưng Nguyên', '', 21, 1, NULL, 0),
(261, 'Huyện Qùy Châu', '', 21, 1, NULL, 0),
(262, 'Huyện Kỳ Sơn', '', 21, 1, NULL, 0),
(263, 'Huyện Nam Đàn', '', 21, 1, NULL, 0),
(264, 'Huyện Nghi Lộc', '', 21, 1, NULL, 0),
(265, 'Huyện Nghĩa Đàn', '', 21, 1, NULL, 0),
(266, 'Huyện Quế Phong', '', 21, 1, NULL, 0),
(267, 'Huyện Qùy Hợp', '', 21, 1, NULL, 0),
(268, 'Huyện Quỳnh Lưu', '', 21, 1, NULL, 0),
(269, 'Huyện Tân Kỳ', '', 21, 1, NULL, 0),
(270, 'Huyện Thanh Chương', '', 21, 1, NULL, 0),
(271, 'Huyện Tương Dương', '', 21, 1, NULL, 0),
(272, 'Huyện Yên Thành', '', 21, 1, NULL, 0),
(273, 'Tp. Vĩnh Long', '', 66, 1, NULL, 0),
(274, 'Huyện Bình Minh', '', 66, 1, NULL, 0),
(275, 'Huyện Bình Tân', '', 66, 1, NULL, 0),
(276, 'Huyện Long Hồ', '', 66, 1, NULL, 0),
(277, 'Huyện Mang Thít', '', 66, 1, NULL, 0),
(278, 'Huyện Tam Bình', '', 66, 1, NULL, 0),
(279, 'Huyện Trà Ôn', '', 66, 1, NULL, 0),
(280, 'Huyện Vũng Liêm', '', 66, 1, NULL, 0),
(281, 'Tp. Hà Tĩnh', '', 35, 1, NULL, 0),
(282, 'Thị Xã Hồng Lĩnh', '', 35, 1, NULL, 0),
(283, 'Huyện Cẩm Xuyên', '', 35, 1, NULL, 0),
(284, 'Huyện Can Lộc', '', 35, 1, NULL, 0),
(285, 'Huyện Đức Thọ', '', 35, 1, NULL, 0),
(286, 'Huyện Hương Khê', '', 35, 1, NULL, 0),
(287, 'Huyện Hương Sơn', '', 35, 1, NULL, 0),
(288, 'Huyện Kỳ Anh', '', 35, 1, NULL, 0),
(289, 'Huyện Nghi Xuân', '', 35, 1, NULL, 0),
(290, 'Huyện Thạch Hà', '', 35, 1, NULL, 0),
(291, 'Huyện Vũ Quang', '', 35, 1, NULL, 0),
(292, 'Huyện Lộc Hà', '', 35, 1, NULL, 0),
(293, 'Tp. Vị Thanh', '', 31, 1, NULL, 0),
(294, 'Thị Xã Ngã Bảy', '', 31, 1, NULL, 0),
(295, 'Huyện Châu Thành', '', 31, 1, NULL, 0),
(296, 'Huyện Châu Thành A', '', 31, 1, NULL, 0),
(297, 'Huyện Long Mỹ', '', 31, 1, NULL, 0),
(298, 'Huyện Phụng Hiệp', '', 31, 1, NULL, 0),
(299, 'Huyện Vị Thủy', '', 31, 1, NULL, 0),
(300, 'Tp. Cà Mau', '', 45, 1, NULL, 0),
(301, 'Huyện Đầm Dơi', '', 45, 1, NULL, 0),
(302, 'Huyện Ngọc Hiển', '', 45, 1, NULL, 0),
(303, 'Huyện Cái Nước', '', 45, 1, NULL, 0),
(304, 'Huyện Trần Văn Thời', '', 45, 1, NULL, 0),
(305, 'Huyện U Minh', '', 45, 1, NULL, 0),
(306, 'Huyện Thới Bình', '', 45, 1, NULL, 0),
(307, 'Huyện Năm Căn', '', 45, 1, NULL, 0),
(308, 'Huyện Phú Tân', '', 45, 1, NULL, 0),
(309, 'Tp. Bạc Liêu', '', 52, 1, NULL, 0),
(310, 'Thị Xã Hồng Dân', '', 52, 1, NULL, 0),
(311, 'Huyện Hòa Bình', '', 52, 1, NULL, 0),
(312, 'Huyện Gía Rai', '', 52, 1, NULL, 0),
(313, 'Huyện Phước Long', '', 52, 1, NULL, 0),
(314, 'Huyện Vĩnh Lợi', '', 52, 1, NULL, 0),
(315, 'Huyện Đông Hải', '', 52, 1, NULL, 0),
(316, 'Tp. Huế', '', 68, 1, NULL, 0),
(317, 'Thị Xã Hương Trà', '', 68, 1, NULL, 0),
(318, 'Thị Xã Hương Thủy', '', 68, 1, NULL, 0),
(319, 'Huyện A Lưới', '', 68, 1, NULL, 0),
(320, 'Huyện Nam Đông', '', 68, 1, NULL, 0),
(321, 'Huyện Phong Điền', '', 68, 1, NULL, 0),
(322, 'Huyện Phú Lộc', '', 68, 1, NULL, 0),
(323, 'Huyện Phú Vang', '', 68, 1, NULL, 0),
(324, 'Huyện Quảng Điền', '', 68, 1, NULL, 0),
(325, 'Tp. Kon Tum', '', 27, 1, NULL, 0),
(326, 'Huyện Đắk Glei', '', 27, 1, NULL, 0),
(327, 'Huyện Đắk Hà', '', 27, 1, NULL, 0),
(328, 'Huyện Đắk Tô', '', 27, 1, NULL, 0),
(329, 'Huyện Kon Plông', '', 27, 1, NULL, 0),
(330, 'Huyện Kon Rẫy', '', 27, 1, NULL, 0),
(331, 'Huyện Ngọc Hồi', '', 27, 1, NULL, 0),
(332, 'Huyện Tu Mơ Rông', '', 27, 1, NULL, 0),
(333, 'Huyện Sa Thầy', '', 27, 1, NULL, 0),
(334, 'Tp. Phủ Lý', '', 37, 1, NULL, 0),
(335, 'Huyện Duy Tiên', '', 37, 1, NULL, 0),
(336, 'Huyện Kim Bảng', '', 37, 1, NULL, 0),
(337, 'Huyện Lý Nhân', '', 37, 1, NULL, 0),
(338, 'Huyện Thanh Liêm', '', 37, 1, NULL, 0),
(339, 'Huyện Bình Lục', '', 37, 1, NULL, 0),
(340, 'Tp. Sơn Tây', '', 36, 1, NULL, 0),
(341, 'Tp. Hà Đông', '', 36, 1, NULL, 0),
(342, 'Tp. Quy Nhơn', '', 48, 1, NULL, 0),
(343, 'Thị Xã An Nhơn', '', 48, 1, NULL, 0),
(344, 'Huyện An Lão', '', 48, 1, NULL, 0),
(345, 'Huyện Hoài Ân', '', 48, 1, NULL, 0),
(346, 'Huyện Hoài Nhơn', '', 48, 1, NULL, 0),
(347, 'Huyện Phù Cát', '', 48, 1, NULL, 0),
(348, 'Huyện Phù Mỹ', '', 48, 1, NULL, 0),
(349, 'Huyện Tuy Phước', '', 48, 1, NULL, 0),
(350, 'Huyện Tây Sơn', '', 48, 1, NULL, 0),
(351, 'Huyện Vân Canh', '', 48, 1, NULL, 0),
(352, 'Huyện Vĩnh Thạnh', '', 48, 1, NULL, 0),
(353, 'Tp. Tuy Hòa', '', 18, 1, NULL, 0),
(354, 'Thị Xã Sông Cầu', '', 18, 1, NULL, 0),
(355, 'Huyện Đông Hòa', '', 18, 1, NULL, 0),
(356, 'Huyện Đồng Xuân', '', 18, 1, NULL, 0),
(357, 'Huyện Phú Hòa', '', 18, 1, NULL, 0),
(358, 'Huyện Sơn Hòa', '', 18, 1, NULL, 0),
(359, 'Huyện Sông Hinh', '', 18, 1, NULL, 0),
(360, 'Huyện Tuy An', '', 18, 1, NULL, 0),
(361, 'Huyện Tây Hòa', '', 18, 1, NULL, 0),
(362, 'Tp. Yên Bái', '', 64, 1, NULL, 0),
(363, 'Thị Xã Nghĩa Lộ', '', 64, 1, NULL, 0),
(364, 'Huyện Lục Yên', '', 64, 1, NULL, 0),
(365, 'Huyện Mù Cang Chải', '', 64, 1, NULL, 0),
(366, 'Huyện Trấn Yên', '', 64, 1, NULL, 0),
(367, 'Huyện Trạm Tấu', '', 64, 1, NULL, 0),
(368, 'Huyện Văn Chấn', '', 64, 1, NULL, 0),
(369, 'Huuyện Văn Yên', '', 64, 1, NULL, 0),
(370, 'Huyện Yên Bình', '', 64, 1, NULL, 0),
(371, 'Tp. Hạ Long', '', 13, 1, NULL, 0),
(372, 'Tp. Móng Cái', '', 13, 1, NULL, 0),
(373, 'Tp. Cẩm Phả', '', 13, 1, NULL, 0),
(374, 'Tp. Uông Bí', '', 13, 1, NULL, 0),
(375, 'Thị Xã Quảng Yên', '', 13, 1, NULL, 0),
(376, 'Huyện Đông Triều', '', 13, 1, NULL, 0),
(377, 'Huyện Tiên Yên', '', 13, 1, NULL, 0),
(378, 'Huyện Hải Hà', '', 13, 1, NULL, 0),
(379, 'Huyện Bình Liêu', '', 13, 1, NULL, 0),
(380, 'Huyện Ba Chẽ', '', 13, 1, NULL, 0),
(381, 'Huyện Cô Tô', '', 13, 1, NULL, 0),
(382, 'Huyện Cô Tô', '', 13, 1, NULL, 0),
(383, 'Huyện Đầm Hà', '', 13, 1, NULL, 0),
(384, 'Huyện Hoành Bồ', '', 13, 1, NULL, 0),
(385, 'Huyện Vân Đồn', '', 13, 1, NULL, 0),
(386, 'Tp. Quảng Ngãi', '', 14, 1, NULL, 0),
(387, 'Huyện Ba Tơ', '', 14, 1, NULL, 0),
(388, 'Huyện Bình Sơn', '', 14, 1, NULL, 0),
(389, 'Huyện Đức Phổ', '', 14, 1, NULL, 0),
(390, 'Huyện Minh Long', '', 14, 1, NULL, 0),
(391, 'Huyện Mộ Đức ', '', 14, 1, NULL, 0),
(392, 'Huyện Nghĩa Hành', '', 14, 1, NULL, 0),
(393, 'Huyện Sơn Hà', '', 14, 1, NULL, 0),
(394, 'Huyện Sơn Tây', '', 14, 1, NULL, 0),
(395, 'Huyện Sơn Tịnh', '', 14, 1, NULL, 0),
(396, 'Huyện Tây Trà', '', 14, 1, NULL, 0),
(397, 'Huyện Trà Bồng', '', 14, 1, NULL, 0),
(398, 'Huyện Tư Nghĩa', '', 14, 1, NULL, 0),
(399, 'Huyện Đảo Lý Sơn', '', 14, 1, NULL, 0),
(400, 'Tp. Tam Kỳ', '', 15, 1, NULL, 0),
(401, 'Tp. Hội An', '', 15, 1, NULL, 0),
(402, 'Huyện Điện Bàn', '', 15, 1, NULL, 0),
(403, 'Huyện Thăng Bình', '', 15, 1, NULL, 0),
(404, 'Huyện Bắc Trà My', '', 15, 1, NULL, 0),
(405, 'Huyện Nam Trà My', '', 15, 1, NULL, 0),
(406, 'Huyện Núi Thành', '', 15, 1, NULL, 0),
(407, 'Huyện Phước Sơn', '', 15, 1, NULL, 0),
(408, 'Huyện Tiên Phước ', '', 15, 1, NULL, 0),
(409, 'Huyện Hiệp Đức', '', 15, 1, NULL, 0),
(410, 'Huyện Nông Sơn', '', 15, 1, NULL, 0),
(411, 'Huyện Đông Giang', '', 15, 1, NULL, 0),
(412, 'Huyện Nam Giang', '', 15, 1, NULL, 0),
(413, 'Huyện Đại Lộc', '', 15, 1, NULL, 0),
(414, 'Huyện Phú Ninh', '', 15, 1, NULL, 0),
(415, 'Huyện Tây Giang', '', 15, 1, NULL, 0),
(416, 'Huyện Duy Xuyên', '', 15, 1, NULL, 0),
(417, 'Huyện Quế Sơn', '', 15, 1, NULL, 0),
(418, 'Tp. Đồng Hới', '', 16, 1, NULL, 0),
(419, 'Huyện Minh Hóa', '', 16, 1, NULL, 0),
(420, 'Huyện Tuyên Hóa', '', 16, 1, NULL, 0),
(421, 'Huyện Quảng Trạch', '', 16, 1, NULL, 0),
(422, 'Huyện Bố Trạch', '', 16, 1, NULL, 0),
(423, 'Huyện Quảng Ninh', '', 16, 1, NULL, 0),
(424, 'Huyện Lệ Thủy', '', 16, 1, NULL, 0),
(425, 'Tp. Việt Trì', '', 17, 1, NULL, 0),
(426, 'Thị Xã Phú Thọ', '', 17, 1, NULL, 0),
(427, 'Huyện Cẩm Khê', '', 17, 1, NULL, 0),
(428, 'Huyện Đoan Hùng', '', 17, 1, NULL, 0),
(429, 'Huyện Hạ Hòa', '', 17, 1, NULL, 0),
(430, 'Huyện Lâm Thao', '', 17, 1, NULL, 0),
(431, 'Huyện Phù Ninh', '', 17, 1, NULL, 0),
(432, 'Huyện Tam Nông', '', 17, 1, NULL, 0),
(433, 'Huyện Tân Sơn', '', 17, 1, NULL, 0),
(434, 'Huyện Thanh Ba', '', 17, 1, NULL, 0),
(435, 'Huyện Thanh Sơn', '', 17, 1, NULL, 0),
(436, 'Huyện Thanh Thủy', '', 17, 1, NULL, 0),
(437, 'Huyện Yên Lập', '', 17, 1, NULL, 0),
(438, 'Tp. Phan Rang - Tháp Chàm', '', 19, 1, NULL, 0),
(439, 'Huyện Bác Ái', '', 19, 1, NULL, 0),
(440, 'Huyện Ninh Hải', '', 19, 1, NULL, 0),
(441, 'Huyện Ninh Phước', '', 19, 1, NULL, 0),
(442, 'Huyện Ninh Sơn', '', 19, 1, NULL, 0),
(443, 'Huyện Thuận Bắc', '', 19, 1, NULL, 0),
(444, 'Huyện Thuận Nam', '', 19, 1, NULL, 0),
(445, 'Tp. Ninh Bình', '', 20, 1, NULL, 0),
(446, 'Thị Xã Tam Điệp', '', 20, 1, NULL, 0),
(447, 'Huyện Nho Quan', '', 20, 1, NULL, 0),
(448, 'Huyện Gia Viễn', '', 20, 1, NULL, 0),
(449, 'Huyện Hoa Lư ', '', 20, 1, NULL, 0),
(450, 'Huyện Yên Khánh', '', 20, 1, NULL, 0),
(451, 'Huyện Kim Sơn', '', 20, 1, NULL, 0),
(452, 'Huyện Yên Mô', '', 20, 1, NULL, 0),
(453, 'Tp. Nam Định', '', 22, 1, NULL, 0),
(454, 'Huyện Giao Thủy', '', 22, 1, NULL, 0),
(455, 'Huyện Hải Hậu', '', 22, 1, NULL, 0),
(456, 'Huyện Mỹ Lộc', '', 22, 1, NULL, 0),
(457, 'Huyện Nam Trực', '', 22, 1, NULL, 0),
(458, 'Huyện Nghĩa Hưng', '', 22, 1, NULL, 0),
(459, 'Huyện Trực Ninh', '', 22, 1, NULL, 0),
(460, 'Huyện Vụ Bản', '', 22, 1, NULL, 0),
(461, 'Huyện Xuân Trường', '', 22, 1, NULL, 0),
(462, 'Huyện Ý Yên', '', 22, 1, NULL, 0),
(463, 'Tp. Lạng Sơn', '', 24, 1, NULL, 0),
(464, 'Huyện Tràng Định', '', 24, 1, NULL, 0),
(465, 'Huyện Văn Lãng', '', 24, 1, NULL, 0),
(466, 'Huyện Văn Quan', '', 24, 1, NULL, 0),
(467, 'Huyện Bình Gia', '', 24, 1, NULL, 0),
(468, 'Huyện Bắc Sơn', '', 24, 1, NULL, 0),
(469, 'Huyện Hữu Lũng', '', 24, 1, NULL, 0),
(470, 'Huyện Chi Lăng', '', 24, 1, NULL, 0),
(471, 'Huyện Cao Lộc', '', 24, 1, NULL, 0),
(472, 'Huyện Lộc Bình', '', 24, 1, NULL, 0),
(473, 'Huyện Đình Lập', '', 24, 1, NULL, 0),
(474, 'Tp. Lào Cai', '', 25, 1, NULL, 0),
(475, 'Huyện Bảo Thắng ', '', 25, 1, NULL, 0),
(476, 'Huyện Bảo Yên', '', 25, 1, NULL, 0),
(477, 'Huyện Bát Xát', '', 25, 1, NULL, 0),
(478, 'Huyện Bắc Hà', '', 25, 1, NULL, 0),
(479, 'Huyện Mường Khương', '', 25, 1, NULL, 0),
(480, 'Huyện Sa Pa', '', 25, 1, NULL, 0),
(481, 'Huyện Si Ma Cai', '', 25, 1, NULL, 0),
(482, 'Huyện Văn Bàn', '', 25, 1, NULL, 0),
(483, 'Thị Xã Lai Châu', '', 26, 1, NULL, 0),
(484, 'Huyện Mường Tè', '', 26, 1, NULL, 0),
(485, 'Huyện Phong Thổ', '', 26, 1, NULL, 0),
(486, 'Huyện Sìn Hồ', '', 26, 1, NULL, 0),
(487, 'Huyện Tam Đường', '', 26, 1, NULL, 0),
(488, 'Huyện Than Uyên', '', 26, 1, NULL, 0),
(489, 'Huyện Tân Uyên', '', 26, 1, NULL, 0),
(490, 'Huyện Nậm Nhùn', '', 26, 1, NULL, 0),
(491, 'Tp. Hưng Yên', '', 30, 1, NULL, 0),
(492, 'Huyện Ân Thi', '', 30, 1, NULL, 0),
(493, 'Huyện Khoái Châu', '', 30, 1, NULL, 0),
(494, 'Huyện Kim Động', '', 30, 1, NULL, 0),
(495, 'Huyện Mỹ Hào', '', 30, 1, NULL, 0),
(496, 'Huyện Phù Cừ', '', 30, 1, NULL, 0),
(497, 'Huyện Tiên Lữ', '', 30, 1, NULL, 0),
(498, 'Huyện Văn Giang', '', 30, 1, NULL, 0),
(499, 'Huyện Văn Lâm', '', 30, 1, NULL, 0),
(500, 'Huyện Yên Mỹ', '', 30, 1, NULL, 0),
(501, 'Tp. Hòa Bình', '', 32, 1, NULL, 0),
(502, 'Huyện Lương Sơn', '', 32, 1, NULL, 0),
(503, 'Huyện Cao Phong', '', 32, 1, NULL, 0),
(504, 'Huyện Đài Bắc', '', 32, 1, NULL, 0),
(505, 'Huyện Kim Bôi', '', 32, 1, NULL, 0),
(506, 'Huyện Kỳ Sơn ', '', 32, 1, NULL, 0),
(507, 'Huyện Lạc Sơn', '', 32, 1, NULL, 0),
(508, 'Huyện Lạc Thủy', '', 32, 1, NULL, 0),
(509, 'Huyện Mai Châu', '', 32, 1, NULL, 0),
(510, 'Huyện Tân Lạc', '', 32, 1, NULL, 0),
(511, 'Huyện Yên Thủy', '', 32, 1, NULL, 0),
(512, 'Tp. Hải Dương', '', 34, 1, NULL, 0),
(513, 'Thị Xã Chí Linh', '', 34, 1, NULL, 0),
(514, 'Huyện Nam Sách', '', 34, 1, NULL, 0),
(515, 'Huyện Kinh Môn', '', 34, 1, NULL, 0),
(516, 'Huyện Kim Thành', '', 34, 1, NULL, 0),
(517, 'Huyện Thanh Hà', '', 34, 1, NULL, 0),
(518, 'Huyện Cẩm Gìang', '', 34, 1, NULL, 0),
(519, 'Huyện Bình Giang', '', 34, 1, NULL, 0),
(520, 'Huyện Gia Lộc', '', 34, 1, NULL, 0),
(521, 'Huyện Tứ Kỳ', '', 34, 1, NULL, 0),
(522, 'Huyện Ninh Giang', '', 34, 1, NULL, 0),
(523, 'Huyện Thanh Miện', '', 34, 1, NULL, 0),
(524, 'Tp. Hà Giang', '', 38, 1, NULL, 0),
(525, 'Huyện Bắc Mê', '', 38, 1, NULL, 0),
(526, 'Huyện Bắc Quang', '', 38, 1, NULL, 0),
(527, 'Huyện Đồng Văn', '', 38, 1, NULL, 0),
(528, 'Huyện Hoàng Su Phì', '', 38, 1, NULL, 0),
(529, 'Huyện Mèo Vạc', '', 38, 1, NULL, 0),
(530, 'Huyện Quản Bạ', '', 38, 1, NULL, 0),
(531, 'Huyện Quang Bình', '', 38, 1, NULL, 0),
(532, 'Huyện Vị Xuyên', '', 38, 1, NULL, 0),
(533, 'Huyện Xín Mần', '', 38, 1, NULL, 0),
(534, 'Huyện Yên Minh', '', 38, 1, NULL, 0),
(535, 'Tp. Pleiku', '', 39, 1, NULL, 0),
(536, 'Thị Xã An Khê', '', 39, 1, NULL, 0),
(537, 'Thị Xã Ayun Pa', '', 39, 1, NULL, 0),
(538, 'Huyện Chư Păh', '', 39, 1, NULL, 0),
(539, 'Huyện Chư Prông', '', 39, 1, NULL, 0),
(540, 'Huyện Chư Sê', '', 39, 1, NULL, 0),
(541, 'Huyện Vị Xuyên', '', 38, 1, NULL, 0),
(542, 'Huyện Đăk Đoa', '', 39, 1, NULL, 0),
(543, 'Huyện Đak Pơ', '', 39, 1, NULL, 0),
(544, 'Huyện Đức Cơ', '', 39, 1, NULL, 0),
(545, 'Huyện La Grai', '', 39, 1, NULL, 0),
(546, 'Huyện La Pa', '', 39, 1, NULL, 0),
(547, 'Huyện Kông Chro', '', 39, 1, NULL, 0),
(548, 'Huyện Krông Pa', '', 39, 1, NULL, 0),
(549, 'Huyện Mang Yang', '', 39, 1, NULL, 0),
(550, 'Huyện Phú Thiện', '', 39, 1, NULL, 0),
(551, 'Huyện Chư Pưh', '', 39, 1, NULL, 0),
(552, 'Tp. Điện Biên Phủ', '', 41, 1, NULL, 0),
(553, 'Thị Xã Mường Lay', '', 41, 1, NULL, 0),
(554, 'Huyện Điện Biên', '', 41, 1, NULL, 0),
(555, 'Huyện Điện Biên Đông', '', 41, 1, NULL, 0),
(556, 'Huyện Mường Ảng ', '', 41, 1, NULL, 0),
(557, 'Huyện Mường Chà', '', 41, 1, NULL, 0),
(558, 'Huyện Mường Nhé', '', 41, 1, NULL, 0),
(559, 'Huyện Mường Chà', '', 41, 1, NULL, 0),
(560, 'Huyện Tủa Chùa', '', 41, 1, NULL, 0),
(561, 'Huyện Tuần Gíao', '', 41, 1, NULL, 0),
(562, 'Huyện Nậm Pồ', '', 41, 1, NULL, 0),
(563, 'Thị Xã Gia Nghĩa', '', 42, 1, NULL, 0),
(564, 'Huyện Cư Jút', '', 42, 1, NULL, 0),
(565, 'Huyện Đắk Glông', '', 42, 1, NULL, 0),
(566, 'Huyện Đắk Mil', '', 42, 1, NULL, 0),
(567, 'Huyện Đắk Song', '', 42, 1, NULL, 0),
(568, 'Huyện Krông Nô', '', 42, 1, NULL, 0),
(569, 'Huyện Tuy Đức', '', 42, 1, NULL, 0),
(570, 'Tp. Buôn Ma Thuộc', '', 43, 1, NULL, 0),
(571, 'Thị Xã Buôn Hồ', '', 43, 1, NULL, 0),
(572, 'Huyện Buôn Đôn', '', 43, 1, NULL, 0),
(573, 'Huyện Cư Kuin', '', 43, 1, NULL, 0),
(574, 'Huyên Ea Kar', '', 43, 1, NULL, 0),
(575, 'Huyện Ea Súp', '', 43, 1, NULL, 0),
(576, 'Huyện Krông Bông', '', 43, 1, NULL, 0),
(577, 'Huyện Krông Buk', '', 43, 1, NULL, 0),
(578, 'Huyện Krông Buk', '', 43, 1, NULL, 0),
(579, 'Huyện Krông Pak', '', 43, 1, NULL, 0),
(580, 'Huyện Lắk', '', 43, 1, NULL, 0),
(581, 'Huyện Krông Ana', '', 43, 1, NULL, 0),
(582, 'Huyện Krông Năng', '', 43, 1, NULL, 0),
(583, 'Tp. Cao bằng', '', 44, 1, NULL, 0),
(584, 'Huyện Bảo Lạc', '', 44, 1, NULL, 0),
(585, 'Huyện Bảo lâm ', '', 44, 1, NULL, 0),
(586, 'Huyện Hạ Lang', '', 44, 1, NULL, 0),
(587, 'Huyện Hà Quảng ', '', 44, 1, NULL, 0),
(588, 'Huyện Hòa An', '', 44, 1, NULL, 0),
(589, 'Huyện Nguyên Bình', '', 44, 1, NULL, 0),
(590, 'Huyện Phục Hòa', '', 44, 1, NULL, 0),
(591, 'Huyện Quảng Uyên ', '', 44, 1, NULL, 0),
(592, 'Huyện Thạch An', '', 44, 1, NULL, 0),
(593, 'Huyện Thông Nông', '', 44, 1, NULL, 0),
(594, 'Huyện Trà Lĩnh', '', 44, 1, NULL, 0),
(595, 'Huyện Trùng Khánh', '', 44, 1, NULL, 0),
(596, 'Tp. Bắc Ninh', '', 49, 1, NULL, 0),
(597, 'Thị Xã Từ Sơn', '', 49, 1, NULL, 0),
(598, 'Huyện Gia Bình', '', 49, 1, NULL, 0),
(599, 'Huyện Lương Tài', '', 49, 1, NULL, 0),
(600, 'Huyện Quế Võ', '', 49, 1, NULL, 0),
(601, 'Huyện Thuận Thành', '', 49, 1, NULL, 0),
(602, 'Huyện Tiên Du', '', 49, 1, NULL, 0),
(603, 'Huyện Yên Phong', '', 49, 1, NULL, 0),
(604, 'Tp. Bắc Giang', '', 50, 1, NULL, 0),
(605, 'Huyện Yên Thế', '', 50, 1, NULL, 0),
(606, 'Huyện Tân Yên', '', 50, 1, NULL, 0),
(607, 'Huyện Lục Ngạn', '', 50, 1, NULL, 0),
(608, 'Huyện Hiệp Hòa', '', 50, 1, NULL, 0),
(609, 'Huyện Lạng Giang', '', 50, 1, NULL, 0),
(610, 'Huyện Sơn Động', '', 50, 1, NULL, 0),
(611, 'Huyện Lục Nam', '', 50, 1, NULL, 0),
(612, 'Huyện Việt Yên', '', 50, 1, NULL, 0),
(613, 'Huyện Yên Dũng', '', 50, 1, NULL, 0),
(614, 'Thị Xã Bắc Kạn', '', 51, 1, NULL, 0),
(615, 'Huyện Ba Bể', '', 51, 1, NULL, 0),
(616, 'Huyện Bạch Thông', '', 51, 1, NULL, 0),
(617, 'Huyện Chợ Đồn', '', 51, 1, NULL, 0),
(618, 'Huyện Chợ Mới ', '', 51, 1, NULL, 0),
(619, 'Huyện Na Rì', '', 51, 1, NULL, 0),
(620, 'Huyện Ngân Sơn', '', 51, 1, NULL, 0),
(621, 'Huyện Pác Nặm', '', 51, 1, NULL, 0),
(622, 'Tp. Thanh Hóa', '', 69, 1, NULL, 0),
(623, 'Thị Xã Bỉm Sơn', '', 69, 1, NULL, 0),
(624, 'Thị Xã Sầm Sơn', '', 69, 1, NULL, 0),
(625, 'Huyện Bá Thước', '', 69, 1, NULL, 0),
(626, 'Huyện Cẩm Thủy', '', 0, 1, NULL, 0),
(627, 'Huyện Đông Sơn', '', 69, 1, NULL, 0),
(628, 'Huyện Hà Trung', '', 69, 1, NULL, 0),
(629, 'Huyện Hậu Lộc', '', 69, 1, NULL, 0),
(630, 'Huyện Hoằng Hóa', '', 69, 1, NULL, 0),
(631, 'Huyện Lang Chánh', '', 69, 1, NULL, 0),
(632, 'Huyện Mường Lát', '', 69, 1, NULL, 0),
(633, 'Huyện Nga Sơn ', '', 69, 1, NULL, 0),
(634, 'Huyện Ngọc Lặc', '', 69, 1, NULL, 0),
(635, 'Huyện Như Thanh', '', 69, 1, NULL, 0),
(636, 'Huyện Như Xuân', '', 69, 1, NULL, 0),
(637, 'Huyện Nông Cống', '', 69, 1, NULL, 0),
(638, 'Huyện Quan Hóa', '', 69, 1, NULL, 0),
(639, 'Huyện Quan Sơn', '', 69, 1, NULL, 0),
(640, 'Huyện Quảng Xương', '', 69, 1, NULL, 0),
(641, 'Huyện Thạch Thành', '', 69, 1, NULL, 0),
(642, 'Huyện Thiệu Hóa', '', 69, 1, NULL, 0),
(643, 'Huyện Thọ Xuân', '', 69, 1, NULL, 0),
(644, 'Huyện Thường Xuân', '', 69, 1, NULL, 0),
(645, 'Huyện Tĩnh Gia ', '', 69, 1, NULL, 0),
(646, 'Huyện Triệu Sơn', '', 69, 1, NULL, 0),
(647, 'Huyện Vĩnh Lộc', '', 69, 1, NULL, 0),
(648, 'Huyện Yên Định', '', 69, 1, NULL, 0),
(649, 'Tp. Vĩnh Yên', '', 65, 1, NULL, 0),
(650, 'Thị Xã Phúc Yên', '', 65, 1, NULL, 0),
(651, 'Huyện Bình Xuyên', '', 65, 1, NULL, 0),
(652, 'Huyện Lập Thạch', '', 65, 1, NULL, 0),
(653, 'Huyện Sông Lô', '', 65, 1, NULL, 0),
(654, 'Huyện Tam Dương ', '', 65, 1, NULL, 0),
(655, 'Huyện Tam Đảo', '', 65, 1, NULL, 0),
(656, 'Huyện Vĩnh Tường', '', 65, 1, NULL, 0),
(657, 'Huyện Yên Lạc', '', 65, 1, NULL, 0),
(658, 'Tp. Tuyên Quang', '', 67, 1, NULL, 0),
(659, 'Huyện Chiêm Hóa', '', 67, 1, NULL, 0),
(660, 'Huyện Hàm Yên', '', 67, 1, NULL, 0),
(661, 'Huyện Na Hang', '', 67, 1, NULL, 0),
(662, 'Huyện Sơn Dương', '', 67, 1, NULL, 0),
(663, 'Huyện Yên Sơn', '', 67, 1, NULL, 0),
(664, 'Huyện Lâm Bình', '', 67, 1, NULL, 0),
(665, 'Tp. Thái Nguyên', '', 70, 1, NULL, 0),
(666, 'Thị Xã Sông Công', '', 70, 1, NULL, 0),
(667, 'Huyện Đại Từ', '', 70, 1, NULL, 0),
(668, 'Huyện Định Hóa', '', 70, 1, NULL, 0),
(669, 'Huyện Đồng Hỷ', '', 70, 1, NULL, 0),
(670, 'Huyện Phổ Yên', '', 70, 1, NULL, 0),
(671, 'Huyện Phú Bình', '', 70, 1, NULL, 0),
(672, 'Huyện Phú Lương', '', 70, 1, NULL, 0),
(673, 'Huyện Võ Nhai', '', 70, 1, NULL, 0),
(674, 'Tp. Thái Bình', '', 71, 1, NULL, 0),
(675, 'Huyện Đông Hưng', '', 71, 1, NULL, 0),
(676, 'Huyện Hưng Hà', '', 71, 1, NULL, 0),
(677, 'Huyện Kiến Xương', '', 71, 1, NULL, 0),
(678, 'Huyện Quỳnh Phụ', '', 71, 1, NULL, 0),
(679, 'Huyện Thái Thụy', '', 71, 1, NULL, 0),
(680, 'Huyện Tiền Hải', '', 71, 1, NULL, 0),
(681, 'Huyện Vũ Thư', '', 71, 1, NULL, 0),
(682, 'Tp. Sơn La', '', 72, 1, NULL, 0),
(683, 'Huyện Quỳnh Nhai', '', 72, 1, NULL, 0),
(684, 'Huyện Mường La', '', 72, 1, NULL, 0),
(685, 'Huyện Thuận Châu', '', 72, 1, NULL, 0),
(686, 'Huyện Phù Yên', '', 72, 1, NULL, 0),
(687, 'Huyện Bắc Yên', '', 72, 1, NULL, 0),
(688, 'Huyện Mai Sơn', '', 72, 1, NULL, 0),
(689, 'Huyện Sông Mã', '', 72, 1, NULL, 0),
(690, 'Huyện Yên Châu', '', 72, 1, NULL, 0),
(691, 'Huyện Mộc Châu', '', 72, 1, NULL, 0),
(692, 'Huyện Sốp Cộp', '', 72, 1, NULL, 0),
(693, 'Tp. Đông Hà', '', 75, 1, NULL, 0),
(694, 'Thị Xã Quảng Trị', '', 75, 1, NULL, 0),
(695, 'Huyện Cam Lộ', '', 75, 1, NULL, 0),
(696, 'Huyện Cồn Cỏ', '', 75, 1, NULL, 0),
(697, 'Huyện Đa Krông', '', 75, 1, NULL, 0),
(698, 'Huyện Gio Linh', '', 75, 1, NULL, 0),
(699, 'Huyện Hải Lăng', '', 75, 1, NULL, 0),
(700, 'Huyện Hướng Hóa', '', 75, 1, NULL, 0),
(701, 'Huyện Triệu Phong', '', 75, 1, NULL, 0),
(702, 'Huyện Vĩnh Linh', '', 75, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `emails`
--

CREATE TABLE `emails` (
  `email` varchar(128) NOT NULL,
  `request_date` datetime NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `added_date`, `updated_date`, `updated_by`) VALUES
(1, 'TEst', '2015-10-15 22:16:37', '2015-10-15 22:16:37', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `home_sliders`
--

CREATE TABLE `home_sliders` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(256) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `home_sliders`
--

INSERT INTO `home_sliders` (`id`, `name`, `description`, `ordering`, `status`) VALUES
(3, '2017-02-13-09-40-44-126834373-banner-web-1641.jpg', '3', 3, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `display_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `alt` varchar(128) NOT NULL,
  `thumbnails` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `items`
--

INSERT INTO `items` (`id`, `product_id`, `name`, `display_order`, `status`, `alt`, `thumbnails`) VALUES
(6, 2, '5.jpg', 0, 0, 'mặt sau', 1),
(7, 2, '6.jpg', 0, 0, 'trước', 0),
(8, 2, '2.jpg', 0, 0, 'c', 0),
(9, 2, '3.jpg', 0, 0, 'd', 0),
(10, 2, '1.jpg', 0, 0, 'e', 0),
(11, 2, '4.jpg', 0, 0, 'f', 0),
(12, 1, 'Body-Massage-2.jpg', 0, 0, '', 0),
(13, 1, 'Body-Massage.jpg', 0, 0, '', 0),
(14, 3, 'Massage-Vip-Room-3.jpg', 0, 0, 'a', 0),
(15, 3, 'Massage-Vip-Room-2.jpg', 0, 0, 'b', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `request_date` datetime NOT NULL,
  `fullname` varchar(128) NOT NULL,
  `address` varchar(128) NOT NULL,
  `district_id` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `notes` varchar(765) NOT NULL,
  `status` int(11) NOT NULL,
  `total` float NOT NULL,
  `fee_ship` float NOT NULL,
  `view` int(11) NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_status` varchar(128) NOT NULL,
  `payment_date` date NOT NULL,
  `updated_date` datetime NOT NULL,
  `promotion_code` varchar(20) NOT NULL,
  `promotion_code_value` float NOT NULL,
  `email` varchar(128) NOT NULL,
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `order_no`, `request_date`, `fullname`, `address`, `district_id`, `city`, `user_id`, `phone`, `notes`, `status`, `total`, `fee_ship`, `view`, `payment_method`, `payment_status`, `payment_date`, `updated_date`, `promotion_code`, `promotion_code_value`, `email`, `reason`) VALUES
(1, 45234927, '2016-12-28 16:12:36', 'Tap', '180/21', 18, 2, 1, '0982007996', '', 0, 400000, 0, 1, '', 'new', '0000-00-00', '0000-00-00 00:00:00', '', 0, 'admin@gmail.com', ''),
(2, 16485908, '2017-01-05 17:01:04', 'nhung', '500 NGUYEN VAN QUA, DONG HUNG THUAN, QUAN 12, TP.HCM', 18, 2, 0, '01643767583', '', 0, 410000, 0, 1, '', 'new', '0000-00-00', '0000-00-00 00:00:00', '', 0, 'nhung@gmail.com', ''),
(3, 63572626, '2017-02-27 14:02:52', 'Tap', '180/21', 18, 2, 1, '0982007996', '', 0, 390000, 0, 0, '', 'new', '0000-00-00', '0000-00-00 00:00:00', '', 0, 'admin@gmail.com', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` varchar(128) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attributes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `category_id`, `price`, `quantity`, `product_name`, `user_id`, `attributes`) VALUES
(1, 1, 222, '3', 200000, 2, 'aaaaaaaaaaaa', 1, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}'),
(2, 2, 1, '1', 410000, 1, 'Giày cao gót bc01', 0, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}'),
(3, 3, 11, '1', 390000, 1, 'GIÀY CAO GÓT HỞ MŨI', 1, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `meta_title` varchar(256) NOT NULL,
  `id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `slug` varchar(256) NOT NULL,
  `color` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(64) NOT NULL,
  `price_discount` float NOT NULL,
  `type` int(11) NOT NULL,
  `cover_photo` varchar(128) NOT NULL,
  `material` text NOT NULL,
  `height` varchar(28) NOT NULL,
  `clothesline` text NOT NULL,
  `description` text NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `meta_keyword` varchar(256) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `hot` int(11) NOT NULL,
  `online` int(11) NOT NULL,
  `selling` int(11) NOT NULL,
  `2x` int(11) NOT NULL,
  `promotion_flag` int(11) NOT NULL,
  `promotion_text` varchar(256) NOT NULL,
  `unit` varchar(128) NOT NULL,
  `weight` float NOT NULL,
  `featured` int(11) NOT NULL,
  `combination` varchar(765) NOT NULL,
  `provider` varchar(128) NOT NULL,
  `barcode` varchar(128) NOT NULL,
  `multiversion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`meta_title`, `id`, `category_id`, `sub_category_id`, `sub_sub_category_id`, `name`, `product_code`, `slug`, `color`, `size`, `quantity`, `price`, `price_discount`, `type`, `cover_photo`, `material`, `height`, `clothesline`, `description`, `added_date`, `updated_date`, `updated_by`, `status`, `meta_keyword`, `meta_description`, `hot`, `online`, `selling`, `2x`, `promotion_flag`, `promotion_text`, `unit`, `weight`, `featured`, `combination`, `provider`, `barcode`, `multiversion`) VALUES
('giay', 2, '1', 0, 0, 'giay', 'bc1', 'giay', 0, 0, 0, '400000', 0, 0, '', '', '', '', '<p>1111</p>\r\n', '2017-01-05 17:20:20', '2017-01-05 17:20:20', 1, 0, '', '', 1, 1, 0, 0, 0, '', '', 0, 1, '', '', '', 0),
('Giày búp bê đẹp', 7, '3', 0, 0, 'Giày búp bê đẹp', 'test', 'giay-bup-be-p', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>Gi&agrave;y BCC</p>\r\n', '2017-02-17 08:31:26', '2017-02-17 08:31:26', 1, 0, '', '', 1, 1, 1, 0, 0, '', '', 0, 1, '', '', '', 0),
('GIÀY BÚP BÊ QUÝ TỘC', 8, '3', 0, 0, 'GIÀY BÚP BÊ QUÝ TỘC', 'test1', 'giay-bup-be-qu-toc', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>gi&agrave;y đẹp</p>\r\n', '2017-02-17 08:34:02', '2017-02-17 08:34:02', 1, 0, '', '', 1, 1, 1, 0, 0, '', '', 0, 1, '', '', '', 0),
('GIÀY BÚP BÊ PHONG CÁCH', 9, '3', 0, 0, 'GIÀY BÚP BÊ PHONG CÁCH', 'test2', 'giay-bup-be-phong-cach', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y B&Uacute;P B&Ecirc; PHONG C&Aacute;CH</p>\r\n', '2017-02-17 08:36:32', '2017-02-17 08:36:32', 1, 0, '', '', 1, 1, 1, 0, 0, '', '', 0, 1, '', '', '', 0),
('GIÀY CAO GÓT ĐÍNH NƠ ', 10, '3', 0, 0, 'GIÀY CAO GÓT ĐÍNH NƠ ', 'test3', 'giay-cao-got-inh-no', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y CAO G&Oacute;T Đ&Iacute;NH NƠ&nbsp;</p>\r\n', '2017-02-17 08:38:13', '2017-02-17 08:38:13', 1, 0, '', '', 1, 1, 1, 0, 0, '', '', 0, 1, '', '', '', 0),
('GIÀY CAO GÓT HỞ MŨI', 11, '1', 0, 0, 'GIÀY CAO GÓT HỞ MŨI', 'test4', 'giay-cao-got-ho-mi', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y CAO G&Oacute;T HỞ MŨI</p>\r\n', '2017-02-17 08:44:48', '2017-02-17 08:44:48', 1, 0, '', '', 1, 1, 0, 0, 0, '', '', 0, 0, '', '', '', 0),
('GIÀY CAO GÓT HỞ MŨI PHỐI MÀU', 12, '1', 0, 0, 'GIÀY CAO GÓT HỞ MŨI PHỐI MÀU', 'test5', 'giay-cao-got-ho-mi-phoi-mau', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y CAO G&Oacute;T HỞ MŨI PHỐI M&Agrave;U</p>\r\n', '2017-02-17 08:45:50', '2017-02-17 08:45:50', 1, 0, '', '', 0, 0, 1, 0, 0, '', '', 0, 1, '', '', '', 0),
('GIÀY CAO GÓT BÍT MŨI', 13, '1', 0, 0, 'GIÀY CAO GÓT BÍT MŨI', 'test6', 'giay-cao-got-bit-mi', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y CAO G&Oacute;T B&Iacute;T MŨI</p>\r\n', '2017-02-17 08:46:37', '2017-02-17 08:46:37', 1, 0, '', '', 0, 1, 0, 0, 0, '', '', 0, 0, '', '', '', 0),
('GIÀY BOOT CÁCH ĐIỆU GÓT NHỌN', 14, '4', 0, 0, 'GIÀY BOOT CÁCH ĐIỆU GÓT NHỌN', 'test68', 'giay-boot-cach-iu-got-nhon', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y BOOT C&Aacute;CH ĐIỆU G&Oacute;T NHỌN</p>\r\n', '2017-02-17 08:53:35', '2017-02-17 08:53:35', 1, 0, '', '', 0, 0, 1, 0, 0, '', '', 0, 1, '', '', '', 0),
('GIÀY CAO GÓT QUÝ TỘC', 15, '4', 0, 0, 'GIÀY CAO GÓT QUÝ TỘC', 'test67', 'giay-cao-got-qu-toc', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y CAO G&Oacute;T QU&Yacute; TỘC</p>\r\n', '2017-02-17 08:54:22', '2017-02-17 08:54:22', 1, 0, '', '', 0, 0, 0, 0, 0, '', '', 0, 1, '', '', '', 0),
('BC 6124-SDD', 16, '2', 0, 0, 'BC 6124-SDD', 'test69', 'bc-6124-sdd', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y HIỆU BCC CAO CẤP</p>\r\n', '2017-02-17 08:56:08', '2017-02-17 08:56:08', 1, 0, '', '', 0, 1, 1, 0, 0, '', '', 0, 0, '', '', '', 0),
('BC 6123-SDD', 17, '2', 0, 0, 'BC 6123-SDD', 'BC 6123-SDD', 'bc-6123-sdd', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>GI&Agrave;Y HIỆU BCC</p>\r\n', '2017-02-17 09:04:27', '2017-02-17 09:04:27', 1, 0, '', '', 0, 1, 1, 0, 0, '', '', 0, 0, '', '', '', 0),
('BC 6122-SDD', 18, '2', 0, 0, 'BC 6122-SDD', '6122-SDD', 'bc-6122-sdd', 0, 0, 0, '390000', 0, 0, '', '', '', '', '<p>BC 6122-SDD</p>\r\n', '2017-02-17 09:06:33', '2017-02-17 09:06:33', 1, 0, '', '', 0, 1, 0, 0, 0, '', '', 0, 1, '', '', '', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL,
  `barcode` varchar(128) NOT NULL,
  `weight` float NOT NULL,
  `product_id` int(11) NOT NULL,
  `attributes` text NOT NULL,
  `price` varchar(32) NOT NULL,
  `photo` varchar(128) NOT NULL,
  `product_code` varchar(32) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `barcode`, `weight`, `product_id`, `attributes`, `price`, `photo`, `product_code`, `quantity`, `status`) VALUES
(8, '', 0, 27, '{\"M\\u00e0u s\\u1eafc\":\"xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"24\"}', '150000', '', 'asdadsa h-1', 0, 0),
(9, '', 0, 27, '{\"M\\u00e0u s\\u1eafc\":\"xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"25\"}', '150000', '', 'asdadsa h-2', 0, 0),
(10, '', 0, 27, '{\"M\\u00e0u s\\u1eafc\":\"do\",\"K\\u00edch th\\u01b0\\u1edbc\":\"24\"}', '150000', '', 'asdadsa h-3', 0, 0),
(11, '', 0, 27, '{\"M\\u00e0u s\\u1eafc\":\"do\",\"K\\u00edch th\\u01b0\\u1edbc\":\"25\"}', '150000', '', 'asdadsa h-4', 0, 0),
(12, '', 0, 28, '{\"M\\u00e0u s\\u1eafc\":\"xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"24\",\"Ki\\u1ec3u d\\u00e1ng\":\"thon\"}', '150000', 'Chrysanthemum (1).jpg', 'aaa-1', 0, 0),
(13, '', 0, 28, '{\"M\\u00e0u s\\u1eafc\":\"xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"25\",\"Ki\\u1ec3u d\\u00e1ng\":\"thon\"}', '150000', 'Chrysanthemum (1).jpg', 'aaa-2', 0, 0),
(14, '', 0, 28, '{\"M\\u00e0u s\\u1eafc\":\"xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"26\",\"Ki\\u1ec3u d\\u00e1ng\":\"thon\"}', '150000', 'Penguins.jpg', 'aaa-3', 0, 0),
(15, '', 0, 28, '{\"M\\u00e0u s\\u1eafc\":\"do\",\"K\\u00edch th\\u01b0\\u1edbc\":\"24\",\"Ki\\u1ec3u d\\u00e1ng\":\"thon\"}', '150000', '', 'aaa-4', 0, 0),
(16, '', 0, 28, '{\"M\\u00e0u s\\u1eafc\":\"do\",\"K\\u00edch th\\u01b0\\u1edbc\":\"25\",\"Ki\\u1ec3u d\\u00e1ng\":\"thon\"}', '150000', '', 'aaa-5', 0, 0),
(17, '', 0, 28, '{\"M\\u00e0u s\\u1eafc\":\"do\",\"K\\u00edch th\\u01b0\\u1edbc\":\"26\",\"Ki\\u1ec3u d\\u00e1ng\":\"thon\"}', '150000', '', 'aaa-6', 0, 0),
(18, '', 0, 29, '{\"\":\"XL\"}', '200000', '', 'Ma01234-1', 0, 1),
(19, '', 0, 29, '{\"\":\"L\"}', '200000', '', 'Ma01234-2', 0, 0),
(20, '', 0, 29, '{\"\":\"M\"}', '200000', 'news-3.jpg', 'Ma01234-3', 0, 1),
(21, '', 0, 29, '{\"\":\"S\"}', '200000', '', 'Ma01234-4', 0, 0),
(22, '50', 50, 30, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\"}', '200', '', 'MA0983222-1', 0, 0),
(23, '', 0, 30, '{\"\":\"L\"}', '200000', '', 'MA0983222-2', 0, 1),
(24, '', 0, 30, '{\"\":\"XL\"}', '200000', '', 'MA0983222-3', 0, 1),
(25, '', 0, 31, '{\"\":\"M\"}', '250000', '', 'AK3000-1', 0, 1),
(26, '', 0, 31, '{\"\":\"S\"}', '250000', 'image1x (1).jpg', 'AK3000-2', 0, 0),
(27, '', 0, 31, '{\"\":\"L\"}', '250000', '', 'AK3000-3', 0, 1),
(28, '', 0, 31, '{\"\":\"XL\"}', '250000', 'news-3 (1).jpg', 'AK3000-4', 0, 0),
(29, '', 0, 32, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', 'image1x (1).jpg', 'AK03894-1', 0, 1),
(30, '', 0, 32, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'AK03894-2', 0, 0),
(31, '', 0, 32, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'AK03894-3', 0, 0),
(32, '', 0, 32, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'AK03894-4', 0, 0),
(33, '', 0, 32, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"L\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'AK03894-5', 0, 0),
(34, '', 0, 32, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"L\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'AK03894-6', 0, 0),
(35, '', 0, 33, '{\"\":\"\\u0110\\u1ecf\"}', '245000', 'bgColor-2.jpg', 'AK99999-1', 0, 0),
(36, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-2', 0, 1),
(37, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-3', 0, 1),
(38, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-4', 0, 1),
(39, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', 'bgColor-4.jpg', 'AK99999-5', 0, 1),
(40, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-6', 0, 1),
(41, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-7', 0, 1),
(42, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-8', 0, 1),
(43, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-9', 0, 1),
(44, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-10', 0, 1),
(45, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-11', 0, 1),
(46, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"39\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-12', 0, 1),
(47, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-13', 0, 1),
(48, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-14', 0, 1),
(49, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-15', 0, 1),
(50, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-16', 0, 1),
(51, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-17', 0, 1),
(52, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-18', 0, 1),
(53, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-19', 0, 1),
(54, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-20', 0, 1),
(55, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-21', 0, 1),
(56, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-22', 0, 1),
(57, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"40\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-23', 0, 1),
(58, '', 0, 33, '{\"\":\"\\u0110\\u1ecf\"}', '245000', '', 'AK99999-24', 0, 0),
(59, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-25', 0, 1),
(60, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-26', 0, 1),
(61, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-27', 0, 1),
(62, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-28', 0, 1),
(63, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-29', 0, 1),
(64, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-30', 0, 1),
(65, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-31', 0, 1),
(66, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-32', 0, 1),
(67, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-33', 0, 1),
(68, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-34', 0, 1),
(69, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"41\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-35', 0, 1),
(70, '', 0, 33, '{\"\":\"\\u0110\\u1ecf\"}', '245000', 'image1x (2).jpg', 'AK99999-36', 0, 0),
(71, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-37', 0, 1),
(72, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-38', 0, 1),
(73, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-39', 0, 1),
(74, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-40', 0, 1),
(75, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-41', 0, 1),
(76, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-42', 0, 1),
(77, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-43', 0, 1),
(78, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-44', 0, 1),
(79, '', 0, 33, '{\"\":\"\\u0110\\u1ecf\"}', '245000', '', 'AK99999-45', 0, 0),
(80, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-46', 0, 1),
(81, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-47', 0, 1),
(82, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-48', 0, 1),
(83, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-49', 0, 1),
(84, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-50', 0, 1),
(85, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-51', 0, 1),
(86, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-52', 0, 1),
(87, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-53', 0, 1),
(88, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-54', 0, 1),
(89, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-55', 0, 1),
(90, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-56', 0, 1),
(91, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-57', 0, 1),
(92, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Inox\"}', '245000', '', 'AK99999-58', 0, 1),
(93, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Th\\u00e9p\"}', '245000', '', 'AK99999-59', 0, 1),
(94, '', 0, 33, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\",\"Ch\\u1ea5t li\\u1ec7u\":\"Nh\\u1ef1a\"}', '245000', '', 'AK99999-60', 0, 1),
(95, '', 0, 33, '{\"\":\"\\u0110\\u1ecf\"}', '20000', '', 'M1992893', 0, 0),
(96, '', 0, 34, '{\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '210000', '', 'A2232323-1', 0, 0),
(97, '', 0, 34, '{\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '210000', '', 'A2232323-2', 0, 0),
(98, '', 0, 34, '{\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '210000', '', 'A2232323-3', 0, 0),
(99, '', 0, 34, '{\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '210000', '', 'A2232323-4', 0, 0),
(100, '', 0, 34, '{\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '210000', '', 'A2232323-5', 0, 0),
(101, '', 0, 34, '{\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '210000', '', 'A2232323-6', 0, 0),
(102, '', 0, 36, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '200000', '', 'A2222-', 0, 0),
(103, '', 0, 37, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MKN0001-1', 0, 0),
(104, '', 0, 37, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', 'news-3 (1).jpg', 'MKN0001-2', 0, 0),
(105, '', 0, 37, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MKN0001-3', 0, 0),
(106, '', 0, 37, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MKN0001-4', 0, 0),
(107, '', 0, 37, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MKN0001-5', 0, 0),
(108, '', 0, 37, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MKN0001-6', 0, 0),
(109, '', 0, 38, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MKA0001-1', 0, 0),
(110, '', 0, 38, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"42\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MKA0001-2', 0, 0),
(111, '', 0, 38, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MKA0001-3', 0, 0),
(112, '', 0, 38, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"43\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MKA0001-4', 0, 0),
(113, '', 0, 38, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"44\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MKA0001-5', 0, 0),
(114, '', 0, 38, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"44\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MKA0001-6', 0, 0),
(115, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MA00043-1', 0, 0),
(116, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MA00043-2', 0, 0),
(117, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"Caro\"}', '200000', '', 'MA00043-3', 0, 0),
(118, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"L\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MA00043-4', 0, 0),
(119, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"L\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MA00043-5', 0, 0),
(120, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"L\",\"M\\u00e0u s\\u1eafc\":\"Caro\"}', '200000', '', 'MA00043-6', 0, 0),
(121, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MA00043-7', 0, 0),
(122, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '200000', '', 'MA00043-8', 0, 0),
(123, '', 0, 39, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"Caro\"}', '200000', '', 'MA00043-9', 0, 0),
(124, '', 0, 40, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '2.67533e+06', '', 'A1123131-', 0, 0),
(125, '', 0, 41, '{\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"L\"}', '300', '', '345345-1', 0, 1),
(126, '', 0, 41, '{\"M\\u00e0u s\\u1eafc\":\"345345\",\"K\\u00edch th\\u01b0\\u1edbc\":\"3453445665\"}', '300000', '', '345345-2', 0, 1),
(127, '', 0, 41, '{\"M\\u00e0u s\\u1eafc\":\"3453456445\",\"K\\u00edch th\\u01b0\\u1edbc\":\"345345\"}', '300000', '', '345345-3', 0, 1),
(128, '', 0, 41, '{\"M\\u00e0u s\\u1eafc\":\"3453456445\",\"K\\u00edch th\\u01b0\\u1edbc\":\"3453445665\"}', '300000', '', '345345-4', 0, 1),
(129, '', 0, 41, '{\"M\\u00e0u s\\u1eafc\":\"\\u0110en\",\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\"}', '0', '', '', 0, 1),
(130, '', 0, 41, '{\"M\\u00e0u s\\u1eafc\":\"Xanh\",\"K\\u00edch th\\u01b0\\u1edbc\":\"X\"}', '0', '', '', 0, 1),
(131, '', 0, 42, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '546456', '', '234234-', 0, 0),
(132, '', 34, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '400000', '', 'tén-1', 0, 0),
(133, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '300000', '', 'tén-2', 0, 0),
(134, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\"}', '300000', '', 'tén-3', 0, 0),
(135, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"XL\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\"}', '300000', '', 'tén-4', 0, 0),
(136, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '300000', '', 'tén-5', 0, 0),
(137, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '300', '', 'tén-6', 0, 0),
(138, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\"}', '300', '', 'tén-7', 0, 0),
(139, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"X\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\"}', '500', '', 'tén-8', 0, 0),
(140, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '300000', '', 'tén-9', 0, 0),
(141, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"\\u0110\\u1ecf\"}', '300000', '', 'tén-10', 0, 0),
(142, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"T\\u00edm\"}', '300000', '', 'tén-11', 0, 0),
(143, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"M\",\"M\\u00e0u s\\u1eafc\":\"V\\u00e0ng\"}', '300000', '', 'tén-12', 0, 0),
(144, '', 10, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"MM\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '1e+06', '', 'VASdAS', 0, 0),
(145, '', 0, 43, '{\"K\\u00edch th\\u01b0\\u1edbc\":\"37\",\"M\\u00e0u s\\u1eafc\":\"Xanh\"}', '200000', '', 'MA09877', 0, 0),
(146, '', 1, 44, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"\"}', '175', '', 'A0', 0, 0),
(147, '', 0, 45, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '175000', '', 'A001', 0, 0),
(148, '', 0, 46, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Cu\\u1ed9n\"}', '175000', '', 'A001-', 0, 0),
(149, '', 1, 47, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '54000', '', 'A002-', 0, 0),
(150, '', 0, 48, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '54000', '', 'A003-', 0, 0),
(151, '', 1, 49, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '53000', '', 'A004-', 0, 0),
(152, '', 1, 50, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '45000', '', 'A005-', 0, 0),
(153, '', 1, 51, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '225000', '', 'A006-', 0, 0),
(154, '', 1, 52, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '52000', '', 'A007-', 0, 0),
(155, '', 1, 53, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '106000', '', 'A008-', 0, 0),
(156, '', 1, 54, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '92000', '', 'A009-', 0, 0),
(157, '', 1, 55, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '24000', '', 'A010-', 0, 0),
(158, '', 1, 56, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '28000', '', 'A011-', 0, 0),
(159, '', 1, 57, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '8000', '', 'A012-', 0, 0),
(160, '', 0, 58, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '28000', '', 'A013-', 0, 0),
(161, '', 1, 59, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '65000', '', 'A014-', 0, 0),
(162, '', 1, 60, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '51000', '', 'A015-', 0, 0),
(163, '', 0, 61, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '68000', '', 'A016-', 0, 0),
(164, '', 0, 62, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '68000', '', 'A016', 0, 0),
(165, '', 0, 63, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '68000', '', 'A016-', 0, 0),
(166, '', 1, 64, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '60000', '', 'A017-', 0, 0),
(167, '', 1, 65, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '75000', '', 'A018-', 0, 0),
(168, '', 1, 66, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y in\"}', '185000', '', 'A019', 0, 0),
(169, '', 0, 67, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y in\"}', '185000', '', 'A019-', 0, 0),
(170, '', 0, 68, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '64000', '', 'A020-', 0, 0),
(171, '', 1, 69, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y in m\\u00e0u\"}', '65000', '', 'A021-', 0, 0),
(172, '', 1, 70, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In M\\u00e0u\"}', '32000', '', 'A022-', 0, 0),
(173, '', 1, 71, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Fax\"}', '17000', '', 'A023-', 0, 0),
(174, '', 1, 72, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '345000', '', 'A024-', 0, 0),
(175, '', 1, 73, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '215000', '', 'A025-', 0, 0),
(176, '', 1, 74, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '185000', '', 'A026-', 0, 0),
(177, '', 1, 75, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '495000', '', 'A027-', 0, 0),
(178, '', 1, 76, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '345000', '', 'A028-', 0, 0),
(179, '', 1, 77, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y In\"}', '295000', '', 'A029-', 0, 0),
(180, '', 1, 78, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '15000', '', 'A030-', 0, 0),
(181, '', 1, 79, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '7000', '', 'A031-', 0, 0),
(182, '', 1, 80, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '6000', '', 'A032-', 0, 0),
(183, '', 0, 81, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '9000', '', 'A033-', 0, 0),
(184, '', 1, 82, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Than\"}', '250000', '', 'A034-', 0, 0),
(185, '', 1, 83, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '11000', '', 'A035-', 0, 0),
(186, '', 1, 84, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '7000', '', 'A036-', 0, 0),
(187, '', 1, 85, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '5500', '', 'A037-', 0, 0),
(188, '', 1, 86, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '11500', '', 'A038-', 0, 0),
(189, '', 1, 87, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '7000', '', 'A039-', 0, 0),
(190, '', 1, 88, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '4000', '', 'A040-', 0, 0),
(191, '', 1, 89, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y note\"}', '4000', '', 'A041-', 0, 0),
(192, '', 1, 90, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '5000', '', 'A042-', 0, 0),
(193, '', 1, 91, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '21000', '', 'A043-', 0, 0),
(194, '', 1, 92, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y note\"}', '18000', '', 'A044-', 0, 0),
(195, '', 1, 93, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '18000', '', 'A045-', 0, 0),
(196, '', 0, 94, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '9000', '', 'A046-', 0, 0),
(197, '', 1, 95, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '7500', '', 'A047-', 0, 0),
(198, '', 1, 96, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '6000', '', 'A048-', 0, 0),
(199, '', 1, 97, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '5000', '', 'A049-', 0, 0),
(200, '', 0, 98, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y note\"}', '10000', '', 'A050-', 0, 0),
(201, '', 1, 99, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Note\"}', '17000', '', 'A051-', 0, 0),
(202, '', 1, 100, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Than\"}', '59000', '', 'A052-', 0, 0),
(203, '', 1, 101, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Than\"}', '85000', '', 'A053-', 0, 0),
(204, '', 1, 102, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y Than\"}', '50000', '', 'A055-', 0, 0),
(205, '', 1, 103, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y B\\u00eca\"}', '45000', '', 'A056-', 0, 0),
(206, '', 1, 104, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y B\\u00eca\"}', '132000', '', 'A057-', 0, 0),
(207, '', 1, 105, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Decal\"}', '115000', '', 'A058-', 0, 0),
(208, '', 1, 106, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Decal\"}', '7500', '', 'A059-', 0, 0),
(209, '', 0, 107, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y B\\u00eca\"}', '33000', '', 'A060-', 0, 0),
(210, '', 1, 108, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Decal\"}', '65000', '', 'A061-', 0, 0),
(211, '', 0, 109, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Decal\"}', '85000', '', 'A062-', 0, 0),
(212, '', 1, 110, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Decal\"}', '62000', '', 'A063-', 0, 0),
(213, '', 1, 111, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Decal\"}', '72000', '', 'A065-', 0, 0),
(214, '', 1, 112, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Gi\\u1ea5y\"}', '64000', '', 'A064-', 0, 0),
(215, '', 1, 113, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00eca - Decal\"}', '33000', '', 'A054-', 0, 0),
(216, '', 1, 114, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat Bi\"}', '2500', '', 'TL027-Xanh-', 0, 0),
(217, '', 1, 115, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat Bi\"}', '2500', '', 'B001-TL007-', 0, 0),
(218, '', 1, 116, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"But\"}', '8000', '', 'B002-', 0, 0),
(219, '', 1, 117, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '12500', '', 'B003-', 0, 0),
(220, '', 1, 118, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '2300', '', 'B004-TL08', 0, 0),
(221, '', 0, 119, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat Bi\"}', '2500', '', 'B004-', 0, 0),
(222, '', 1, 120, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B005-', 0, 0),
(223, '', 0, 121, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '7000', '', 'B006-', 0, 0),
(224, '', 1, 122, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '7000', '', 'B007-', 0, 0),
(225, '', 1, 123, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '7000', '', 'B008-', 0, 0),
(226, '', 1, 124, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '1800', '', 'B009-', 0, 0),
(227, '', 1, 125, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '1800', '', 'B010-', 0, 0),
(228, '', 1, 126, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '1800', '', 'B011-', 0, 0),
(229, '', 1, 127, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B012-', 0, 0),
(230, '', 1, 128, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat \"}', '3000', '', '012-', 0, 0),
(231, '', 1, 129, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B013-', 0, 0),
(232, '', 1, 130, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B014-', 0, 0),
(233, '', 1, 131, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"But\"}', '3000', '', 'B015-', 0, 0),
(234, '', 1, 132, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B030', 0, 0),
(235, '', 1, 133, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B017-', 0, 0),
(236, '', 1, 134, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '2500', '', 'B018-', 0, 0),
(237, '', 1, 135, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '2500', '', 'B019-', 0, 0),
(238, '', 0, 136, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '3000', '', 'B020-', 0, 0),
(239, '', 0, 137, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '3000', '', 'B021-', 0, 0),
(240, '', 1, 138, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '2300', '', 'B022-', 0, 0),
(241, '', 1, 139, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '2300', '', 'B023-', 0, 0),
(242, '', 1, 141, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '21000', '', 'B025-', 0, 0),
(243, '', 1, 142, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '5500', '', 'B026-', 0, 0),
(244, '', 0, 143, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"b\\u00fat\"}', '34000', '', 'B027-', 0, 0),
(245, '', 0, 144, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '7800', '', 'B028-', 0, 0),
(246, '', 1, 145, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"But\"}', '5000', '', 'B029-', 0, 0),
(247, '', 1, 146, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '5000', '', 'B030-', 0, 0),
(248, '', 1, 147, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"B\\u00fat\"}', '10000', '', 'B031-', 0, 0),
(249, '', 0, 148, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '234234', '', '24234234-', 0, 0),
(250, '', 0, 149, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '1.11111e+13', '', '111111111111-', 0, 0),
(251, '', 0, 150, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '123131', '', '14242-', 0, 0),
(252, '', 0, 152, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '121313', '', '231-', 0, 0),
(253, '', 0, 211, '{\"Ch\\u1ea5t li\\u1ec7u\":\"Default Title\"}', '120000', '', 'N121313-1213', 0, 0),
(254, '', 0, 212, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '0', '', '34534543', 0, 0),
(255, '', 0, 211, '{\"Ch\\u1ea5t li\\u1ec7u\":\"qeqe\"}', '900000', '', 'm75444', 0, 0),
(256, '', 0, 213, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', 'Call', '', 'KHO03911-', 0, 0),
(257, '', 0, 213, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Td2\"}', '1029293', '', 'MK9494984', 0, 0),
(258, '', 0, 214, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', 'Thươnglượng', '', 'MA028843-', 0, 0),
(259, '', 0, 215, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', 'Thuonglượng', '', 'MA12121-', 0, 0),
(260, '', 0, 216, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', 'Call', '', 'MASS-', 0, 0),
(261, '', 0, 217, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '200000', '', '54455-1', 0, 0),
(262, '', 0, 218, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '1000000', '', '123-1', 0, 0),
(263, '', 0, 219, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '400000', '', 'b123-1', 0, 0),
(264, '123', 500, 220, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Size 35\"}', '140000', '', '1234-1', 0, 0),
(265, '', 0, 221, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '2000000', '', '432341234-1', 0, 0),
(266, '', 0, 222, '{\"Tiêu đề\":\"Default Title\"}', '200000', '', 'tỷty-1', 100, 0),
(267, '', 0, 1, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '410000', '', 'BC01-1', 0, 0),
(268, '', 0, 2, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"M\\u00e0u S\\u1eafc\"}', '400000', '', 'bc1-1', 0, 0),
(269, '', 500, 2, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Size\"}', '400000', '', 'bc1', 0, 0),
(270, '', 0, 3, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '100000', '', '324234-1', 0, 0),
(271, '', 0, 4, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'BC03-1', 0, 0),
(272, '', 0, 5, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '234234', '', '234234-1', 0, 0),
(273, '', 0, 6, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '456456', '', 'rtyrty-1', 0, 0),
(274, '', 0, 7, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test-1', 0, 0),
(275, '', 0, 8, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test1-1', 0, 0),
(276, '', 0, 9, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test2-1', 0, 0),
(277, '', 0, 10, '{\"Tiêu đề\":\"Default Title\"}', '390000', '', 'test3-1', 0, 0),
(278, '', 0, 11, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test4-1', 0, 0),
(279, '', 0, 12, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test5-1', 0, 0),
(280, '', 0, 13, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test6-1', 0, 0),
(281, '', 0, 14, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test68-1', 0, 0),
(282, '', 0, 15, '{\"Tiêu đề\":\"Default Title\"}', '390000', '', 'test67-1', 0, 0),
(283, '', 0, 16, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'test69-1', 0, 0),
(284, '', 0, 17, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', 'BC 6123-SDD-1', 0, 0),
(285, '', 0, 18, '{\"Ti\\u00eau \\u0111\\u1ec1\":\"Default Title\"}', '390000', '', '6122-SDD-1', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_color`
--

CREATE TABLE `product_color` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_color`
--

INSERT INTO `product_color` (`id`, `product_id`, `color_id`) VALUES
(1, 3, 9),
(2, 3, 10),
(3, 4, 11),
(4, 4, 12),
(5, 5, 12),
(6, 6, 12),
(7, 18, 1),
(8, 19, 4),
(9, 20, 1),
(10, 21, 1),
(11, 21, 2),
(12, 21, 3),
(13, 21, 4),
(14, 21, 5),
(15, 21, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_photo`
--

CREATE TABLE `product_photo` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `alt` varchar(128) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_photo`
--

INSERT INTO `product_photo` (`id`, `product_id`, `color_id`, `name`, `status`, `alt`, `order`) VALUES
(28, 5, 12, '2015-11-01-18-23-15-kem__2_.jpg', 0, '', 0),
(29, 5, 12, '2015-11-01-18-23-15-kem__3_.jpg', 0, '', 0),
(30, 5, 12, '2015-11-01-18-23-15-kem__4_.jpg', 0, '', 0),
(31, 5, 12, '2015-11-01-18-23-15-kem__5_.jpg', 0, '', 0),
(32, 5, 12, '2015-11-01-18-23-15-kem__6_.jpg', 0, '', 0),
(40, 18, 1, '2015-11-16-09-33-41-Chrysanthemum.jpg', 0, '', 0),
(41, 18, 1, '2015-11-16-09-33-41-Desert.jpg', 0, '', 0),
(47, 19, 4, '2015-11-25-11-57-59-Chrysanthemum.jpg', 0, '', 0),
(48, 19, 4, '2015-11-25-11-58-38-Lighthouse.jpg', 0, '', 0),
(49, 20, 1, '2015-11-26-14-53-18-Koala.jpg', 0, '', 0),
(50, 21, 1, '2015-11-26-14-54-01-Koala.jpg', 0, '', 0),
(51, 21, 1, '2015-11-26-15-00-39-Penguins.jpg', 0, '', 0),
(52, 27, 0, 'Tulips.jpg', 0, '', 0),
(53, 27, 0, 'Penguins.jpg', 0, '', 0),
(54, 28, 0, 'Hydrangeas.jpg', 0, '', 0),
(55, 28, 0, 'Desert (1).jpg', 0, '', 0),
(56, 28, 0, 'Chrysanthemum (1).jpg', 0, '', 0),
(57, 28, 0, 'Penguins.jpg', 0, '', 0),
(58, 28, 0, 'Koala.jpg', 0, '', 0),
(59, 29, 0, 'news.jpg', 0, '', 1),
(60, 29, 0, 'news-1.jpg', 0, '', 2),
(61, 29, 0, 'news-2.jpg', 0, '', 3),
(62, 29, 0, 'news-0.jpg', 0, '', 4),
(63, 29, 0, 'news-3.jpg', 0, '', 5),
(64, 30, 0, 'news.jpg', 1, '', 1),
(65, 30, 0, 'news-1.jpg', 0, '', 2),
(66, 31, 0, 'image1x (1).jpg', 0, '', 1),
(67, 31, 0, 'news-3 (1).jpg', 0, '', 2),
(68, 32, 0, 'image1x (1).jpg', 0, '', 1),
(69, 32, 0, 'news.jpg', 0, '', 2),
(70, 33, 0, 'image1x (2).jpg', 0, '', 1),
(71, 33, 0, 'news (1).jpg', 0, '', 2),
(72, 33, 0, 'bgColor-1.jpg', 0, '', 3),
(73, 33, 0, 'bgColor-2.jpg', 0, '', 4),
(74, 33, 0, 'bgColor-4.jpg', 0, '', 5),
(75, 33, 0, 'bgColor-3.jpg', 0, '', 6),
(76, 34, 0, 'image1x (3).jpg', 1, '', 1),
(77, 34, 0, 'news (2).jpg', 0, '', 2),
(78, 34, 0, 'news-0.jpg', 0, '', 3),
(79, 34, 0, 'news-1.jpg', 0, '', 4),
(80, 35, 0, 'news-0 (1).jpg', 1, '', 1),
(81, 35, 0, 'news-1 (1).jpg', 0, '', 2),
(82, 35, 0, 'image1x (4).jpg', 0, '', 3),
(83, 34, 0, 'bgColor-3 (1).jpg', 0, '', 5),
(84, 36, 0, 'bgColor-5.jpg', 0, '', 1),
(85, 37, 0, 'news-3 (1).jpg', 0, '', 1),
(86, 37, 0, 'news-0.jpg', 0, '', 2),
(87, 37, 0, 'image1x (1).jpg', 0, '', 3),
(88, 38, 0, 'image1x (1).jpg', 0, '', 1),
(92, 39, 0, 'image2x.jpg', 0, '', 1),
(96, 40, 0, 'xam3.jpg', 1, '', 1),
(97, 40, 0, 'news-1 (2).jpg', 0, '', 2),
(98, 40, 0, 'news-2.jpg', 0, '', 3),
(99, 39, 0, 'den__1.jpg', 0, '', 2),
(100, 39, 0, 'den__2.jpg', 0, '', 3),
(101, 39, 0, 'den__3.jpg', 0, '', 4),
(103, 38, 0, 'do__3.jpg', 0, '', 3),
(104, 38, 0, 'do__2.jpg', 0, '', 4),
(105, 38, 0, 'do__1.jpg', 0, '', 5),
(106, 41, 0, '211074-he-thong-wrap-roll-khuyen-mai-cuc-soc-50.jpg', 0, '', 1),
(107, 41, 0, 'Tien_Mat.png', 0, '', 2),
(108, 42, 0, '211074-he-thong-wrap-roll-khuyen-mai-cuc-soc-50 (1).jpg', 1, '', 1),
(109, 42, 0, '1448891534147_22793.jpg', 0, '', 2),
(110, 43, 0, '1448891534147_22793 (1).jpg', 0, '', 1),
(111, 43, 0, 'Tien_Mat (1).png', 0, '', 2),
(112, 44, 0, '44043giaycuonả-150x113.jpg', 0, '', 1),
(113, 45, 0, '44043giaycuonả-150x113.jpg', 0, '', 1),
(114, 46, 0, '44043giaycuonả-150x113.jpg', 0, '', 1),
(115, 46, 0, '44043giaycuonả.jpg', 0, '', 2),
(116, 47, 0, '65424Double A-Pic03-150x113.jpeg', 0, '', 1),
(117, 47, 0, '16926Double A-Pic05-600x450.jpg', 0, '', 2),
(118, 47, 0, '65424Double A-Pic03.jpeg', 0, '', 3),
(119, 47, 0, '58665Double A-Pic04-600x450.jpeg', 0, '', 4),
(120, 48, 0, '103641703_giay-in-Idea-A4-70-gsm-300-150x113.jpg', 0, '', 1),
(121, 48, 0, '103641703_giay-in-Idea-A4-70-gsm-300.jpg', 0, '', 2),
(122, 49, 0, '29515kug1376555480-150x113.jpg', 1, '', 1),
(123, 49, 0, '29515kug1376555480.jpg', 0, '', 2),
(124, 50, 0, 'giấy A4 Excel 70gsm.jpg', 1, '', 1),
(125, 50, 0, '42549uan1395884122.jpg', 0, '', 2),
(128, 52, 0, '103641703_giay-in-Idea-A4-70-gsm-300-150x113 (1).jpg', 1, '', 1),
(129, 52, 0, '103641703_giay-in-Idea-A4-70-gsm-300 (1).jpg', 0, '', 2),
(130, 53, 0, 'giay a3 excel 80.jpg', 1, '', 1),
(131, 53, 0, '13057giay-excel-80-a4-500x500-600x450.jpg', 0, '', 2),
(132, 53, 0, 'giay a3 excel 80 (1).jpg', 0, '', 3),
(133, 53, 0, '42267ugl1349620941-600x450.jpg', 0, '', 4),
(134, 54, 0, 'giay a3 excel 80 (2).jpg', 1, '', 1),
(135, 54, 0, '42267ugl1349620941-600x450 (1).jpg', 0, '', 2),
(136, 54, 0, '13057giay-excel-80-a4-500x500-600x450 (1).jpg', 0, '', 3),
(140, 56, 0, '65424Double A-Pic03-150x113 (1).jpeg', 0, '', 1),
(141, 56, 0, '16926Double A-Pic05-600x450 (1).jpg', 0, '', 2),
(142, 56, 0, '65424Double A-Pic03 (1).jpeg', 0, '', 3),
(143, 56, 0, '58665Double A-Pic04-600x450 (1).jpeg', 0, '', 4),
(146, 57, 0, '54142giay a0 roki.jpg', 0, '', 1),
(147, 57, 0, '54142giay a0 roki-150x113.jpg', 0, '', 2),
(153, 58, 0, '13057giay-excel-80-a4-500x500-150x113.jpg', 0, '', 1),
(154, 58, 0, '13057giay-excel-80-a4-500x500-600x450 (3).jpg', 0, '', 2),
(155, 58, 0, '42267ugl1349620941-600x450 (3).jpg', 0, '', 3),
(159, 59, 0, '103641703_giay-in-Idea-A4-70-gsm-300-150x113 (2).jpg', 0, '', 1),
(160, 59, 0, '103641703_giay-in-Idea-A4-70-gsm-300 (2).jpg', 0, '', 2),
(163, 60, 0, '13057giay-excel-80-a4-500x500-150x113 (1).jpg', 0, '', 1),
(164, 60, 0, '13057giay-excel-80-a4-500x500.jpg', 0, '', 2),
(171, 62, 0, '53268po80.jpg', 0, '', 1),
(172, 62, 0, '53268po80-150x113.jpg', 0, '', 2),
(175, 63, 0, '53268po80-150x113.jpg', 0, '', 1),
(176, 63, 0, '53268po80.jpg', 0, '', 2),
(181, 64, 0, 'plus 80 a4 1.jpg', 0, '', 1),
(182, 64, 0, 'plus 80 a4.jpg', 0, '', 2),
(183, 65, 0, '65424Double A-Pic03-150x113 (2).jpeg', 0, '', 1),
(184, 65, 0, '16926Double A-Pic05-600x450 (2).jpg', 0, '', 2),
(185, 65, 0, '58665Double A-Pic04-600x450 (2).jpeg', 0, '', 3),
(186, 65, 0, '65424Double A-Pic03 (2).jpeg', 0, '', 4),
(187, 66, 0, '87774giay-in-lien-luc-lien-son-150x113 (1).jpg', 0, '', 1),
(188, 66, 0, '87774giay-in-lien-luc-lien-son.jpg', 0, '', 2),
(189, 67, 0, '87774giay-in-lien-luc-lien-son-150x113 (1).jpg', 1, '', 1),
(190, 67, 0, '87774giay-in-lien-luc-lien-son.jpg', 0, '', 2),
(194, 68, 0, '87774giay-in-lien-luc-lien-son-150x113.jpg', 0, '', 1),
(195, 68, 0, '29216giayinanh01.jpg', 0, '', 2),
(196, 69, 0, '87774giay-in-lien-luc-lien-son-150x113 (2).jpg', 0, '', 1),
(197, 69, 0, '29216giayinanh01 (1).jpg', 0, '', 2),
(200, 71, 0, '15198796_Giay-fax-nhiet-Kosaka-210-216---trung-50m-150x113.jpg', 0, '', 1),
(201, 71, 0, '15198796_Giay-fax-nhiet-Kosaka-210-216---trung-50m.jpg', 0, '', 2),
(202, 72, 0, '35972pyy1367896031-150x113.jpg', 0, '', 1),
(203, 72, 0, '35972pyy1367896031.jpg', 0, '', 2),
(204, 73, 0, '35972pyy1367896031-150x113 (1).jpg', 0, '', 1),
(205, 73, 0, '35972pyy1367896031 (1).jpg', 0, '', 2),
(206, 74, 0, '87774giay-in-lien-luc-lien-son-150x113 (3).jpg', 1, '', 1),
(207, 74, 0, '87774giay-in-lien-luc-lien-son (1).jpg', 0, '', 2),
(208, 75, 0, '98734380-279 special-150x113.jpg', 0, '', 1),
(209, 75, 0, '98734380-279 special.jpg', 0, '', 2),
(210, 76, 0, '98734380-279 special-150x113 (1).jpg', 1, '', 1),
(211, 76, 0, '98734380-279 special (1).jpg', 0, '', 2),
(212, 77, 0, '87774giay-in-lien-luc-lien-son-150x113 (4).jpg', 1, '', 1),
(213, 77, 0, '87774giay-in-lien-luc-lien-son (2).jpg', 0, '', 2),
(214, 78, 0, '1827839_giay-pelure-150x113.jpg', 1, '', 1),
(215, 78, 0, '1827839_giay-pelure.jpg', 0, '', 2),
(216, 79, 0, '58464giay-note-3x3-n-u-303-150x113.gif', 1, '', 1),
(217, 79, 0, '58464giay-note-3x3-n-u-303.gif', 0, '', 2),
(218, 80, 0, '58464giay-note-3x3-n-u-303-150x113 (1).gif', 1, '', 1),
(219, 80, 0, '58464giay-note-3x3-n-u-303 (1).gif', 0, '', 2),
(220, 81, 0, '889291396921720_555-150x113.jpg', 0, '', 1),
(221, 81, 0, '889291396921720_555.jpg', 0, '', 2),
(222, 82, 0, '80100_L6A0587-150x113.JPG', 1, '', 1),
(223, 82, 0, '15465_L6A0594-120x90.JPG', 0, '', 2),
(224, 82, 0, '28700_L6A0592-120x90.JPG', 0, '', 3),
(225, 82, 0, '31947_L6A0590-120x90.JPG', 0, '', 4),
(226, 82, 0, '78136_L6A0593-120x90.JPG', 0, '', 5),
(227, 82, 0, '73625_L6A0588-120x90.JPG', 0, '', 6),
(228, 82, 0, '80100_L6A0587.JPG', 0, '', 7),
(229, 83, 0, '627241409386564_3_3-150x113.JPG', 1, '', 1),
(230, 83, 0, '627241409386564_3_3.JPG', 0, '', 2),
(231, 84, 0, '627241409386564_3_3-150x113 (1).JPG', 1, '', 1),
(232, 84, 0, '627241409386564_3_3 (1).JPG', 0, '', 2),
(233, 85, 0, '627241409386564_3_3-150x113 (2).JPG', 1, '', 1),
(234, 85, 0, '627241409386564_3_3 (2).JPG', 0, '', 2),
(237, 86, 0, '19287Post it 91 x 76-150x113.jpg', 0, '', 1),
(238, 86, 0, '19287Post it 91 x 76.jpg', 0, '', 2),
(239, 87, 0, '19287Post it 91 x 76-150x113 (1).jpg', 1, '', 1),
(240, 87, 0, '19287Post it 91 x 76 (1).jpg', 0, '', 2),
(241, 88, 0, '6744imh1275728903-150x113.jpg', 0, '', 1),
(242, 88, 0, '6744imh1275728903.jpg', 0, '', 2),
(243, 89, 0, '58464giay-note-3x3-n-u-303-150x113 (2).gif', 0, '', 1),
(244, 89, 0, '58464giay-note-3x3-n-u-303 (2).gif', 0, '', 2),
(245, 90, 0, '58464giay-note-3x3-n-u-303-150x113 (3).gif', 1, '', 1),
(246, 90, 0, '58464giay-note-3x3-n-u-303 (3).gif', 0, '', 2),
(249, 91, 0, '889291396921720_555-150x113 (1).jpg', 0, '', 1),
(250, 91, 0, '889291396921720_555 (1).jpg', 0, '', 2),
(253, 93, 0, '86809Giấy note 4 màu Post-it 20mm x 76mm - (5 xấpgói) 3M 671-5 3M-800x533-150x113.jpg', 0, '', 1),
(254, 93, 0, '86809Giấy note 4 màu Post-it 20mm x 76mm - (5 xấpgói) 3M 671-5 3M-800x533.jpg', 0, '', 2),
(255, 94, 0, '92696giaynotePronoti3x4-150x113.JPG', 0, '', 1),
(256, 94, 0, '92696giaynotePronoti3x4.JPG', 0, '', 2),
(257, 95, 0, '92696giaynotePronoti3x4-150x113 (1).JPG', 0, '', 1),
(258, 95, 0, '92696giaynotePronoti3x4 (1).JPG', 0, '', 2),
(259, 96, 0, '92696giaynotePronoti3x4-150x113 (2).JPG', 0, '', 1),
(260, 96, 0, '92696giaynotePronoti3x4 (2).JPG', 0, '', 2),
(261, 97, 0, '92696giaynotePronoti3x4-150x113 (3).JPG', 0, '', 1),
(262, 97, 0, '92696giaynotePronoti3x4 (3).JPG', 0, '', 2),
(263, 98, 0, '9932Pronoti-150x113.jpg', 1, '', 1),
(264, 98, 0, '9932Pronoti.jpg', 0, '', 2),
(265, 99, 0, '96883image.aspx-150x113.jpg', 0, '', 1),
(266, 99, 0, '96883image.aspx.jpg', 0, '', 2),
(267, 100, 0, '62621giay_than_kokusai-500x500-150x113.jpg', 0, '', 1),
(268, 100, 0, '62621giay_than_kokusai-500x500.jpg', 0, '', 2),
(269, 101, 0, '58481giấy than Horse-150x113.jpg', 0, '', 1),
(270, 101, 0, '58481giấy than Horse.jpg', 0, '', 2),
(275, 103, 0, '53662sunf-150x113.jpg', 1, '', 1),
(276, 103, 0, '53662sunf.jpg', 0, '', 2),
(277, 104, 0, '2925bkieng-150x113.jpg', 0, '', 1),
(278, 104, 0, '2925bkieng.jpg', 0, '', 2),
(279, 105, 0, '6605645-decal-tomy-125-149-150x113.jpg', 0, '', 1),
(280, 105, 0, '6605645-decal-tomy-125-149.jpg', 0, '', 2),
(283, 106, 0, '47787tomy-150x113.jpg', 0, '', 1),
(284, 106, 0, '47787tomy.jpg', 0, '', 2),
(285, 107, 0, '29449giay bia thai-150x113.jpeg', 0, '', 1),
(286, 107, 0, '29449giay bia thai.jpeg', 0, '', 2),
(287, 108, 0, '37376_L6A0581-150x113.JPG', 0, '', 1),
(288, 108, 0, '37376_L6A0581.JPG', 0, '', 2),
(289, 109, 0, '1506_L6A0578-150x113.JPG', 0, '', 1),
(290, 109, 0, '1506_L6A0578.JPG', 0, '', 2),
(291, 110, 0, '2925bkieng-150x113 (1).jpg', 0, '', 1),
(292, 110, 0, '2925bkieng (1).jpg', 0, '', 2),
(293, 111, 0, '71783_L6A0584-150x113.JPG', 1, '', 1),
(294, 111, 0, '71783_L6A0584.JPG', 0, '', 2),
(295, 112, 0, '41027euv1374473281-150x113.jpg', 1, '', 1),
(296, 112, 0, '41027euv1374473281.jpg', 0, '', 2),
(297, 113, 0, '29449giay bia thai-150x113 (1).jpeg', 0, '', 1),
(298, 113, 0, '29449giay bia thai (1).jpeg', 0, '', 2),
(304, 114, 0, '65460Tl027xanh-150x113.jpg', 0, '', 1),
(305, 114, 0, '65460Tl027xanh.jpg', 0, '', 2),
(308, 116, 0, '6017jeli-150x113.jpg', 1, '', 1),
(309, 116, 0, '6017jeli.jpg', 0, '', 2),
(310, 115, 0, '65460Tl027xanh-150x113.jpg', 0, '', 1),
(311, 115, 0, '65460Tl027xanh.jpg', 0, '', 2),
(312, 117, 0, '91256dan-150x113.jpg', 1, '', 1),
(313, 117, 0, '91256dan.jpg', 0, '', 2),
(316, 118, 0, '4223TL08 xanh-150x113.jpg', 0, '', 1),
(317, 118, 0, '4223TL08 xanh.jpg', 0, '', 2),
(318, 119, 0, '822611364548205_VIET IT 777-150x113.jpg', 0, '', 1),
(319, 119, 0, '822611364548205_VIET IT 777.jpg', 0, '', 2),
(320, 120, 0, '97451TL025-150x113.jpg', 0, '', 1),
(323, 121, 0, '24785images-150x113.jpg', 0, '', 1),
(324, 121, 0, 'agq1290561478.jpg', 0, '', 2),
(325, 120, 0, 'yxd1291434182.jpg', 0, '', 2),
(326, 122, 0, '11379but-bi-thien-long-tl036-150x113.jpg', 0, '', 1),
(328, 122, 0, 'small_1835_in-but-bi-TL-036.jpg', 0, '', 2),
(329, 123, 0, '11379but-bi-thien-long-tl036-150x113 (2).jpg', 1, '', 1),
(330, 123, 0, 'small_1835_in-but-bi-TL-036 (2).jpg', 0, '', 2),
(331, 124, 0, '54542B__T_BI_THI__N_L_4f6d8b723482e.jpg', 0, '', 2),
(332, 124, 0, '54542B__T_BI_THI__N_L_4f6d8b723482e-150x113.jpg', 0, '', 1),
(333, 125, 0, '54542B__T_BI_THI__N_L_4f6d8b723482e-150x113 (1).jpg', 0, '', 1),
(334, 125, 0, '54542B__T_BI_THI__N_L_4f6d8b723482e (1).jpg', 0, '', 2),
(335, 126, 0, '54542B__T_BI_THI__N_L_4f6d8b723482e (2).jpg', 0, '', 2),
(336, 126, 0, '54542B__T_BI_THI__N_L_4f6d8b723482e-150x113 (2).jpg', 0, '', 1),
(337, 127, 0, '63974but-kim-aihao-chu-a-150x113.jpg', 0, '', 1),
(338, 127, 0, '63974but-kim-aihao-chu-a.jpg', 0, '', 2),
(339, 128, 0, '63974but-kim-aihao-chu-a-150x113 (1).jpg', 1, '', 1),
(340, 128, 0, '63974but-kim-aihao-chu-a (1).jpg', 0, '', 2),
(341, 129, 0, '63974but-kim-aihao-chu-a-150x113 (2).jpg', 0, '', 1),
(342, 129, 0, '63974but-kim-aihao-chu-a (2).jpg', 0, '', 2),
(343, 130, 0, '1221Tl032do-150x113.jpg', 0, '', 1),
(344, 130, 0, '1221Tl032do.jpg', 0, '', 2),
(345, 131, 0, '1221Tl032do (1).jpg', 0, '', 2),
(346, 131, 0, '1221Tl032do-150x113 (1).jpg', 0, '', 1),
(347, 132, 0, '99528Tl032den.jpg', 0, '', 2),
(348, 132, 0, '99528Tl032den-150x113.jpg', 0, '', 1),
(349, 133, 0, '67330TL032xanh.jpg', 0, '', 2),
(350, 133, 0, '67330TL032xanh-150x113.jpg', 0, '', 1),
(351, 134, 0, '22071tl027do.jpg', 0, '', 2),
(352, 134, 0, '22071tl027do-150x113.jpg', 0, '', 1),
(353, 135, 0, '63526TL027den.jpg', 0, '', 2),
(354, 135, 0, '63526TL027den-150x113.jpg', 0, '', 1),
(355, 136, 0, '22071tl027do (1).jpg', 0, '', 2),
(356, 136, 0, '22071tl027do-150x113 (1).jpg', 0, '', 1),
(357, 137, 0, '15991Tl025den-150x113.jpg', 0, '', 1),
(358, 137, 0, '15991Tl025den.jpg', 0, '', 2),
(359, 138, 0, '51546tl08den-150x113.jpg', 0, '', 1),
(363, 139, 0, 'tải xuống (1).jpg', 0, '', 2),
(364, 139, 0, 'tải xuống (2).jpg', 0, '', 3),
(365, 139, 0, 'tải xuống.jpg', 0, '', 4),
(366, 139, 0, '14593Tl025do-150x113.jpg', 0, '', 1),
(367, 138, 0, 'tải xuống (3).jpg', 0, '', 2),
(368, 138, 0, 'tải xuống (4).jpg', 0, '', 3),
(369, 138, 0, 'tải xuống (5).jpg', 0, '', 4),
(370, 140, 0, '1.jpg', 0, '', 1),
(371, 140, 0, '2.jpg', 0, '', 2),
(372, 141, 0, '1 (1).jpg', 0, '', 1),
(373, 141, 0, '2 (1).jpg', 0, '', 2),
(374, 142, 0, '1 (2).jpg', 0, '', 1),
(375, 142, 0, '2 (2).jpg', 0, '', 2),
(376, 143, 0, '1 (3).jpg', 0, '', 1),
(377, 143, 0, '94849999999999.jpg', 0, '', 2),
(380, 144, 0, '67367But-long-dau-ghi-CD-PM04-171-150x113.jpg', 0, '', 1),
(381, 144, 0, '67367But-long-dau-ghi-CD-PM04-171.jpg', 0, '', 2),
(382, 145, 0, '1 (4).jpg', 0, '3454534', 1),
(383, 145, 0, '2 (3).jpg', 0, '3454534', 2),
(386, 147, 0, '53972988-bl02-150x113.jpg', 0, '', 1),
(387, 147, 0, '2 (5).jpg', 0, '', 2),
(390, 145, 0, 'Slide_Goldship.jpg', 0, '', 3),
(392, 146, 0, 'lacoste-live-pour-homme-100-ml-eau-de-toilette_150x1501200.jpg', 0, '', 1),
(393, 146, 0, 'nuoc-hoa-chanel-coco-noir-100ml_150x1503500.jpg', 0, '', 1),
(394, 146, 0, 'chanel-allure-eau-de-toilette_150x1502600 (1).jpg', 0, '', 1),
(395, 146, 0, 'tom-ford-noir-for-men_150x1502700.jpg', 0, '', 1),
(396, 146, 0, 'dolce-gabbana-the-one-for-men_150x1501500.jpg', 0, '', 1),
(397, 146, 0, 'chanel-coco-mademoiselle_150x1503500.jpg', 0, '', 1),
(398, 102, 0, 'chanel-coco-mademoiselle_150x1503500 (1).jpg', 0, '', 1),
(399, 102, 0, 'nuoc-hoa-chanel-coco-noir-100ml_150x1503500 (1).jpg', 0, '', 1),
(400, 102, 0, 'tom-ford-noir-for-men_150x1502700 (1).jpg', 0, '', 1),
(401, 102, 0, 'lacoste-live-pour-homme-100-ml-eau-de-toilette_150x1501200 (1).jpg', 0, '', 1),
(402, 102, 0, 'dolce-gabbana-the-one-for-men_150x1501500 (1).jpg', 0, '', 1),
(403, 102, 0, 'chanel-allure-eau-de-toilette_150x1502600 (2).jpg', 0, '', 1),
(404, 92, 0, 'chanel-allure-eau-de-toilette_150x1502600 (3).jpg', 0, '', 3),
(405, 92, 0, 'dolce-gabbana-the-one-for-men_150x1501500 (2).jpg', 0, '', 3),
(406, 92, 0, 'chanel-coco-mademoiselle_150x1503500 (2).jpg', 0, '', 3),
(407, 92, 0, 'nuoc-hoa-chanel-coco-noir-100ml_150x1503500 (2).jpg', 0, '', 4),
(408, 92, 0, 'tom-ford-noir-for-men_150x1502700 (2).jpg', 0, '', 4),
(409, 92, 0, 'lacoste-live-pour-homme-100-ml-eau-de-toilette_150x1501200 (2).jpg', 0, '', 4),
(410, 70, 0, 'chanel-allure-eau-de-toilette_150x1502600 (4).jpg', 0, '', 1),
(411, 70, 0, 'dolce-gabbana-the-one-for-men_150x1501500 (3).jpg', 0, '', 1),
(412, 70, 0, 'chanel-coco-mademoiselle_150x1503500 (3).jpg', 0, '', 1),
(413, 70, 0, 'nuoc-hoa-chanel-coco-noir-100ml_150x1503500 (3).jpg', 0, '', 1),
(414, 70, 0, 'lacoste-live-pour-homme-100-ml-eau-de-toilette_150x1501200 (3).jpg', 0, '', 1),
(415, 70, 0, 'tom-ford-noir-for-men_150x1502700 (3).jpg', 0, '', 1),
(418, 148, 0, 'dolce-gabbana-the-one-for-men_150x1501500 (4).jpg', 0, '', 2),
(419, 148, 0, 'lacoste-live-pour-homme-100-ml-eau-de-toilette_150x1501200 (4).jpg', 0, '', 3),
(420, 148, 0, 'nuoc-hoa-chanel-coco-noir-100ml_150x1503500 (4).jpg', 0, '', 3),
(421, 148, 0, 'tom-ford-noir-for-men_150x1502700 (4).jpg', 0, '', 3),
(422, 148, 0, 'beauty-marine-collagen-10000_150x150 (1)470.jpg', 0, '', 4),
(423, 148, 0, 'collagen-de-happy_150x150 (1)1500.jpg', 0, '', 4),
(424, 148, 0, 'collagen-aishodo-150-000gm_150x150 (1)800.jpg', 0, '', 4),
(425, 148, 0, 'collagenaid-110g_150x150 (1)750.jpg', 0, '', 4),
(426, 148, 0, 'collagen-dhc-vien_150x150 (1)370.jpg', 0, '', 4),
(427, 148, 0, 'collagen-dhc-beauty-7000_150x150 (2)630.png', 0, '', 4),
(428, 148, 0, 'collagen-enriched-shiseido-dang-vien-_150x150 (1)1600.jpg', 0, '', 5),
(429, 148, 0, 'collagen-hanamai-pig_150x150 (1)280.jpg', 0, '', 5),
(430, 148, 0, 'collagen-hanamai-tea_150x150 (1)280.jpg', 0, '', 5),
(431, 148, 0, 'collagen-meiji-vien_150x150 (1)470.jpg', 0, '', 5),
(432, 148, 0, 'collagen-shiseido-dang-vien_150x150 (1)470.jpg', 0, '', 5),
(433, 148, 0, 'collagen-shiseido-ex_150x150 (1)450.jpg', 0, '', 6),
(434, 148, 0, 'collgen-hanamai-fish_150x150 (2)280.jpg', 0, '', 6),
(435, 148, 0, 'collagen-sun-vi-ca-map_150x150 (1)680.jpg', 0, '', 6),
(436, 148, 0, 'meiji-amino-collagen_150x150 (3)530.jpg', 0, '', 6),
(437, 148, 0, 'meiji-amino-collagen_150x150 (3)380.jpg', 0, '', 6),
(439, 148, 0, 'nhau-thai-ngua-nhat-ban_150x150 (1)1250.jpg', 0, '', 7),
(440, 148, 0, 'meiji-collagen-premium_150x150 (1)530.jpg', 0, '', 7),
(441, 148, 0, 'pure-white-dang-vien_150x150 (1)650.jpg', 0, '', 7),
(442, 148, 0, 'nhau-thai-ngua-nhat-ban-dang-vien_150x150 (1)5900.jpg', 0, '', 7),
(443, 148, 0, 'pure-white_150x150 (1)450.jpg', 0, '', 7),
(444, 148, 0, 'shiseido-collagen-enriched_150x150 (1)650.jpg', 0, '', 8),
(445, 148, 0, 'shiseido-collagen-ex-dang-vien_150x150 (1)820.jpg', 0, '', 8),
(446, 148, 0, 'shiseido-q10_150x150 (1)480.jpg', 0, '', 8),
(447, 148, 0, 'shiseido-the-collagen_150x150 (1)500.jpg', 0, '', 8),
(448, 148, 0, 'shiseido-the-collagen-dang-bot_150x150 (1)380.jpg', 0, '', 8),
(449, 148, 0, 'tao-nhat-spirulina-2200-vien_150x150480.jpg', 0, '', 9),
(450, 148, 0, 'thach-collagen-jelly-mau-cam-_150x150 (1)380.png', 0, '', 9),
(451, 148, 0, 'thuoc-uong-no-nguc-bbb-orihiro_150x150 (2)550.jpg', 0, '', 9),
(452, 148, 0, 'tri-nam-transino-san-pham-dac-tri-nam_150x150 (1)1480.jpg', 0, '', 9),
(453, 148, 0, 'transino-white-c-180v_150x150 (2)460.jpg', 0, '', 10),
(454, 148, 0, 'thach-collagen-jelly-mau-hong-_150x150 (1)380.png', 0, '', 10),
(455, 148, 0, 'whitening-collagen_150x150 (1)600.jpg', 0, '', 10),
(456, 148, 0, 'vita-white-plus-ceb2_150x150 (1)290.jpg', 0, '', 10),
(473, 51, 0, 'collagen-meiji-premium-5000m-dang-bot-hop-thiet-240x240.jpg', 0, '', 3),
(520, 141, 0, 'amino-collagen-meiji-240x240 (1).jpg', 0, '', 3),
(521, 149, 0, 'amino-collagen-meiji-240x240 (3).jpg', 0, '', 1),
(522, 150, 0, 'Beauty-1-71550.jpg', 0, '', 1),
(523, 151, 0, 'women_access3.png', 1, '', 1),
(524, 152, 0, 'Calvin-Klein-Man-1-8450.jpg', 1, '', 1),
(525, 152, 0, 'CK-One-Shock-for-Him-1-9450.jpg', 0, '', 2),
(526, 154, 0, 'image-4397021-1-catalog_233.jpg', 0, '', 1),
(527, 155, 0, 'image-4397021-1-catalog_233.jpg', 0, '', 1),
(528, 156, 0, '2.jpg', 1, '', 1),
(529, 157, 0, '3.jpg', 1, '', 1),
(530, 158, 0, '4.jpg', 1, '', 1),
(531, 159, 0, '5.jpg', 1, '', 1),
(532, 160, 0, '6.jpg', 1, '', 1),
(533, 161, 0, '6 (2).jpg', 1, '', 1),
(534, 162, 0, '7.jpg', 1, '', 1),
(535, 163, 0, '8.jpg', 1, '', 1),
(536, 164, 0, '1.jpg', 1, '', 1),
(537, 165, 0, '2 (1).jpg', 1, '', 1),
(538, 166, 0, '185.jpg', 1, '', 1),
(539, 167, 0, '215.jpg', 1, '', 1),
(540, 168, 0, '95.jpg', 1, '', 1),
(541, 169, 0, '1169.jpg', 1, '', 1),
(542, 170, 0, '120.jpg', 1, '', 1),
(543, 171, 0, '160.jpg', 1, '', 1),
(544, 172, 0, '600 (2).jpg', 1, '', 1),
(545, 173, 0, '195.jpg', 1, '', 1),
(546, 174, 0, '190.jpg', 1, '', 1),
(547, 175, 0, '185 (1).jpg', 1, '', 1),
(548, 176, 0, '95 (1).jpg', 1, '', 1),
(549, 177, 0, '951.jpg', 1, '', 1),
(550, 178, 0, '315.jpg', 1, '', 1),
(551, 179, 0, '244.jpg', 1, '', 1),
(552, 180, 0, '263.jpg', 1, '', 1),
(553, 181, 0, '180.jpg', 1, '', 1),
(554, 182, 0, '190 (1).jpg', 1, '', 1),
(555, 183, 0, '135.jpg', 1, '', 1),
(556, 184, 0, '135 (1).jpg', 1, '', 1),
(557, 185, 0, '95 (2).jpg', 1, '', 1),
(558, 186, 0, '79.jpg', 1, '', 1),
(559, 187, 0, '225.jpg', 1, '', 1),
(560, 188, 0, '230.jpg', 1, '', 1),
(561, 189, 0, '99.jpg', 1, '', 1),
(562, 190, 0, '999.jpg', 1, '', 1),
(563, 191, 0, '199.jpg', 1, '', 1),
(564, 192, 0, '150.jpg', 1, '', 1),
(565, 193, 0, '170.jpg', 1, '', 1),
(566, 194, 0, '115.jpg', 1, '', 1),
(567, 195, 0, '250.jpg', 1, '', 1),
(568, 196, 0, '195 (1).jpg', 1, '', 1),
(569, 197, 0, '185 (2).jpg', 0, '', 1),
(570, 198, 0, '195 (2).jpg', 0, '', 1),
(571, 199, 0, '400.jpg', 0, '', 1),
(572, 200, 0, '400 (1).jpg', 1, '', 1),
(573, 201, 0, '350.jpg', 1, '', 1),
(574, 202, 0, '300.jpg', 1, '', 1),
(575, 203, 0, '300 (1).jpg', 1, '', 1),
(576, 204, 0, '350 (1).jpg', 1, '', 1),
(577, 205, 0, '3500.jpg', 1, '', 1),
(578, 206, 0, '750.jpg', 1, '', 1),
(579, 207, 0, '250.png', 1, '', 1),
(580, 208, 0, '380.png', 1, '', 1),
(581, 209, 0, '950.jpg', 1, '', 1),
(582, 210, 0, '400 (2).jpg', 1, '', 1),
(583, 212, 0, 'Slide.jpg', 0, '', 1),
(584, 213, 0, 'women_access3.png', 0, '', 1),
(585, 214, 0, 'women_access3 (1).png', 1, '', 1),
(586, 215, 0, 'women_access3 (2).png', 1, '', 1),
(587, 216, 0, 'women_access3 (3).png', 1, '', 1),
(588, 217, 0, 'bo-hinh-nen-full-hd-cho-may-tinh-cuc-de-thuong-5.jpg', 1, '', 1),
(592, 222, 0, 'migration.png', 0, '', 1),
(593, 222, 0, 'model.png', 0, '', 2),
(594, 222, 0, 'database.png', 0, '', 3),
(595, 222, 0, 'app.png', 0, '', 4),
(596, 222, 0, '1.png', 0, '', 5),
(597, 1, 0, 'bc (1).png', 0, '', 2),
(598, 1, 0, 'bc (5).jpg', 0, '', 3),
(599, 1, 0, 'bc (8).jpg', 0, '', 4),
(600, 1, 0, 'bc (12).jpg', 0, '', 5),
(601, 1, 0, 'bc (1) - Copy.png', 0, '', 5),
(602, 2, 0, 'bc (1) - Copy (1).png', 0, '', 1),
(603, 2, 0, 'bc (13).jpg', 0, '', 2),
(606, 3, 0, '2.jpg', 0, '', 3),
(607, 2, 0, '3.jpg', 0, '', 3),
(608, 4, 0, 'nen.jpg', 0, '', 1),
(609, 4, 0, 'bc03.jpg', 0, '', 2),
(610, 4, 0, 'bc031.jpg', 0, '', 3),
(611, 5, 0, 'bc03 (1).jpg', 0, '', 1),
(614, 6, 0, 'bc03 (3).jpg', 0, '', 1),
(615, 6, 0, 'b1-1481120921.jpg', 0, '', 2),
(616, 7, 0, 'giay-cao-got-dinh-no-ro71-1476438140.jpg', 1, '', 1),
(617, 8, 0, 'giay-bup-be-quy-toc-ro66-1472704809.jpg', 1, '', 1),
(619, 10, 0, 'giay-cao-got-dinh-no-ro71-1476438140 (1).jpg', 0, '', 1),
(620, 9, 0, 'giay-bup-be-phong-cach-ro72-1475205484.jpg', 0, '', 1),
(621, 11, 0, 'giay-cao-got-bit-mui-ro61-1469497308.jpg', 1, '', 1),
(622, 12, 0, 'giay-cao-got-ho-mui-phoi-mau-ro62-1469498131.jpg', 1, '', 1),
(623, 13, 0, 'giay-cao-got-bit-mui-ro61-1469497308 (1).jpg', 0, '', 1),
(624, 14, 0, 'giay-boot-cach-dieu-got-nhon-ro82-1482813445.jpg', 0, '', 1),
(625, 15, 0, 'giay-boot-got-nhon-ro69-1476362022.jpg', 0, '', 1),
(628, 17, 0, '01-257-SDD-350K-320x320.jpg', 1, '', 1),
(630, 18, 0, '01-261-SDD-360K-320x320.jpg', 0, '', 1),
(631, 16, 0, '01-217-BOSS-380K-320x320.jpg', 0, '', 1),
(632, 18, 0, 'BC01 (1).jpg', 0, '', 2),
(634, 10, 0, '6a004569e6570c095546.jpg', 0, '', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_size`
--

INSERT INTO `product_size` (`id`, `product_id`, `color_id`, `size_id`, `quantity`) VALUES
(1, 3, 9, 1, 0),
(2, 3, 9, 2, 0),
(3, 3, 10, 1, 0),
(4, 3, 10, 2, 0),
(5, 4, 11, 1, 15),
(6, 4, 11, 2, 15),
(7, 5, 12, 1, 15),
(8, 5, 12, 2, 0),
(9, 4, 12, 1, 25),
(10, 4, 12, 2, 12),
(11, 6, 12, 1, 0),
(12, 6, 12, 2, 0),
(13, 18, 1, 1, 0),
(14, 18, 1, 2, 0),
(15, 19, 4, 1, 0),
(16, 19, 4, 2, 0),
(17, 21, 1, 2, 0),
(18, 21, 1, 3, 0),
(19, 21, 1, 4, 0),
(20, 21, 1, 5, 0),
(21, 21, 1, 6, 0),
(22, 21, 1, 7, 0),
(23, 21, 1, 20, 0),
(24, 21, 1, 21, 0),
(25, 21, 1, 8, 0),
(26, 21, 2, 3, 0),
(27, 21, 2, 5, 0),
(28, 21, 2, 6, 0),
(29, 21, 2, 21, 0),
(30, 21, 3, 2, 0),
(31, 21, 3, 3, 0),
(32, 21, 3, 20, 0),
(33, 21, 3, 21, 0),
(34, 21, 4, 3, 0),
(35, 21, 4, 5, 0),
(36, 21, 4, 7, 0),
(37, 21, 5, 9, 0),
(38, 21, 5, 10, 0),
(39, 21, 5, 11, 0),
(40, 21, 6, 5, 0),
(41, 21, 6, 6, 0),
(42, 21, 6, 7, 0),
(43, 21, 2, 1, 0),
(44, 21, 2, 2, 0),
(45, 21, 2, 4, 0),
(46, 21, 2, 7, 0),
(47, 21, 2, 8, 0),
(48, 21, 2, 9, 0),
(49, 21, 2, 10, 0),
(50, 21, 2, 11, 0),
(51, 21, 2, 12, 0),
(52, 21, 2, 13, 0),
(53, 21, 2, 14, 0),
(54, 21, 2, 15, 0),
(55, 21, 2, 16, 0),
(56, 21, 2, 17, 0),
(57, 21, 2, 18, 0),
(58, 21, 2, 19, 0),
(59, 21, 2, 20, 0),
(60, 21, 3, 1, 0),
(61, 21, 3, 4, 0),
(62, 21, 3, 5, 0),
(63, 21, 3, 6, 0),
(64, 21, 3, 7, 0),
(65, 21, 3, 8, 0),
(66, 21, 3, 9, 0),
(67, 21, 3, 10, 0),
(68, 21, 3, 11, 0),
(69, 21, 3, 12, 0),
(70, 21, 3, 13, 0),
(71, 21, 3, 14, 0),
(72, 21, 3, 15, 0),
(73, 21, 3, 16, 0),
(74, 21, 3, 17, 0),
(75, 21, 3, 18, 0),
(76, 21, 3, 19, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotions`
--

CREATE TABLE `promotions` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `none_expired` int(11) NOT NULL,
  `type` varchar(28) NOT NULL,
  `value` float NOT NULL,
  `apply_for` enum('all','order','product','group','customer') NOT NULL,
  `min_quantity` int(11) NOT NULL,
  `product` text NOT NULL,
  `group_product` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `promotions`
--

INSERT INTO `promotions` (`id`, `name`, `from_date`, `to_date`, `none_expired`, `type`, `value`, `apply_for`, `min_quantity`, `product`, `group_product`, `status`, `added_date`, `updated_date`, `updated_by`) VALUES
(7, 'giảm 50%', '2015-12-17', '2016-01-09', 0, 'percent', 50, 'group', 1, '', 'hot', 0, '0000-00-00 00:00:00', '2015-12-17 21:17:39', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotion_code`
--

CREATE TABLE `promotion_code` (
  `code` varchar(12) NOT NULL,
  `note` varchar(256) NOT NULL,
  `quantity` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `value` float NOT NULL,
  `min_order_value` float NOT NULL,
  `no_of_using` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `allowcoupon` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `apply_for` enum('all','order','product','group','customer') NOT NULL,
  `group_product` varchar(32) NOT NULL,
  `product` text NOT NULL,
  `min_quantity` int(11) NOT NULL,
  `none_expired` int(11) NOT NULL,
  `apply_order` varchar(32) NOT NULL,
  `none_limited` int(11) NOT NULL,
  `no_of_used` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `promotion_code`
--

INSERT INTO `promotion_code` (`code`, `note`, `quantity`, `added_date`, `updated_date`, `updated_by`, `from_date`, `to_date`, `value`, `min_order_value`, `no_of_using`, `status`, `allowcoupon`, `type`, `apply_for`, `group_product`, `product`, `min_quantity`, `none_expired`, `apply_order`, `none_limited`, `no_of_used`) VALUES
('DONXUAN', '', 0, '0000-00-00 00:00:00', '2017-01-06 13:59:48', 1, '2017-01-06', '2017-01-13', 20000, 0, 1, 0, 1, 'money', 'all', '', '', 0, 0, 'PerOrder', 0, 0),
('Mã', '', 0, '0000-00-00 00:00:00', '2017-01-06 13:57:36', 1, '2017-01-06', '2017-01-14', 10000, 0, 0, 0, 0, 'money', 'group', 'online', '', 0, 0, 'PerOrder', 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotion_products`
--

CREATE TABLE `promotion_products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `product_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `price` float NOT NULL,
  `price_discount` float NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `promotion_products`
--

INSERT INTO `promotion_products` (`id`, `product_name`, `product_id`, `promotion_id`, `product_code`, `price`, `price_discount`, `added_date`, `updated_date`, `updated_by`) VALUES
(5, '', 3, 3, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, '', 4, 3, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` text,
  `ordering` int(11) DEFAULT '0',
  `block` tinyint(1) DEFAULT '0',
  `ship` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `province`
--

INSERT INTO `province` (`id`, `name`, `ordering`, `block`, `ship`) VALUES
(2, 'Tp. Hồ Chí Minh', 0, 0, 20000),
(3, 'Tp. Hà Nội', 0, 0, 20000),
(4, 'Tp. Đà Nẵng', 0, 0, 20000),
(5, 'Tp. Cần Thơ', 0, 0, 20000),
(6, 'Bình Dương', 0, 0, 20000),
(7, 'Long An', 0, 0, 20000),
(8, 'Tiền Giang', 0, 0, 20000),
(9, 'Đồng Nai', 0, 0, 20000),
(10, 'Tây Ninh', 0, 0, 20000),
(11, 'Bà Rịa - Vũng Tàu', 0, 0, 20000),
(12, 'Bến Tre', 0, 0, 20000),
(13, 'Quảng Ninh', 0, 0, 20000),
(14, 'Quảng Ngãi', 0, 0, 20000),
(15, 'Quảng Nam', 0, 0, 20000),
(16, 'Quảng Bình', 0, 0, 20000),
(17, 'Phú Thọ', 0, 0, 20000),
(18, 'Phú Yên', 0, 0, 20000),
(19, 'Ninh Thuận', 0, 0, 20000),
(20, 'Ninh Bình ', 0, 0, 20000),
(21, 'Nghệ An', 0, 0, 20000),
(22, 'Nam Định ', 0, 0, 20000),
(23, 'Lâm Đồng', 0, 0, 20000),
(24, 'Lạng Sơn', 0, 0, 20000),
(25, 'Lào Cai', 0, 0, 20000),
(26, 'Lai Châu', 0, 0, 20000),
(27, 'Kon Tum', 0, 0, 20000),
(28, 'Kiên Giang', 0, 0, 20000),
(29, 'Khánh Hòa', 0, 0, 20000),
(30, 'Hưng Yên', 0, 0, 20000),
(31, 'Hậu Giang', 0, 0, 20000),
(32, 'Hòa Bình', 0, 0, 20000),
(33, 'Hải Phòng', 0, 0, 20000),
(34, 'Hải Dương ', 0, 0, 20000),
(35, 'Hà Tĩnh', 0, 0, 20000),
(36, 'Hà Tây', 0, 0, 20000),
(37, 'Hà Nam', 0, 0, 20000),
(38, 'Hà Giang', 0, 0, 20000),
(39, 'Gia Lai', 0, 0, 20000),
(40, 'Đồng Tháp', 0, 0, 20000),
(41, 'Điện Biên', 0, 0, 20000),
(42, 'Đắk Nông ', 0, 0, 20000),
(43, 'Đắk Lắk', 0, 0, 20000),
(44, 'Cao Bằng', 0, 0, 20000),
(45, 'Cà Mau', 0, 0, 20000),
(46, 'Bình Thuận', 0, 0, 20000),
(47, 'Bình Phước', 0, 0, 20000),
(48, 'Bình Định', 0, 0, 20000),
(49, 'Bắc Ninh', 0, 0, 20000),
(50, 'Bắc Giang', 0, 0, 20000),
(51, 'Bắc Cạn', 0, 0, 20000),
(52, 'Bạc Liêu', 0, 0, 20000),
(53, 'An Giang', 0, 0, 20000),
(64, 'Yên Bái', 0, 0, 20000),
(65, 'Vĩnh Phúc', 0, 0, 20000),
(66, 'Vĩnh Long', 0, 0, 20000),
(67, 'Tuyên Quang ', 0, 0, 20000),
(68, 'Thừa Thiên Huế', 0, 0, 20000),
(69, 'Thanh Hóa', 0, 0, 20000),
(70, 'Thái Nguyên', 0, 0, 20000),
(71, 'Thái Bình', 0, 0, 20000),
(72, 'Sơn La', 0, 0, 20000),
(73, 'Trà Vinh', 0, 0, 20000),
(74, 'Sóc Trăng', 0, 0, 20000),
(75, 'Quảng Trị', 0, 0, 20000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `role_name` varchar(64) NOT NULL,
  `access_rights` text NOT NULL,
  `remark` varchar(255) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`role_name`, `access_rights`, `remark`, `added_date`, `updated_date`, `updated_by`) VALUES
('supper admin', '{\"order\":[\"admin\",\"delete\",\"view\",\"report\"],\"category\":[\"admin\",\"create\",\"update\",\"delete\"],\"subcategory\":[\"admin\",\"create\",\"update\",\"delete\"],\"subsubcategory\":[\"admin\",\"create\",\"update\",\"delete\"],\"color\":[\"admin\",\"create\",\"update\",\"delete\"],\"size\":[\"admin\",\"create\",\"update\",\"delete\"],\"product\":[\"admin\",\"create\",\"update\",\"delete\"],\"promotioncode\":[\"admin\",\"create\",\"update\",\"delete\"],\"promotion\":[\"admin\",\"create\",\"update\",\"delete\",\"deleteproduct\",\"updateproduct\",\"view\"],\"cms\":[\"admin\",\"create\",\"update\",\"delete\"],\"gallery\":[\"admin\",\"create\",\"update\",\"delete\"],\"slideshow\":[\"admin\",\"create\",\"delete\",\"update\"],\"settings\":[\"admin\",\"create\",\"update\"],\"user\":[\"admin\",\"create\",\"update\",\"delete\"],\"roles\":[\"admin\",\"create\",\"update\",\"delete\"],\"seo\":[\"admin\",\"create\",\"update\",\"delete\"]}', 'Quyền cao nhất', '2015-10-31 18:40:07', '2015-10-31 18:40:07', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `seo_h1` varchar(128) NOT NULL,
  `seo_title` varchar(128) NOT NULL,
  `seo_keywords` varchar(128) NOT NULL,
  `seo_description` varchar(128) NOT NULL,
  `seo_extra` varchar(128) NOT NULL,
  `uri` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `seo`
--

INSERT INTO `seo` (`id`, `seo_h1`, `seo_title`, `seo_keywords`, `seo_description`, `seo_extra`, `uri`) VALUES
(3, '', 'Blog tin tức thời trang 2015', 'xu huong thoi trang', 'Blog tin tức thời trang 2015', '', 'http://localhost/shoponline/blogs/tin-tuc');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `size`
--

CREATE TABLE `size` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `size`
--

INSERT INTO `size` (`id`, `name`, `status`) VALUES
(1, 35, 0),
(2, 36, 0),
(3, 27, 0),
(4, 28, 0),
(5, 29, 0),
(6, 30, 0),
(7, 31, 0),
(8, 32, 0),
(9, 33, 0),
(10, 34, 0),
(11, 37, 0),
(12, 38, 0),
(13, 39, 0),
(14, 40, 0),
(15, 41, 0),
(16, 42, 0),
(17, 43, 0),
(18, 44, 0),
(19, 45, 0),
(20, 46, 0),
(21, 47, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `type` varchar(30) NOT NULL,
  `slug` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `display_order` int(11) NOT NULL,
  `meta_keyword` varchar(256) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `type`, `slug`, `description`, `display_order`, `meta_keyword`, `meta_description`, `status`, `added_date`, `updated_date`, `updated_by`) VALUES
(1, 2, 'Áo sơ mi', 'style', 'ao-so-mi', '', 1, 'Áo sơ mi', 'Giày nữ', 0, '2015-10-15 06:50:56', '2015-11-15 23:35:33', 1),
(2, 2, 'Áo khoác', 'style', 'ao-khoac', '', 2, 'Áo khoác', '', 0, '2015-10-15 06:51:44', '2015-11-15 23:37:12', 1),
(3, 1, 'Áo khoác', '', 'ao-khoac', '<p>&Aacute;o kho&aacute;c</p>\r\n', 2, 'Áo khoác', 'Áo khoác', 0, '2015-10-20 21:06:02', '2015-11-16 10:24:24', 1),
(4, 3, 'Pin (Quartz)', '', 'pin-(quartz)', '', 3, '', '', 0, '2015-11-07 15:45:26', '2015-11-15 23:40:11', 1),
(5, 4, 'Túi cỡ lớn', '', 'tui-co-lon', '', 1, '', '', 0, '2015-11-07 15:45:36', '2015-11-07 15:45:36', 1),
(6, 5, 'Leather (Da)', '', 'leather-(da)', '', 3, '', '', 0, '2015-11-07 15:45:47', '2015-11-15 23:43:56', 1),
(7, 6, 'Giày cao gót', '', 'giay-cao-got', '', 3, '', '', 0, '2015-11-07 15:46:06', '2015-11-15 23:45:43', 1),
(8, 7, 'Nước hoa', '', 'nuoc-hoa', '', 3, '', '', 0, '2015-11-07 15:46:18', '2015-11-15 23:47:28', 1),
(9, 8, 'Đồ điện gia dụng', '', 'do-dien-gia-dung', '', 1, '', '', 0, '2015-11-07 15:46:27', '2015-11-07 15:46:27', 1),
(10, 8, 'Thiết bị nhà bếp, chế biến', '', 'thiet-bi-nha-bep-che-bien', '', 2, '', '', 0, '2015-11-07 15:46:38', '2015-11-07 15:46:38', 1),
(11, 1, 'Áo vest', '', 'ao-vest', '<p>&Aacute;o vest</p>\r\n', 3, 'Áo vest', 'Áo vest', 0, '2015-11-08 06:55:12', '2015-11-16 10:24:36', 1),
(12, 1, 'Áo thun', '', 'ao-thun', '<p>&Aacute;o kiểu</p>\r\n', 4, 'Áo kiểu', 'Áo kiểu', 0, '2015-11-08 06:55:47', '2015-11-16 10:24:50', 1),
(13, 1, 'Áo kiểu', '', 'ao-kieu', '<p>&Aacute;o kiểu</p>\r\n', 5, 'Áo kiểu', 'Áo kiểu', 0, '2015-11-08 06:56:22', '2015-11-16 10:25:08', 1),
(14, 1, 'Quần tây', '', 'quan-tay', '<p>Quần t&acirc;y</p>\r\n', 6, 'Quần tây', 'Quần tây', 0, '2015-11-08 06:56:34', '2015-11-16 10:25:21', 1),
(15, 1, 'Quần jeans', '', 'quan-jeans', '<p>Quần jeans</p>\r\n', 7, 'Quần jeans', 'Quần jeans', 0, '2015-11-08 06:56:49', '2015-11-16 10:25:33', 1),
(16, 1, 'Quần kaki', '', 'quan-kaki', '<p>Quần kaki</p>\r\n', 8, 'Quần kaki', 'Quần kaki', 0, '2015-11-08 06:57:04', '2015-11-16 10:25:45', 1),
(17, 1, 'Quần short', '', 'quan-short', '<p>Quần short</p>\r\n', 9, 'Quần short', 'Quần short', 0, '2015-11-08 06:57:20', '2015-11-16 10:25:55', 1),
(18, 1, 'Đồ thể thao', '', 'do-the-thao', '<p>Đồ thể thao</p>\r\n', 10, 'Đồ thể thao', 'Đồ thể thao', 0, '2015-11-08 06:57:37', '2015-11-16 10:26:08', 1),
(19, 1, 'Đồ bơi', '', 'do-boi', '<p>Đồ bơi</p>\r\n', 11, 'Đồ bơi', 'Đồ bơi', 0, '2015-11-08 06:57:55', '2015-11-16 10:26:18', 1),
(20, 2, 'Áo vest', '', 'ao-vest', '', 3, 'Áo vest', '', 0, '2015-11-15 23:37:28', '2015-11-15 23:37:28', 1),
(21, 2, 'Áo thun/ Áo kiểu', '', 'ao-thun/-ao-kieu', '', 4, 'Áo thun/ Áo kiểu', '', 0, '2015-11-15 23:37:40', '2015-11-15 23:37:40', 1),
(22, 2, 'Áo len', '', 'ao-len', '', 5, 'Áo len', '', 0, '2015-11-15 23:37:53', '2015-11-15 23:37:53', 1),
(23, 2, 'Đầm, Váy', '', 'dam-vay', '', 6, 'Đầm, Váy', '', 0, '2015-11-15 23:38:05', '2015-11-15 23:38:05', 1),
(24, 2, 'Chân váy', '', 'chan-vay', '', 7, 'Chân váy', '', 0, '2015-11-15 23:38:16', '2015-11-15 23:38:16', 1),
(25, 2, 'Quần nữ', '', 'quan-nu', '', 8, '', '', 0, '2015-11-15 23:38:26', '2015-11-15 23:38:26', 1),
(26, 2, 'Áo CARDIGAN', '', 'ao-cardigan', '', 9, 'Áo CARDIGAN', '', 0, '2015-11-15 23:38:36', '2015-11-15 23:38:36', 1),
(27, 2, 'Đồ thể thao', '', 'do-the-thao', '', 10, 'Đồ thể thao', '', 0, '2015-11-15 23:38:51', '2015-11-15 23:38:51', 1),
(28, 2, 'Đồ bơi', '', 'do-boi', '', 11, '', '', 0, '2015-11-15 23:39:08', '2015-11-15 23:39:08', 1),
(29, 2, 'Đồ lót/ Đồ ngủ', '', 'do-lot/-do-ngu', '', 12, 'Đồ lót/ Đồ ngủ', '', 0, '2015-11-15 23:39:24', '2015-11-15 23:39:24', 1),
(30, 3, 'Đồng hồ Nam', '', 'dong-ho-nam', '', 2, 'Đồng hồ Nam', '', 0, '2015-11-15 23:40:27', '2015-11-15 23:40:27', 1),
(31, 3, 'Đồng hồ Nữ', '', 'dong-ho-nu', '', 1, 'Đồng hồ Nữ', '', 0, '2015-11-15 23:40:48', '2015-11-15 23:40:48', 1),
(32, 3, 'Cơ (Automatic)', '', 'co-(automatic)', '', 4, 'Cơ (Automatic)', '', 0, '2015-11-15 23:41:00', '2015-11-15 23:41:00', 1),
(33, 3, 'Năng lượng mặt trời', '', 'nang-luong-mat-troi', '', 5, 'Năng lượng mặt trời', '', 0, '2015-11-15 23:41:11', '2015-11-15 23:41:11', 1),
(34, 3, 'Kinetic (vừa pin, tự động)', '', 'kinetic-(vua-pin-tu-dong)', '', 6, 'Kinetic (vừa pin, tự động)', '', 0, '2015-11-15 23:41:22', '2015-11-15 23:41:22', 1),
(35, 4, 'Túi cỡ trung', '', 'tui-co-trung', '', 2, 'Túi cỡ trung', '', 0, '2015-11-15 23:42:02', '2015-11-15 23:42:02', 1),
(36, 4, 'Túi cỡ nhỏ', '', 'tui-co-nho', '', 3, 'Túi cỡ nhỏ', '', 0, '2015-11-15 23:42:13', '2015-11-15 23:42:13', 1),
(37, 4, 'Hình Ovan', '', 'hinh-ovan', '', 4, 'Hình Ovan', '', 0, '2015-11-15 23:42:28', '2015-11-15 23:42:28', 1),
(38, 4, 'Hình chữ nhật', '', 'hinh-chu-nhat', '', 5, 'Hình chữ nhật', '', 0, '2015-11-15 23:42:40', '2015-11-15 23:43:28', 1),
(39, 4, 'Hình vuông', '', 'hinh-vuong', '', 6, 'Hình vuông', '', 0, '2015-11-15 23:43:02', '2015-11-15 23:43:02', 1),
(40, 4, 'Hình con vật', '', 'hinh-con-vat', '', 7, 'Hình con vật', '', 0, '2015-11-15 23:43:12', '2015-11-15 23:43:12', 1),
(41, 5, 'Ví Nữ', '', 'vi-nu', '', 1, '', '', 0, '2015-11-15 23:44:08', '2015-11-15 23:44:08', 1),
(42, 5, 'Ví Nam', '', 'vi-nam', '', 2, 'Ví Nam', '', 0, '2015-11-15 23:44:22', '2015-11-15 23:44:22', 1),
(43, 5, 'Giả da', '', 'gia-da', '', 4, 'Giả da', '', 0, '2015-11-15 23:44:32', '2015-11-15 23:44:32', 1),
(44, 5, 'Suede (Da lộn)', '', 'suede-(da-lon)', '', 5, 'Suede (Da lộn)', '', 0, '2015-11-15 23:44:42', '2015-11-15 23:44:42', 1),
(45, 5, 'Velour (Nhung)', '', 'velour-(nhung)', '', 6, 'Velour (Nhung)', '', 0, '2015-11-15 23:44:53', '2015-11-15 23:44:53', 1),
(46, 5, 'Simili', '', 'simili', '', 7, 'Simili', '', 0, '2015-11-15 23:45:03', '2015-11-15 23:45:03', 1),
(47, 5, 'Sợi tổng hợp', '', 'soi-tong-hop', '', 8, 'Sợi tổng hợp', '', 0, '2015-11-15 23:45:13', '2015-11-15 23:45:13', 1),
(48, 6, 'Giày, Dép Nam', '', 'giay-dep-nam', '', 2, 'Giày, Dép Nam', '', 0, '2015-11-15 23:46:00', '2015-11-15 23:46:00', 1),
(49, 6, 'Giày, Dép Nữ', '', 'giay-dep-nu', '', 1, 'Giày, Dép Nữ', '', 0, '2015-11-15 23:46:12', '2015-11-15 23:46:12', 1),
(50, 6, 'Giày Búp bê', '', 'giay-bup-be', '', 4, 'Giày Búp bê', '', 0, '2015-11-15 23:46:25', '2015-11-15 23:46:25', 1),
(51, 6, 'Giày Boots', '', 'giay-boots', '', 5, 'Giày Boots', '', 0, '2015-11-15 23:46:36', '2015-11-15 23:46:36', 1),
(52, 6, 'Giày Sandal', '', 'giay-sandal', '', 6, 'Giày Sandal', '', 0, '2015-11-15 23:46:45', '2015-11-15 23:46:45', 1),
(53, 6, 'Giày lười', '', 'giay-luoi', '', 7, 'Giày lười', '', 0, '2015-11-15 23:46:56', '2015-11-15 23:46:56', 1),
(54, 6, 'Giày vải', '', 'giay-vai', '', 8, 'Giày vải', '', 0, '2015-11-15 23:47:06', '2015-11-15 23:47:06', 1),
(55, 7, 'Mỹ phẩm Nam', '', 'my-pham-nam', '', 2, 'Mỹ phẩm Nam', '', 0, '2015-11-15 23:47:40', '2015-11-15 23:47:40', 1),
(56, 7, 'Mỹ phẩm Nữ', '', 'my-pham-nu', '', 1, '', '', 0, '2015-11-15 23:47:55', '2015-11-15 23:47:55', 1),
(57, 7, 'Tắm và chăm sóc cơ thể', '', 'tam-va-cham-soc-co-the', '', 4, 'Tắm và chăm sóc cơ thể', '', 0, '2015-11-15 23:48:05', '2015-11-15 23:48:05', 1),
(58, 7, 'Trang điểm mặt', '', 'trang-diem-mat', '', 5, 'Trang điểm mặt', '', 0, '2015-11-15 23:48:15', '2015-11-15 23:48:15', 1),
(59, 7, 'Tóc', '', 'toc', '', 6, 'Tóc', '', 0, '2015-11-15 23:48:30', '2015-11-15 23:48:30', 1),
(60, 7, 'Tóc', '', 'toc', '', 6, 'Tóc', '', 0, '2015-11-15 23:48:30', '2015-11-15 23:48:30', 1),
(61, 7, 'Thiết bị y tế', '', 'thiet-bi-y-te', '', 7, 'Thiết bị y tế', '', 0, '2015-11-15 23:48:39', '2015-11-15 23:48:39', 1),
(62, 7, 'Phụ kiện khác', '', 'phu-kien-khac', '', 8, 'Phụ kiện khác', '', 0, '2015-11-15 23:48:55', '2015-11-15 23:48:55', 1),
(63, 8, 'Trang trí', '', 'trang-tri', '', 3, 'Trang trí', '', 0, '2015-11-15 23:49:23', '2015-11-15 23:49:23', 1),
(64, 8, 'Quà tặng, hàng thủ công', '', 'qua-tang-hang-thu-cong', '', 4, 'Quà tặng, hàng thủ công', '', 0, '2015-11-15 23:49:36', '2015-11-15 23:49:36', 1),
(65, 8, 'Đồ dùng sinh hoạt', '', 'do-dung-sinh-hoat', '', 5, 'Đồ dùng sinh hoạt', '', 0, '2015-11-15 23:49:47', '2015-11-15 23:49:47', 1),
(66, 8, 'Đồ dùng phòng ngủ', '', 'do-dung-phong-ngu', '', 6, 'Đồ dùng phòng ngủ', '', 0, '2015-11-15 23:49:58', '2015-11-15 23:49:58', 1),
(67, 8, 'Thiết bị sửa chữa', '', 'thiet-bi-sua-chua', '', 7, 'Thiết bị sửa chữa', '', 0, '2015-11-15 23:50:14', '2015-11-15 23:50:14', 1),
(68, 8, 'Phòng khách', '', 'phong-khach', '', 8, 'Phòng khách', '', 0, '2015-11-15 23:50:25', '2015-11-15 23:50:25', 1),
(69, 8, 'Phụ kiện khác', '', 'phu-kien-khac', '', 9, 'Phụ kiện khác', '', 0, '2015-11-15 23:50:38', '2015-11-15 23:50:38', 1),
(70, 1, 'Áo sơ mi', '', 'ao-so-mi', '', 1, 'Áo sơ mi', '', 0, '2015-11-16 10:24:09', '2015-11-16 10:24:09', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `display_order` int(11) NOT NULL,
  `meta_keyword` varchar(256) NOT NULL,
  `meta_description` varchar(256) NOT NULL,
  `status` int(11) NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `category_id`, `sub_category_id`, `name`, `slug`, `description`, `display_order`, `meta_keyword`, `meta_description`, `status`, `added_date`, `updated_date`, `updated_by`) VALUES
(1, 2, 1, 'Giày búp bê', 'giay-bup-be', '<p>Gi&agrave;y b&uacute;p b&ecirc;</p>\r\n', 1, 'Giày búp bê', 'Giày búp bê', 0, '2015-10-20 21:02:38', '2015-10-20 22:24:48', 1),
(2, 2, 1, 'Giày xăng đan', 'giay-xang-dan', '<p>GI&agrave;y xăng đan</p>\r\n', 3, 'giay xang dan,giày xăng đan', 'GIày xăng đan', 0, '2015-10-26 10:52:05', '2015-10-26 10:52:05', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(11) NOT NULL,
  `s_value` text NOT NULL,
  `s_key` varchar(32) NOT NULL,
  `category` varchar(64) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `special` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `system_settings`
--

INSERT INTO `system_settings` (`id`, `s_value`, `s_key`, `category`, `remarks`, `special`) VALUES
(10, 'https://www.facebook.com', 'link_fb', '', 'Link tới fanpage facebook', 0),
(11, 'https://www.google.com', 'link_gg', '', 'Link tới google plus', 0),
(12, 'https://www.twitter.com', 'link_tt', '', '', 0),
(13, 'https://www.linkedin.com', 'linked_in', '', '', 0),
(14, 'https://www.youtube.com', 'link_youtube', '', '', 0),
(15, 'http://www.pinterest.com', 'link_pinterest', '', '', 0),
(16, '0935135349', '0981681669', '', 'Số điện thoại hotline', 1),
(17, 'mở cửa từ 24/24 từ thứ 2 - chủ nhật', 'open_time', '', 'Giờ mở cửa', 1),
(18, '588 Nguyễn Văn Quá, Đông Hưng Thuận, Quận 12, HCM', 'address', '', 'sdgdsgs', 1),
(19, 'Sáng: 8:00 - 12:00<br>Chiều: 13:00 - 22:00', 'mon_fri', '', 'Lịch làm việc từ thứ 2 tới thứ 6', 1),
(20, 'Sáng: 8:00 - 12:00 / Chiều: 13:00 - 18:00<br/>Chủ nhật, Lễ, Tết làm việc bình thường', 'sat', '', 'Lịch làm việc thứ 7, chủ nhật và ngày lễ', 0),
(21, 'giay cao got, giay nu', 'keywords', '', 'Từ khóa thường được tìm kiếm', 1),
(22, '10', 'home_pagination', '', 'Số sản phẩm load ra ( là sản phẩm 1x, nếu là 2x thì tính là 2 sp)', 0),
(23, '30', 'category_pagination', '', 'Số sản phẩm lấy ra trên 1 trang thuộc trang danh mục sản phẩm', 0),
(24, '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3295.650089005278!2d106.63753341601142!3d10.795181053555076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752952622700e9%3A0x16b420b069c63dad!2zMThBIEJhIFbDom4sIHBoxrDhu51uZyAxNCwgVMOibiBCw6xuaCwgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1448100994422\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'map', '', 'Bản đồ', 1),
(25, '+ Mở cửa từ <b>8:30 - 22:00</b> (T2 đến CN)<br	                        + Giao hàng tận nơi trên toàn quốc - Thanh toán khi nhận hàng<br>	                        + Đổi trả hàng trọng vòng 30 ngày.<br>	                        + ĐIỆN THOẠI ĐẶT HÀNG: <b>(091) 2171707</b><br>	                        Sản phẩm sẽ có mặt tại nhà bạn trong vòng 2 -7 ngày làm việc.<br> 	                        Đước cung cấp bởi GIÀY HIỆU BCC', 'open_time_detail_product', '', 'Giờ mở cửa ở trang chi tiết sản phẩm', 1),
(26, '2015-12-28-07-30-11-Chrysanthemum.jpg', 'free_ship', '', 'Danh sách giao hàng miễn phí', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `trackings`
--

CREATE TABLE `trackings` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(50) NOT NULL,
  `object_type` varchar(50) DEFAULT NULL,
  `object_id` varchar(50) NOT NULL,
  `object_name` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `trackings`
--

INSERT INTO `trackings` (`id`, `type`, `object_type`, `object_id`, `object_name`, `date`, `user_id`, `value`) VALUES
(1884, 'create', 'Role', 'supper admin', 'supper admin', '2015-10-31 18:40:07', 1, '{\"old_value\":[null], \"new_value\":[{\"role_name\":\"supper admin\",\"remark\":\"Quy\\u1ec1n cao nh\\u1ea5t\",\"added_date\":\"2015-10-31 18:40:07\",\"updated_date\":\"2015-10-31 18:40:07\",\"updated_by\":\"1\",\"access_rights\":\"{\\\"order\\\":[\\\"admin\\\",\\\"delete\\\",\\\"view\\\",\\\"report\\\"],\\\"category\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"subcategory\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"subsubcategory\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"color\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"size\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"product\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"promotioncode\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"promotion\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\",\\\"deleteproduct\\\",\\\"updateproduct\\\",\\\"view\\\"],\\\"cms\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"gallery\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"slideshow\\\":[\\\"admin\\\",\\\"create\\\",\\\"delete\\\",\\\"update\\\"],\\\"settings\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\"],\\\"user\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"roles\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"],\\\"seo\\\":[\\\"admin\\\",\\\"create\\\",\\\"update\\\",\\\"delete\\\"]}\"}]}'),
(1885, 'update', 'Order', '8', 'Xác nhận giao hàng', '2015-11-14 22:17:56', 1, '{\"old_value\":[{\"status\":\"2\",\"cancel_date\":\"2015-11-14 22:11:17\"}], \"new_value\":[{\"status\":\"2\",\"cancel_date\":\"2015-11-14 22:11:17\"}]}'),
(1886, 'update', 'Order', '9', 'Đơn hàng mới', '2015-11-16 16:53:36', 1, '{\"old_value\":[[]], \"new_value\":[{}]}'),
(1887, 'update', 'Order', '12', 'Đơn hàng mới', '2015-11-25 11:44:04', 1, '{\"old_value\":[{\"status\":\"2\",\"cancel_date\":\"2015-11-25 11:11:44\",\"reason\":\"test l\\u1ecbch s\\u1eed\"}], \"new_value\":[{\"status\":\"2\",\"cancel_date\":\"2015-11-25 11:11:44\",\"reason\":\"test lịch sử\"}]}'),
(1888, 'update', 'Order', '10', 'Đơn hàng mới', '2015-11-25 11:50:02', 1, '{\"old_value\":[{\"status\":\"3\",\"cancel_date\":\"2015-11-25 11:11:50\"}], \"new_value\":[{\"status\":\"3\",\"cancel_date\":\"2015-11-25 11:11:50\"}]}'),
(1889, 'update', 'Order', '13', 'Đơn hàng mới', '2015-11-25 21:28:55', 1, '{\"old_value\":[{\"cancel_date\":\"2015-11-25 21:11:28\"}], \"new_value\":[{\"cancel_date\":\"2015-11-25 21:11:28\"}]}'),
(1890, 'update', 'Order', '16', 'Đơn hàng mới', '2015-12-20 18:46:27', 1, '{\"old_value\":[{\"cancel_date\":\"2015-12-20 18:12:46\"}], \"new_value\":[{\"cancel_date\":\"2015-12-20 18:12:46\"}]}'),
(1891, 'update', 'Order', '30', 'Đơn hàng mới', '2015-12-28 13:48:34', 1, '{\"old_value\":[[]], \"new_value\":[{}]}'),
(1892, 'update', 'Order', '37', '', '2016-02-22 11:55:28', 1, '{\"old_value\":[[]], \"new_value\":[{}]}'),
(1893, 'update', 'Order', '37', '', '2016-02-22 11:56:10', 1, '{\"old_value\":[{\"status\":\"cancelled\"}], \"new_value\":[{\"status\":\"cancelled\"}]}');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(128) NOT NULL,
  `fullname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `type` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `last_login_date` datetime NOT NULL,
  `added_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','U') NOT NULL,
  `roles` varchar(128) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `address` varchar(256) NOT NULL,
  `district` int(11) NOT NULL,
  `province` int(11) NOT NULL,
  `phone` varchar(22) NOT NULL,
  `wishlish` varchar(765) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `fullname`, `lastname`, `type`, `is_admin`, `ip_address`, `last_login_date`, `added_date`, `updated_date`, `updated_by`, `status`, `roles`, `gender`, `address`, `district`, `province`, `phone`, `wishlish`) VALUES
(1, 'admin', 'adcd7048512e64b48da55b027577886ee5a36350', 'admin@gmail.com', 'Tap', 'Nguyen', 1, 1, '14.161.22.110', '2017-02-27 11:02:34', '0000-00-00 00:00:00', '2017-01-05 23:43:44', 1, 'A', 'supper admin', 'male', '180/21', 18, 2, '0982007996', '16'),
(28, 'JimmiXS', 'd58cded29764e6fcd85aae5f27b98a75f03d348a', 'jimos4581rt@hotmail.com', 'JimmiXS', '', 0, 0, '', '0000-00-00 00:00:00', '2016-08-12 17:58:46', '2016-08-12 17:58:46', 0, 'A', '', 'male', 'nadLUzuBqdKt', 3, 10, '64692272454', '');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`email`);

--
-- Chỉ mục cho bảng `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `home_sliders`
--
ALTER TABLE `home_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_photo`
--
ALTER TABLE `product_photo`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `promotion_code`
--
ALTER TABLE `promotion_code`
  ADD PRIMARY KEY (`code`);

--
-- Chỉ mục cho bảng `promotion_products`
--
ALTER TABLE `promotion_products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_name`);

--
-- Chỉ mục cho bảng `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `trackings`
--
ALTER TABLE `trackings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT cho bảng `cms`
--
ALTER TABLE `cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT cho bảng `color`
--
ALTER TABLE `color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT cho bảng `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=703;
--
-- AUTO_INCREMENT cho bảng `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `home_sliders`
--
ALTER TABLE `home_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT cho bảng `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;
--
-- AUTO_INCREMENT cho bảng `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT cho bảng `product_photo`
--
ALTER TABLE `product_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=635;
--
-- AUTO_INCREMENT cho bảng `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT cho bảng `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT cho bảng `promotion_products`
--
ALTER TABLE `promotion_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT cho bảng `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `size`
--
ALTER TABLE `size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT cho bảng `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT cho bảng `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT cho bảng `trackings`
--
ALTER TABLE `trackings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1894;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
