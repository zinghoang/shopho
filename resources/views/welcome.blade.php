<?php $amount_no_decimals_with_comma_separator=2?>
<!DOCTYPE html>
<html lang="vi">
    @yield('header_html') 
    <body>
        <header id="header" class="wrap-header">
    <section class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-3">


                    
                    <div class="logo text-lg-left">
                        <a href="/"><img src="http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/logo.png" alt="Breshka shoes" title="Breshka shoes"></a>
                    </div>
                        

                    <ul class="content-header pull-xs-right hidden-lg-up">
                        
                        <li>
                            <div class="align-items"><i class="fa fa-user" aria-hidden="true"></i><a href="/account/login">Đăng nhập</a></div>/
                        </li>
                        <li>
                            <div class="align-items"><i class="fa fa-unlock" aria-hidden="true"></i><a href="/account/register">Đăng ký</a></div>
                        </li>
                        
                    </ul>

                </div>
                <div class="col-xs-12 col-sm-7 col-lg-9 hidden-md-down">
                    <!-- Giỏ hàng -->
                    <div class="mini-cart pull-xs-right hidden-md-down">
                        <a href="/cart" class="icon-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="cartCount">0</span> Giỏ hàng</a>
                        <div>
                            <div style="" class="top-cart-content arrow_box">
                                <!-- <div class="block-subtitle">Sản phẩm đã cho vào giỏ hàng</div> -->
                                <ul id="cart-sidebar" class="mini-products-list">

                                </ul>
                            </div>
                        </div>
                    </div>


                    <ul class="content-header pull-lg-right hidden-xs-down">
                        <li class="hidden-md-down no-hover">
                            <div class="align-items">
                                <i class="fa fa-phone" aria-hidden="true"></i> <a href="#">1900.6750</a></div>
                        </li>
                        <li class="hidden-lg-down no-hover">
                            <div class="align-items">
                                <form action="/search" method="get" role="search" class="form-inline form-search-top">
                                    <button class="btn btn-search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control search-field" value="" name="query" id="search" placeholder="Tìm kiếm">
                                </form>
                            </div>
                        </li>
                        
                        
                        <li class="hidden-md-down">
                            <div class="align-items"><i class="fa fa-user" aria-hidden="true"></i><a href="#" data-toggle="modal" data-target="#dangnhap">Đăng nhập</a></div></li>
                        <li class="hidden-md-down">
                            <div class="align-items"><i class="fa fa-unlock" aria-hidden="true"></i><a href="#" data-toggle="modal" data-target="#dangky">Đăng ký</a></div>
                        </li>
                        
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="wrap-menu">
        <div class="container">
            <!-- MENU -->
            <div class="main-menu">
                <div class="wrap-nav-toggler">
                    <button class="navbar-toggler pull-xs-left hidden-lg-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">&#9776;</button>
                    <!-- Top Cart -->
                    <div class="mini-cart pull-xs-right hidden-lg-up">
                        <a href="/cart"><i class="fa fa-shopping-cart"></i><span class="cartCount">0</span></a>
                        <div>
                            <div style="" class="top-cart-content arrow_box">
                                <!-- <div class="block-subtitle">Sản phẩm đã cho vào giỏ hàng</div> -->
                                <ul id="cart-sidebar" class="mini-products-list">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Form search -->
                    <form action="/search" method="get" role="search" class="form-inline search-box pull-xs-right hidden-lg-up">
                        <button class="btn btn-search btn-s1" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <button class="btn btn-search btn-s2" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <input class="form-control search-field" type="text" value="" name="query" id="search" placeholder="Tìm kiếm">
                    </form>
                </div>
                <!-- End Nav Toggler -->

                <nav class="nav-main collapse navbar-toggleable-md" id="exCollapsingNavbar2">
                    <ul class="nav navbar-nav navbar-nav-style">
                        
                        
                        
                        
                        
                        
                        <li class="nav-item active"><a class="nav-link" href="/">Trang chủ</a></li>
                        
                        
                        
                        
                        
                        
                        
                        <li class="nav-item "><a class="nav-link" href="/gioi-thieu">Giới thiệu</a></li>
                        
                        
                        
                        
                        
                        
                        
                        <li class="nav-item   has-mega">
                            <a href="/collections/all" class="nav-link">Sản phẩm<i class="fa fa-caret-right fixfa"></i></a>
                            <a type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            
                            <div class="mega-content hidden-md-down">
    <div class="container">
        <div class="nav-block nav-block-center">
            <ul class="level0">
                
                
                <li class="level1"> <h2 class="h4"><a href="/ssandal"><span>Sandal</span></a></h2> 
                    <ul class="">
                        
                        <li class="level2"> <a href="/ssandal"><span>Sandal xỏ ngón</span></a> </li>
                        
                        <li class="level2"> <a href="/ssandal"><span>Sandal thường</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Sandal Mỹ</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Sandal Việt Nam</span></a> </li>
                        
                    </ul>
                </li>
                
                    
                
                <li class="level1"> <h2 class="h4"><a href="/giay-tay"><span>Giày tây</span></a></h2> 
                    <ul class="">
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây cao cấp</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây nhập khẩu</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây Trung Quốc</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây 2017</span></a> </li>
                        
                    </ul>
                </li>
                
                    
                
                <li class="level1"> <h2 class="h4"><a href="/giay-casual"><span>Giày Casual</span></a></h2> 
                    <ul class="">
                        
                        <li class="level2"> <a href="/collections/all"><span>Giay casual mẫu mới</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giay casual đặc biệt</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giay casual 2017</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giay casual hot</span></a> </li>
                        
                    </ul>
                </li>
                
                    
                
                <li class="level1"> <h2 class="h4"><a href="/giay-luoi"><span>Giày lười</span></a></h2> 
                    <ul class="">
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày lười cao cấp</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày lười nhập khẩu</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày lười China</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày lười 2017</span></a> </li>
                        
                    </ul>
                </li>
                
                    
                
                <li class="level1"> <h2 class="h4"><a href="/giay-co-cao"><span>Giày cao cổ</span></a></h2> 
                    <ul class="">
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày cao cổ nam</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày cao cổ nữ</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày cao cổ cao cấp</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày cao cổ 2017</span></a> </li>
                        
                    </ul>
                </li>
                
                    
                
                <li class="level1"> <h2 class="h4"><a href="/giay-di-choi"><span>Giày đi chơi</span></a></h2> 
                    <ul class="">
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày đi chơi giá rẻ</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày đá bóng</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày bóng bàn</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày thể thao</span></a> </li>
                        
                    </ul>
                </li>
                
                    
            </ul>
        </div>
    </div>
</div>
<ul class="dropdown-menu hidden-lg-up">
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/ssandal">Sandal</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/ssandal">Sandal xỏ ngón</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/ssandal">Sandal thường</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Sandal Mỹ</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Sandal Việt Nam</a>
            </li>                                       
            
        </ul>                      
    </li>
    
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/giay-tay">Giày tây</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây cao cấp</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây nhập khẩu</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây Trung Quốc</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây 2017</a>
            </li>                                       
            
        </ul>                      
    </li>
    
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/giay-casual">Giày Casual</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giay casual mẫu mới</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giay casual đặc biệt</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giay casual 2017</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giay casual hot</a>
            </li>                                       
            
        </ul>                      
    </li>
    
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/giay-luoi">Giày lười</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày lười cao cấp</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày lười nhập khẩu</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày lười China</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày lười 2017</a>
            </li>                                       
            
        </ul>                      
    </li>
    
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/giay-co-cao">Giày cao cổ</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày cao cổ nam</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày cao cổ nữ</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày cao cổ cao cấp</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày cao cổ 2017</a>
            </li>                                       
            
        </ul>                      
    </li>
    
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/giay-di-choi">Giày đi chơi</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày đi chơi giá rẻ</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày đá bóng</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày bóng bàn</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày thể thao</a>
            </li>                                       
            
        </ul>                      
    </li>
    
    

</ul>
                            
                        </li>
                        
                        
                        
                        
                        
                        
                        
                        <li class="nav-item "><a class="nav-link" href="/tin-tuc">Tin tức</a></li>
                        
                        
                        
                        
                        
                        
                        
                        <li class="nav-item "><a class="nav-link" href="/ban-do">Bản đồ</a></li>
                        
                        
                        
                        
                        
                        
                        
                        <li class="nav-item "><a class="nav-link" href="/lien-he">Liên hệ</a></li>
                        
                        
                    </ul>
                </nav>
            </div><!-- /MENU -->
        </div>
    </div><!-- /wrap MENU -->
</header><!-- /header -->



<!-- Modal Đăng nhập -->
<div class="modal fade" id="dangnhap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog wrap-modal-login" role="document">
        <div class="text-xs-center">
            <div id="login">
                <h5 class="title-modal">Đăng nhập</h5>
                <form accept-charset='UTF-8' action='/account/login' id='customer_login' method='post'>
<input name='FormType' type='hidden' value='customer_login' />
<input name='utf8' type='hidden' value='true' />
                <div class="form-signup" >
                    
                </div>
                <div class="form-signup clearfix">
                    <fieldset class="form-group">
                        <input type="email" class="form-control form-control-lg" value="" name="email" id="customer_email" placeholder="Nhập ID*" required requiredmsg="Nhập tài khoản" >
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="password" class="form-control form-control-lg" value="" name="password" id="customer_password" placeholder="Nhập mật khẩu*" required requiredmsg="Nhập mật khẩu">
                    </fieldset>
                    <fieldset class="form-group">
                        <input class="btn btn-lg btn-style btn-style-active col-xs-12" type="submit" value="Đăng nhập" />
                    </fieldset> 
                    <fieldset class="form-group">
                        <button type="button" class="btn btn-lg btn-style col-xs-12" data-dismiss="modal">Hủy</button>
                    </fieldset>                 
                    <div>
                        <p><a href="#" class="btn-link-style btn-link-style-active" onclick="showRecoverPasswordForm();return false;">Quên mật khẩu ?</a></p>
                        <!-- <a href="/account/register" class="btn-link-style">Đăng ký tài khoản mới</a> -->
                    </div>
                </div>
                </form>
            </div>

            <div id="recover-password" class="form-signup">
                <h2 class="title-head text-xs-center"><a href="#">Lấy lại mật khẩu</a></h2>
                <p>Chúng tôi sẽ gửi thông tin lấy lại mật khẩu vào email đăng ký tài khoản của bạn</p>

                <form accept-charset='UTF-8' action='/account/recover' id='recover_customer_password' method='post'>
<input name='FormType' type='hidden' value='recover_customer_password' />
<input name='utf8' type='hidden' value='true' />
                <div class="form-signup" >
                    
                </div>
                <div class="form-signup clearfix">
                    <fieldset class="form-group">
                        <input type="email" class="form-control form-control-lg" value="" name="Email" id="recover-email" placeholder="Email*" required requiredmsg="Nhập email">
                    </fieldset>
                </div>
                <div class="action_bottom">
                    <input class="btn btn-lg btn-style btn-recover-send" type="submit" value="Gửi" /> hoặc
                    <a href="#" class="btn btn-lg btn-style btn-style-active btn-recover-cancel" onclick="hideRecoverPasswordForm();return false;">Hủy</a>
                </div>
                </form>

            </div>

            <script type="text/javascript">
                function showRecoverPasswordForm() {
                    document.getElementById('recover-password').style.display = 'block';
                    document.getElementById('login').style.display='none';
                }

                function hideRecoverPasswordForm() {
                    document.getElementById('recover-password').style.display = 'none';
                    document.getElementById('login').style.display = 'block';
                }

                if (window.location.hash == '#recover') { showRecoverPasswordForm() }
            </script>

        </div>
    </div>
</div>
<!-- Modal Đăng ký-->
<div class="modal fade" id="dangky" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog wrap-modal-login" role="document">
        <div class="text-xs-center">
            <div id="login">
                <h5 class="title-modal">Đăng ký nhanh</h5>
                <form accept-charset='UTF-8' action='/account/register' id='customer_register' method='post'>
<input name='FormType' type='hidden' value='customer_register' />
<input name='utf8' type='hidden' value='true' />
                <div class="form-signup" >
                    
                </div>
                <div class="form-signup clearfix">
                    <fieldset class="form-group">
                        <input type="text" class="form-control form-control-lg" value="" name="firstName" id="firstName"  placeholder="Họ tên*" required >
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="email" class="form-control form-control-lg" value="" name="email" id="email"  placeholder="Email" required="">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="Mật khẩu*" required >
                    </fieldset>
                    <p>Tôi đã đọc và đồng ý với điều khoản sử dụng</p>
                    <fieldset class="form-group">
                        <button type="summit" value="Đăng ký" class="btn btn-lg btn-style btn-style-active col-xs-12">Đăng ký</button>
                    </fieldset>
                    <fieldset class="form-group">
                        <button type="button" class="btn btn-lg btn-style col-xs-12" data-dismiss="modal">Hủy</button>
                    </fieldset>
                </div>
                </form>
                
            </div>
        </div>
    </div>
</div>

        <h1 class="hidden-xs-up">Breshka shoes</h1>

<section id="custom1" class="banner-slider owl-carousel owl-theme">
    
    <div class="item"><a href="#" title="Ảnh banner số 1"><img src='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/slide1.jpg' alt='Ảnh banner số 1' /></a></div>
    
    
    <div class="item"><a href="#" title="Ảnh banner số 2"><img src='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/banner2a.jpg' alt='Ảnh banner số 2' /></a></div>
    
        
</section>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-lg-9 col-lg-push-3">
            <div class="row">

                
                <div class="col-lg-12">
                    <div class="heading">
                        <h2 class="title-head"><a href="#">Sản phẩm mới</a></h2>
                    </div>
                    <div class="owl-sanphammoi owl-carousel owl-theme">
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/converse-all-star" title="Giày tây nâu đỏ thương hiệu Converse All Star">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/2-382751d6-ebd4-477c-83d5-5045a3a23999.jpg?v=1466415099313" alt="Giày tây nâu đỏ thương hiệu Converse All Star">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/converse-all-star" title="Giày tây nâu đỏ thương hiệu Converse All Star">Giày tây nâu đỏ thương hiệu Converse All Star</a></h3>


<div class="sale-flash"> 
38% 
</div>
<div class="product-price">500.000₫</div>
<div class="product-price-old">800.000₫</div>


<form id="product-actions-3062548" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062548" data-variant="4599718"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062548" data-variant="4599718"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599718" /> 
    <a href="/converse-all-star" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/converse-all-star" data-handle="converse-all-star" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 













           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-converse-star-collar-break" title="Giày Converse Star Collar Break">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/zal1.jpg?v=1466482812400" alt="Giày Converse Star Collar Break">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-converse-star-collar-break" title="Giày Converse Star Collar Break">Giày Converse Star Collar Break</a></h3>


<div class="sale-flash"> 
10% 
</div>
<div class="product-price">450.000₫</div>
<div class="product-price-old">500.000₫</div>


<form id="product-actions-3062509" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062509" data-variant="4599650"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062509" data-variant="4599650"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599650" /> 
    <a href="/giay-converse-star-collar-break" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-converse-star-collar-break" data-handle="giay-converse-star-collar-break" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 













           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-converse-star-camo-rubber" title="Giày Converse Star Camo Rubber">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/2521102040-2-3-1.jpg?v=1466482446723" alt="Giày Converse Star Camo Rubber">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-converse-star-camo-rubber" title="Giày Converse Star Camo Rubber">Giày Converse Star Camo Rubber</a></h3>


<div class="sale-flash"> 
14% 
</div>
<div class="product-price">600.000₫</div>
<div class="product-price-old">700.000₫</div>


<form id="product-actions-3062515" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062515" data-variant="4599660"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062515" data-variant="4599660"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599660" /> 
    <a href="/giay-converse-star-camo-rubber" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-converse-star-camo-rubber" data-handle="giay-converse-star-camo-rubber" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-converse-by-john-varvatos" title="Giày Converse by John Varvatos">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/1-267dd86e-37ab-405d-823c-bc2a1916106c.jpg?v=1474014275500" alt="Giày Converse by John Varvatos">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-converse-by-john-varvatos" title="Giày Converse by John Varvatos">Giày Converse by John Varvatos</a></h3>


<div class="sale-flash"> 
38% 
</div>
<div class="product-price">500.000₫</div>
<div class="product-price-old">800.000₫</div>


<form id="product-actions-3062568" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062568" data-variant="4599747"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062568" data-variant="4599747"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599747" /> 
    <a href="/giay-converse-by-john-varvatos" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-converse-by-john-varvatos" data-handle="giay-converse-by-john-varvatos" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 













           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-di-choi-converse-9" title="Giày Converse Star Seasonal">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/2491102020-2-1-1.jpg?v=1466482373977" alt="Giày Converse Star Seasonal">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-di-choi-converse-9" title="Giày Converse Star Seasonal">Giày Converse Star Seasonal</a></h3>


<div class="sale-flash"> 
29% 
</div>
<div class="product-price">400.000₫</div>
<div class="product-price-old">560.000₫</div>


<form id="product-actions-3062521" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062521" data-variant="4610753"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062521" data-variant="4610753"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4610753" /> 
    <a href="/giay-di-choi-converse-9" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-di-choi-converse-9" data-handle="giay-di-choi-converse-9" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-converse-one-star-74-camo" title="Giày Converse One Star '74 Camo">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/4-3ef777ec-4b68-4c4f-82fb-82042b2e1a67.jpg?v=1466414878547" alt="Giày Converse One Star '74 Camo">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-converse-one-star-74-camo" title="Giày Converse One Star '74 Camo">Giày Converse One Star '74 Camo</a></h3>


<div class="sale-flash"> 
38% 
</div>
<div class="product-price">500.000₫</div>
<div class="product-price-old">800.000₫</div>


<form id="product-actions-3062566" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062566" data-variant="4599743"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062566" data-variant="4599743"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599743" /> 
    <a href="/giay-converse-one-star-74-camo" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-converse-one-star-74-camo" data-handle="giay-converse-one-star-74-camo" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-12 hidden-sm-down">
                    <div class="banner-pc">
                        <a href="#">
                            <img src="http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/banner1.jpg" alt="Hàng Mới Về">
                            <div class="name-qc"><span>Hàng Mới Về</span></div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-12">
                    <div class="heading">
                        <h2 class="title-head"><a href="#">Sản phẩm khuyến mại</a></h2>
                    </div>
                    <div class="owl-khuyenmai owl-carousel owl-theme">
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-vai-star-floral-crochet" title="Giày vải Star Floral Crochet">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/7-e9c7f538-1228-42ce-88e6-2757c0777b4c.jpg?v=1466415022193" alt="Giày vải Star Floral Crochet">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-vai-star-floral-crochet" title="Giày vải Star Floral Crochet">Giày vải Star Floral Crochet</a></h3>


<div class="sale-flash"> 
20% 
</div>
<div class="product-price">600.000₫</div>
<div class="product-price-old">750.000₫</div>


<form id="product-actions-3062559" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062559" data-variant="4599732"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062559" data-variant="4599732"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599732" /> 
    <a href="/giay-vai-star-floral-crochet" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-vai-star-floral-crochet" data-handle="giay-vai-star-floral-crochet" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-the-thao-converse-2" title="Sandal nam da dê thật Converse®">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/5-dd2b8dd9-fa4e-4da3-ba38-4c000a0e57ac.jpg?v=1466415377410" alt="Sandal nam da dê thật Converse®">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-the-thao-converse-2" title="Sandal nam da dê thật Converse®">Sandal nam da dê thật Converse®</a></h3>


<div class="sale-flash"> 
16% 
</div>
<div class="product-price">670.000₫</div>
<div class="product-price-old">800.000₫</div>


<form id="product-actions-3062542" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062542" data-variant="4599708"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062542" data-variant="4599708"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599708" /> 
    <a href="/giay-the-thao-converse-2" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-the-thao-converse-2" data-handle="giay-the-thao-converse-2" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-vai-converse-3" title="Sandal xỏ buộc dây">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/9-ea248091-e5bd-4e3f-986a-acfb2db000a4.jpg?v=1466415476577" alt="Sandal xỏ buộc dây">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-vai-converse-3" title="Sandal xỏ buộc dây">Sandal xỏ buộc dây</a></h3>


<div class="sale-flash"> 
12% 
</div>
<div class="product-price">700.000₫</div>
<div class="product-price-old">800.000₫</div>


<form id="product-actions-3062537" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062537" data-variant="4599701"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062537" data-variant="4599701"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599701" /> 
    <a href="/giay-vai-converse-3" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-vai-converse-3" data-handle="giay-vai-converse-3" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-the-thao-converse-4" title="Sandal quai ngang">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/8-6c5927fe-b17e-4d08-9f11-560e542d1ebe.jpg?v=1466415607943" alt="Sandal quai ngang">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-the-thao-converse-4" title="Sandal quai ngang">Sandal quai ngang</a></h3>


<div class="sale-flash"> 
33% 
</div>
<div class="product-price">600.000₫</div>
<div class="product-price-old">900.000₫</div>


<form id="product-actions-3062534" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062534" data-variant="4599696"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062534" data-variant="4599696"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599696" /> 
    <a href="/giay-the-thao-converse-4" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-the-thao-converse-4" data-handle="giay-the-thao-converse-4" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-da-converse-cao-cap" title="Sandal đế cao da dê">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/13-9e7cab69-1232-4602-bf4e-1760d1ab166c.jpg?v=1466415738220" alt="Sandal đế cao da dê">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-da-converse-cao-cap" title="Sandal đế cao da dê">Sandal đế cao da dê</a></h3>


<div class="sale-flash"> 
14% 
</div>
<div class="product-price">1.200.000₫</div>
<div class="product-price-old">1.400.000₫</div>


<form id="product-actions-3062530" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062530" data-variant="4599690"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062530" data-variant="4599690"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599690" /> 
    <a href="/giay-da-converse-cao-cap" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-da-converse-cao-cap" data-handle="giay-da-converse-cao-cap" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-12 hidden-sm-down">
                    <div class="banner-pc">
                        <a href="#">
                            <img src="http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/banner2.jpg" alt="Giày Bé Nam">
                            <div class="name-qc"><span>Giày Bé Nam</span></div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-4">
                    <div class="heading">
                        <h2 class="title-head"><a href="#">Sản phẩm nổi bật</a></h2>
                    </div>
                    <div class="owl-noibat owl-carousel owl-theme">
                                        
                        <div class="item">
                            







                 
                 















           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/converse-all-star" title="Giày tây nâu đỏ thương hiệu Converse All Star">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/2-382751d6-ebd4-477c-83d5-5045a3a23999.jpg?v=1466415099313" alt="Giày tây nâu đỏ thương hiệu Converse All Star">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/converse-all-star" title="Giày tây nâu đỏ thương hiệu Converse All Star">Giày tây nâu đỏ thương hiệu Converse All Star</a></h3>


<div class="sale-flash"> 
38% 
</div>
<div class="product-price">500.000₫</div>
<div class="product-price-old">800.000₫</div>


<form id="product-actions-3062548" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062548" data-variant="4599718"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062548" data-variant="4599718"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599718" /> 
    <a href="/converse-all-star" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/converse-all-star" data-handle="converse-all-star" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 













           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-converse-star-collar-break" title="Giày Converse Star Collar Break">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/zal1.jpg?v=1466482812400" alt="Giày Converse Star Collar Break">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-converse-star-collar-break" title="Giày Converse Star Collar Break">Giày Converse Star Collar Break</a></h3>


<div class="sale-flash"> 
10% 
</div>
<div class="product-price">450.000₫</div>
<div class="product-price-old">500.000₫</div>


<form id="product-actions-3062509" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062509" data-variant="4599650"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062509" data-variant="4599650"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599650" /> 
    <a href="/giay-converse-star-collar-break" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-converse-star-collar-break" data-handle="giay-converse-star-collar-break" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                                        
                        <div class="item">
                            







                 
                 













           
           <div class="product-box">
<div class="product-thumbnail">
    <a href="/giay-converse-star-camo-rubber" title="Giày Converse Star Camo Rubber">
        
        <img src="http://bizweb.dktcdn.net/thumb/large/100/091/133/products/2521102040-2-3-1.jpg?v=1466482446723" alt="Giày Converse Star Camo Rubber">
        
        
    </a>
</div>
<h3 class="product-name"><a href="/giay-converse-star-camo-rubber" title="Giày Converse Star Camo Rubber">Giày Converse Star Camo Rubber</a></h3>


<div class="sale-flash"> 
14% 
</div>
<div class="product-price">600.000₫</div>
<div class="product-price-old">700.000₫</div>


<form id="product-actions-3062515" action="/cart/add" method="post" enctype="multipart/form-data">
    
    <a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062515" data-variant="4599660"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Thêm vào yêu thích</span></a>
    <a class="iWishAdded iwishAddWrapper iWishHidden" href="javascript:;" data-customer-id="0" data-product="3062515" data-variant="4599660"><span class="iwishAddChild iwishAddBorder">
        </span><span class="iwishAddChild">Đã yêu thích</span></a>

    
    <input type="hidden" name="variantId" value="4599660" /> 
    <a href="/giay-converse-star-camo-rubber" class="btn btn-style add_to_cart">Thêm vào giỏ hàng</a>
    
    <a href="/giay-converse-star-camo-rubber" data-handle="giay-converse-star-camo-rubber" class="btn btn-style quick-view">
        <i class="fa fa-eye"></i>
    </a>
</form>
</div>
                        </div>
                        
                    </div>
                </div>
                
                
                <div class="col-lg-8">
                    <div class="heading">
                        <h2 class="title-head"><a href="san-pham-noi-bat">Sản phẩm mua nhiều</a></h2>
                    </div>
                    <div class="owl-muanhieu slide-list-pro2 owl-carousel owl-theme">
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                        
                        
                        
                        <div class="row">      
                            
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/converse-all-star"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/2-382751d6-ebd4-477c-83d5-5045a3a23999.jpg?v=1466415099313" alt="Giày tây nâu đỏ thương hiệu Converse All Star"></a>
                                </div>
                                <h3 class="product-item-name"><a href="/converse-all-star">Giày tây nâu đỏ thương hiệu Converse All Star</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">500.000₫</span>
                                    <span class="product-item-price-old">800.000₫</span>
                                </div>
                                
                                <a href="/converse-all-star" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                            
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                        
                        
                        
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/giay-converse-star-collar-break"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/zal1.jpg?v=1466482812400" alt="Giày Converse Star Collar Break"></a>
                                </div>
                                <h3 class="product-item-name"><a href="/giay-converse-star-collar-break">Giày Converse Star Collar Break</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">450.000₫</span>
                                    <span class="product-item-price-old">500.000₫</span>
                                </div>
                                
                                <a href="/giay-converse-star-collar-break" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                            
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                        
                        
                        
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/giay-converse-star-camo-rubber"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/2521102040-2-3-1.jpg?v=1466482446723" alt="Giày Converse Star Camo Rubber"></a>
                                </div>
                                <h3 class="product-item-name"><a href="/giay-converse-star-camo-rubber">Giày Converse Star Camo Rubber</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">600.000₫</span>
                                    <span class="product-item-price-old">700.000₫</span>
                                </div>
                                
                                <a href="/giay-converse-star-camo-rubber" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                                                   
                        </div> 
                        
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                        
                        
                        
                        <div class="row">      
                            
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/giay-converse-by-john-varvatos"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/1-267dd86e-37ab-405d-823c-bc2a1916106c.jpg?v=1474014275500" alt="Giày Converse by John Varvatos"></a>
                                </div>
                                <h3 class="product-item-name"><a href="/giay-converse-by-john-varvatos">Giày Converse by John Varvatos</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">500.000₫</span>
                                    <span class="product-item-price-old">800.000₫</span>
                                </div>
                                
                                <a href="/giay-converse-by-john-varvatos" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                            
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                        
                        
                        
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/giay-di-choi-converse-9"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/2491102020-2-1-1.jpg?v=1466482373977" alt="Giày Converse Star Seasonal"></a>
                                </div>
                                <h3 class="product-item-name"><a href="/giay-di-choi-converse-9">Giày Converse Star Seasonal</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">400.000₫</span>
                                    <span class="product-item-price-old">560.000₫</span>
                                </div>
                                
                                <a href="/giay-di-choi-converse-9" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                            
                        
                        
                        
                        
                        
                        
                        
                        
                         
                        
                        
                        
                        
                        
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/giay-converse-one-star-74-camo"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/4-3ef777ec-4b68-4c4f-82fb-82042b2e1a67.jpg?v=1466414878547" alt="Giày Converse One Star '74 Camo"></a>
                                </div>
                                <h3 class="product-item-name"><a href="/giay-converse-one-star-74-camo">Giày Converse One Star '74 Camo</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">500.000₫</span>
                                    <span class="product-item-price-old">800.000₫</span>
                                </div>
                                
                                <a href="/giay-converse-one-star-74-camo" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                                                   
                        </div> 
                        
                        
                    </div>
                </div>
                


            </div>
        </div>
        <div class="col-xs-12 col-lg-3 col-lg-pull-9">

            <aside class="box-aside hidden-md-down">
                <h2 class="title-box">
                    Danh mục sản phẩm
                </h2>
                <nav class="nav-list-dm">
                    <ul class="nav navbar-pills">
                        
                        
                        <li class="nav-item ">
                            <a href="/ssandal" class="nav-link">Sandal</a>
                            <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu">
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/ssandal">Sandal xỏ ngón</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/ssandal">Sandal thường</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Sandal Mỹ</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Sandal Việt Nam</a>
                                </li>
                                
                                
                            </ul>
                        </li>
                        
                        
                        
                        <li class="nav-item ">
                            <a href="/giay-tay" class="nav-link">Giày tây</a>
                            <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu">
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày tây cao cấp</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày tây nhập khẩu</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày tây Trung Quốc</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày tây 2017</a>
                                </li>
                                
                                
                            </ul>
                        </li>
                        
                        
                        
                        <li class="nav-item ">
                            <a href="/giay-casual" class="nav-link">Giày Casual</a>
                            <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu">
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giay casual mẫu mới</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giay casual đặc biệt</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giay casual 2017</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giay casual hot</a>
                                </li>
                                
                                
                            </ul>
                        </li>
                        
                        
                        
                        <li class="nav-item ">
                            <a href="/giay-luoi" class="nav-link">Giày lười</a>
                            <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu">
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày lười cao cấp</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày lười nhập khẩu</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày lười China</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày lười 2017</a>
                                </li>
                                
                                
                            </ul>
                        </li>
                        
                        
                        
                        <li class="nav-item ">
                            <a href="/giay-co-cao" class="nav-link">Giày cao cổ</a>
                            <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu">
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày cao cổ nam</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày cao cổ nữ</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày cao cổ cao cấp</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày cao cổ 2017</a>
                                </li>
                                
                                
                            </ul>
                        </li>
                        
                        
                        
                        <li class="nav-item ">
                            <a href="/giay-di-choi" class="nav-link">Giày đi chơi</a>
                            <i class="fa fa-angle-right" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu">
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày đi chơi giá rẻ</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày đá bóng</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày bóng bàn</a>
                                </li>
                                
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/collections/all">Giày thể thao</a>
                                </li>
                                
                                
                            </ul>
                        </li>
                        
                        
                    </ul>
                </nav>
            </aside>

            
            <aside class="box-aside hidden-md-down">
                <h2 class="title-box">
                    <a href="san-pham-noi-bat">Sản phẩm bán chạy</a>
                </h2>
                <div class="list-wrap row">
                    
                    
                    
                    
                    
                    
                    
                    
                     
                    
                    
                    
                    <div class="product-item2 col-xs-12">
                        <div class="product-item-thumbnail">
                            <a href="/converse-all-star"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/2-382751d6-ebd4-477c-83d5-5045a3a23999.jpg?v=1466415099313" alt="Giày tây nâu đỏ thương hiệu Converse All Star"></a>
                        </div>
                        <h3 class="product-item-name"><a href="/converse-all-star">Giày tây nâu đỏ thương hiệu Converse All Star</a></h3>
                        
                        
                        <div>
                            <span class="product-item-price">500.000₫</span>
                            <span class="product-item-price-old">800.000₫</span>
                        </div>
                        
                        <a href="/converse-all-star" class="btn btn-style">MUA HÀNG</a>
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                     
                    
                    
                    
                    <div class="product-item2 col-xs-12">
                        <div class="product-item-thumbnail">
                            <a href="/giay-converse-star-collar-break"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/zal1.jpg?v=1466482812400" alt="Giày Converse Star Collar Break"></a>
                        </div>
                        <h3 class="product-item-name"><a href="/giay-converse-star-collar-break">Giày Converse Star Collar Break</a></h3>
                        
                        
                        <div>
                            <span class="product-item-price">450.000₫</span>
                            <span class="product-item-price-old">500.000₫</span>
                        </div>
                        
                        <a href="/giay-converse-star-collar-break" class="btn btn-style">MUA HÀNG</a>
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                     
                    
                    
                    
                    <div class="product-item2 col-xs-12">
                        <div class="product-item-thumbnail">
                            <a href="/giay-converse-star-camo-rubber"><img src="http://bizweb.dktcdn.net/thumb/small/100/091/133/products/2521102040-2-3-1.jpg?v=1466482446723" alt="Giày Converse Star Camo Rubber"></a>
                        </div>
                        <h3 class="product-item-name"><a href="/giay-converse-star-camo-rubber">Giày Converse Star Camo Rubber</a></h3>
                        
                        
                        <div>
                            <span class="product-item-price">600.000₫</span>
                            <span class="product-item-price-old">700.000₫</span>
                        </div>
                        
                        <a href="/giay-converse-star-camo-rubber" class="btn btn-style">MUA HÀNG</a>
                        
                    </div>
                    
                </div>
                <a class="btn-link-style" href="san-pham-noi-bat">Xem thêm sản phẩm</a>
            </aside>
            
            <aside class="box-aside hidden-md-down">
                <div class="feature-l">
                    <ul class="list-group">
                        <li class="media">
                            <div class="media-left media-middle">
                                <img class="media-object" src="http://bizweb.dktcdn.net/thumb/thumb/100/091/133/themes/517838/assets/feature1.png" alt="feature1">
                            </div>
                            <div class="media-body">
                                <span class="feature-title">Hỗ trợ trực tuyến</span>
                                <p>1900.6750 </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
            
            <aside class="box-aside hidden-md-down">
                <div class="banner-pc">
                    <a href="#">
                        <img src="http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/banner-qc.jpg" alt="Giày hót">
                        <div class="name-qc">
                            <span>
                                
                                <img src="http://bizweb.dktcdn.net/thumb/medium/100/091/133/themes/517838/assets/text-banner.png" alt="Hót giày">
                                
                                
                            </span>
                        </div>
                    </a>
                </div>
            </aside>
            
            
            <aside class="box-aside">
                <div class="head-box">
                    <h2 class="title-box">
                        <a href="#">Tin tức</a>
                    </h2>
                </div>
                <div class="list-blogs owl-carousel owl-theme">
                    
                    <div class="blog-item">
                        <div class="blog-item-thumbnail">
                            <a href="/chac-chan-khong-the-co-sao-han-nao-giong-chi-pu-bang-ca-si-nay">
                                
                                <img class="img-fluid" src="http://bizweb.dktcdn.net/thumb/medium/100/091/133/articles/5.jpg?v=1470734234983" alt="Chắc chắn không thể có sao Hàn nào giống Chi Pu bằng ca sĩ này">
                                
                            </a>
                        </div>
                        <h3 class="blog-item-name"><a href="/chac-chan-khong-the-co-sao-han-nao-giong-chi-pu-bang-ca-si-nay">Chắc chắn không thể có sao Hàn nào giống Chi Pu bằng ca sĩ này</a></h3>
                        <div class="postby">
                            <div class="pull-xs-left mr-left-15"><i class="fa fa-user" aria-hidden="true"></i> Nguyễn Ngọc Dũng </div>
                            <div><i class="fa fa-clock-o" aria-hidden="true"></i> 28, Tháng Sáu, 2016</div>
                        </div>
                        <p class="blog-item-summary"> Là một trong những sao trẻ 9X được yêu thích hàng đầu showbiz Việt, Chi Pu không chỉ năng nổ trong nhiều lĩnh vực mà còn sở hữu khuôn...</p>
                    </div>
                    
                    <div class="blog-item">
                        <div class="blog-item-thumbnail">
                            <a href="/xu-huong-thoi-trang-vintage-du-nhap-vao-viet-nam-1">
                                
                                <img class="img-fluid" src="http://bizweb.dktcdn.net/thumb/medium/100/091/133/articles/2.png?v=1470734256293" alt="Ngắm những mẫu giày mới lên kệ cuối tháng 3/2016">
                                
                            </a>
                        </div>
                        <h3 class="blog-item-name"><a href="/xu-huong-thoi-trang-vintage-du-nhap-vao-viet-nam-1">Ngắm những mẫu giày mới lên kệ cuối tháng 3/2016</a></h3>
                        <div class="postby">
                            <div class="pull-xs-left mr-left-15"><i class="fa fa-user" aria-hidden="true"></i> Nguyễn Ngọc Dũng </div>
                            <div><i class="fa fa-clock-o" aria-hidden="true"></i> 07, Tháng Sáu, 2016</div>
                        </div>
                        <p class="blog-item-summary"> Ngắm những mẫu giày mới lên kệ trong ngày cuối tháng 3/2016, tiếp tục là những mẫu giày thời trang mới đầy phong cách trong dịp đầu năm 2016....</p>
                    </div>
                    
                </div>
            </aside>
            
        </div>
    </div>
</div>


        <footer id="footer">
    <div class="container" id="accordion" role="tablist">
        <div class="row">
            <div class="col-lg-3">
                <div class="widget-item info-contact">
                    <div class="logo">
                        <a href="/"><img src="http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/logof.png" alt="Breshka shoes" title="Breshka shoes"></a>
                    </div>
                    <!-- End .widget-title -->
                    <ul class="widget-menu">
                        <li><i class="fa fa-map-marker color-x" aria-hidden="true"></i> Tầng 4, Tòa nhà Hanoi Group 
<br>
Số 442 Đội Cấn, P. Cống Vị,
<br>
Q. Ba Đình, Hà Nội</li>
                        <li><i class="fa fa-phone color-x" aria-hidden="true"></i> (04) 7308 6880 - (04) 3786 8904</li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="support@bizweb.vn">support@bizweb.vn</a> </li>
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
            <div class="col-lg-3 hidden-md-down">
                <div class="widget-item">
                    <h4 class="widget-title">Về chúng tôi</h4>
                    <!-- End .widget-title -->
                    <ul class="widget-menu">
                        
                        <li><a href="/gioi-thieu">Tầm nhìn & giá trị cốt lõi</a></li>
                        
                        <li><a href="/">Ý kiến khách hàng</a></li>
                        
                        <li><a href="/">Báo chí nói về chúng tôi</a></li>
                        
                        <li><a href="/die-u-khoa-n-di-ch-vu">Quy định sử dụng</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách bảo mật</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách bảo mật</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
            
            <div class="col-lg-3 hidden-md-down">
                <div class="widget-item">
                    <h4 class="widget-title">Dịch vụ</h4>
                    <!-- End .widget-title -->
                    <ul class="widget-menu">
                        
                        <li><a href="/huo-ng-da-n">Đăng ký tên miền</a></li>
                        
                        <li><a href="/huo-ng-da-n">Tên miền miễn phí</a></li>
                        
                        <li><a href="/huo-ng-da-n">Dịch vụ email</a></li>
                        
                        <li><a href="/">Thiết kế đồ họa</a></li>
                        
                        <li><a href="/">Quảng cáo Google Adwords</a></li>
                        
                        <li><a href="/">Phần mền quản lý bán hàng</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
            
            <div class="col-lg-3 hidden-md-down">
                <div class="widget-item">
                    <h4 class="widget-title">Liên hệ</h4>
                    <!-- End .widget-title -->
                    <ul class="widget-menu">
                        
                        <li><a href="/tin-tuc">Tin tức nổi bật</a></li>
                        
                        <li><a href="/account/register">Đăng ký thành viên</a></li>
                        
                        <li><a href="/">Thông tin thành viên</a></li>
                        
                        <li><a href="/huo-ng-da-n">Hướng dẫn mua hàng</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách vận chuyển</a></li>
                        
                        <li><a href="/">Phần mền quản lý bán hàng</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            


            
            
            <div class="col-xs-12 hidden-lg-up">
                <div class="widget-item panel">
                    <a class="widget-title" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Về chúng tôi<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <!-- End .widget-title -->
                    <ul class="widget-menu panel-collapse collapse in" id="collapseOne">
                        
                        <li><a href="/gioi-thieu">Tầm nhìn & giá trị cốt lõi</a></li>
                        
                        <li><a href="/">Ý kiến khách hàng</a></li>
                        
                        <li><a href="/">Báo chí nói về chúng tôi</a></li>
                        
                        <li><a href="/die-u-khoa-n-di-ch-vu">Quy định sử dụng</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách bảo mật</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách bảo mật</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
            
            <div class="col-xs-12 hidden-lg-up">
                <div class="widget-item panel">
                    <a class="widget-title" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Dịch vụ<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <!-- End .widget-title -->
                    <ul class="widget-menu panel-collapse collapse" id="collapseTwo">
                        
                        <li><a href="/huo-ng-da-n">Đăng ký tên miền</a></li>
                        
                        <li><a href="/huo-ng-da-n">Tên miền miễn phí</a></li>
                        
                        <li><a href="/huo-ng-da-n">Dịch vụ email</a></li>
                        
                        <li><a href="/">Thiết kế đồ họa</a></li>
                        
                        <li><a href="/">Quảng cáo Google Adwords</a></li>
                        
                        <li><a href="/">Phần mền quản lý bán hàng</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
            
            <div class="col-xs-12 hidden-lg-up">
                <div class="widget-item panel">
                    <a class="widget-title" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Liên hệ<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <!-- End .widget-title -->
                    <ul class="widget-menu panel-collapse collapse" id="collapseThree">
                        
                        <li><a href="/tin-tuc">Tin tức nổi bật</a></li>
                        
                        <li><a href="/account/register">Đăng ký thành viên</a></li>
                        
                        <li><a href="/">Thông tin thành viên</a></li>
                        
                        <li><a href="/huo-ng-da-n">Hướng dẫn mua hàng</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách vận chuyển</a></li>
                        
                        <li><a href="/">Phần mền quản lý bán hàng</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <hr class="hidden-md-down hr-an-footer">
                </div>
                <div class="col-xs-12 col-lg-9 text-xs-left">
                    <span class="info-website">
                        <span>© Bản quyền thuộc về Avent Team</span>
                        <span class="hidden-xs-down"> | </span>
                        <span>Cung cấp bởi <a href="https://www.bizweb.vn/?utm_source=site-khach-hang&utm_campaign=referral_bizweb&utm_medium=footer&utm_content=cung-cap-boi-bizweb" target="_blank" title="Bizweb">Bizweb</a></span>
                    </span>
                </div>
                <div class="col-xs-12 col-lg-3 text-xs-left">
                    <div class="label-jssocials">Follow us</div>
                    <div class="jssocials" id="share"></div>
                </div>
            </div>
        </div>
    </div>
</footer>

        <!-- ================= Google Fonts ================== -->
    <link href='////fonts.googleapis.com/css?family=Roboto:400,900,500&subset=vietnamese' rel='stylesheet' type='text/css' />
    
    <!-- Font Awesome CSS -->
    <link href='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/font-awesome.css' rel='stylesheet' type='text/css' />
    <link href='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/iwish.css' rel='stylesheet' type='text/css' />





        <!-- Bizweb javascript -->
        <script src='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/option-selectors.js' type='text/javascript'></script>
        <script src='http://bizweb.dktcdn.net/assets/themes_support/api.jquery.js?4' type='text/javascript'></script> 



        
    <script src='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/iwish.js' type='text/javascript'></script>

        <script src='http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/owl.carousel.min.js' type='text/javascript'></script>
        <script>

    Bizweb.updateCartFromForm = function(cart, cart_summary_id, cart_count_id) {
        if ((typeof cart_summary_id) === 'string') {
            var cart_summary = jQuery(cart_summary_id);
            if (cart_summary.length) {
                // Start from scratch.
                cart_summary.empty();
                // Pull it all out.        
                jQuery.each(cart, function(key, value) {
                    if (key === 'items') {

                        var table = jQuery(cart_summary_id);           
                        if (value.length) {   
                            jQuery('<ul class="list-item-cart"></ul>').appendTo(table);
                            jQuery.each(value, function(i, item) {  

                                var src = item.image;
                                if(src == null){
                                    src = "http:http://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif";
                                }
                                var buttonQty = "";
                                if(item.quantity == '1'){
                                    buttonQty = 'disabled';
                                }else{
                                    buttonQty = '';
                                }
                                jQuery('<li class="item productid-' + item.variant_id +'"><a class="product-image" href="' + item.url + '" title="' + item.name + '">'
                                       + '<img alt="'+  item.name  + '" src="' + src +  '"width="'+ '80' +'"\></a>'
                                       + '<div class="detail-item"><div class="product-details"> <a href="javascript:;" data-id="'+ item.variant_id +'" title="Xóa" class="remove-item-cart fa fa-times-circle">&nbsp;</a>'
                                       + '<p class="product-name"> <a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></p></div>'
                                       + '<div class="product-details-bottom"><span class="price">' + Bizweb.formatMoney(item.price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</span>'
                                       + '<div class="quantity-select"><label>Số lượng: '+item.quantity+'</lable></div></div></li>').appendTo(table.children('.list-item-cart'));
                            }); 
                            jQuery('<div><div class="top-subtotal">Tổng cộng: <span class="price">' + Bizweb.formatMoney(cart.total_price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</span></div></div>').appendTo(table);
                            jQuery('<div><div class="actions"><a href="/checkout" class="btn-checkout"><span>Tiến hành thanh toán</span></a></div></div>').appendTo(table);
                        }
                        else {
                            jQuery('<div class="no-item"><p>Không có sản phẩm nào trong giỏ hàng.</p></div>').appendTo(table);

                        }
                    }
                });
            }
        }
        updateCartDesc(cart);
        var numInput = document.querySelector('#cart-sidebar input.input-text');
        if (numInput != null){
            // Listen for input event on numInput.
            numInput.addEventListener('input', function(){
                // Let's match only digits.
                var num = this.value.match(/^\d+$/);
                if (num == 0) {
                    // If we have no match, value will be empty.
                    this.value = 1;
                }
                if (num === null) {
                    // If we have no match, value will be empty.
                    this.value = "";
                }
            }, false)
        }
    }

    Bizweb.updateCartPageForm = function(cart, cart_summary_id, cart_count_id) {
        if ((typeof cart_summary_id) === 'string') {
            var cart_summary = jQuery(cart_summary_id);
            if (cart_summary.length) {
                // Start from scratch.
                cart_summary.empty();
                // Pull it all out.        
                jQuery.each(cart, function(key, value) {
                    if (key === 'items') {
                        var table = jQuery(cart_summary_id);           
                        if (value.length) {  

                            var pageCart = '<div class="cart page_cart hidden-xs-down">'
                            + '<form action="/cart" method="post" novalidate><div class="bg-scroll"><div class="cart-thead">'
                            + '<div style="width: 17%">Ảnh sản phẩm</div><div style="width: 33%"><span class="nobr">Tên sản phẩm</span></div><div style="width: 15%" class="a-center"><span class="nobr">Đơn giá</span></div><div style="width: 14%" class="a-center">Số lượng</div><div style="width: 15%" class="a-center">Thành tiền</div><div style="width: 6%">Xoá</div></div>'
                            + '<div class="cart-tbody"></div></div></form></div>'; 
                            var pageCartCheckout = '<div class="cart-collaterals cart_submit row"><div class="totals col-sm-6 col-md-5 col-xs-12 col-md-offset-7"><div class="totals"><div class="inner">'
                            + '<table class="table shopping-cart-table-total" id="shopping-cart-totals-table"><colgroup><col><col></colgroup>'
                            + '<tfoot><tr><td colspan="1" class="a-left"><strong>Tổng tiền</strong></td><td class="a-right"><strong><span class="totals_price price">' + Bizweb.formatMoney(cart.total_price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</span></strong></td></tr></tfoot></table>'
                            + '<ul class="checkout"><li><button class="button btn-proceed-checkout" title="Tiến hành đặt hàng" type="button" onclick="window.location.href=\'/checkout\'"><span>Tiến hành đặt hàng</span></button></li>'
                            + '</ul></div></div></div></div>';
                            jQuery(pageCart).appendTo(table);
                            jQuery.each(value, function(i, item) {
                                var buttonQty = "";
                                if(item.quantity == '1'){
                                    buttonQty = 'disabled';
                                }else{
                                    buttonQty = '';
                                }
                                var link_img1 = Bizweb.resizeImage(item.image, 'compact');
                                if(link_img1=="null" || link_img1 =='' || link_img1 ==null){
                                    link_img1 = 'https:http://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif';
                                }
                                var pageCartItem = '<div class="item-cart productid-' + item.variant_id +'"><div style="width: 17%" class="image"><a class="product-image" title="' + item.name + '" href="' + item.url + '"><img width="75" height="auto" alt="' + item.name + '" src="' + link_img1 +  '"></a></div>'
                                + '<div style="width: 33%" class="a-center"><h2 class="product-name"> <a href="' + item.url + '">' + item.title + '</a> </h2><span class="variant-title">' + item.variant_title + '</span></div><div style="width: 15%" class="a-center"><span class="item-price"> <span class="price">' + Bizweb.formatMoney(item.price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</span></span></div>'
                                + '<div style="width: 14%" class="a-center"><div class="input_qty_pr"><input class="pro_id" type="hidden" name="variantId" value="'+ item.variant_id +'"><button onClick="var result = document.getElementById(\'qtyItem'+ item.variant_id +'\'); var qtyItem'+ item.variant_id +' = result.value; if( !isNaN( qtyItem'+ item.variant_id +' ) &amp;&amp; qtyItem'+ item.variant_id +' &gt; 1 ) result.value--;return false;" ' + buttonQty + ' class="reduced_pop items-count btn-minus" type="button">–</button><input type="text" maxlength="12" min="0" class="input-text number-sidebar input_pop input_pop qtyItem'+ item.variant_id +'" id="qtyItem'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'"><button onClick="var result = document.getElementById(\'qtyItem'+ item.variant_id +'\'); var qtyItem'+ item.variant_id +' = result.value; if( !isNaN( qtyItem'+ item.variant_id +' )) result.value++;return false;" class="increase_pop items-count btn-plus" type="button">+</button></div></div>'
                                + '<div style="width: 15%" class="a-center"><span class="cart-price"> <span class="price">'+ Bizweb.formatMoney(item.price * item.quantity, "{{$amount_no_decimals_with_comma_separator}}₫") +'</span> </span></div>'
                                + '<div style="width: 6%"><a class="button remove-item remove-item-cart" title="Xóa" href="javascript:;" data-id="'+ item.variant_id +'"><span><span>Xóa</span></span></a></div></div>';
                                jQuery(pageCartItem).appendTo(table.find('.cart-tbody'));
                                if(item.variant_title == 'Default Title'){
                                    $('.variant-title').hide();
                                }
                            }); 
                            jQuery(pageCartCheckout).appendTo(table.children('.cart'));
                        }else {
                            jQuery('<p class="hidden-xs-down">Không có sản phẩm nào trong giỏ hàng. Quay lại <a href="/" style="color:;">cửa hàng</a> để tiếp tục mua sắm.</p>').appendTo(table);
                            jQuery('.cart_desktop_page').css('min-height', 'auto');
                        }
                    }
                });
            }
        }
        updateCartDesc(cart);
        jQuery('#wait').hide();
    }
    Bizweb.updateCartPopupForm = function(cart, cart_summary_id, cart_count_id) {

        if ((typeof cart_summary_id) === 'string') {
            var cart_summary = jQuery(cart_summary_id);
            if (cart_summary.length) {
                // Start from scratch.
                cart_summary.empty();
                // Pull it all out.        
                jQuery.each(cart, function(key, value) {
                    if (key === 'items') {
                        var table = jQuery(cart_summary_id);           
                        if (value.length) { 
                            jQuery.each(value, function(i, item) {
                                var src = item.image;
                                if(src == null){
                                    src = "http:http://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif";
                                }
                                var buttonQty = "";
                                if(item.quantity == '1'){
                                    buttonQty = 'disabled';
                                }else{
                                    buttonQty = '';
                                }
                                var pageCartItem = '<div class="item-popup productid-' + item.variant_id +'"><div style="width: 55%;" class="text-left"><div class="item-image">'
                                + '<a class="product-image" href="' + item.url + '" title="' + item.name + '"><img alt="'+  item.name  + '" src="' + src +  '"width="'+ '80' +'"\></a>'
                                + '</div><div class="item-info"><p class="item-name"><a href="' + item.url + '" title="' + item.name + '">' + item.title + ' - ' + item.variant_title + '</a></p>'                              
                                + '<p class="item-remove"><a href="javascript:;" class="remove-item-cart" title="Xóa" data-id="'+ item.variant_id +'"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p><p class="addpass" style="color:#fff;">'+ item.variant_id +'</p></div></div>'
                                + '<div style="width: 15%;" class="text-right"><div class="item-price"><span class="price">' + Bizweb.formatMoney(item.price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</span>'
                                + '</div></div><div style="width: 15%;" class="text-center"><div class="fixab"><input class="pro_id" type="hidden" name="variantId" value="'+ item.variant_id +'">'
                                + '<button onClick="var result = document.getElementById(\'qtyItem'+ item.variant_id +'\'); var qtyItem'+ item.variant_id +' = result.value; if( !isNaN( qtyItem'+ item.variant_id +' ) &amp;&amp; qtyItem'+ item.variant_id +' &gt; 1 ) result.value--;return false;" ' + buttonQty + ' class="reduced items-count btn-minus" type="button"><i class="fa fa-caret-down"></i></button>'
                                + '<input type="text" maxlength="12" min="0" class="input-text number-sidebar qtyItem'+ item.variant_id +'" id="qtyItem'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'">'
                                + '<button onClick="var result = document.getElementById(\'qtyItem'+ item.variant_id +'\'); var qtyItem'+ item.variant_id +' = result.value; if( !isNaN( qtyItem'+ item.variant_id +' )) result.value++;return false;" class="increase items-count btn-plus" type="button"><i class="fa fa-caret-up"></i></button></div></div>'
                                + '<div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">'+ Bizweb.formatMoney(item.price * item.quantity, "{{$amount_no_decimals_with_comma_separator}}₫") +'</span> </span></div></div>';
                                jQuery(pageCartItem).appendTo(table);
                                if(item.variant_title == 'Default Title'){
                                    $('.variant-title-popup').hide();
                                }
                                $('.link_product').text();
                            }); 
                        }
                    }
                });
            }
        }
        jQuery('.total-price').html(Bizweb.formatMoney(cart.total_price, "{{$amount_no_decimals_with_comma_separator}}₫"));
        updateCartDesc(cart);
    }
    Bizweb.updateCartPageFormMobile = function(cart, cart_summary_id, cart_count_id) {
        if ((typeof cart_summary_id) === 'string') {
            var cart_summary = jQuery(cart_summary_id);
            if (cart_summary.length) {
                // Start from scratch.
                cart_summary.empty();
                // Pull it all out.        
                jQuery.each(cart, function(key, value) {
                    if (key === 'items') {

                        var table = jQuery(cart_summary_id);           
                        if (value.length) {   
                            jQuery('<div class="cart_page_mobile content-product-list"></div>').appendTo(table);
                            jQuery.each(value, function(i, item) {
                                if( item.image != null){
                                var src = Bizweb.resizeImage(item.image, 'small');
                                }else{
                                var src = "https:http://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif";
                                }
                                jQuery('<div class="item-product item productid-' + item.variant_id +' "><div class="item-product-cart-mobile"><a href="' + item.url + '">  <a class="product-images1" href=""  title="' + item.name + '"><img width="80" height="150" alt="" src="' + src +  '" alt="' + item.name + '"></a></a></div>'
                                       + '<div class="title-product-cart-mobile"><h3><a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></h3><p>Giá: <span>' + Bizweb.formatMoney(item.price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</span></p></div>'
                                       + '<div class="select-item-qty-mobile"><div class="txt_center">'
                                       + '<input class="pro_id" type="hidden" name="variantId" value="'+ item.variant_id +'"><button onClick="var result = document.getElementById(\'qtyMobile'+ item.variant_id +'\'); var qtyMobile'+ item.variant_id +' = result.value; if( !isNaN( qtyMobile'+ item.variant_id +' ) &amp;&amp; qtyMobile'+ item.variant_id +' &gt; 0 ) result.value--;return false;" class="reduced items-count btn-minus" type="button">–</button><input type="text" maxlength="12" min="0" class="input-text number-sidebar qtyMobile'+ item.variant_id +'" id="qtyMobile'+ item.variant_id +'" name="Lines" id="updates_'+ item.variant_id +'" size="4" value="'+ item.quantity +'"><button onClick="var result = document.getElementById(\'qtyMobile'+ item.variant_id +'\'); var qtyMobile'+ item.variant_id +' = result.value; if( !isNaN( qtyMobile'+ item.variant_id +' )) result.value++;return false;" class="increase items-count btn-plus" type="button">+</button></div>'
                                       + '<a class="button remove-item remove-item-cart" href="javascript:;" data-id="'+ item.variant_id +'">Xoá</a></div>').appendTo(table.children('.content-product-list'));

                                });
                            
                            jQuery('<div class="header-cart-price" style=""><div class="title-cart "><h3 class="text-xs-left">Tổng tiền</h3><a class="text-xs-right totals_price_mobile">' + Bizweb.formatMoney(cart.total_price, "{{$amount_no_decimals_with_comma_separator}}₫") + '</a></div>'
                                    + '<div class="checkout"><button class="btn-proceed-checkout-mobile" title="Tiến hành thanh toán" type="button" onclick="window.location.href=\'/checkout\'">'
                                    + '<span>Tiến hành thanh toán</span></button></div></div>').appendTo(table);
                        }
                        
                    }
                });
            }
        }
        updateCartDesc(cart);
    }
    
    
    function updateCartDesc(data){
        var $cartPrice = Bizweb.formatMoney(data.total_price, "{{$amount_no_decimals_with_comma_separator}}₫"),
            $cartMobile = $('#header .cart-mobile .quantity-product'),
            $cartDesktop = $('.count_item_pr'),
            $cartDesktopList = $('.cart-counter-list'),
            $cartPopup = $('.cart-popup-count');

        switch(data.item_count){
            case 0:
                $cartMobile.text('0');
                $cartDesktop.text('0');
                $cartDesktopList.text('0');
                $cartPopup.text('0');

                break;
            case 1:
                $cartMobile.text('1');
                $cartDesktop.text('1');
                $cartDesktopList.text('1');
                $cartPopup.text('1');

                break;
            default:
                $cartMobile.text(data.item_count);
                $cartDesktop.text(data.item_count);
                $cartDesktopList.text(data.item_count);
                $cartPopup.text(data.item_count);

                break;
        }
        $('.top-cart-content .top-subtotal .price, aside.sidebar .block-cart .subtotal .price, .popup-total .total-price').html($cartPrice);
        $('.popup-total .total-price').html($cartPrice);
        $('.shopping-cart-table-total .totals_price').html($cartPrice);
        $('.header-cart-price .totals_price_mobile').html($cartPrice);
        $('.cartCount').html(data.item_count);
    }
    
    Bizweb.onCartUpdate = function(cart) {
        Bizweb.updateCartFromForm(cart, '.mini-products-list');
        Bizweb.updateCartPopupForm(cart, '#popup-cart-desktop .tbody-popup');
        
    };
    Bizweb.onCartUpdateClick = function(cart, variantId) {
        jQuery.each(cart, function(key, value) {
            if (key === 'items') {    
                jQuery.each(value, function(i, item) {  
                    if(item.variant_id == variantId){
                        $('.productid-'+variantId).find('.cart-price span.price').html(Bizweb.formatMoney(item.price * item.quantity, "{{$amount_no_decimals_with_comma_separator}}₫"));
                        $('.productid-'+variantId).find('.items-count').prop("disabled", false);
                        $('.productid-'+variantId).find('.number-sidebar').prop("disabled", false);
                        $('.productid-'+variantId +' .number-sidebar').val(item.quantity);
                        if(item.quantity == '1'){
                            $('.productid-'+variantId).find('.items-count.btn-minus').prop("disabled", true);
                        }
                    }
                }); 
            }
        });
        updateCartDesc(cart);
    }
    Bizweb.onCartRemoveClick = function(cart, variantId) {
        jQuery.each(cart, function(key, value) {
            if (key === 'items') {    
                jQuery.each(value, function(i, item) {  
                    if(item.variant_id == variantId){
                        $('.productid-'+variantId).remove();
                    }
                }); 
            }
        });
        updateCartDesc(cart);
    }
    $(window).ready(function(){
        $.ajax({
            type: 'GET',
            url: '/cart.js',
            async: false,
            cache: false,
            dataType: 'json',
            success: function (cart){
                Bizweb.updateCartFromForm(cart, '.mini-products-list');
                Bizweb.updateCartPopupForm(cart, '#popup-cart-desktop .tbody-popup'); 
                
            }
        });
    });
    
</script>
        <div id="popup-cart" class="modal fade" role="dialog">
    <div id="popup-cart-desktop" class="clearfix">
        <div class="title-popup-cart">
            <i class="fa fa-check check" aria-hidden="true"></i>
            <span class="pop-title">Bạn đã thêm <span class="cart-popup-name"></span> vào giỏ hàng</span>
            <div class="title-quantity-popup" onclick="window.location.href='/cart'">
                Giỏ hàng của bạn (<span class="cart-popup-count"></span> sản phẩm)
            </div>
            <div class="content-popup-cart">
                <div class="thead-popup">
                    <div style="width: 55%;" class="text-left">Sản phẩm</div>
                    <div style="width: 15%;" class="text-right">Đơn giá</div>
                    <div style="width: 15%;" class="text-center">Số lượng</div>
                    <div style="width: 15%;" class="text-right">Thành tiền</div>
                </div>
                <div class="tbody-popup">
                </div>
                <div class="tfoot-popup">
                    <div class="tfoot-popup-1 clearfix">
                        <div class="pull-left popup-ship">
                            <p>Giao hàng trên toàn quốc</p>
                        </div>
                        <div class="pull-right popup-total">
                            <p>Thành tiền: <span class="total-price"></span></p>
                        </div>
                    </div>
                    <div class="tfoot-popup-2 clearfix">
                        <a class="button btn-proceed-checkout" title="Tiến hành đặt hàng" href="/checkout"><span>Tiến hành đặt hàng <i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
                        <a class="button btn-continue" title="Tiếp tục mua hàng" onclick="$('#popup-cart').modal('hide');"><span><span><i class="fa fa-caret-left" aria-hidden="true"></i> Tiếp tục mua hàng</span></span></a>
                    </div>
                </div>
            </div>
            <a title="Close" class="quickview-close close-window" href="javascript:;" onclick="$('#popup-cart').modal('hide');"><i class="fa  fa-close"></i></a>
        </div>
    </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
</div>
        

<div id="quick-view-product" class="modal fade" role="dialog">
    <div class="quickview-overlay fancybox-overlay fancybox-overlay-fixed"></div>
    <div class="quick-view-product"></div>
    <div id="quickview-modal" style="display:none;">
        <div class="block-quickview primary_block row">
            <div class="product-left-column col-xs-12 col-sm-4 col-md-5">
                <div class="clearfix image-block">
                    <span class="view_full_size">
                        <a class="img-product" title="" href="#">
                            <img id="product-featured-image-quickview" class="img-responsive product-featured-image-quickview" src="http://bizweb.dktcdn.net/100/091/133/themes/517838/assets/logo.png" alt="quickview"  />
                        </a>
                    </span>
                    <div class="loading-imgquickview" style="display:none;"></div>
                </div>
                <div class="more-view-wrapper clearfix">
                    <div id="thumbs_list_quickview">
                        <ul class="product-photo-thumbs quickview-more-views-owlslider" id="thumblist_quickview"></ul>
                    </div>
                </div>
            </div>
            <div class="product-center-column product-info product-item col-xs-12 col-sm-8 col-md-7">
                <h3 class="qwp-name">&nbsp;</h3>
                <div class="qwp-vendor"><label>Thương hiệu: Không</label></div>
                <div class="quickview-info">
                    <span class="prices">
                        <span class="price h2"></span>
                        <del class="old-price"></del>
                    </span>
                </div>
                <div class="product-description rte"></div>
                <form action="/cart/add" method="post" enctype="multipart/form-data" class="variants form-ajaxtocart">                  
                    <div class="qvariants clearfix">
                        <select name='variantId' class="hidden" style="display:none"></select>
                    </div>                  
                    <div class="clearfix"></div>
                    <div class="quantity_wanted_p">
                        <label for="quantity-detail" class="quantity-selector">Số lượng</label>
                        <input type="number" id="quantity-detail" name="quantity" value="1" min="1" class="quantity-selector text-center">
                        <button type="submit" name="add" class="btn btn-primary add_to_cart_detail ajax_addtocart">
                            <span >Mua sản phẩm</span>
                        </button>
                    </div>
                    <div class="total-price" style="display:none">
                        <label>Tổng cộng: </label>
                        <span></span>
                    </div>

                </form>
                <p class="qv-contact">Gọi <b><a href = "tel:1900.6750">1900.6750</a></b> để được trợ giúp</p>
            </div>

        </div>      
        <a title="Close" class="quickview-close close-window" href="javascript:;"><i class="fa   fa-times-circle"></i></a>
    </div>    
</div>
<script type="text/javascript">  
    Bizweb.doNotTriggerClickOnThumb = false;
    function changeImageQuickView(img, selector) {
        var src = $(img).attr("src");
        src = src.replace("_compact", "");
        $(selector).attr("src", src);
    }
    var selectCallbackQuickView = function(variant, selector) {
        $('#quick-view-product form').show();
        var productItem = jQuery('.quick-view-product .product-item');
        addToCart = productItem.find('.add_to_cart_detail'),
            productPrice = productItem.find('.price'),
            comparePrice = productItem.find('.old-price'),
            totalPrice = productItem.find('.total-price span');
        if (variant && variant.available) {

            addToCart.removeClass('disabled').removeAttr('disabled');
            $(addToCart).find("span").text("Mua hàng");

            if(variant.price < 1){             
                $("#quick-view-product .price").html('Liên hệ');
                $("#quick-view-product del, #quick-view-product .quantity_wanted_p").hide();
                $("#quick-view-product .prices .old-price").hide();

            }else{
                productPrice.html(Bizweb.formatMoney(variant.price, "{{$amount_no_decimals_with_comma_separator}}₫"));
                if ( variant.compare_at_price > variant.price ) {
                    comparePrice.html(Bizweb.formatMoney(variant.compare_at_price, "{{$amount_no_decimals_with_comma_separator}}₫")).show();         
                    productPrice.addClass('on-sale');
                } else {
                    comparePrice.hide();
                    productPrice.removeClass('on-sale');
                }

                $(".quantity_wanted_p").show();


            }


            
             updatePricingQuickView();
              
               /*begin variant image*/
               if (variant && variant.featured_image) {

                   var originalImage = $("#product-featured-image-quickview");
                   var newImage = variant.featured_image;
                   var element = originalImage[0];
                   Bizweb.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
                       $('#thumblist_quickview img').each(function() {
                           var parentThumbImg = $(this).parent();
                           var productImage = $(this).parent().data("image");
                           if (newImageSizedSrc.includes(productImage)) {
                               $(this).parent().trigger('click');
                               return false;
                           }
                       });
                   });
                   $('#product-featured-image-quickview').attr('src',variant.featured_image.src);
               }
               } else {
                  
                   addToCart.addClass('disabled').attr('disabled', 'disabled');
                   $(addToCart).find("span").text("Hết hàng");

                   if(variant.price < 1){              

                       $("#quick-view-product .price").html('Liên hệ');
                       $("#quick-view-product del").hide();
                       $("#quick-view-product .quantity_wanted_p").show();
                       $("#quick-view-product .prices .old-price").hide();
                       comparePrice.hide();
                       productPrice.removeClass('on-sale');
                       addToCart.addClass('disabled').attr('disabled', 'disabled');
                       $(addToCart).find("span").text("Hết hàng");                 
                   }else{
                       if ( variant.compare_at_price > variant.price ) {
                           comparePrice.html(Bizweb.formatMoney(variant.compare_at_price, "{{$amount_no_decimals_with_comma_separator}}₫")).show();         
                           productPrice.addClass('on-sale');
                       } else {
                           comparePrice.hide();
                           productPrice.removeClass('on-sale');
                       }
                       $("#quick-view-product .price").html(Bizweb.formatMoney(variant.price, "{{$amount_no_decimals_with_comma_separator}}₫"));
                       $("#quick-view-product del ,#quick-view-product .quantity_wanted_p").show();
                       $("#quick-view-product .prices .old-price").show();

                       addToCart.addClass('disabled').attr('disabled', 'disabled');
                       $(addToCart).find("span").text("Hết hàng");
                   }
               }
             

              };
</script>

        <script src='/common/js/script.js' type='text/javascript'></script>
        <!-- Bootstrap Js -->
        <script src='/common/js/bootstrap.min.js' type='text/javascript'></script>
        <!-- Dropdowns -->
        <script src='/common/js/dropdowns-enhancement.min.js' type='text/javascript'></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials.min.js"></script>
        <!-- Owl Carousel Js -->
        <script src='/common/js/update.js' type='text/javascript'></script>


        <script src='/common/js/seting-js.js' type='text/javascript'></script>
    </body>
</html>