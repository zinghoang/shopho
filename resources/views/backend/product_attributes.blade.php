{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('attr_fk_pro_id', 'Attr_fk_pro_id:') !!}
			{!! Form::text('attr_fk_pro_id') !!}
		</li>
		<li>
			{!! Form::label('attr_fk_type_id', 'Attr_fk_type_id:') !!}
			{!! Form::text('attr_fk_type_id') !!}
		</li>
		<li>
			{!! Form::label('attr_barcode', 'Attr_barcode:') !!}
			{!! Form::text('attr_barcode') !!}
		</li>
		<li>
			{!! Form::label('attr_code', 'Attr_code:') !!}
			{!! Form::text('attr_code') !!}
		</li>
		<li>
			{!! Form::label('attr_price_import', 'Attr_price_import:') !!}
			{!! Form::text('attr_price_import') !!}
		</li>
		<li>
			{!! Form::label('attr_price_export', 'Attr_price_export:') !!}
			{!! Form::text('attr_price_export') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}