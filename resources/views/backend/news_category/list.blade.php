@extends('backend.layout.main')
@section("content")

            <!-- BEGIN PAGE HEADER-->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/manager/category">News Category</a>
                    </li>
                </ul>
            </div>
            <h3 class="page-title">
                Danh sách thư mục
            </h3>
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS -->
            <div class="">
                 <!-- Main content -->
          
              <!-- Small boxes (Stat box) -->
              <div class="row">
                <div class="col-xs-12">
                    <div class="portlet light bordered">
                      <div class=" box-danger">
                       
                        <div class="box-body">
                          <form class="form" method="get">
                            <div class="row">
                            <!--   <div class="col-xs-1">
                                <input type="text" class="form-control" placeholder="Code" name="code" value="{{Request::get('code')}}">
                              </div> -->
                              <div class="col-xs-3">
                                <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="{{Request::get('keyword')}}">
                              </div>
                              <!-- <div class="col-xs-2">
                                <select name="shop" class="form-control">
                               
                                </select>
                              </div> -->
                             <!--  <div class="col-xs-2">
                                  <input id="range_1" type="text" name="range" value="">
                              </div> -->
                              <div class="col-xs-2">
                                <button type="submit" class="btn btn-block btn-primary">Seach</button>
                              </div>
                               <a href="news-category/add" class="btn btn-success pull-right ">Thêm thư mục</a>
                            </div>
                          </form>

                        </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box-header -->
                      <div class="">
                        <table id="example2" class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Tên thư mục</th>
                            
                              <th>Trạng thái</th>
                           
                              <th>Điều khiển</th>
                            </tr>
                          </thead>
                          <tbody>
                          @if($categories->count())
                            @foreach($categories as $category)
                              <tr>
                                <td>{{$category->news_cat_id}}</td>
                                <td>{{$category->news_cat_name}}</td>
                                <td>{{ \App\Helpers\CommonHelper::statusIsDeleted($category->news_cat_is_deleted) }} </td>
                              
                                <td class="bs-glyphicons">
                                 <a href="/manager/product-category/edit/{{$category->news_cat_id}}">
                                  <span class="ml10 cursor-pointer glyphicon glyphicon-edit edit-admin" data-id="{{$category->news_cat_id}}"></span></a>
                                     <a href="/manager/product-category/delete/{{$category->news_cat_id}}">
                                  <span class="ml10 cursor-pointer glyphicon glyphicon-remove delete_category" data-id="{{$category->news_cat_id}}" > </span> </a>
                                </td>
                              </tr>
                            @endforeach
                          @endif
                          </tbody>
                         
                        </table>
                      </div>
                      <div class=" center">
                        <?php echo $categories->render(); ?>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
              </div>
          
            <!-- /.content -->
            </div>
        
            <!-- END DASHBOARD STATS -->
            <div class="clearfix"></div
         
           
@stop        