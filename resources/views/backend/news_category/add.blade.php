@extends('backend.layout.main')

@section('content')
<div role="main" class="">
	<section class="page-header mb-lg">

		<div class="">
			<ul class="breadcrumb">
				<li><a href="/">Home</a></li>

				<li class="active">Tạo thư mục sản phẩm</li>
			</ul>
		</div>
	</section>
	<div class="">
		<div class="">
			<div class="col-sm-12 col-md-6 ">
				<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
					<div class="box-content">
						<div class="box box-info">
				            <h4 class="heading-primary text-uppercase mb-lg">Thêm Category</h4>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form class="form-horizontal" action="/manager/news-category/add-store"   method="post" >
				            	{{ csrf_field() }}
				            	@if (count($errors) > 0)
			                    <div class="alert alert-danger">
			                      <ul>
			                        @foreach ($errors->all() as $error)
			                          <li>{{ $error }}</li>
			                        @endforeach
			                      </ul>
			                    </div>
			                    @endif
			                   
				              <div class="box-body">
				              
				                <div class="form-group">
				                  <label for="productName" class="col-sm-2 control-label">Tên Category</label>

				                  <div class="col-sm-10">

				                   {{ Form::select('size', array('L' => 'Large', 'S' => 'Small')) }}
				                    <input type="text" class="form-control" id="productName" placeholder="category name" name="cat[news_cat_name]" value="{{ old('cat[news_cat_name]') }}">
				                  </div>
				                </div>
				       
				                </div>
				           
				  

				               <div class="form-group">
				                  <div class="col-sm-2"></div>
				                  <div class="col-sm-10">
								  	<input value="0" type="checkbox" name="cat[news_cat_is_deleted]" id="new_flag" {{(true)?"checked":""}}>
								  	<label for="new_flag">Hoạt động</label>
								  </div>
				                </div>
				             
				               
				              </div>
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <a href="/manager/catduct/" class="btn btn-default">Back</a>
				                <button type="submit" class="btn btn-info pull-right">Add</button>
				              </div>
				              <!-- /.box-footer -->
				            </form>
				        </div>
		        	</div>
		        </div>
			</div>

			
		</div>
	</div>
</div>
<script type="text/javascript">

 var  Product  =  {


 	init : function(){

 		this.uploadImage(); 
 		$(document).ready(function() {
		  $('.summernote').summernote({
		  	  height: 600,                 // set editor height
			  minHeight: null,             // set minimum height of editor
			  maxHeight: null,             // set maximum height of editor
			  focus: true        
		  });
		  
		});
		
 	},

 	uploadImage : function(){

 		$(document).ready(function(){

			$(".inputImage a").on("click",function(){

				var image  = $(this).parents(".inputImage").find("input");
				image.click();

				image.change(function(){

					var reader = new FileReader();
    				reader.onload = function (e) {
		             // get loaded data and render thumbnail.
		             image.parents(".itemImage").find("img").attr("src", e.target.result);	
		   		 };
		       // read the image file as a data URL.
		         reader.readAsDataURL(this.files[0]);	
				});
				return false;

			});
	});

 	}

 }

Product.init();
	

</script>

@endsection
@push("myScript")
 
@endpush