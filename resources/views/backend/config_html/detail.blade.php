@extends('backend.layout.main')
@section('content')

       <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">File</a>
                    </li>
                </ul>
            </div>
            <h3 class="page-title">
               Thông tin file
            </h3>
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS -->
            <div class="row">
                
                <div class="col-md-12 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-red-sunglo">
                                            <i class="icon-settings font-red-sunglo"></i>
                                            <span class="caption-subject bold uppercase">Chỉnh sửa</span>
                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body form">
                                     
                                        {{Form::open(['url'=>'/manager/config-html/update','method'=>'post'])}}
                                            <div class="form-body">
                                  
                                              
                                            <input type="hidden" name="conf_html_name" value="{{$conf->conf_html_name}}">
                                                <div class="form-group">
                                                    <label>Nội dung</label>
                                                    <textarea rows="20" class="summernote"  name="conf_html_content" style="height: 500px">{{$conf->conf_html_content}}</textarea>
                                                      
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn blue">Lưu</button>
                                                <button type="button" onclick="window.history.back();" class="btn default">Trở về</button>
                                            </div>
                                       {{Form::close()}}
                                    </div>
                                </div>

            </div>



            </div>
            <div class="clearfix">
            </div>

            <!-- END DASHBOARD STATS -->
 <script type="text/javascript">
        
        $(document).ready(function() {
              $('textarea.summernote').summernote({ 
                height: 500 ,
                
            })


           $("#image").change(function(){
            previewFile();
           });


         });


 function previewFile() {
  var preview = document.querySelector('#img_review');
  var file    = document.querySelector('#image').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}


    </script>



@endsection