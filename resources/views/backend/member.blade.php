{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('mem_name', 'Mem_name:') !!}
			{!! Form::text('mem_name') !!}
		</li>
		<li>
			{!! Form::label('mem_email', 'Mem_email:') !!}
			{!! Form::text('mem_email') !!}
		</li>
		<li>
			{!! Form::label('mem_password', 'Mem_password:') !!}
			{!! Form::text('mem_password') !!}
		</li>
		<li>
			{!! Form::label('mem_remember_token', 'Mem_remember_token:') !!}
			{!! Form::text('mem_remember_token') !!}
		</li>
		<li>
			{!! Form::label('mem_address', 'Mem_address:') !!}
			{!! Form::text('mem_address') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}