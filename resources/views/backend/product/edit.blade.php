@extends('backend.layout.main')

@section('content')
<div role="main" class="main">
	<section class="page-header mb-lg">

		<div class="">
			<ul class="breadcrumb">
				<li><a href="/">Home</a></li>

				<li class="active">My Account</li>
			</ul>
		</div>
	</section>
	<div class="row">
		<div class="">
			<div class="col-md-12 ">
				<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
					<div class="box-content">
						<div class="box box-info">
				            <h4 class="heading-primary text-uppercase mb-lg">Chỉnh sửa sản phẩm</h4>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form class="form-horizontal" action="/manager/product/edit-store"   method="post" enctype="multipart/form-data" >
				            	{{ csrf_field() }}
				            	@if (count($errors) > 0)
			                    <div class="alert alert-danger">
			                      <ul>
			                        @foreach ($errors->all() as $error)
			                          <li>{{ $error }}</li>
			                        @endforeach
			                      </ul>
			                    </div>
			                    @endif
			                    <input type="hidden" name="pro_id" value="{{$product->pro_id}}">
				              <div class="box-body">
				              	<div class="form-group">
				                  <label for="productName" class="col-sm-2 control-label">Mã sản phẩm</label>

				                  <div class="col-sm-10">
				                    <input  type="text" class="form-control" id="productCode" placeholder="Product code" name="attr[attr_code]" value="{{ old('code',$product->attr()->attr_code) }}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="productName" class="col-sm-2 control-label">Tên sản phẩm</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="productName" placeholder="Product name" name="pro[pro_name]" value="{{ old('name',$product->pro_name) }}">
				                  </div>
				                </div>
				              <!--   <div class="form-group">
				                  <label for="Supplier" class="col-sm-2 control-label">Supplier</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="Supplier" placeholder="Supplier" name="supplier" value="{{old('supplier', $product->supplier)}}">
				                  </div>
				                </div> -->
				                <div class="form-group">
				                  <label for="Supplier" class="col-sm-2 control-label">Thư mục</label>

				                  <div class="col-sm-5">
				                  		{{ Form::select('pro[pro_fk_cat_id]', $categorys, old('category_id', $product->pro_fk_cat_id) , array("class" => "form-control")) }}
				                  </div>
				                </div>


				                <div class="form-group">
				                  <label for="Supplier" class="col-sm-2 control-label">Màu sắc</label>

				                  <div class="col-sm-5">
				                  		{{ Form::select('attr[attr_fk_color_id]', $colors, old('attr[attr_fk_color_id]',$product->attr()->attr_fk_color_id) , array("class" => "form-control")) }}
				                  </div>
				                </div>



				                <div class="form-group">
				                  <label for="" class="col-sm-2 control-label pt0">Hình ảnh</label>

				                  <div class="col-sm-10">
				                  
									<div class="boxUpload"> 
									 
									 @for($i=0 ;$i<5 ;$i++)

									  @php $images = $product->getImage()->toArray(); @endphp

									    <div class="itemImage">
									      @if(isset($images[$i]))
									   	    <img src="{{$images[$i]['pro_img_url']}}">
									   	     <input type="hidden"  value="{{ $images[$i]['pro_img_id']}}" name="files_exist[{{$i}}]"  />
									   	     <a class="block">Xóa</a>
									   	  @else
											<img src="/asset/images/noimage.png">
									   	  @endif
									      <div class="inputImage">
									   	     <a class="upload" href="#">Chọn hình</a>
                                              <div class="inputupload"> 
									   	           <input type="file"  style="" name="files[{{$i}}]" multiple />
									   	     </div>
									   	   </div>
									   </div>
									   @endfor
									    
									</div>
				                  </div>
				                </div>
				           
				               <div class="form-group">
				                  <label for="Price" class="col-sm-2 control-label">Giá nhập khẩu</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="Price" placeholder="Price" name="attr[attr_price_import]" value="{{old('attr[attr_price_import]',$product->attr()->attr_price_import)}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="Price" class="col-sm-2 control-label">Giá bán</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="SalePrice" placeholder="Sale Price" name="attr[attr_price_export]" value="{{old('attr[attr_price_export]',$product->attr()->attr_price_export)}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="Quantity" class="col-sm-2 control-label">Số lượng</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="Quantity" placeholder="Quantity" name="pro[pro_quantity]" value="{{old('pro[pro_quantity]',$product->pro_quantity)}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="Description" class="col-sm-2 control-label">Description</label>

				                  <div class="col-sm-10">
				                    <textarea class="summernote" name="attr[attr_description]">{{old('attr[attr_description]',$product->attr()->attr_description)}}</textarea>
				                  </div>
				                </div>

				                  <div class="form-group">
				                  <label for="Description" class="col-sm-2 control-label">Giảm giá (%)</label>

				                  <div class="col-sm-10">
				                    <input class="form-control" rows="10" value="{{old('pro[pro_is_selling]',$product->pro_is_selling)}}" name="pro[pro_is_selling]">
				                  </div>
				                </div>


				                 <div class="form-group">
				                  <div class="col-sm-2"></div>
				                  <div class="col-sm-10">
								     {{Form::checkbox('pro[pro_is_hot]', '1',$product->pro_is_hot ? 'true':false)}}
								  	<label for="new_flag">Sản phẩm hot</label>
								  </div>
				                </div>
				                 <div class="form-group">
				                  <div class="col-sm-2"></div>
				                  <div class="col-sm-10">
				                   {{Form::checkbox('pro[pro_is_featured]', '1',$product->pro_is_featured ? 'true':false)}}
								  
								  	<label for="new_flag">Đặc sắc</label>
								  </div>
				                </div>
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <a href="/manager/product/" class="btn btn-default">Back</a>
				                <button type="submit" class="btn btn-info pull-right">Add</button>
				              </div>
				              <!-- /.box-footer -->
				            </form>
				        </div>
		        	</div>
		        </div>
			</div>

			
		</div>
	</div>
</div>
<script type="text/javascript">

Product.init();
	
</script>

@endsection
@push("myScript")
 
@endpush