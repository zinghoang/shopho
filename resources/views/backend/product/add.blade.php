@extends('backend.layout.main')

@section('content')
<div role="main" class="">
	<section class="page-header mb-lg">

		<div class="">
			<ul class="breadcrumb">
				<li><a href="/">Home</a></li>

				<li class="active">Tạo sản phẩm</li>
			</ul>
		</div>
	</section>
	<div class="row">
		<div class="">
			<div class="col-md-12 ">
				<div class="featured-box featured-box-primary featured-box-flat featured-box-text-left mt-md">
					<div class="box-content">
						<div class="box box-info">
				            <h4 class="heading-primary text-uppercase mb-lg">Thêm Product</h4>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form class="form-horizontal" action="/manager/product/add-store"   method="post" enctype="multipart/form-data" >
				            	{{ csrf_field() }}
				            	@if (count($errors) > 0)
			                    <div class="alert alert-danger">
			                      <ul>
			                        @foreach ($errors->all() as $error)
			                          <li>{{ $error }}</li>
			                        @endforeach
			                      </ul>
			                    </div>
			                    @endif
			                   
				              <div class="box-body">
				              	<div class="form-group">
				                  <label for="productName" class="col-sm-2 control-label">Mã sản phẩm</label>

				                  <div class="col-sm-10">
				                    <input  type="text" class="form-control" id="productCode" placeholder="Product code" name="attr[attr_code]" value="{{ old('code')}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="productName" class="col-sm-2 control-label">Tên sản phẩm</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="productName" placeholder="Product name" name="pro[pro_name]" value="{{ old('name') }}">
				                  </div>
				                </div>
				       
				                <div class="form-group">
				                  <label for="Supplier" class="col-sm-2 control-label">Thư mục</label>

				                  <div class="col-sm-5">
				                  		{{ Form::select('pro[pro_fk_cat_id]', $categorys, old('pro[pro_fk_cat_id]') , array("class" => "form-control")) }}
				                  </div>
				                </div>

				                <div class="form-group">
				                  <label for="Supplier" class="col-sm-2 control-label">Màu sắc</label>

				                  <div class="col-sm-5">
				                  		{{ Form::select('attr[attr_fk_color_id]', $colors, old('attr[attr_fk_color_id]') , array("class" => "form-control")) }}
				                  </div>
				                </div>


				                <div class="form-group">
				                  <label for="" class="col-sm-2 control-label pt0">Hình ảnh</label>

				                  <div class="col-sm-10">
				                  
									<div class="boxUpload"> 
									 
									 @for($i=0 ;$i<5 ;$i++)
									    	  <div class="itemImage">
									   	  <img src="/asset/images/noimage.png">
									      <div class="inputImage">
									   	     <a href="#">Chọn hình</a>
                                              <div class="inputupload"> 
									   	           <input type="file"  style="" data-exist="0" name="files[]" multiple />
									   	     </div>
									   	   </div>
									   </div>
									   @endfor
									    
									</div>
				                  </div>
				                </div>
				           
				                <div class="form-group">
				                  <label for="Price" class="col-sm-2 control-label">Giá nhập khẩu</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="Price" placeholder="Price" name="attr[attr_price_import]" value="{{old('price')}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="Price" class="col-sm-2 control-label">Giá bán</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="SalePrice" placeholder="Sale Price" name="attr[attr_price_export]" value="{{old('sale_price')}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="Quantity" class="col-sm-2 control-label">Số lượng</label>

				                  <div class="col-sm-10">
				                    <input type="text" class="form-control" id="Quantity" placeholder="Quantity" name="pro[pro_quantity]" value="{{old('number')}}">
				                  </div>
				                </div>
				                <div class="form-group">
				                  <label for="Description" class="col-sm-2 control-label">Description</label>

				                  <div class="col-sm-10">
				                    <textarea class="summernote" name="attr[attr_description]">{{old('description')}}</textarea>
				                  </div>
				                </div>

				                  <div class="form-group">
				                  <label for="Description" class="col-sm-2 control-label">Giảm giá (%)</label>

				                  <div class="col-sm-10">
				                    <input class="form-control" rows="10" name="pro[pro_is_selling]">{{old('pro[pro_is_selling]')}}</textarea>
				                  </div>
				                </div>


				                 <div class="form-group">
				                  <div class="col-sm-2"></div>
				                  <div class="col-sm-10">
								  	<input value="1" type="checkbox" name="pro[pro_is_hot]" id="new_flag" {{(true)?"checked":""}}>
								  	<label for="new_flag">Sản phẩm hot</label>
								  </div>
				                </div>
				                 <div class="form-group">
				                  <div class="col-sm-2"></div>
				                  <div class="col-sm-10">
								  	<input value="1" type="checkbox" name="pro[pro_is_featured]" id="new_flag" {{(true)?"checked":""}}>
								  	<label for="new_flag">Đặc sắc</label>
								  </div>
				                </div>
				             
				               
				              </div>
				              <!-- /.box-body -->
				              <div class="box-footer">
				                <a href="/manager/product/" class="btn btn-default">Back</a>
				                <button type="submit" class="btn btn-info pull-right">Add</button>
				              </div>
				              <!-- /.box-footer -->
				            </form>
				        </div>
		        	</div>
		        </div>
			</div>

			
		</div>
	</div>
</div>
<script type="text/javascript">

 var  Product  =  {


 	init : function(){

 		this.uploadImage(); 
 		$(document).ready(function() {
		  $('.summernote').summernote({
		  	  height: 600,                 // set editor height
			  minHeight: null,             // set minimum height of editor
			  maxHeight: null,             // set maximum height of editor
			  focus: true        
		  });
		  
		});
		
 	},

 	uploadImage : function(){

 		$(document).ready(function(){

			$(".inputImage a").on("click",function(){

				var image  = $(this).parents(".inputImage").find("input");
				image.click();

				image.change(function(){

					var reader = new FileReader();
    				reader.onload = function (e) {
		             // get loaded data and render thumbnail.
		             image.parents(".itemImage").find("img").attr("src", e.target.result);	
		   		 };
		       // read the image file as a data URL.
		         reader.readAsDataURL(this.files[0]);	
				});
				return false;

			});
	});

 	}

 }

Product.init();
	

</script>

@endsection
@push("myScript")
 
@endpush