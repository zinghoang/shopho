{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('cat_name', 'Cat_name:') !!}
			{!! Form::text('cat_name') !!}
		</li>
		<li>
			{!! Form::label('cat_slug', 'Cat_slug:') !!}
			{!! Form::text('cat_slug') !!}
		</li>
		<li>
			{!! Form::label('cat_is_deleted', 'Cat_is_deleted:') !!}
			{!! Form::text('cat_is_deleted') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}