{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('shop_name', 'Shop_name:') !!}
			{!! Form::text('shop_name') !!}
		</li>
		<li>
			{!! Form::label('shop_slug', 'Shop_slug:') !!}
			{!! Form::text('shop_slug') !!}
		</li>
		<li>
			{!! Form::label('shop_is_deleted', 'Shop_is_deleted:') !!}
			{!! Form::text('shop_is_deleted') !!}
		</li>
		<li>
			{!! Form::label('shop_fk_mem_id', 'Shop_fk_mem_id:') !!}
			{!! Form::text('shop_fk_mem_id') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}