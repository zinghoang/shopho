<head>
    <meta name="robots" content="NOINDEX,NOFOLLOW" />
    <meta name="Googlebot" content="NOINDEX,NOFOLLOW" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Operation System</title>
    <base href="" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="/asset/css/font-awesome.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/css/simple-line-icons.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/uniform/css/uniform.default.min.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/bootstrap-switch/css/bootstrap-switch.min.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/dataTables/dataTables.bootstrap.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/bootstrap-summernote/summernote.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/bootstrap-select/bootstrap-select.min.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/select2/select2.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/jquery-multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/plugins/icheck/skins/all.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/css/components.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/css/asset/plugins.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/css/layout.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/css/darkblue.css" media="screen" rel="stylesheet" type="text/css" >
    <link href="/asset/css/custom.css" media="screen" rel="stylesheet" type="text/css" >
   
    <script type="text/javascript" src="/asset/plugins/jquery-2.2.2.min.js"></script>
    @if(isset($loadJS))
     <script type="text/javascript" src="{{url($loadJS)}}"></script>
    @endif
</head>