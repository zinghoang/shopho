<!-- BEGIN FOOTER -->


<script type="text/javascript" src="/asset/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/asset/plugins/uniform/jquery.uniform.js"></script>
<script type="text/javascript" src="/asset/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="/asset/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/asset/plugins/jquery.blockui.min.js"></script>
<script type="text/javascript" src="/asset/plugins/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/asset/js/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="/asset/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="/asset/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/asset/plugins/bootstrap-summernote/summernote.min.js"></script>
<script type="text/javascript" src="/asset/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/asset/plugins/select2/select2.js"></script>
<script type="text/javascript" src="/asset/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="/asset/plugins/icheck/icheck.min.js"></script>


<script src="/asset/js/libs/metronic.js" type="text/javascript"></script>
<script src="/asset/js/libs/layout.js" type="text/javascript"></script>
<script src="/asset/js/libs/quick-sidebar.js" type="text/javascript"></script>
<script src="/asset/js/libs/demo.js" type="text/javascript"></script>

<!-- END FOOTER -->
<script>
    jQuery(document).ready(function() {
        
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
    });
</script>