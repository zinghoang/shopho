
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html class="no-js" dir="ltr" lang=""><!--<![endif]-->
@include('backend.layout.header')
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/" style="font-size: 20px;font-weight:bold; margin-top:6px;">

                <img  src="/asset/images/logo-web.png"  alt="logo" class=""/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">


                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="/asset/images/avatar_small.jpg"/>
                    <span class="username username-hide-on-mobile">
                           Administrator
                    </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="/index/update-info">
                                <i class="icon-user"></i>Thông tin tài khoản</a>
                            <a href="/login/logout">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->

    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER --><div class="clearfix"></div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    
    @include("backend.layout.bar_left")
    <div class="page-content-wrapper">
        <div class="page-content">
   
             @yield("content")
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
 @include("backend.layout.footer")

</body>
</html>
