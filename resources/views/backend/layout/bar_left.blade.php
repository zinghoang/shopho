 <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper" >
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu" id="admin_sidebar" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                <li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler"></div>
                    <!-- END SIDEBAR TOGGLER BUTTON -->
                </li>
                <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                <li class="start active open">
                    <a href="/">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:;">
                        <i class="icon-users"></i>
                        <span class="title">Sản Phẩm</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class=" ">
                            <a href="/manager/product">
                                <i class="icon-calendar"></i>
                                Danh sách sản phẩm </a>
                        </li>

                         <li class=" candidate_view">
                            <a href="/manager/product/add">
                                <i class="icon-calendar"></i>
                                Thêm sản phẩm</a>
                        </li>
                  
                    </ul>
                </li>

              
           
                 <li class="">
                    <a href="javascript:;">
                        <i class="icon-users"></i>
                        <span class="title">Thư mục sản phẩm</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class=" ">
                            <a href="/manager/product-category">
                                <i class="icon-calendar"></i>
                                Danh sách thư mục</a>
                        </li>

                         <li class=" candidate_view">
                            <a href="/manager/product-category/add">
                                <i class="icon-calendar"></i>
                                Thêm thư mục</a>
                        </li>
                  
                    </ul>
                </li>


                    <li class="">
                    <a href="javascript:;">
                        <i class="icon-users"></i>
                        <span class="title">Tin tức</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class=" ">
                            <a href="/manager/news">
                                <i class="icon-calendar"></i>
                                Danh sách tic tức</a>
                        </li>

                         <li class=" candidate_view">
                            <a href="/manager/news/add">
                                <i class="icon-calendar"></i>
                                Thêm tin tức</a>
                        </li>
                  
                    </ul>
                </li>


                   <li class="">
                    <a href="javascript:;">
                        <i class="icon-users"></i>
                        <span class="title">Trang thông tin </span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" style="display: none;">
                        <li class=" ">
                            <a href="/manager/config-html/HTML_INTRO">
                                <i class="icon-calendar"></i>
                                Trang giới thiệu</a>
                        </li>

                         <li class=" candidate_view">
                            <a href="/manager/config-html/HTML_MAP">
                                <i class="icon-calendar"></i>
                                Trang bản đồ</a>
                        </li>


                         <li class=" candidate_view">
                            <a href="/manager/config-html/HTML_CONTACT">
                                <i class="icon-calendar"></i>
                                Trang liên hệ</a>
                        </li>
                  
                    </ul>
                </li>


             
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->    <!-- BEGIN CONTENT -->