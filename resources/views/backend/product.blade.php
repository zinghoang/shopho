{!! Form::open(array('method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('pro_name', 'Pro_name:') !!}
			{!! Form::text('pro_name') !!}
		</li>
		<li>
			{!! Form::label('pro_quantity', 'Pro_quantity:') !!}
			{!! Form::text('pro_quantity') !!}
		</li>
		<li>
			{!! Form::label('pro_is_hot', 'Pro_is_hot:') !!}
			{!! Form::text('pro_is_hot') !!}
		</li>
		<li>
			{!! Form::label('pro_is_deleted', 'Pro_is_deleted:') !!}
			{!! Form::text('pro_is_deleted') !!}
		</li>
		<li>
			{!! Form::label('pro_is_selling', 'Pro_is_selling:') !!}
			{!! Form::text('pro_is_selling') !!}
		</li>
		<li>
			{!! Form::label('pro_is_featured', 'Pro_is_featured:') !!}
			{!! Form::text('pro_is_featured') !!}
		</li>
		<li>
			{!! Form::label('pro_fk_cat_id', 'Pro_fk_cat_id:') !!}
			{!! Form::text('pro_fk_cat_id') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}