@extends('backend.layout.main')
@section('content')
       <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="/">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="/page/news">Event</a>
                    </li>
                </ul>
            </div>
            <h3 class="page-title">
               Thêm tin tức
            </h3>
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS -->
            <div class="row">
                
                <div class="col-md-12 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-red-sunglo">
                                            <i class="icon-settings font-red-sunglo"></i>
                                            <span class="caption-subject bold uppercase">Thêm</span>
                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body form">
                                     
                                        {{Form::open(['url'=>'/manager/news/add-store','method'=>'post', 'files'=>true])}}
                                            <div class="form-body">
                                  
                                                <div class="form-group">
                                                    <label>Tiêu đề</label>
                                                        <input type="text" name='data[news_title]' class="form-control" >
                                                </div>
                                               
                                                  <div class="form-group">
                                                    <label>Hình ảnh</label>
                                                  
                                                    <input type="file" id="image" name="image">
                                                   
                                                      <img id="img_review" style="margin-top:10px;" width="60" height="60" src="/upload/image/no_img.jpg">
                                                </div>


                                                <div class="form-group">
                                                    <label>Nội dung</label>
                                                    <textarea rows="20" class="summernote"  name="data[news_description]" style="height: 500px"> </textarea>
                                                   
                                                  
                                                </div>

                                               <div class="form-group">
                                                    <label>Thư mục</label>
                                                
                                                   {{   Form::select('data[news_fk_news_cat_id]',$arrCategory,false,["class"=>"form-control"])}}
                                                </div>
                                                
                                             <!--    <div class="form-group">
                                                    <label>Trạng thái</label>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio"  name='data[news_status]' id="optionsRadios4" value="1" checked> Hoạt động (hiển thị)
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name='data[news_status]' id="optionsRadios5" value="0"> Bị khóa 
                                                            <span></span>
                                                        </label>
                                                      
                                                    </div>
                                                </div> -->

                                            

                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn blue">Lưu</button>
                                                <button type="button" onclick="window.history.back();" class="btn default">Trở về</button>
                                            </div>
                                       {{Form::close()}}
                                    </div>
                                </div>

            </div>



            </div>
            <div class="clearfix">
            </div>

            <!-- END DASHBOARD STATS -->
 <script type="text/javascript">
        
        $(document).ready(function() {
              $('textarea.summernote').summernote({ 
                height: 500 ,
                
            })

           $('.datetimepicker').datetimepicker({
                format: 'dd-mm-yyyy hh:ii',
                 autoclose: true,
   
            });

           $("#image").change(function(){
            previewFile();
           });


         });


 function previewFile() {
  var preview = document.querySelector('#img_review');
  var file    = document.querySelector('#image').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}


    </script>


  
@endsection
