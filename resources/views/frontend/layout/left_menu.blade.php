        <div class="col-xs-12 col-lg-3 col-lg-pull-9">

            <aside class="box-aside hidden-md-down">
                <h2 class="title-box">
                    Danh mục sản phẩm
                </h2>
                <nav class="nav-list-dm">
                    <ul class="nav navbar-pills">
                        
                        @foreach($categories as $cat)
                        <li class="nav-item ">
                            <a href="/category/{{$cat->cat_slug}}-{{$cat->cat_id}}" class="nav-link">{{$cat->cat_name}}</a>
                        </li>
                        
                        @endforeach
               
                        
                    </ul>
                </nav>
            </aside>

            
            <aside class="box-aside hidden-md-down">
                <h2 class="title-box">
                    <a href="san-pham-noi-bat">Sản phẩm bán chạy</a>
                </h2>
                <div class="list-wrap row">
                            @php $limit_hot  = 0 ; @endphp
                            @foreach($product_hots as $product)  
                        
                            <div class="product-item2 col-xs-12">
                                <div class="product-item-thumbnail">
                                    <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html"><img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage())}}" alt=""></a>
                                </div>
                                <h3 class="product-item-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>
                                
                                
                                <div>
                                    <span class="product-item-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</span>
                                    <span class="product-item-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</span>
                                </div>
                                
                                <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 
                              @php
                               if($limit_hot ==2)  break;
                               $limit_hot++;

                               @endphp
                        @endforeach
                    
                    
                    
                     

                 
                    
                </div>
               <!--  <a class="btn-link-style" href="san-pham-noi-bat">Xem thêm sản phẩm</a> -->
            </aside>
            
            <aside class="box-aside hidden-md-down">
                <div class="feature-l">
                    <ul class="list-group">
                        <li class="media">
                            <div class="media-left media-middle">
                                <img class="media-object" src="/common/image/feature1.png" alt="feature1">
                            </div>
                            <div class="media-body">
                                <span class="feature-title">Hỗ trợ trực tuyến</span>
                                <p>0905.70.72.70</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
            
            <aside class="box-aside hidden-md-down">
                <div class="banner-pc">
                    <a href="#">
                        <img src="/common/image/banner-qc.jpg" alt="Giày hót">
                        <div class="name-qc">
                            <span>
                                
                                <img src="/common/image/text-banner.png" alt="Hót giày">
                                
                                
                            </span>
                        </div>
                    </a>
                </div>
            </aside>
            
            
            <aside class="box-aside">
                <div class="head-box">
                    <h2 class="title-box">
                        <a href="#">Tin tức</a>
                    </h2>
                </div>
                <div class="list-blogs owl-carousel owl-theme">
                      
                   @foreach($news as $new) 
                <div class="blog-item">
                    <div class="blog-item-thumbnail">
                            <a href="tin-tuc/{{str_slug($new->news_title)}}-{{$new->news_id}}"><img height="175" src="{{$new->news_thumb}}"></a>
                    </div>
                        <h3 class="blog-item-name"><a href="/tin-tuc/{{str_slug($new->news_title)}}-{{$new->news_id}}">
                                     {{$new->news_title}}       
                                    </a></h3>
                        <div class="postby">
                                        <div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin</div>
                                        <div><i class="fa fa-clock-o" aria-hidden="true"></i>{{$new->created_at}} </div>
                                    </div>
                    <p class="blog-item-summary"> {{nl2br($new->news_description)}}</p> 
                </div>

                @endforeach
                    
                </div>
            </aside>
            
        </div>