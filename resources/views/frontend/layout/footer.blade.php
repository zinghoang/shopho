
        <footer id="footer">
    <div class="container" id="accordion" role="tablist">
        <div class="row">
            <div class="col-lg-3">
                <div class="widget-item info-contact">
                    <div class="logo">
                        <a href="/"><img src="/common/image/logo2.png" alt="Breshka shoes" title="Breshka shoes"></a>
                    </div>
                    <!-- End .widget-title -->
                    <ul class="widget-menu">
                        <li><i class="fa fa-map-marker color-x" aria-hidden="true"></i> Đang cập nhật</li>
                        <li><i class="fa fa-phone color-x" aria-hidden="true"></i> 0905707270</li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="support@shopho.com">surport@shopho.com</a> </li>
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
            <div class="col-lg-3 hidden-md-down">
                <div class="widget-item">
                    <h4 class="widget-title">Về chúng tôi</h4>
                    <!-- End .widget-title -->
                    <ul class="widget-menu">
                        
                          <li><a href="/tin-tuc">Tin tức nổi bật</a></li>
                        
                        <li><a href="/register">Đăng ký thành viên</a></li>
                    
                        <li><a href="/huong-dan">Hướng dẫn mua hàng</a></li>
                        
                        <li><a href="/chinh-sach">Chính sách</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            



            
            
            <div class="col-xs-12 hidden-lg-up">
                <div class="widget-item panel">
                    <a class="widget-title" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Về chúng tôi<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <!-- End .widget-title -->
                    <ul class="widget-menu panel-collapse collapse in" id="collapseOne">
                        
                        <li><a href="/gioi-thieu">Tầm nhìn & giá trị cốt lõi</a></li>
                        
                        <li><a href="/">Ý kiến khách hàng</a></li>
                        
                        <li><a href="/">Báo chí nói về chúng tôi</a></li>
                        
                        <li><a href="/die-u-khoa-n-di-ch-vu">Quy định sử dụng</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách bảo mật</a></li>
                        
                        <li><a href="/chi-nh-sa-ch">Chính sách bảo mật</a></li>
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
         
            
            
            <div class="col-xs-12 hidden-lg-up">
                <div class="widget-item panel">
                    <a class="widget-title" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Liên hệ<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    <!-- End .widget-title -->
                    <ul class="widget-menu panel-collapse collapse" id="collapseThree">
                        
                        <li><a href="/tin-tuc">Tin tức nổi bật</a></li>
                        
                        <li><a href="/register">Đăng ký thành viên</a></li>
                    
                        <li><a href="/huong-dan">Hướng dẫn mua hàng</a></li>
                        
                        <li><a href="/chinh-sach">Chính sách</a></li>
                        
                        
                        
                    </ul>
                    <!-- End .widget-menu -->
                </div>
            </div>
            
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <hr class="hidden-md-down hr-an-footer">
                </div>
                <div class="col-xs-12 col-lg-9 text-xs-left">
                    <span class="info-website">
                        <span>© Copyright  HO shop</span>
                        <span class="hidden-xs-down"> | </span>
                        <span>By <a href="/" target="_blank">shopho.com</a></span>
                    </span>
                </div>
               
            </div>
        </div>
    </div>
</footer>

