
<head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <title>
            Breshka shoes 
        </title>
        
    <script>
      var iwish_template='index';
      var iwish_cid=parseInt('0',10);
    </script>
    <script src='/common/js/iwishheader.js' type='text/javascript'></script>

        
    <meta name="description" content="">
    
    <meta name="keywords" content="dkt, bizweb, theme, breshka-shoes theme"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="canonical" href=""/>
    <meta name='revisit-after' content='1 days' />
    <meta name="robots" content="noodp,index,follow" />

    <!-- ================= Favicon ================== -->
    <link rel="icon" href="" type="image/x-icon" />
    <!-- Product meta ================================================== -->
        
    <meta property="og:type" content="website">
    <meta property="og:title" content="Breshka shoes">
    <meta property="og:description" content="doxinh.com">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="Breshka shoes">


    <link href='/common/css/bootstrap.min.css' rel='stylesheet' type='text/css' />
    <link href='/common/css/styles.css' rel='stylesheet' type='text/css' />
    <link href='/common/css/update.css' rel='stylesheet' type='text/css' />

    <!-- Owl Carousel CSS -->
    <link href='/common/css/owl.carousel.min.css' rel='stylesheet' type='text/css' />
    <link href='/common/plugins/jBox/Source/jBox.css' rel='stylesheet' type='text/css' />
    <!-- Animate CSS -->
    <script>
      window.Tether = {};
    </script>
    <!-- Jquery Js -->
    <script src="/common/js/jquery.min.js"></script>
    <script src="/common/plugins/jBox/Source/jBox.min.js"></script>

    <script src="/common/plugins/jBox/Source/plugins/Notice/jBox.Notice.js"></script>
    <link href="/common/plugins/jBox/Source/plugins/Notice/jBox.Notice.css" rel="stylesheet">


    <script>
    var Bizweb = Bizweb || {};
    </script>
     @if(isset($loadJS))
     <script type="text/javascript" src="{{url($loadJS)}}"></script>
    @endif
</head>
