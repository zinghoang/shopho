    <header id="header" class="wrap-header">
    <section class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-3">


                    
                    <div class="logo text-lg-left">
                        <a href="/"><img style="" src="/common/image/logo2.png" alt="shopho" ></a>
                    </div>
                        

                    <ul class="content-header pull-xs-right hidden-lg-up">
                        @if(!Session::get("user"))    
                        <li>
                            <div class="align-items"><i class="fa fa-user" aria-hidden="true"></i><a href="/dang-nhap">Đăng nhập</a></div>/
                        </li>
                        <li>
                            <div class="align-items"><i class="fa fa-unlock" aria-hidden="true"></i><a href="/dang-ky">Đăng ký</a></div>
                        </li>
                        @else
               
                            <div class="align-items dropdown " style="background: white"><i class="fa fa-user" ></i><a  data-toggle="dropdown"  href="#">{{Session::get("user")['mem_name']}}</a>


                             <ul class="dropdown-menu smart-user">
                                <li><a href="/bookmark">Bookmark sản phẩm</a></li>
                                <li><a href="/shop/">Trở thành đại lý</a></li>
                                <li><a href="/logout">Đăng xuất</a></li>
                               
                              </ul> 


                            </div>
                       
                        @endif


                    </ul>

                </div>
                <div class="col-xs-12 col-sm-7 col-lg-9 hidden-md-down">
                    <!-- Giỏ hàng -->
                    <div class="mini-cart pull-xs-right hidden-md-down">
                        <a href="/cart-list" class="icon-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="cartCount"></span> Giỏ hàng</a>
                      <!--   <div>
                            <div style="" class="top-cart-content arrow_box">
                              
                                <ul id="cart-sidebar" class="mini-products-list">

                                </ul>
                            </div>
                        </div> -->
                    </div>


                    <ul class="content-header pull-lg-right hidden-xs-down">
                        <li class="hidden-md-down no-hover">
                            <div class="align-items">
                                <i class="fa fa-phone" aria-hidden="true"></i> <a href="#">0905.70.72.70</a></div>
                        </li>
                        <li class="hidden-lg-down no-hover">
                            <div class="align-items">
                                <form action="/search" method="get" role="search" class="form-inline form-search-top">
                                    <button class="btn btn-search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    <input type="text" class="form-control search-field" value="" name="query" id="search" placeholder="Tìm kiếm">
                                </form>
                            </div>
                        </li>
                         @if(!Session::get("user"))     
                        
                        <li class="hidden-md-down">
                            <div class="align-items"><i class="fa fa-user" aria-hidden="true"></i><a href="/dang-nhap" >Đăng nhập</a></div></li>
                        <li class="hidden-md-down">
                            <div class="align-items"><i class="fa fa-unlock" aria-hidden="true"></i><a href="/dang-ky" >Đăng ký</a></div>
                        </li>
                       @else
                         <li class="hidden-md-down no-hover">
                            <div class="align-items dropdown" ><i class="fa fa-user" ></i><a data-toggle="dropdown" href="#">{{Session::get("user")['mem_name']}}</a>
                                <ul class="dropdown-menu">
                                <li><a href="/bookmark">Bookmark sản phẩm</a></li>
                                <li><a href="/shop/">Trở thành đại lý</a></li>
                                <li><a href="/logout">Đăng xuất</a></li>
                               
                              </ul> 
                            </div>
                        </li>

                        @endif
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="wrap-menu">
        <div class="container">
            <!-- MENU -->
            <div class="main-menu">
                <div class="wrap-nav-toggler">
                    <button class="navbar-toggler pull-xs-left hidden-lg-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">&#9776;</button>
                    <!-- Top Cart -->
                    <div class="mini-cart pull-xs-right hidden-lg-up">
                        <a href="/cart-list"><i class="fa fa-shopping-cart"></i><span class="cartCount"></span></a>
                       <!--  <div>
                            <div style="" class="top-cart-content arrow_box">
                             
                                <ul id="cart-sidebar" class="mini-products-list">

                                </ul>
                            </div>
                        </div> -->
                    </div>
                    <!-- Form search -->
                    <form action="/search" method="get" role="search" class="form-inline search-box pull-xs-right hidden-lg-up">
                        <button class="btn btn-search btn-s1" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <button class="btn btn-search btn-s2" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <input class="form-control search-field" type="text" value="" name="query" id="search" placeholder="Tìm kiếm">
                    </form>
                </div>
                <!-- End Nav Toggler -->

                <nav class="nav-main collapse navbar-toggleable-md" id="exCollapsingNavbar2">
                    <ul class="nav navbar-nav navbar-nav-style">
                        
                        <li class="nav-item active"><a class="nav-link" href="/">Trang chủ</a></li>
 
                        <li class="nav-item "><a class="nav-link" href="/gioi-thieu">Giới thiệu</a></li> 
                        <li class="nav-item   has-mega">
                            <a href="#" class="nav-link">Sản phẩm<i class="fa fa-caret-right fixfa"></i></a>
                            <a type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            
                            <div class="mega-content hidden-md-down">
    <div class="container">
        <div class="nav-block nav-block-center">
            <ul class="level0">
                
                
                <li class="level1"> <h2 class="h4"><a href="/"><span>Túi ,Balo</span></a></h2> 
                   <!--  <ul class="">
                        
                        <li class="level2"> <a href="/ssandal"><span>Sandal xỏ ngón</span></a> </li>
                        
                        <li class="level2"> <a href="/ssandal"><span>Sandal thường</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Sandal Mỹ</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Sandal Việt Nam</span></a> </li>
                        
                    </ul> -->
                </li>
                
                    
                
                <li class="level1"> <h2 class="h4"><a href="/"><span>Giày thời trang</span></a></h2> 
                    <!-- <ul class="">
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây cao cấp</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây nhập khẩu</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây Trung Quốc</span></a> </li>
                        
                        <li class="level2"> <a href="/collections/all"><span>Giày tây 2017</span></a> </li>
                        
                    </ul> -->
                </li>
                
                    
    
                
                    
            </ul>
        </div>
    </div>
</div>
<ul class="dropdown-menu hidden-lg-up">
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/">Túi,Balo</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
          <!--                                                                                   
            <li class="nav-item">
                <a class="nav-link" href="/ssandal">Sandal xỏ ngón</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/ssandal">Sandal thường</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Sandal Mỹ</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Sandal Việt Nam</a>
            </li>                                 -->       
            
        </ul>                      
    </li>
    
    
    
    <li class="dropdown-submenu nav-item">
        <a class="nav-link" href="/">Giày thời trang</a>
        <button type="button" class="fa fa-caret-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <ul class="dropdown-menu">
                                                                                            
           <!--  <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây cao cấp</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây nhập khẩu</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây Trung Quốc</a>
            </li>                                       
                                                                                            
            <li class="nav-item">
                <a class="nav-link" href="/collections/all">Giày tây 2017</a>
            </li>                                       
             -->
        </ul>                      
    </li>
    
    
    
  
    

</ul>
                            
                        </li>
                        <li class="nav-item "><a class="nav-link" href="/tin-tuc">Tin tức</a></li>
                        <li class="nav-item "><a class="nav-link" href="/ban-do">Bản đồ</a></li>   
                        <li class="nav-item "><a class="nav-link" href="/lien-he">Liên hệ</a></li>
                        
                        
                    </ul>
                </nav>
            </div><!-- /MENU -->
        </div>
    </div><!-- /wrap MENU -->
</header><!-- /header -->