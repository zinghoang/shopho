@extends('frontend.layout.main')

@section("content")
<style type="text/css">
    
    .form-group .form-control {
      margin-bottom: 5px;  
    }

</style>


<section class="bread-crumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">             
                <ul class="breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">                  
                    <li class="home">
                        <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>                      
                        <span><!-- <i class="fa fa-angle-right"></i> --> / </span>
                    </li>
                    
                  
                    <li><strong itemprop="title">Đăng ký thành viên</strong></li>
                    
                </ul>
            </div>
        </div>
    </div>
</section>


      <div class="row">
      <div class="col-md-9  col-main">
         <h3 class="" style="text-align: center"> Đăng ký thành viên </h3>


         <div class="">
            <div id="" style="">
              
                    <form class="form-horizontal" role="form" method="POST" action="/register">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('mem_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Tên nick *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="mem_name" value="{{ old('mem_name') }}" required autofocus>
                               
                                @if ($errors->has('mem_name'))
                                    <span class="mes-error">
                                        <strong>{{ $errors->first('mem_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mem_email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email *</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="mem_email" value="{{ old('mem_email') }}" required>

                                @if ($errors->has('mem_email'))
                                    <span class="mes-error">
                                        <strong>{{ $errors->first('mem_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Địa chỉ</label>

                            <div class="col-md-6">
                                <input id="address" class="form-control" name="mem_address" value="{{ old('mem_address') }}" >

                             
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Số điện thoại</label>

                            <div class="col-md-6">
                                <input id="phone" class="form-control" name="mem_phone" value="{{ old('mem_phone') }}" >

                             
                            </div>
                        </div>




                        <div class="form-group{{ $errors->has('mem_password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Mật khẩu *</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="mem_password" required>

                                @if ($errors->has('mem_password'))
                                    <span class="mes-error">
                                        <strong>{{ $errors->first('mem_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Nhập lại mật khẩu *</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="mem_password_confirmation" required>
                            </div>
                             @if ($errors->has('mem_password_confirmation'))
                                    <span class="mes-error">
                                        <strong>{{ $errors->first('mem_password_confirmation') }}</strong>
                                    </span>
                                @endif
                                
                        </div>

                        <div class="form-group">


                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Đăng Ký
                                </button>
                                <br>
                           <span class="mes-error">Xác nhận: Đăng ký tức là bạn đã chấp nhận điều khoản và dịch vụ của chúng tôi  </span>
                            </div>
                        </div>
                      
                       
                         
                    </form>

               
            </div>

         </div>
         </div>
         </div>

         <br> <br> <br> <br> <br> <br>
@endsection

