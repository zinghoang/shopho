@extends('frontend.layout.main')

@section("content")
<style type="text/css">
    
    .form-group .form-control {
      margin-bottom: 5px;  
    }

</style>


<section class="bread-crumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">             
                <ul class="breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">                  
                    <li class="home">
                        <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>                      
                        <span><!-- <i class="fa fa-angle-right"></i> --> / </span>
                    </li>
                    
                  
                    <li><strong itemprop="title">Đăng nhập thành viên</strong></li>
                    
                </ul>
            </div>
        </div>
    </div>
</section>


  <div class="">
      <div class="col-lg-12 col-md-9 col-main">
         <h3 class="panel_h1"><i class="glyphicon glyphicon-lock"></i> Đăng Nhập thành viên !</h3>


         <div class="widgetPanel">
            <div id="logon" style="max-width:600px">
              
                <form class="" role="form" method="POST" action="/login">
                        {{ csrf_field() }}

                  <div class="form-group">
                     <label for="UserName">Email</label>
                      <input id="email" type="email" class="form-control" name="mem_email" value="{{ old('mem_email') }} "  required >

                    @if ($errors->has('mem_email'))
                        <span class=" mes-error">
                            <strong>{{ $errors->first('mem_email') }}</strong>
                        </span>
                   @endif
                      
                  </div>


                  <div class="form-group">
                     <label for="Password">Mật khẩu</label> 
                      <input id="password" required type="password" class="form-control" name="password" >

                    @if ($errors->has('password'))
                        <span class=" mes-error">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                  </div>


                  <button class="btn btn-primary " title="Log In" type="submit">Đăng nhập</button>
               </form>
             
            </div>

         </div>
         <br>
         <br>    <br>
         <br>    <br>
         <br>    <br>
         <br>    <br>
         <br>    <br>
         <br>
  
      </div>
   </div>
@endsection

