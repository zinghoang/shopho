@extends('frontend.layout.main')

@section("content")

  <div class="row">
        <div class="col-xs-12 col-lg-9 col-lg-push-3">
            <div class="row">

                
                <div class="col-lg-12">
                    <div class="heading">
                        <h2 class="title-head"><a href="#">Sản phẩm mới</a></h2>
                    </div>
                    <div class="owl-sanphammoi owl-carousel owl-theme">
          	                              
         @foreach($product_hots as $product)   	  	
        <div class="item">
                            
           <div class="product-box">
            <div class="product-thumbnail">
            <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="{{$product->pro_name}}">
                
                <img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage(),400,400)}}" alt="">
             
            </a>
        </div>
        <h3 class="product-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>


        <div class="sale-flash"> {{ \App\Helpers\CommonHelper::productPersent($product)}} </div>
        <div class="product-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</div>
        <div class="product-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</div>


        	<form id="product-actions-3062548" action="" method="post" enctype="multipart/form-data">
        	    
        	    <a class="iWishAdd iwishAddWrapper" href="javascript:;" ><span class="iwishAddChild iwishAddBorder"></span></a>
        	    <a href="#" data-id="{{$product->pro_id}}" class="btn btn-style btn_add_to_cart">Thêm vào giỏ hàng</a>
        	    <a href="#" data-id="{{$product->pro_id}}"  class="btn btn-style quick-view btn_quick_view">
        	        <i class="fa fa-eye"></i>
        	    </a>
        	</form>
        </div>
         </div>
             
  @endforeach                                                       
  </div>


  </div>


        
                
                <div class="col-lg-12">
                    <div class="heading">
                        <h2 class="title-head"><a href="#">Sản phẩm khuyến mại</a></h2>
                    </div>
                    <div class="owl-khuyenmai owl-carousel owl-theme">
                                        
                                                          
         @foreach($product_sellings as $product)                
        <div class="item">
                            
           <div class="product-box">
    <div class="product-thumbnail">
    <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="{{$product->pro_name}}">
        
        <img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage())}}" alt="">
     
    </a>
</div>
<h3 class="product-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>


<div class="sale-flash"> {{ \App\Helpers\CommonHelper::productPersent($product)}} </div>
<div class="product-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</div>
<div class="product-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</div>


    <form id="product-actions-3062548" action="" method="post" enctype="multipart/form-data">
        
        <a class="iWishAdd iwishAddWrapper" href="javascript:;" ><span class="iwishAddChild iwishAddBorder"></span></a>
        <a href="#" data-id="{{$product->pro_id}}" class="btn btn-style btn_add_to_cart">Thêm vào giỏ hàng</a>
        <a href="#" data-id="{{$product->pro_id}}"  class="btn btn-style quick-view btn_quick_view">
            <i class="fa fa-eye"></i>
        </a>
    </form>
</div>
 </div>
     
  @endforeach              

                        
                    </div>
                </div>
                
                <div class="col-lg-12 hidden-sm-down">
                    <div class="banner-pc">
                        <a href="#">
                            <img src="/common/image/banner2.jpg" alt="Giày Bé Nam">
                            <div class="name-qc"><span>Giày Bé Nam</span></div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-4">
                    <div class="heading">
                        <h2 class="title-head"><a href="#">Sản phẩm nổi bật</a></h2>
                    </div>
                    <div class="owl-noibat owl-carousel owl-theme">
                                        
                                                                                  
         @foreach($product_sellings as $product)                
        <div class="item">
                            
           <div class="product-box">
    <div class="product-thumbnail">
    <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="{{$product->pro_name}}">
        
        <img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage())}}" alt="">
     
    </a>
</div>
<h3 class="product-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>


<div class="sale-flash"> {{ \App\Helpers\CommonHelper::productPersent($product)}} </div>
<div class="product-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</div>
<div class="product-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</div>


    <form id="product-actions-3062548" action="" method="post" enctype="multipart/form-data">
        
        <a class="iWishAdd iwishAddWrapper" href="javascript:;" ><span class="iwishAddChild iwishAddBorder"></span></a>
        <a href="#" data-id="{{$product->pro_id}}" class="btn btn-style btn_add_to_cart">Thêm vào giỏ hàng</a>
        <a href="#" data-id="{{$product->pro_id}}"  class="btn btn-style quick-view btn_quick_view">
            <i class="fa fa-eye"></i>
        </a>
    </form>
</div>
 </div>
     
  @endforeach    
                    
 </div>
 </div>
                
                <div class="col-lg-8">
                    <div class="heading">
                        <h2 class="title-head"><a href="san-pham-noi-bat">Sản phẩm mua nhiều</a></h2>
                    </div>
                    <div class="owl-muanhieu slide-list-pro2 owl-carousel owl-theme">
                            
                            @php 

                            $isOpen=false ;
                            $total  = count($product_hots); 
                            $count  =0;
                            $countItem  = 0;
                            @endphp

                            @foreach($product_hots as $product)  
                            
                           

                            @if($countItem==0 || !$isOpen)
                            <div class="row">  

                               @php  $isOpen= true; @endphp       
                             @endif   
                            <div class="product-item2 col-xs-12 {{$product->pro_id}}">
                                <div class="product-item-thumbnail">
                                    <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html"><img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage())}}" alt=""></a>
                                </div>
                                <h3 class="product-item-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>
                                
                                <div>
                                    <span class="product-item-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</span>
                                    <span class="product-item-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</span>
                                </div>
                                
                                <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" class="btn btn-style">MUA HÀNG</a>
                                
                            </div> 


                
                              @if($countItem==2 )
                                </div>
                                   @php  $isOpen= false; 
                                         $countItem = 0; 
                                   @endphp    
                              @else
                                @php $countItem++;  @endphp

                              @endif
                            


                               @php $count++; @endphp

                               @if(($total ==$count && $isOpen))

                               </div>
                               @endif
                            
                        @endforeach
                   
                        
                    </div>


                </div>
                


            </div>

        </div>

    @include("frontend.layout.left_menu")

    </div>

 <script type="text/javascript"> Product.init();</script>
@endsection    