<!-- Modal Đăng ký-->
<div class="modal fade" id="dangky" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog wrap-modal-login" role="document">
        <div class="text-xs-center">
            <div id="login">
                <h5 class="title-modal">Đăng ký nhanh</h5>
                <form accept-charset='UTF-8' action='/account/register' id='customer_register' method='post'>
<input name='FormType' type='hidden' value='customer_register' />
<input name='utf8' type='hidden' value='true' />
                <div class="form-signup" >
                    
                </div>
                <div class="form-signup clearfix">
                    <fieldset class="form-group">
                        <input type="text" class="form-control form-control-lg" value="" name="firstName" id="firstName"  placeholder="Họ tên*" required >
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="email" class="form-control form-control-lg" value="" name="email" id="email"  placeholder="Email" required="">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="Mật khẩu*" required >
                    </fieldset>
                    <p>Tôi đã đọc và đồng ý với điều khoản sử dụng</p>
                    <fieldset class="form-group">
                        <button type="summit" value="Đăng ký" class="btn btn-lg btn-style btn-style-active col-xs-12">Đăng ký</button>
                    </fieldset>
                    <fieldset class="form-group">
                        <button type="button" class="btn btn-lg btn-style col-xs-12" data-dismiss="modal">Hủy</button>
                    </fieldset>
                </div>
                </form>
                
            </div>
        </div>
    </div>
</div>

<!--End  Modal Đăng ký-->