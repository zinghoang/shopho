<!-- Modal Đăng nhập -->
<div class="modal fade" id="dangnhap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog wrap-modal-login" role="document">
        <div class="text-xs-center">
            <div id="login">
                <h5 class="title-modal">Đăng nhập</h5>
                <form accept-charset='UTF-8' action='/account/login' id='customer_login' method='post'>
<input name='FormType' type='hidden' value='customer_login' />
<input name='utf8' type='hidden' value='true' />
                <div class="form-signup" >
                    
                </div>
                <div class="form-signup clearfix">
                    <fieldset class="form-group">
                        <input type="email" class="form-control form-control-lg" value="" name="email" id="customer_email" placeholder="Nhập ID*" required requiredmsg="Nhập tài khoản" >
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="password" class="form-control form-control-lg" value="" name="password" id="customer_password" placeholder="Nhập mật khẩu*" required requiredmsg="Nhập mật khẩu">
                    </fieldset>
                    <fieldset class="form-group">
                        <input class="btn btn-lg btn-style btn-style-active col-xs-12" type="submit" value="Đăng nhập" />
                    </fieldset> 
                    <fieldset class="form-group">
                        <button type="button" class="btn btn-lg btn-style col-xs-12" data-dismiss="modal">Hủy</button>
                    </fieldset>                 
                    <div>
                        <p><a href="#" class="btn-link-style btn-link-style-active" onclick="showRecoverPasswordForm();return false;">Quên mật khẩu ?</a></p>
                        <!-- <a href="/account/register" class="btn-link-style">Đăng ký tài khoản mới</a> -->
                    </div>
                </div>
                </form>
            </div>

            <div id="recover-password" class="form-signup">
                <h2 class="title-head text-xs-center"><a href="#">Lấy lại mật khẩu</a></h2>
                <p>Chúng tôi sẽ gửi thông tin lấy lại mật khẩu vào email đăng ký tài khoản của bạn</p>

                <form accept-charset='UTF-8' action='/account/recover' id='recover_customer_password' method='post'>
<input name='FormType' type='hidden' value='recover_customer_password' />
<input name='utf8' type='hidden' value='true' />
                <div class="form-signup" >
                    
                </div>
                <div class="form-signup clearfix">
                    <fieldset class="form-group">
                        <input type="email" class="form-control form-control-lg" value="" name="Email" id="recover-email" placeholder="Email*" required requiredmsg="Nhập email">
                    </fieldset>
                </div>
                <div class="action_bottom">
                    <input class="btn btn-lg btn-style btn-recover-send" type="submit" value="Gửi" /> hoặc
                    <a href="#" class="btn btn-lg btn-style btn-style-active btn-recover-cancel" onclick="hideRecoverPasswordForm();return false;">Hủy</a>
                </div>
                </form>

            </div>

            <script type="text/javascript">
                function showRecoverPasswordForm() {
                    document.getElementById('recover-password').style.display = 'block';
                    document.getElementById('login').style.display='none';
                }

                function hideRecoverPasswordForm() {
                    document.getElementById('recover-password').style.display = 'none';
                    document.getElementById('login').style.display = 'block';
                }

                if (window.location.hash == '#recover') { showRecoverPasswordForm() }
            </script>

        </div>
    </div>
</div>


<!--End Modal Đăng nhập -->