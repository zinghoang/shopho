@php  $listImageThumbnail   = $product->getImage(); @endphp	

		<div class="quick-view-product">
		<div class="block-quickview primary_block row">
			<div class="product-left-column col-xs-12 col-sm-4 col-md-5">
				<div class="clearfix image-block">
					<span class="view_full_size">
						<a class="img-product" title="" href="#">
							<img style="min-height:300px; max-height: 300px;" id="product-featured-image-quickview" class="img-responsive product-featured-image-quickview" src="{{\App\Helpers\CommonHelper::ProductImageThumbnail($listImageThumbnail,400,400)}}" alt="">
						</a>
					</span>
					<div class="loading-imgquickview" style="display:none;"></div>
				</div>
				<div class="more-view-wrapper clearfix">
					<div id="thumbs_list_quickview">
						<ul class="product-photo-thumbs quickview-more-views-owlslider owl-loaded owl-drag" id="thumblist_quickview" style="visibility: visible;">
						<div class="owl-stage-outer">
						<div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: 0s; width: 254px;">
				
						
						
						@if(count($listImageThumbnail) >0 )
						@foreach($listImageThumbnail as  $img)
						<div class="owl-item  active" style="width: 74.5px; margin-right: 10px;">
						<li>
							<a href="javascript:void(0)"  data-zoom-image="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl($img->pro_img_url)}}"><img width="50" height="50" src="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl($img->pro_img_url,400,400)}}" alt="">
							</a>
						</li>
						</div>
						@endforeach
						@else 
						<div class="owl-item  active" style="width: 74.5px; margin-right: 10px;">
							<li>
							<a href="javascript:void(0)" data-imageid="3062548" "="" data-zoom-image="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl('no')}}"><img width="50" height="50" src="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl('no')}}" alt="">
							</a>
						</li>
						</div>
						@endif					
						 </div>
					  </div>
						
						</ul>
					</div>
				</div>
			</div>
			<div class="product-center-column product-info product-item col-xs-12 col-sm-8 col-md-7" id="product-3062548">
				<h3 class="qwp-name">{{$product->pro_name}}</h3>
				<div class="qwp-vendor"><label>Thương hiệu: Đang cập nhật...</label></div>
				<div class="quickview-info">
					<span class="prices">
						<span class="price h2 sale-price on-sale">{{ \App\Helpers\CommonHelper::productPrice($product)}}</span>
						<del class="old-price">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</del>
					</span>
				</div>
				<div class="product-description rte">
					{!!$product->attr()->attr_description!!}
				</div>
				<form action="" method="post" enctype="multipart/form-data" class="variants form-ajaxtocart" id="product-actions-3062548">					
					<div class="qvariants clearfix">
						<div class="selector-wrapper"><label>Màu sắc</label>
						<select class="single-option-selector" >
						    <option value="{{$product->attr()->attr_fk_color_id}}">{{\App\Helpers\CommonHelper::showColorProduct($product->attr()->attr_fk_color_id)}}</option>
						</select>
					
					</div>					
					<div class="clearfix"></div>
					<div class="quantity_wanted_p box-update-to-cart">
						<label for="quantity-detail" class="quantity-selector">Số lượng</label>
						<input type="" id="quantity-detail" name="quantity" value="1" class="quantity-selector text-center quantity">
						<button type="button" name="add" data-id="{{$product->pro_id}}"  class="btn btn-primary add_to_cart_detail btn_add_to_cart ajax_addtocart">
							<span>Mua hàng</span>
						</button>
					</div>
					<div class="total-price" style="display:none">0₫</div>

				<input type="hidden" name="id" value="3062548"></form>
				<p class="qv-contact">Gọi <b><a href="tel:1900.6750"> 0905 70.72.70</a></b> để được trợ giúp</p>
			</div>

		</div>      
		<a title="Close" class="quickview-close close-window" href="javascript:;"><i class="fa   fa-times-circle"></i></a>
