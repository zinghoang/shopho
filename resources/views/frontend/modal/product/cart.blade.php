 <div id="popup-cart-desktop" class="clearfix">
        <div class="title-popup-cart">
            <i class="fa fa-check check" aria-hidden="true"></i>
            <span class="pop-title">Bạn đã thêm <span class="cart-popup-name"></span> vào giỏ hàng</span>
            <div class="title-quantity-popup" onclick="window.location.href='/cart'">
                Giỏ hàng của bạn (<span class="cart-popup-count"></span> sản phẩm)
            </div>
            <div class="content-popup-cart">
                <div class="thead-popup">
                    <div style="width: 55%;" class="text-left">Sản phẩm</div>
                    <div style="width: 15%;" class="text-right">Đơn giá</div>
                    <div style="width: 15%;" class="text-center">Số lượng</div>
                    <div style="width: 15%;" class="text-right">Thành tiền</div>
                </div>
                <div class="tbody-popup">
                    <div class="item-popup productid">
                            <div style="width: 55%;" class="text-left">
                                <div class="item-image"><a class="product-image" href="/converse-all-star" title="Giày tây nâu đỏ thương hiệu Converse All Star - Nâu"><img alt="Giày tây nâu đỏ thương hiệu Converse All Star - Nâu" src="//bizweb.dktcdn.net/100/091/133/products/2-382751d6-ebd4-477c-83d5-5045a3a23999.jpg?v=1466415099313" width="80"></a></div>
                                <div class="item-info">
                                    <p class="item-name"><a href="" title=""></a></p>
                                    <p class="item-remove"><a href="javascript:;" class="remove-item-cart" title="Xóa" data-id="4599718"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>
                                    <p class="addpass" style="color:#fff;"><span class="add_sus" style="color:#898989;"><i style="margin-right:5px; color:red; font-size:13px;" class="fa fa-check" aria-hidden="true"></i>Sản phẩm vừa thêm!</span></p>
                                </div>
                              </div>
                        <div style="width: 15%;" class="text-right">
                            <div class="item-price"><span class="price">500.000₫</span></div>
                        </div>
                        <div style="width: 15%;" class="text-center">
                            <div class="fixab"><input class="pro_id" type="hidden" name="variantId" value="4599718"><button onclick="var result = document.getElementById('qtyItem4599718'); var qtyItem4599718 = result.value; if( !isNaN( qtyItem4599718 ) &amp;&amp; qtyItem4599718 > 1 ) result.value--;return false;" class="reduced items-count btn-minus" type="button"><i class="fa fa-caret-down"></i></button><input type="text" maxlength="12" min="0" class="input-text number-sidebar qtyItem4599718" id="qtyItem4599718" name="Lines" size="4" value="4"><button onclick="var result = document.getElementById('qtyItem4599718'); var qtyItem4599718 = result.value; if( !isNaN( qtyItem4599718 )) result.value++;return false;" class="increase items-count btn-plus" type="button"><i class="fa fa-caret-up"></i></button></div>
                        </div>
                        <div style="width: 15%;" class="text-right"><span class="cart-price"> <span class="price">2.000.000₫</span> </span>
                        </div>
                  </div>

                </div>
                <div class="tfoot-popup">
                    <div class="tfoot-popup-1 clearfix">
                        <div class="pull-left popup-ship">
                            <p>Giao hàng trên toàn quốc</p>
                        </div>
                        <div class="pull-right popup-total">
                            <p>Thành tiền: <span class="total-price"></span></p>
                        </div>
                    </div>
                    <div class="tfoot-popup-2 clearfix">
                        <a class="button btn-proceed-checkout" title="Tiến hành đặt hàng" href="/checkout"><span>Tiến hành đặt hàng <i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
                        <a class="button btn-continue" title="Tiếp tục mua hàng" onclick="$('#popup-cart').modal('hide');"><span><span><i class="fa fa-caret-left" aria-hidden="true"></i> Tiếp tục mua hàng</span></span></a>
                    </div>
                </div>
            </div>
            <a title="Close" class="quickview-close close-window" href="javascript:;" onclick="$('#popup-cart').modal('hide');"><i class="fa  fa-close"></i></a>
        </div>
</div>
