@extends('frontend.layout.main')

@section("content")

<div class="col-md-offset-3 col-md-7 thankyou-message" style="margin-bottom: 20px;padding-top: 10px">
                        <div class="thankyou-message-icon unprint">
                            <div class="icon icon--order-success svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="72px" height="72px">
                                    <g fill="none" stroke="#8EC343" stroke-width="2">
                                        <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                        <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="thankyou-message-text">
                            <h3>Cảm ơn bạn đã đặt hàng</h3>
                            <p>
                              Chúng tôi sẽ liên lạc với bạn trong vòng 24h để xác nhận thông tin đơn hàng <br>
                              Nếu có thắc mắc về sản phẩm bạn có thể liên hệ : 0905.70.72.70 <br>
                              Hoặc facebook :   00000
                                
                            </p>
                            <div style="">
                                <a href="/"> TIẾP TỤC MUA HÀNG TẠI SHOP</a>
                            </div>
                        </div>
                        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
 </div>

@endsection  