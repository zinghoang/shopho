
@extends('frontend.layout.main')

@section("content")
@php  $listImageThumbnail   = $product->getImage(); @endphp	


<section class="bread-crumb">
	<div class="">
		<div class="row">
			<div class="col-xs-12">				
				<ul class="breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>						
						<span><!-- <i class="fa fa-angle-right"></i> --> / </span>
					</li>
					
				
					<li><strong itemprop="title">{{$product->pro_name}}</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>


<div class="row">
<div class="col-xs-12 col-lg-12">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-5 details-product">
						<div class="large-image">
							<img id="zoom_01" src="{{\App\Helpers\CommonHelper::ProductImageThumbnail($listImageThumbnail,400,400)}}" alt="">
						</div>
						<div class="owl-carousel owl-theme thumbnail-product">
							
						@if(count($listImageThumbnail) >0 )
						@foreach($listImageThumbnail as  $img)

							<div class="item" data-image="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl($img->pro_img_url,400,400)}}">
								<img src="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl($img->pro_img_url)}}" alt="">
							</div>
							
						@endforeach
						@else 
							<div class="item" data-image="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl('no')}}">
								<img src="{{\App\Helpers\CommonHelper::showProductImageThumbnailUrl('no')}}" alt="">
							</div>
						@endif	
						</div>
					</div>

					<div class="product-shop col-xs-12 col-sm-12 col-lg-7 details-product">
						<h1 class="product-name"  itemprop="name">{{$product->pro_name}}</h1>
						<ul class="info-model-pro">
							<li itemprop="brand"><span><b>Thương hiệu:</b> ...</span></li>
							<li>
								<b>Tình Trạng:&nbsp;</b> 
								
								<span class="availability in-stock">Còn hàng </span>
								
							</li>
						</ul>
						

						<div class="price-box">
							<span class="special-price"><span class="price product-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</span> </span> <!-- Giá Khuyến mại -->
							<span class="old-price"><span class="price product-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</span> </span> <!-- Giá gốc -->
							
						</div>

						
						<div class="product-description product_description rte">
							<!-- <p>- Bảo hành: 12 tháng</p> -->

						</div>
						

						<div class="form-product">
							<form enctype="multipart/form-data" id="add-to-cart-form" action="" method="post" class="form-inline">
								
								<label class="pull-xs-left label-form-pro form-group2 ">Số lượng: &nbsp;&nbsp;&nbsp;</label>
								<div class="box-update-to-cart">
								<div class="custom custom-btn-number form-control form-group2 ">
									<input type="text" class="input-text qty quantity" title="Số lượng" value="1" id="qty" name="quantity" >
							
								</div>
								<div class="btn-form-product pull-sm-left form-group2 ">
									
									<button type="button" data-id="{{$product->pro_id}}"  class="btn btn-lg btn-style btn-style-active btn-cart add_to_cart btn_add_to_cart" title="Thêm vào giỏ hàng"><span><i class="icon-basket"></i> GIỎ HÀNG</span></button>
									
								</div>
								</div>																
								
								<label class="label-form-pro call-phone pull-xs-left "><span class="hidden-xs-down">&nbsp;&nbsp;&nbsp;</span>Gọi <a href="tel:046674 2332" title="Liên hệ">0905 70.72.70</a> để được trợ giúp</label>
								
								<br />
							<a class="iWishAdd iwishAddWrapper" href="javascript:;" data-customer-id="0" data-product="3062548"><span class="iwishAddChild iwishAddBorder"><img class="iWishImg" src="/common/image/iwish_add.png" /></span><span class="iwishAddChild">Thêm vào yêu thích</span></a>

							</form>
							
							<div class="tag-product">
								<label class="inline">Tags: </label>
							<!-- 	
								<a href="/collections/all/chuck-taylor">Chuck Taylor</a>						
								
								<a href="/collections/all/converse">converse</a>						
								
								<a href="/collections/all/giay-tay">Giày tây</a>						
								
								<a href="/collections/all/giay-vai">Giày vải</a>						 -->
								
							</div>
							
							<div class="social-media text-xs-left">
								
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-tab-detailspro" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#home" role="tab">THÔNG TIN SẢN PHẨM</a>
							</li>
							
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#profile" role="tab">Hướng dẫn mua hàng</a>
							</li>
							
							
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#messages" role="tab">Bình luận</a>
							</li>
							
						</ul>
						<div class="tab-content tab-content-detailspro">
							<div class="tab-pane fade in active rte" id="home" role="tabpanel">
								{!!$product->attr()->attr_description!!}
							</div>
							
							<div class="tab-pane fade" id="profile" role="tabpanel">
								Các nội dung Hướng dẫn mua hàng viết ở đây
							</div>
							
							
							<div class="tab-pane fade" id="messages" role="tabpanel">
								Nội dung đánh giá chi tiết viết ở đây
							</div>
							
						</div>
					</div>
					
					
					
					
					<div class="col-xs-12" style="margin-bottom:30px;">
						<div class="heading">
							<h2 class="title-head"><a href="/giay-luoi">Sản phẩm liên quan</a></h2>
						</div>
						<div class="owl-sanphamlienquan owl-carousel owl-theme products-view-grid">
							
							
							
							
						         @foreach($product_categories as $product)                
							        <div class="item">
							                            
							           <div class="product-box">
							    <div class="product-thumbnail">
							    <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="{{$product->pro_name}}">
							        
							        <img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage())}}" alt="">
							     
							    </a>
							</div>
							<h3 class="product-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>


							<div class="sale-flash"> {{ \App\Helpers\CommonHelper::productPersent($product)}} </div>
							<div class="product-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</div>
							<div class="product-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</div>


							    <form id="product-actions-3062548" action="" method="post" enctype="multipart/form-data">
							        
							        <a class="iWishAdd iwishAddWrapper" href="javascript:;" ><span class="iwishAddChild iwishAddBorder"></span></a>
							        <a href="#" data-id="{{$product->pro_id}}" class="btn btn-style btn_add_to_cart">Thêm vào giỏ hàng</a>
							        <a href="#" data-id="{{$product->pro_id}}"  class="btn btn-style quick-view btn_quick_view">
							            <i class="fa fa-eye"></i>
							        </a>
							    </form>
							</div>
							 </div>
							     
							  @endforeach
								
						</div>
					</div>
										
				</div>
			</div>


<script type="text/javascript">
	
	$('.selector-wrapper').css({
			   'text-align':'left',
			   'margin-bottom':'0px'
		   });


	$('.details-product .thumbnail-product .item').click(function(e){
		$('img#zoom_01').attr('src',$(this).attr('data-image'));
	})

Product.init();
</script>
</div>
@endsection    			