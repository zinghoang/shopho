@extends('frontend.layout.main')

@section("content")

 <div class="col-main cart_desktop_page cart-page">
    <div class="cart page_cart hidden-xs-down">
        <form action="/cart" method="post" novalidate="">
            <div class="bg-scroll">
                <div class="cart-thead">
                    <div style="width: 17%">Ảnh sản phẩm</div>
                    <div style="width: 33%"><span class="nobr">Tên sản phẩm</span></div>
                    <div style="width: 15%" class="a-center"><span class="nobr">Đơn giá</span></div>
                    <div style="width: 14%" class="a-center">Số lượng</div>
                    <div style="width: 15%" class="a-center">Thành tiền</div>
                    <div style="width: 6%">Xoá</div>
                </div>
                <div class="cart-tbody">
                @php $total_price  = 30000; @endphp 
                	@foreach($cartList as $cart )

                    @php $total_price+= $cart->subtotal; @endphp
                    <div class="item-cart productid-{{$cart->id}}">
                        <div style="width: 17%" class="image"><a class="product-image" title="" href="/product/{{str_slug($cart->product->pro_name) .'-'.$cart->product->pro_id}}.html"><img width="75" height="auto" alt="" src="{{\App\Helpers\CommonHelper::productImageThumbnail($cart->product->getImage())}}"></a></div>
                        <div style="width: 33%" class="a-center">
                            <h2 class="product-name"> <a href="/product/{{str_slug($cart->product->pro_name) .'-'.$cart->product->pro_id}}.html">{{$cart->product->pro_name}}</a> </h2><span class="variant-title">{{App\Helpers\CommonHelper::showColorProduct($cart->product->attr()->attr_fk_color_id)}}</span></div>
                        <div style="width: 15%" class="a-center"><span class="item-price"> <span class="price">{{App\Helpers\CommonHelper::numberFormatPrice($cart->price)}}</span></span>
                        </div>
                        <div style="width: 14%" class="a-center">
                            <div class="input_qty_pr"><input class="pro_id" type="hidden" name="variantId" value="{{$cart->id}}"><button onclick="var result = document.getElementById('qtyItem{{$cart->id}}'); var qtyItem{{$cart->id}} = result.value; if( !isNaN( qtyItem{{$cart->id}} ) &amp;&amp; qtyItem{{$cart->id}} > 1 ) result.value--;return false;" class="reduced_pop items-count btn-minus" type="button">–</button><input type="text" maxlength="12" min="0" class="input-text number-sidebar input_pop input_pop qtyItem{{$cart->id}}" id="qtyItem{{$cart->id}}" name="Lines" size="4" value="{{$cart->qty}}"><button onclick="var result = document.getElementById('qtyItem{{$cart->id}}'); var qtyItem{{$cart->id}} = result.value; if( !isNaN( qtyItem{{$cart->id}} )) result.value++;return false;" class="increase_pop items-count btn-plus" type="button">+</button></div>
                        </div>
                        <div style="width: 15%" class="a-center"><span class="cart-price"> <span class="price">{{App\Helpers\CommonHelper::numberFormatPrice($cart->subtotal)}}</span> </span>
                        </div>
                        <div style="width: 6%"><a class="button remove-item  btn_remove_item_cart" title="Xóa" href="javascript:;" data-id="{{$cart->id}}"><span><span>Xóa</span></span></a></div>
                    </div>

                    @endforeach
                </div>
            </div>
        </form>

        @if(count($cartList) >0)
        <div class="cart-collaterals cart_submit row">
            <div class="totals col-sm-6 col-md-5 col-xs-12 col-md-offset-7">
                <div class="totals">
                    <div class="inner">
                        <table class="table shopping-cart-table-total" id="shopping-cart-totals-table">
                            <colgroup>
                                <col>
                                 <tr>
                                    <td colspan="1" class="a-left"><strong>Phí ship </strong></td>
                                    <td class="a-right"><strong><span class=" ">30,000₫</span></strong></td>
                                </tr>
                                <col>
                            </colgroup>
                            <tfoot>
                                <tr>
                                    <td colspan="1" class="a-left"><strong>Tổng tiền</strong></td>
                                    <td class="a-right"><strong><span class="totals_price price">{{App\Helpers\CommonHelper::numberFormatPrice($total_price)}}</span></strong></td>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>

        @else
        <br>
        <h4 style="text-align: center;color:red">Hiện tại bạn chưa tiến hành mua hàng vui lòng chọn sản phẩm .. </h4> <br>

        @endif
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="">
                <form method="POST" action="/checkout">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3 style="margin-bottom: 25px; text-align: center;">TIẾN HÀNH MUA HÀNG</h3>
                    <div class="row">

                           @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="order_cus_name">Tên người nhận</label>
                                <input type="text" value="{{$member->mem_name}}" name="order_cus_name" class="form-control">
                            </div>
                           
                            <div class="form-group">
                                <label for="email">Số điện thoại</label>
                                <input type="text" name="order_cus_phone"  value="{{$member->mem_phone}}"  class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="email">Địa chỉ</label>
                                <input type="text" name="order_cus_address"  value="{{$member->mem_address}}"  class="form-control">
                            </div>


                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="message">Ghi chú thêm</label>
                                <textarea class="form-control" name="order_cus_detail" rows="5"></textarea>
                            </div>
                            <div class="text-right">
                           
                                <ul class="checkout">
                                 <li><button class="button btn-proceed-checkout" title="Tiến hành đặt hàng" type="submit"><span>Tiến hành đặt hàng</span></button>
                                 </li>
                        </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
 <script type="text/javascript"> Product.init();</script>
@endsection    