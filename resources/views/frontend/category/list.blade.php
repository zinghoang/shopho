   
@extends('frontend.layout.main')

@section("content")
<section class="bread-crumb">
	<div class="">
		<div class="row">
			<div class="col-xs-12">				
				<ul class="breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>						
						<span><!-- <i class="fa fa-angle-right"></i> --> / </span>
					</li>
					
					<li>
					 <strong><span  class="title">{{$category->cat_name}}</span></strong>
						
					</li>
					
					
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="row"> 
	<div class="col-xs-12 col-lg-9 col-lg-push-3">
			

			<h1 class="title-head">{{$category->cat_name}}</h1>

		

			<section class="products-view-grid">
				<div class="row">
					
					 @foreach($product_categories as $product)    
					<div class="col-xs-12 col-sm-6 col-lg-4">
            
							       
							                            
							  <div class="product-box">
							    <div class="product-thumbnail">
							    <a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="{{$product->pro_name}}">
							        
							        <img src="{{\App\Helpers\CommonHelper::productImageThumbnail($product->getImage())}}" alt="">
							     
							    </a>
							</div>
							<h3 class="product-name"><a href="/product/{{str_slug($product->pro_name) .'-'.$product->pro_id}}.html" title="">{{$product->pro_name}}</a></h3>


							<div class="sale-flash"> {{ \App\Helpers\CommonHelper::productPersent($product)}} </div>
							<div class="product-price">{{ \App\Helpers\CommonHelper::productPrice($product)}}</div>
							<div class="product-price-old">{{\App\Helpers\CommonHelper::productPriceOld($product)}}</div>


							    <form id="product-actions-3062548" action="" method="post" enctype="multipart/form-data">
							        
							        <a class="iWishAdd iwishAddWrapper" href="javascript:;" ><span class="iwishAddChild iwishAddBorder"></span></a>
							        <a href="#" data-id="{{$product->pro_id}}" class="btn btn-style btn_add_to_cart">Thêm vào giỏ hàng</a>
							        <a href="#" data-id="{{$product->pro_id}}"  class="btn btn-style quick-view btn_quick_view">
							            <i class="fa fa-eye"></i>
							        </a>
							    </form>
							</div>
							
							     
						
					</div>
					
						  @endforeach

					
			</div>
			{{$product_categories->links()}}		 
			</section>
					
		
					
		
		</div>




   @include("frontend.layout.left_menu")
 </div>

 <script type="text/javascript"> Product.init();</script>

 @endsection