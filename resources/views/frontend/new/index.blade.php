@extends('frontend.layout.main')

@section("content")

<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">				
				<ul class="breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>						
						<span><!-- <i class="fa fa-angle-right"></i> --> / </span>
					</li>
					
					
					<li><strong itemprop="title">Tin tức</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>


<div class="container" itemscope="" itemtype="http://schema.org/Blog">
	<meta itemprop="name" content="Tin tức nổi bật">
	<meta itemprop="description" content="">
	
	<div class="row">		
		<div class="col-xs-12 col-lg-9 col-lg-push-3">			
			
			<div class="box-heading">
				<h1 class="title-head">Tin tức nổi bật</h1>
			</div>
			<div class="row">
				
				

				
				@if(isset($top3[0]))
                    @php  $top  = $top3[0] ; @endphp
				<div class="col-lg-8">
					<div class="blog-item-db">
						<div class="blog-item-thumbnail">
							
							<a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}"><img height="375" src="{{$top->news_thumb}}" alt=""></a>
							
						</div>
						<div class="blog-item-content">
							<h3 class="blog-item-name"><a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}">
						     {{$top->news_title}}		
							</a></h3>
							<div class="postby">
								<div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin</div>
								<div><i class="fa fa-clock-o" aria-hidden="true"></i>{{$top->created_at}} </div>
							</div>
							<p class="blog-item-summary"> {{nl2br($top->news_description)}}</p>	
						</div>
					</div>
				</div>
				
				@endif
				@if(isset($top3[1]))
                    @php  $top  = $top3[1] ; @endphp
				<div class="col-lg-4">
						<div class="blog-item-db">
						<div class="blog-item-thumbnail">
							
							<a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}"><img height="175" src="{{$top->news_thumb}}" alt=""></a>
							
						</div>
						<div class="blog-item-content">
							<h3 class="blog-item-name"><a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}">
						     {{$top->news_title}}		
							</a></h3>
							<div class="postby">
								<div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin</div>
								<div><i class="fa fa-clock-o" aria-hidden="true"></i>{{$top->created_at}} </div>
							</div>
							<p class="blog-item-summary"> {{nl2br($top->news_description)}}</p>	
						</div>
					</div>
				</div>
				
				@endif
				
				@if(isset($top3[2]))
                    @php  $top  = $top3[2] ; @endphp
				<div class="col-lg-4">
						<div class="blog-item-db">
						<div class="blog-item-thumbnail">
							
							<a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}"><img height="175" src="{{$top->news_thumb}}" alt=""></a>
							
						</div>
						<div class="blog-item-content">
							<h3 class="blog-item-name"><a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}">
						     {{$top->news_title}}		
							</a></h3>
							<div class="postby">
								<div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin</div>
								<div><i class="fa fa-clock-o" aria-hidden="true"></i>{{$top->created_at}} </div>
							</div>
							<p class="blog-item-summary"> {{nl2br($top->news_description)}}</p>	
						</div>
					</div>
				</div>
				
				@endif
				
				
	      @foreach($news as $new)
				
				<div class="blog-item">
					<div class="col-md-6 col-lg-4">
						<div class="blog-item-thumbnail">
							
							<a href="tin-tuc/{{str_slug($new->news_title)}}-{{$new->news_id}}"><img height="175" src="{{$new->news_thumb}}"></a>
							
						</div>
					</div>
					<div class="col-md-6 col-lg-8">
						<h3 class="blog-item-name"><a href="tin-tuc/{{str_slug($new->news_title)}}-{{$new->news_id}}"> {{$new->news_title}}		</a></h3>
						<div class="postby">
							<div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin </div>
							<div><i class="fa fa-clock-o" aria-hidden="true"></i> {{$new->created_at}}</div>
						</div>
						<p class="blog-item-summary"> {{nl2br($new->news_description)}} </p>
					</div>
				</div>

			
				
		
				
				

				@endforeach	

				

				<div class="col-xs-12 text-xs-right">
					
				</div>
			</div>

			
		</div>


   @include("frontend.new.menu_left",["top"=>isset($top3[0]) ? $top3[0] : null])

	</div>
</div>


@endsection