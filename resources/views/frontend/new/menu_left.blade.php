<div class="col-xs-12 col-lg-3 col-lg-pull-9">
			<aside class="box-aside">
	<h2 class="title-box">
		Danh sách tin tức
	</h2>
	<nav class="nav-list-dm">
		<ul class="nav navbar-pills">
			
			@foreach($category as $cate)
		
		    @if(isset($current_cate) && $current_cate  == $cate->news_cat_id)
		      @php $active  = "active" ; @endphp 
		    @else
		      @php $active ="" ; @endphp

		    @endif   	
			<li class="nav-item {{$active}}"><a class="nav-link" href="/tin-tuc?cate_id={{$cate->news_cat_id}}">{{$cate->news_cat_name}}</a></li>
			
			
			@endforeach
			
		
			
		</ul>
	</nav>
</aside>



<aside class="box-aside">
	<div class="head-box">
		<h2 class="title-box">
			<a href="#">Tin tức</a>
		</h2>
	</div>
	<div>
		
		@if($top != null)
		<div class="blog-item">
			<div class="blog-item-thumbnail">
					<a href="tin-tuc/{{str_slug($top->news_title)}}-{{$top->tops_id}}"><img height="175" src="{{$top->news_thumb}}"></a>
			</div>
				<h3 class="blog-item-name"><a href="/tin-tuc/{{str_slug($top->news_title)}}-{{$top->news_id}}">
						     {{$top->news_title}}		
							</a></h3>
				<div class="postby">
								<div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin</div>
								<div><i class="fa fa-clock-o" aria-hidden="true"></i>{{$top->created_at}} </div>
							</div>
			<p class="blog-item-summary"> {{nl2br($top->news_description)}}</p>	
		</div>
		@else 
		
	<!-- 	
		<div class="blog-item-link">
			<h3 class="blog-item-name">
				<i class="fa fa-caret-right" aria-hidden="true"></i>
				<a href="/tin-tuc/xu-huong-thoi-trang-vintage-du-nhap-vao-viet-nam-1">Ngắm những mẫu giày mới lên kệ cuối tháng 3/2016</a>
			</h3>
			<div class="postby">
				07/06/2016
			</div>
		</div>
		
		
		
		<div class="blog-item-link">
			<h3 class="blog-item-name">
				<i class="fa fa-caret-right" aria-hidden="true"></i>
				<a href="/tin-tuc/5-mau-giay-sneaker-nu-dep-danh-cho-xuan-he-2016">5 mẫu giày sneaker nữ đẹp dành cho Xuân-Hè 2016</a>
			</h3>
			<div class="postby">
				07/06/2016
			</div>
		</div> -->
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		@endif
	</div>
</aside>

		</div>