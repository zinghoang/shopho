@extends('frontend.layout.main')

@section("content")

<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">				
				<ul class="breadcrumbs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">					
					<li class="home">
						<a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>						
						<span><!-- <i class="fa fa-angle-right"></i> --> / </span>
					</li>
					
					<li>
						<a itemprop="url" href="/tin-tuc"><span itemprop="title">Tin tức nổi bật</span></a>	
						<span><!-- <i class="fa fa-angle-right"></i> --> / </span>
					</li>
					<li><strong itemprop="title">{{$new->news_title}}</strong></li>
					
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="container" itemscope="" itemtype="http://schema.org/Blog">
	<meta itemprop="name" content="Tin tức nổi bật">
	<meta itemprop="description" content="">
	
	<div class="row">		
		<div class="col-xs-12 col-lg-9 col-lg-push-3">			
			
			<section class="article-main">
				<div class="row">
					<div class="col-lg-12">
						<div class="article-details">
							<div class="article-image">
								
								<img class="img-fluid" src="{{$new->news_thumb}}" alt="">
								
							</div>
							<h1 class="article-title" itemprop="name">{{$new->news_title}}</h1>
							<div class="postby">
							<div style="float:left; margin-right:20px;"><i class="fa fa-user" aria-hidden="true"></i> Admin </div>
							<div><i class="fa fa-clock-o" aria-hidden="true"></i> {{$new->created_at}}</div>
						</div>
							<div class="article-content rte" itemprop="description">
								{{nl2br($new->news_description)}}
							</div>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="row" style="border-top:1px #ebebeb solid; border-bottom:1px #ebebeb solid; padding:15px 0px;">
						
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs">
								{!!$new->news_content!!}
							</div>
						</div>
					</div>

					 
					<div class="col-xs-12">
						

						<form accept-charset="UTF-8" action="" id="article_comments" method="post">
<input name="FormType" type="hidden" value="article_comments">
<input name="utf8" type="hidden" value="true"> 

						
						
						<div id="article-post-comment" class="form-coment">
							<div class="row">
								<div class="col-xs-12">
									<h5 class="title-form-coment">Viết bình luận:</h5>
								</div>
								<fieldset class="form-group col-xs-12 col-sm-12 col-md-12">
									<label>Họ và tên: <span>*</span></label>
									<input type="text" class="form-control form-control-lg" value="" id="full-name" name="Author" required="">
								</fieldset>
								<fieldset class="form-group col-xs-12 col-sm-12 col-md-12">
									<label>Địa chỉ email: <span>*</span></label>
									<input type="email" class="form-control form-control-lg" value="" id="email" name="Email" required="">
								</fieldset>
								<fieldset class="form-group col-xs-12 col-sm-12 col-md-12">
									<label>Viết bình luận: <span>*</span></label>
									<textarea placeholder="Bình luận :" rows="6" class="form-control form-control-lg input-text" required="" data-validation-required-message="Vui lòng nhập bình luận" minlength="5" data-validation-minlength-message="Tối thiểu là 5 ký tự" maxlength="500" style="resize:none" data-toggle="tooltip" title="Chiều dài bình luận tối thiểu là 5 và tối đa là 500 ký tự" id="comment" name="Body"></textarea>

								</fieldset>
								<div class="col-xs-12">
									<button type="submit" class="btn btn-lg btn-style btn-style-active">Gửi bình luận</button>
								</div>
							</div>
						</div> <!-- End form mail -->
						</form>

					</div>
					
				</div>
			</section>

			
		</div>

       @include("frontend.new.menu_left")

	</div>
</div>


@endsection