<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use Notifiable;
    const CREATED_AT = 'mem_created_at';
    const UPDATED_AT = 'mem_update_at';


    protected $table = 'member';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = ['member_id','mem_name','mem_email','mem_address'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'mem_password', 'remember_token',
    ];



     public function getAuthPassword()
    {
        return $this->mem_password;
    }



}
