<?php
namespace App\Helpers;

use  App\Models\Color;
class CommonHelper {


		static function  load_acctive_lang($lang){

			  switch ($lang) {
			  	case 'vi':
			  		 $name  = "Vietnamese";
			  		 $img   = "vi";
			  		break;

			  	default:
			  		 $name  = "English";
			  		 $img   = "en";
			  		break;
			  }
			   return  '<a href="#"><img src="/assets/static/img/demos/shop/'.$img.'.png" alt="$name">'.$name.' <i class="fa fa-caret-down"></i></a>';

		}

		static function get_total_parameter($param  = array()){


		  	return http_build_query($param);
		}

		static function slug_title($string)
		{


		    return str_slug($string);
		}

		static function convert_time($string_time , $type ="full"){

			  if($type=="month"){

			  	return date("m",strtotime($string_time));
			  }

			  if($type=="day"){
			  	return date("d",strtotime($string_time));
			  }

			  if($type=="year"){
			  	return date("Y",strtotime($string_time));
			  }

			  if($type=="time"){
			  	return date("HH:MM",strtotime($string_time));
			  }


			  return date("d-m-Y H:m",strtotime($string_time));

		}

	

		static function numberFormat($number){
			
			return number_format($number);
		}


		static function numberFormatPrice($number){

			return number_format($number,0) ."₫";
		}


		static function statusIsDeleted($status){

			switch ($status) {
				case 0:
					return "Hoạt động";
					break;
				
				case 1:
					return "Không hoạt động";
					break;
			}
		}


		static function productPrice($product,$price =true){

			if($product->pro_is_selling >0){

				 $persent   = $product->attr()->attr_price_export * ($product->pro_is_selling /100);
				 if(!$price) return $product->attr()->attr_price_export  - $persent;

				 return     self::numberFormatPrice($product->attr()->attr_price_export  - $persent) ;
			}
			if(!$price){
				return $product->attr()->attr_price_export;
			}
			return self::numberFormatPrice($product->attr()->attr_price_export);

		}


		static function productSelling($product){

			if($product->pro_is_selling >0){

				 return  ($product->pro_is_selling /100);

				
			}
			return false;

		}
		static function productPriceOld($product){

			if($product->pro_is_selling ==0){

				 return  false;	
			}
			return  self::numberFormatPrice($product->attr()->attr_price_export) ;

		}

		static function productPersent($product){

			if($product->pro_is_selling >0){

			     $persent   = ($product->pro_is_selling);

					return  $persent. "%";
			}
			return false;
		}

		static function productImageThumbnail($listImage,$w=260,$h=260){

			
			foreach ($listImage as $img) {
					
				     return self::showProductImageThumbnailUrl($img->pro_img_url,$w,$h);
			  

			}
			

			return "/upload/image/no_img.jpg";
		}

		static function showProductImageThumbnailUrl($img,$w=260,$h=260){

		 
			if(file_exists(public_path($img))) {

				return \ImageThumb::url($img,$w,$h);
			}
			return "/upload/image/no_img.jpg";
		}


		static function showColorProduct($id){

			$color  =Color::find($id);

			if($color){

				return $color->color_name;
			}
			return "Đang cập nhật ...";
		}

		

}