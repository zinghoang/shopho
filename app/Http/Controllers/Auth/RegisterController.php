<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Libs\FrontEnd_Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User as Member;
class RegisterController extends FrontEnd_Controller
{

    protected $registerView = 'frontend.member.register';
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dang-nhap';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'mem_name' => 'required|alpha|max:255|unique:member|regex:/(^[A-Za-z0-9 ]+$)+/',
            'mem_email' => 'required|email|max:255|unique:member',
            'mem_password' => 'required|min:6|confirmed',
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
            $mem =  new Member();
         
            $mem->mem_name = $data['mem_name'];
            $mem->mem_email  = $data['mem_email'];
            $mem->mem_address  = $data['mem_address'];
            $mem->mem_phone  = $data['mem_phone'];
            $mem->mem_password = bcrypt($data['mem_password']);
       ;
            $mem->save();
            $mem = Member::where("mem_email",$mem->mem_email)->first();
            
            return $mem;
    }
    public function getRegister()
    {
      
       return view($registerView);
    }

}
