<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Libs\FrontEnd_Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Auth;
class LoginController extends FrontEnd_Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


      public function username()
    {
        return 'mem_email';
    }

    public function password()
    {
       
        return 'mem_password';
    }

    public function id()
    {
        return 'member_id';
    }



  public function login(Request $request)
    {
        $user = $this->attemptLogin($request);

        if($user){
             $request->session()->put("user",$this->guard()->user()->toArray());
             $request->session()->save();

              return redirect()->intended($this->redirectPath());
        }else{

          return    $this->sendFailedLoginResponse($request);
        }
       
       
       
    }

}
