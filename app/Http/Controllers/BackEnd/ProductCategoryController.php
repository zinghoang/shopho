<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\BackEnd_Controller;
use App\Models\Category;
use App\Helpers\CommonHelper;

class ProductCategoryController extends BackEnd_Controller
{
     function indexAction(Request $request){

     $categories = Category::orderBy("cat_id",'desc');

     if($request->keyword){

     		 $categories->where('cat_name', 'like', '%' .$request->keyword . '%');
     		//  $category->orWhere('pro_quantity', 'like', '%' .$request->keyword . '%');

     		//  $category->orWhereHas("attribute",function($query) use($request) {
     		// 	 $query->where('attr_code', 'like', '%' .$request->keyword . '%');
     		// });

     	}
     	
     	$categories = $categories->paginate(10);
     
     	return view("backend.product_category.list",compact("categories"));
     }


     function addAction(){

     	

     	return view("backend.product_category.add",[]);
     }


       function addStoreAction(Request $request){

     
     	$category  = new Category();	
      	$cat = $request->cat;
      	$category->cat_slug = CommonHelper::slug_title($cat['cat_name']);
      	foreach ($cat as $k => $v) {
      			$category->{$k} = $v;
      	}
      	$category->save();

       
        return redirect("/manager/product-category/");   
     }

}
