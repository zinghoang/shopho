<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\BackEnd_Controller;
use App\Models\Category;
use App\Models\NewsCategory;
use App\Helpers\CommonHelper;


class NewsCategoryController extends BackEnd_Controller
{
    function indexAction(Request $request){

     $categories = NewsCategory::orderBy("news_cat_id",'desc');

     if($request->keyword){

     		 $categories->where('news_cat_name', 'like', '%' .$request->keyword . '%');
     		//  $category->orWhere('pro_quantity', 'like', '%' .$request->keyword . '%');

     		//  $category->orWhereHas("attribute",function($query) use($request) {
     		// 	 $query->where('attr_code', 'like', '%' .$request->keyword . '%');
     		// });

     	}
     	
     	$categories = $categories->paginate(10);
     
     	return view("backend.news_category.list",compact("categories"));
     }


     function addAction(){

     	

     	return view("backend.news_category.add",[]);
     }


       function addStoreAction(Request $request){

     
     	$category  = new NewsCategory();	
      	$cat = $request->cat;
      	$category->news_cat_slug = CommonHelper::slug_title($cat['news_cat_name']);
      	foreach ($cat as $k => $v) {
      			$category->{$k} = $v;
      	}
      	$category->save();

       
        return redirect("/manager/news-category/");   
     }
}
