<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\BackEnd_Controller;
use App\Models\News;
use App\Models\NewsCategory;
use App\Helpers\CommonHelper;
use Intervention\Image\Facades\Image;

class NewsController extends BackEnd_Controller
{

	var $category  = array();

     function __construct(){

    	$this->category  = NewsCategory::get();
    
    
    
    }

      function indexAction(Request $request){

     $news = News::orderBy("news_id",'desc');

     if($request->keyword){

     		 $news->where('news_title', 'like', '%' .$request->keyword . '%');
     		//  $category->orWhere('pro_quantity', 'like', '%' .$request->keyword . '%');

     		//  $category->orWhereHas("attribute",function($query) use($request) {
     		// 	 $query->where('attr_code', 'like', '%' .$request->keyword . '%');
     		// });

     	}
     	
     	$news = $news->paginate(10);
     
     	return view("backend.news.list",compact("news"));
     }


     function addAction(){

     	

    	$arrCategory  = $this->category->pluck("news_cat_name","news_cat_id");

      
     	return view("backend.news.add",compact("arrCategory"));
     }


       function addStoreAction(Request $request){

     	  $news  = new News();	
      	$data = $request->data;
      //	$category->news_cat_slug = CommonHelper::slug_title($cat['news_cat_name']);
        $news->news_fk_admin_id = 1;
        $file  = $request->file("image");

        $filename = time() . '.' . $file->getClientOriginalExtension();
        $file_name = "/upload/news/".$filename;
       // $path = public_path('images/' . $filename);
        \ImageThumb::make($file->getRealPath(),["width"=>600,"height"=>600])->save($file_name);
        $news->news_thumb = $file_name;


       
      	foreach ($data as $k => $v) {
      			$news->{$k} = $v;
      	}
      	$news->save();

       
        return redirect("/manager/news/");   
     }
}
