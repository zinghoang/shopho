<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\BackEnd_Controller;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductAttribute;
use App\Models\Category;
use App\Models\Color;
class ProductController extends BackEnd_Controller
{

	
    function __construct(){
    	parent::__construct();
    }

     function indexAction(Request $request){


     	$products = Product::where("pro_is_deleted",0);
     	if($request->keyword){

     		 $products->where('pro_name', 'like', '%' .$request->keyword . '%');
     		 $products->orWhere('pro_quantity', 'like', '%' .$request->keyword . '%');

     		 $products->orWhereHas("attribute",function($query) use($request) {
     			 $query->where('attr_code', 'like', '%' .$request->keyword . '%');
     		});

     	}
     	
     	$products = $products->paginate(10);
     	return view("backend.product.list",compact("products"));
     }

     function editAction($id){

     	$product = Product::find($id);
     	$categorys  = Category::where("cat_is_deleted",0)->pluck("cat_name","cat_id");
      $colors  = Color::pluck("color_name","color_id");
     	return view("backend.product.edit",compact("product",'categorys','colors'));

     }

     function editStoreAction(Request $request){

     	 
     	$files = $request->file('files');

      $product  = Product::find($request->pro_id);
        $pro = $request->pro;
        $pro['pro_is_hot']  = isset($pro['pro_is_hot'] ) ? 1:0;
        $pro['pro_is_featured']  =   isset($pro['pro_is_featured']) ? 1:0;
        foreach ($pro as $k => $v) {
            $product->{$k} = $v;
        }
        $product->update();

        $attribute  =  ProductAttribute::where("attr_fk_pro_id",$product->pro_id)->first();
        $attr  = $request->attr;
       

        foreach ($attr as $k => $v) {
            $attribute->{$k} = $v;
        }
        $attribute->update();

        $files = $request->file('files') ? $request->file('files')  : [];

        $pathFile = [];
        $files_exist = $request->files_exist;
       foreach ($files  as $key => $file) {
          $flag_update_image = false;
        
          if(isset($files_exist[$key])){

            $image_has =  ProductImage::find($files_exist[$key]);
            if(file_exists(base_path().$image_has->pro_img_url)){
               unlink(base_path().$image_has->pro_img_url);
            }
            $flag_update_image = true;

          }


          $imageName= str_random(30).time().".".$file->getClientOriginalExtension();
          $pathFile[$key]  = "/upload/product/".$product->pro_id."/". $imageName;
          if($flag_update_image){
            $image_has->pro_img_url =  $pathFile[$key];
            $image_has->update();
            unset($pathFile[$key]);
          }
          $file->move( base_path() . '/public/upload/product/'.$product->pro_id, $imageName);
       }
        

      foreach ($pathFile as $p ) {
        $proImage   =  new ProductImage();
            $proImage->pro_img_url = $p;
            $proImage->pro_img_fk_pro_id = $product->pro_id;
            $proImage->save();
            $proImage->push();
          }


          return redirect()->back();
     }



     function addAction(){

     	$categorys  = Category::where("cat_is_deleted",0)->pluck("cat_name","cat_id");
     	$colors  = Color::pluck("color_name","color_id");

     	return view("backend.product.add",compact('categorys','colors'));
     }

      function addStoreAction(Request $request){

     	
     	

		 

     	$product  = new Product();	
      	$pro = $request->pro;
      	foreach ($pro as $k => $v) {
      			$product->{$k} = $v;
      	}
      	$product->save();

      	$attribute  =  new ProductAttribute();
      	$attr  = $request->attr;
      	$attribute->attr_fk_pro_id  = $product->pro_id;

      	foreach ($attr as $k => $v) {
      			$attribute->{$k} = $v;
      	}
      	$attribute->save();

      	  $files = $request->file('files');
      	  $pathFile = [];
		   foreach ($files  as $file) {
		         $imageName= str_random(30).time().".".$file->getClientOriginalExtension();
		         $pathFile[]  = "/upload/product/".$product->pro_id."/". $imageName;
		         $file->move( base_path() . '/public/upload/product/'.$product->pro_id, $imageName);
		   }
		   	

			foreach ($pathFile as $p ) {
				$proImage   =  new ProductImage();
	      		$proImage->pro_img_url = $p;
	      		$proImage->pro_img_fk_pro_id = $product->pro_id;
	      		$proImage->save();
	      		$proImage->push();
	      	}

		 
        return redirect("/manager/product");   
     }

}
