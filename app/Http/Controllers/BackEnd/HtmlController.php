<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\BackEnd_Controller;
use App\Models\Html as HtmlModel;
class HtmlController extends BackEnd_Controller
{
    //

    function  detailAction(Request $request){

    	$conf  = HtmlModel::where("conf_html_name",$request->id)->first();
    	
    	return view("backend.config_html.detail",compact("conf"));
    }

    function storeAction(Request $request){


      $conf  = 	 HtmlModel::where("conf_html_name",$request->conf_html_name)->first();
      $conf->conf_html_content = $request->conf_html_content; 
      $conf->update();
   
      return redirect("/manager/config-html/".$request->conf_html_name);
    }
}
