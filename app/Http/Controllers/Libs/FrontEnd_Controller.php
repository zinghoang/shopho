<?php

namespace App\Http\Controllers\Libs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\News;
use Session;
class FrontEnd_Controller extends Controller
{
   
   var $js = null;

   function __construct(){


   		$this->loadJs();


      $categories = \App\Models\Category::where("cat_is_deleted",0)->orderBy("cat_name","desc")->get();
      $product_hots = Product::where("pro_is_deleted",0)->limit(10)->where("pro_is_hot",1)->orderBy("pro_created_at","desc")->get();
       $news  = News::orderBy("news_id","desc")->limit(10)->get();
      \View::share("categories",$categories);
       \View::share("product_hots",$product_hots);
       \View::share("news",$news);
   }


   function loadJs(){

   	
   	  $route  = \Route::getCurrentRoute();

   	  $action  = $route->action["controller"];


  	  // if (preg_match("/ProductController/", $action)){

  	  	  $this->js = "/common/js/my_js/product.js"; 
  	 // }


  	  \View::share("loadJS",$this->js);

   }


    
}
