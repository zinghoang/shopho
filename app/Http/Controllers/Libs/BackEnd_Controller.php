<?php

namespace App\Http\Controllers\Libs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackEnd_Controller extends Controller
{
   
   	var $js = null;

   function __construct(){


   		$this->loadJs();
   }


   function loadJs(){

   	
   	  $route  = \Route::getCurrentRoute();

   	  $action  = $route->action["controller"];


  	  if (preg_match("/ProductController/", $action)){

  	  	  $this->js = "/asset/my_js/product.js,common.js"; 
  	  }


  	  \View::share("loadJS",$this->js);

   }



}
