<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\FrontEnd_Controller;
use App\Models\Product;
use App\Models\News;
use Auth;
use Session;
class HomeController extends FrontEnd_Controller
{
     


	function indexAction(){


		$is_index = true;
		$product_news = Product::where("pro_is_deleted",0)->limit(10)->orderBy("pro_created_at","desc")->get();
		$product_hots = Product::where("pro_is_deleted",0)->limit(10)->where("pro_is_hot",1)->orderBy("pro_created_at","desc")->get();
        $product_sellings = Product::where("pro_is_deleted",0)->limit(10)->where("pro_is_selling", "<>",0)->orderBy("pro_created_at","desc")->get();
         $product_freatureds = Product::where("pro_is_deleted",0)->limit(10)->where("pro_is_featured",1)->orderBy("pro_created_at","desc")->get();
		//dd($products);

   	    $news  = News::orderBy("news_id","desc")->limit(10)->get();

		return view("frontend.home.index",compact("product_news","product_freatureds","product_hots","product_sellings",'is_index'));
	}


}
