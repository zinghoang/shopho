<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\FrontEnd_Controller;
use App\Models\NewsCategory;
use App\Models\News;

class NewController extends FrontEnd_Controller
{
    //
	var $category  = null;

    function __construct(){

    	$this->category  = NewsCategory::get();
    
    
    	\View::share("category",$this->category );
    }
    function indexAction(Request $request){

       $cate_id  = $request->cate_id; 
       $top3  =News::orderBy("news_id","desc");
       if($cate_id){
          $top3  =   $top3->where("news_fk_news_cat_id",$cate_id);
       }
        $top3  =   $top3->limit(3)->get();
      
       $where=  [];
       foreach ($top3 as $key => $new) {
          $where[] = $new->news_id;
       }
       $news  = News::orderBy("news_id","desc");
       if($cate_id)  $news = $news->where("news_fk_news_cat_id",$cate_id);
      
        $news  =  $news->whereNotIn("news_id",$where)->paginate(30);
        
       return view("frontend.new.index",compact("news","top3"));

    }

    function addAction(){


    	       $arrCategory  = $this->category->pluck("news_cat_name","news_cat_id");

        dd($arrCategory);

         return view("frontend.new.add");


    }

    function detailAction(Request $request){


   
        $new = News::find($request->id);
        $current_cate = $new->news_fk_news_cat_id;
        $top  = News::orderBy("news_id","desc")->first();
    	return view("frontend.new.detail",compact("current_cate","new","top"));
    }


}
