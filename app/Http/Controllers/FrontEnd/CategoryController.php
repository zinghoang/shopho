<?php 

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\FrontEnd_Controller;
use App\Models\Product;
use App\Models\Category;
class CategoryController extends FrontEnd_Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function listAction($slug,$id)
  {
    $category  = Category::find($id);
    $product_categories  = Product::where("pro_fk_cat_id",$id)->orderBy("pro_id","desc")->paginate(10);
     
    return view("frontend.category.list",compact("product_categories","category"));
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>