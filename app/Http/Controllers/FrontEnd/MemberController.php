<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\FrontEnd_Controller;
use App\Models\Product;
use App\Models\News;
use Auth;
use Session;
class MemberController extends FrontEnd_Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function registerAction()
  {
    
      $this->middleware("checkLogin");
      return view("frontend.member.register");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function loginAction()
  {
   

     $this->middleware("checkLogin");
     return view("frontend.member.login");
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function logoutAction(Request $request)
  {
       $request->session()->put("user",null);
       $request->session()->save();
       return redirect("/");


  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>