<?php 

namespace App\Http\Controllers\FrontEnd;
use Illuminate\Http\Request;
use App\Http\Controllers\Libs\FrontEnd_Controller;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderCustomer;
use DB;
use Session;
use App\Models\Member; 
class ProductController extends FrontEnd_Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function quickViewProductAction(Request $request)
  {
    $status = ["error" => 1,"data" => ""];  
    if($request->pro_id){
      $product   =  Product::find($request->pro_id);
      if($product){
        $data_html   =  view("frontend.modal.product.quick_view_product",compact("product"))->render();
       
        $status["data"]  = $data_html;
        $status['error'] = 0;
      }  
    } 
    echo json_encode($status);   exit();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function addToCart(Request $request)
  {

      $status = ["error" => 1,"data" => ""];  
      $carts  = $request->session()->get("carts");
      $product = Product::where("pro_is_deleted",0)->where("pro_id",$request->pro_id)->first();

      if(!$product){
         echo   json_encode($status) ; exit();
      }

      $quantity   = intval($request->quantity) ;
      if($quantity ==0 && $request->type !="update") $quantity  =1; 

    

      $price  = \App\Helpers\CommonHelper::productPrice($product,false);
     

      $productList  = [];
      $update  = false;
      if(count($carts ) >0 ) {
        $carts =  $carts->toArray();

          foreach ($carts as $key  => $item) {

              if($item->id == $product->pro_id){

                if($request->type =="update"){
                  if($quantity==0){

                    unset($carts[$key]);
                    $update = true;
                    continue;  
                  }
                  $carts[$key]->qty =$quantity ;
                }else{
                  $carts[$key]->qty  = $quantity + $item->qty ;
                }
                
                $update = true;
              }
              $carts[$key]->subtotal  = $carts[$key]->qty * $item->price;
              $carts[$key] = (object)$carts[$key];

          }
      }


      if(!$update){
        $add_count  = count($carts)+1;
        $carts[$add_count]   = (object)['id' => $product->pro_id, 'name' => $product->pro_name, 'qty' =>$quantity, 'price' =>$price,'subtotal'=>$quantity *$price ];
     
      } 

 
      $carts  = collect($carts);
      $request->session()->put("carts",$carts);
      $request->session()->save();
      $status["data"] = json_encode($carts->toArray());
      $status["error"] = 0;

      echo   json_encode($status) ; exit();

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function cartList(Request $request)
  {
      $cartList  =  $request->session()->get("carts");  

     
      $cartList  = $cartList ? $cartList : [];
      foreach ($cartList as $key => $cart) {
      
          $cart->product  = Product::find($cart->id); 
      }


    if(Session::get("user")){

          $member   = Member::find(Session::get("user")['mem_id']);
          
     }else{
          $member  = new Member();
     }
    
      return view("frontend.product.cart",compact("cartList","member"));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function cartOrderAction(Request $request)
  {

     $this->validate($request, [
        'order_cus_name' => 'required',
        'order_cus_phone' => 'required',
        'order_cus_address' => 'required',
    ],array(

           'order_cus_name.required'    => 'Trường tên người nhận không được để trống',
            'order_cus_phone.required'    => 'Trường điện thoại không được để trống.',
            'order_cus_address.required' => 'Trường địa chỉ không được để trống',
      ));

      $cartList  =  $request->session()->get("carts"); 
      if(!$cartList)  return redirect("/");
      $data_order  = [];
      $data_order['order_total_price'] = 30000; // shiper
      $data_order['order_require_date'] = date("Y-m-d H:i:s");
      $data_order['order_status']  = 0; //chua giao hang
      foreach ($cartList as $key => $cart) {
           $data_order['order_total_price']+= $cart->subtotal;


          $cart->product  = Product::find($cart->id); 
           $data_order['order_detail'] = 
             "Mã sản phẩm: ". $cart->product->attr()->attr_code . 
             "<br>Số lượng: ".$cart->qty ." cái ". 
             "<br>Loại màu: ".\App\Helpers\CommonHelper::showColorProduct($cart->id).
             "<br>Giá bán: ".$cart->price .
             "<br>-------------- <br>";
      }
      DB::beginTransaction();
      try {
          $order  = new Order();
          foreach ($data_order as $key => $od) {
                $order->$key = $od;
          }
          $order->save();

          $order_customer  = new OrderCustomer();
          foreach ($request->all() as $key => $value) {
             if(preg_match("/order_cus/", $key)){
                $order_customer->$key  = $value;

             }
          }
          $order_customer->order_cus_fk_order_id  = $order->order_id;
           $order_customer->save();
          DB::commit();
           $request->session()->put("carts",null);
           $request->session()->save();
          return redirect("/order-thanks");
  
      } catch (\Exception $e) {
      
          DB::rollback(); 
      }





     
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function orderThanksAction()
  {
    
    return view("frontend.product.order_thanks");
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function detailAction($slug,$id)
  {

    $product  = Product::find($id);
    $product_categories  = Product::where("pro_fk_cat_id",$product->pro_fk_cat_id)->orderBy("pro_id","desc")->limit(10);
    return view("frontend.product.detail",compact("product","product_categories"));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>