<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Libs\FrontEnd_Controller;
use App\Models\Html as HtmlModel;
class AboutController extends FrontEnd_Controller
{
    //


    function indexAction(){

      $html  =  HtmlModel::where("conf_html_name","HTML_INTRO")->first();
      return view("frontend.about.intro",compact("html"));

    }

    function mapAction(){
         $html  =  HtmlModel::where("conf_html_name","HTML_MAP")->first();
    	  return view("frontend.about.map",compact("html"));
    }


    function contactAction(){
        $html  =  HtmlModel::where("conf_html_name","HTML_CONTACT")->first();
    	  return view("frontend.about.contact",compact("html"));

    }
}
