<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Html extends Model
{
     protected $table = 'config_html';

    protected $primaryKey = 'conf_html_id';
    public $timestamps = false;
    protected $fillable = ["*"];

}
