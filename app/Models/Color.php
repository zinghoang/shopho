<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends BaseModel 
{

    protected $table = 'colors';

    protected $primaryKey = 'color_id';

    public $timestamps = false;
    protected $fillable = ["*"];


}