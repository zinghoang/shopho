<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends BaseModel 
{
    const CREATED_AT = '';
    const UPDATED_AT = '';
    protected $table = 'member';
    protected $primaryKey = 'mem_id';
    public $timestamps = true;

}