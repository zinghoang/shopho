<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
      protected $table = 'news_category';

    protected $primaryKey = 'news_category_id';

    public $timestamps = false;
    protected $fillable = ["*"];
}
