<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel 
{

    protected $table = 'category';

    protected $primaryKey = 'cat_id';

    public $timestamps = false;
    protected $fillable = ["*"];


}