<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
   

    public function getById($id)
    {
    	return $this->where($this->_pk,$id)->first();
    }
}