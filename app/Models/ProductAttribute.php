<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends BaseModel 
{

    protected $table = 'product_attributes';
     protected $primaryKey = 'attr_id';
    public $timestamps = false;
    protected $fillable = ["*"];

}