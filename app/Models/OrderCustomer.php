<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCustomer extends Model
{
    protected $table = 'order_customer';
    protected $primaryKey = 'order_cus_id';

    public $timestamps = false;
    protected $fillable = ["*"];
}
