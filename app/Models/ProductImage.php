<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends BaseModel 
{

    protected $table = 'product_image';
    public $timestamps = false;
    protected $primaryKey = 'pro_img_id';
    protected $fillable = ["*"];

}