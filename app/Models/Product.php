<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends BaseModel 
{

	const CREATED_AT = 'pro_created_at';
    const UPDATED_AT = 'pro_update_at';

    protected $table = 'product';
    protected $primaryKey = 'pro_id';

    public $timestamps = true;
    protected $fillable = ["*"];



     function attribute(){

      	 return  $this->hasOne(ProductAttribute::class,'attr_fk_pro_id',"pro_id");
   	}
   
   function attr(){

   	 $attr  = $this->hasOne(ProductAttribute::class,'attr_fk_pro_id',"pro_id")->first();
   	 return $attr ? $attr :  new  ProductAttribute();
   	 
   	}


     function images(){

      	 return  $this->hasOne(ProductImage::class,'pro_img_fk_pro_id',"pro_id");
   	}
   

   	   function getImage(){

	   	 $images  = $this->images()->get();
	   	 return $images ? $images :  new  ProductImage();
   	 
   	}
   
}