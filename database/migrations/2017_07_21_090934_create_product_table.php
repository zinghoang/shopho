<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductTable extends Migration {

	public function up()
	{
		Schema::create('product', function(Blueprint $table) {
			$table->increments('pro_id');
			
			$table->string('pro_name', 255);
			$table->integer('pro_quantity');
			$table->tinyInteger('pro_is_hot')->nullable();
			$table->tinyInteger('pro_is_deleted')->default('0');
			$table->tinyInteger('pro_is_selling')->nullable()->default('0');
			$table->tinyInteger('pro_is_featured')->nullable()->default('0');
			$table->integer('pro_fk_cat_id')->unsigned();
			$table->timestamp("pro_created_at")->nullable();
			$table->timestamp("pro_update_at")->nullable();
		});
	}

	public function down()
	{
		Schema::drop('product');
	}
}