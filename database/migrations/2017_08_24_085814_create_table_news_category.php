<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewsCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('news_category', function(Blueprint $table) {
            $table->increments('news_cat_id');
            $table->string('news_cat_name');
            $table->string('news_cat_slug');
            $table->tinyInteger('news_cat_is_deleted')->default('0');
        });
        Schema::table('news', function(Blueprint $table) {
            $table->foreign('news_fk_news_cat_id')->references('news_cat_id')->on('news_category')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('news_category');
    }
}
