<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('order_customer', function(Blueprint $table) {

              $table->increments("order_cus_id"); 
              $table->string("order_cus_name")->nullable(); 
              $table->string("order_cus_phone")->nullable(); 
              $table->string("order_cus_address")->nullable(); 
              $table->integer("order_cus_fk_order_id")->unsigned(); 
              $table->string("order_cus_detail")->nullable(); 

          });


            Schema::table('order_customer', function(Blueprint $table) {
            $table->foreign('order_cus_fk_order_id')->references('order_id')->on('order')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
          });
          

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('order_customer');
    }
}
