<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('news', function (Blueprint $table) {
            $table->increments('news_id');
            $table->text('news_title')->nullable();
            $table->text('news_description')->nullable();
            $table->text('news_content')->nullable();
            $table->text('news_thumb')->nullable();
            $table->integer('news_fk_admin_id')->unsigned();
            $table->integer('news_fk_news_cat_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
