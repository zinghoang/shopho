<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryTable extends Migration {

	public function up()
	{
		Schema::create('category', function(Blueprint $table) {
			$table->increments('cat_id');
			$table->string('cat_name');
			$table->string('cat_slug');
			$table->tinyInteger('cat_is_deleted')->default('0');
		});
	}

	public function down()
	{
		Schema::drop('category');
	}
}