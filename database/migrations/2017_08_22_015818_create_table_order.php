<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('order', function(Blueprint $table) {
           $table->increments("order_id");
           $table->double("order_total_price")->nullable(); 
           $table->datetime("order_shipped_date")->nullable();
           $table->datetime("order_require_date")->nullable(); 
           $table->tinyInteger("order_status")->nullable(); 
           $table->string("order_comments")->nullable(); 
           $table->text("order_detail")->nullable(); 
           $table->text("order_comment_admin")->nullable();
      });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order');
    }
}
