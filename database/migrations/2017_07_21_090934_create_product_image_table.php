<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductImageTable extends Migration {

	public function up()
	{
		Schema::create('product_image', function(Blueprint $table) {
			$table->increments('pro_img_id');
			$table->text('pro_img_url');
			// $table->tinyInteger('pro_img_deleted');
			$table->integer('pro_img_fk_pro_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('product_image');
	}
}