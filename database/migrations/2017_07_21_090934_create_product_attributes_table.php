<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductAttributesTable extends Migration {

	public function up()
	{
		Schema::create('product_attributes', function(Blueprint $table) {
			$table->increments('attr_id');
			$table->integer('attr_fk_pro_id')->unsigned();
			$table->integer('attr_fk_color_id')->unsigned();
			// $table->string('attr_fk_type_id');
			$table->string('attr_barcode', 255)->nullable();
			$table->string('attr_code');
			$table->decimal('attr_price_import');
			$table->decimal('attr_price_export');
			$table->text('attr_description')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('product_attributes');
	}
}