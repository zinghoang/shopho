<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('product', function(Blueprint $table) {
			$table->foreign('pro_fk_cat_id')->references('cat_id')->on('category')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('product_attributes', function(Blueprint $table) {
			$table->foreign('attr_fk_pro_id')->references('pro_id')->on('product')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('shop', function(Blueprint $table) {
			$table->foreign('shop_fk_mem_id')->references('mem_id')->on('member')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('shop_product', function(Blueprint $table) {
			$table->foreign('shop_pro_fk_shop_id')->references('shop_id')->on('shop')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('shop_product', function(Blueprint $table) {
			$table->foreign('shop_pro_fk_pro_id')->references('pro_id')->on('product')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('product_image', function(Blueprint $table) {
			$table->foreign('pro_img_fk_pro_id')->references('pro_id')->on('product')
						->onDelete('restrict')
						->onUpdate('restrict');
		});


       Schema::table('product_attributes', function(Blueprint $table) {
            $table->foreign('attr_fk_color_id')->references('color_id')->on('colors')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });



	}

	public function down()
	{
		Schema::table('product', function(Blueprint $table) {
			$table->dropForeign('product_pro_fk_cat_id_foreign');
		});
		Schema::table('product_attributes', function(Blueprint $table) {
			$table->dropForeign('product_attributes_attr_fk_pro_id_foreign');
		});
		Schema::table('shop', function(Blueprint $table) {
			$table->dropForeign('shop_shop_fk_mem_id_foreign');
		});
		Schema::table('shop_product', function(Blueprint $table) {
			$table->dropForeign('shop_product_shop_pro_fk_shop_id_foreign');
		});
		Schema::table('shop_product', function(Blueprint $table) {
			$table->dropForeign('shop_product_shop_pro_fk_pro_id_foreign');
		});
		Schema::table('product_image', function(Blueprint $table) {
			$table->dropForeign('product_image_pro_img_fk_pro_id_foreign');
		});

		chema::table('product_attributes', function(Blueprint $table) {
           $table->dropForeign('attr_fk_color_id_color_id');
        });

	}
}