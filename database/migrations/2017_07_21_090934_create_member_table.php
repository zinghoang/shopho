<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberTable extends Migration {

	public function up()
	{
		Schema::create('member', function(Blueprint $table) {
			$table->increments('mem_id');
			$table->string('mem_name');
			$table->string('mem_email', 255);
			$table->string('mem_password', 255);
			$table->string('mem_remember_token')->nullable();;
			$table->string('mem_address')->nullable();;
			$table->string('mem_phone')->nullable();
			$table->timestamp("mem_created_at")->nullable();
			$table->timestamp("mem_update_at")->nullable();
		});
	}

	public function down()
	{
		Schema::drop('member');
	}
}