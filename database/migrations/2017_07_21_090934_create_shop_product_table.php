<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopProductTable extends Migration {

	public function up()
	{
		Schema::create('shop_product', function(Blueprint $table) {
			$table->increments('shop_pro_id');
			$table->integer('shop_pro_fk_shop_id')->unsigned();
			$table->integer('shop_pro_fk_pro_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('shop_product');
	}
}