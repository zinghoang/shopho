<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShopTable extends Migration {

	public function up()
	{
		Schema::create('shop', function(Blueprint $table) {
			$table->increments('shop_id');
			$table->string('shop_name');
			$table->string('shop_slug');
			$table->string('shop_is_deleted');
			$table->integer('shop_fk_mem_id')->unsigned();
			$table->timestamp("shop_created_at")->nullable();
			$table->timestamp("shop_update_at")->nullable();
		});
	}

	public function down()
	{
		Schema::drop('shop');
	}
}