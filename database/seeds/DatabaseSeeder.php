<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigHtmlTableSeeder::class);
        $this->call(ColorSeeder::class);
         $this->call(CategorySeeder::class);
        
        
    }
}
