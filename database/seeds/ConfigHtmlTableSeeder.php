<?php

use Illuminate\Database\Seeder;

class ConfigHtmlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
		$data = array(
		    array('conf_html_name'=>'HTML_INTRO'),
		    array('conf_html_name'=>'HTML_MAP'),
		    array('conf_html_name'=>'HTML_CONTACT'),
		    //...
		);
		 DB::table('config_html')->insert($data);


    }
}
