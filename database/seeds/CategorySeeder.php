<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	$data = array(
		    array('cate_name'=>'Ba lô nữ','cat_slug'=>'ba-lo-nu'),
		    array('cate_name'=>'Túi xách','cat_slug'=>'tui-xach'),
		    array('cate_name'=>'Túi đeo','cat_slug'=>'tui-deo'),
		    array('cate_name'=>'Giày nữ','cat_slug'=>'giay-nu'),
		    array('cate_name'=>'Giày nam','cat_slug'=>'giay-nam'),
		    array('cate_name'=>'Áo khoác nữ','cat_slug'=>'ao-khoac-nu'),
		   
		    //...
		);
		 DB::table('category')->insert($data);
    }
}
