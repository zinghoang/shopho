<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      	$data = array(
		    array('color_name'=>'Xanh'),
		    array('color_name'=>'Đỏ'),
		    array('color_name'=>'Tím'),
		    array('color_name'=>'Vàng'),
		    array('color_name'=>'Lục'),
		    array('color_name'=>'Lam'),
		    array('color_name'=>'Chàm'),
		    array('color_name'=>'Tím'),
		    array('color_name'=>'Trắng'),
		    array('color_name'=>'Xám'),
		    array('color_name'=>'Hồng'),
		    array('color_name'=>'Đen'),
		    //...
		);
		 DB::table('colors')->insert($data);

    }
}
