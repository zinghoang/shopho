/*
Navicat MySQL Data Transfer

Source Server         : localhostPC
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-08-21 17:10:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'aaaaaaa', '', '0');
INSERT INTO `category` VALUES ('2', 'bbbbb', '', '0');
INSERT INTO `category` VALUES ('3', 'cccccc', '', '0');
INSERT INTO `category` VALUES ('4', 'dddd', 'tui-thu-xinh', '0');
INSERT INTO `category` VALUES ('6', 'sdfgsdfgdsfg', 'sdfgsdfgdsfg', '1');
INSERT INTO `category` VALUES ('7', 'aaa', 'aaa', '0');
INSERT INTO `category` VALUES ('8', 'Túi thú xinh', 'tui-thu-xinh', '0');

-- ----------------------------
-- Table structure for colors
-- ----------------------------
DROP TABLE IF EXISTS `colors`;
CREATE TABLE `colors` (
  `color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_hex` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of colors
-- ----------------------------
INSERT INTO `colors` VALUES ('1', 'Xanh', '');
INSERT INTO `colors` VALUES ('2', 'Đỏ', '');
INSERT INTO `colors` VALUES ('3', 'Tím', '');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `mem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mem_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mem_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mem_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mem_remember_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mem_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mem_created_at` timestamp NULL DEFAULT NULL,
  `mem_update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of member
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2017_07_21_090934_create_category_table', '1');
INSERT INTO `migrations` VALUES ('2', '2017_07_21_090934_create_member_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_07_21_090934_create_product_attributes_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_07_21_090934_create_product_image_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_07_21_090934_create_product_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_07_21_090934_create_shop_product_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_07_21_090934_create_shop_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_08_01_014507_create_color_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_08_1_090944_create_foreign_keys', '1');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_total_price` double DEFAULT NULL,
  `order_shipped_date` datetime DEFAULT NULL,
  `order_require_date` datetime DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT NULL,
  `order_comments` varchar(255) DEFAULT NULL,
  `order_detail` text,
  `order_comment_admin` text,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('1', '120000', null, '2017-08-17 07:55:10', '0', null, 'Mã sản phẩm: 435345111111111\nSố lượng: 1 cái \nLoại màu: Đang cập nhật ...\nGiá bán: 90000\n-------------- \n', null);
INSERT INTO `order` VALUES ('2', '120000', null, '2017-08-17 07:56:06', '0', null, 'Mã sản phẩm: 435345111111111<br>Số lượng: 1 cái <br>Loại màu: Đang cập nhật ...<br>Giá bán: 90000<br>-------------- <br>', null);
INSERT INTO `order` VALUES ('3', '120000', null, '2017-08-17 08:02:34', '0', null, 'Mã sản phẩm: 435345111111111<br>Số lượng: 1 cái <br>Loại màu: Đang cập nhật ...<br>Giá bán: 90000<br>-------------- <br>', null);
INSERT INTO `order` VALUES ('4', '120000', null, '2017-08-17 08:03:00', '0', null, 'Mã sản phẩm: 435345111111111<br>Số lượng: 1 cái <br>Loại màu: Đang cập nhật ...<br>Giá bán: 90000<br>-------------- <br>', null);

-- ----------------------------
-- Table structure for order_customer
-- ----------------------------
DROP TABLE IF EXISTS `order_customer`;
CREATE TABLE `order_customer` (
  `order_cus_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_cus_name` varchar(255) NOT NULL,
  `order_cus_phone` varchar(255) DEFAULT NULL,
  `order_cus_address` varchar(255) DEFAULT NULL,
  `order_cus_fk_order_id` int(11) DEFAULT NULL,
  `order_cus_detail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_cus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_customer
-- ----------------------------
INSERT INTO `order_customer` VALUES ('1', '567567', '567567', '567567', '1', null);
INSERT INTO `order_customer` VALUES ('2', '567567', '567567', '567567', '2', null);
INSERT INTO `order_customer` VALUES ('3', '567567', '567567', '567567', '3', null);
INSERT INTO `order_customer` VALUES ('4', '567567', '567567', '567567', '4', null);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `pro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_quantity` int(11) NOT NULL,
  `pro_is_hot` tinyint(4) DEFAULT NULL,
  `pro_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `pro_is_selling` tinyint(4) DEFAULT '0',
  `pro_is_featured` tinyint(4) DEFAULT '0',
  `pro_fk_cat_id` int(10) unsigned NOT NULL,
  `pro_created_at` timestamp NULL DEFAULT NULL,
  `pro_update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pro_id`),
  KEY `product_pro_fk_cat_id_foreign` (`pro_fk_cat_id`),
  CONSTRAINT `product_pro_fk_cat_id_foreign` FOREIGN KEY (`pro_fk_cat_id`) REFERENCES `category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('3', '67567567567', '20000', '0', '0', '10', '1', '4', '2017-08-01 08:56:40', '2017-08-01 08:56:40');
INSERT INTO `product` VALUES ('4', '67567567567', '567567', '1', '0', '10', '1', '4', '2017-08-01 08:57:22', '2017-08-01 08:57:22');
INSERT INTO `product` VALUES ('22', '234234234', '234234', '1', '0', '10', '1', '4', '2017-08-02 04:33:25', '2017-08-02 04:33:25');
INSERT INTO `product` VALUES ('23', '234234234', '234234', '1', '0', '10', '1', '4', '2017-08-02 04:34:32', '2017-08-02 04:34:32');
INSERT INTO `product` VALUES ('24', '234234234', '234234', '1', '0', '10', '1', '4', '2017-08-02 04:34:39', '2017-08-02 04:34:39');
INSERT INTO `product` VALUES ('25', '234234234', '234234', '1', '0', '10', '1', '4', '2017-08-02 04:35:23', '2017-08-02 04:35:23');
INSERT INTO `product` VALUES ('26', '234234234', '234234', '1', '0', '10', '1', '4', '2017-08-02 04:40:54', '2017-08-02 04:40:54');
INSERT INTO `product` VALUES ('27', '345345345', '2', '1', '0', '10', '1', '4', '2017-08-03 02:46:40', '2017-08-03 02:46:40');
INSERT INTO `product` VALUES ('28', '345345345', '2', '0', '0', '10', '0', '4', '2017-08-03 02:47:07', '2017-08-08 07:20:06');

-- ----------------------------
-- Table structure for product_attributes
-- ----------------------------
DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE `product_attributes` (
  `attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attr_fk_pro_id` int(10) unsigned NOT NULL,
  `attr_fk_color_id` int(10) unsigned NOT NULL,
  `attr_barcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attr_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attr_price_import` decimal(8,2) NOT NULL,
  `attr_price_export` decimal(8,2) NOT NULL,
  `attr_description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`attr_id`,`attr_price_export`),
  KEY `product_attributes_attr_fk_pro_id_foreign` (`attr_fk_pro_id`),
  KEY `product_attributes_attr_fk_color_id_foreign` (`attr_fk_color_id`),
  CONSTRAINT `product_attributes_attr_fk_color_id_foreign` FOREIGN KEY (`attr_fk_color_id`) REFERENCES `colors` (`color_id`),
  CONSTRAINT `product_attributes_attr_fk_pro_id_foreign` FOREIGN KEY (`attr_fk_pro_id`) REFERENCES `product` (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of product_attributes
-- ----------------------------
INSERT INTO `product_attributes` VALUES ('1', '3', '1', null, '1111', '5000.00', '100000.00', '<p>567567567</p>');
INSERT INTO `product_attributes` VALUES ('2', '4', '1', null, '5675', '5000.00', '100000.00', '<p>567567567</p>');
INSERT INTO `product_attributes` VALUES ('3', '22', '1', null, '234234', '5000.00', '100000.00', '<p>324234234</p>');
INSERT INTO `product_attributes` VALUES ('4', '23', '1', null, '234234', '5000.00', '100000.00', '<p>324234234</p>');
INSERT INTO `product_attributes` VALUES ('5', '24', '1', null, '234234', '5000.00', '100000.00', '<p>324234234</p>');
INSERT INTO `product_attributes` VALUES ('6', '25', '1', null, '234234', '5000.00', '100000.00', '<p>324234234</p>');
INSERT INTO `product_attributes` VALUES ('7', '26', '1', null, '234234', '5000.00', '100000.00', '<p>324234234</p>');
INSERT INTO `product_attributes` VALUES ('8', '28', '3', null, '435345111111111', '5000.00', '100000.00', '<p>drfgdfg zinghoang</p>');
INSERT INTO `product_attributes` VALUES ('9', '27', '3', '', '435345111111111', '5000.00', '100000.00', '<p>drfgdfg zinghoang</p>');

-- ----------------------------
-- Table structure for product_image
-- ----------------------------
DROP TABLE IF EXISTS `product_image`;
CREATE TABLE `product_image` (
  `pro_img_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_img_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_img_fk_pro_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pro_img_id`),
  KEY `product_image_pro_img_fk_pro_id_foreign` (`pro_img_fk_pro_id`),
  CONSTRAINT `product_image_pro_img_fk_pro_id_foreign` FOREIGN KEY (`pro_img_fk_pro_id`) REFERENCES `product` (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of product_image
-- ----------------------------
INSERT INTO `product_image` VALUES ('1', '/upload/product/25/hHIRZ0OWSRHQO4lSnnKYoj5ps0QoTt1501648523.jpg', '25');
INSERT INTO `product_image` VALUES ('2', '/upload/product/25/KC657rWbsMTZn0FvtAYYh2YeO4MRD11501648523.png', '25');
INSERT INTO `product_image` VALUES ('3', '/upload/product/26/gcnvGbj0NbRSgyd2jJ3IEfmG3rSNTP1501648854.jpg', '26');
INSERT INTO `product_image` VALUES ('4', '/upload/product/26/c2Cd3y5fxrPDq91TJe7iqVStIsjYdw1501648854.png', '26');
INSERT INTO `product_image` VALUES ('5', '/upload/product/28/d1sCgJEswzoAmf0vQb5EdS5vB7TEDr1502176861.jpg', '28');
INSERT INTO `product_image` VALUES ('6', '/upload/product/28/7ZLJ6FyHAA7BRb05wAc4TNTJ5gpwOM1501728427.jpg', '28');

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `shop_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_is_deleted` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_fk_mem_id` int(10) unsigned NOT NULL,
  `shop_created_at` timestamp NULL DEFAULT NULL,
  `shop_update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shop_id`),
  KEY `shop_shop_fk_mem_id_foreign` (`shop_fk_mem_id`),
  CONSTRAINT `shop_shop_fk_mem_id_foreign` FOREIGN KEY (`shop_fk_mem_id`) REFERENCES `member` (`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of shop
-- ----------------------------

-- ----------------------------
-- Table structure for shop_product
-- ----------------------------
DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE `shop_product` (
  `shop_pro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shop_pro_fk_shop_id` int(10) unsigned NOT NULL,
  `shop_pro_fk_pro_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`shop_pro_id`),
  KEY `shop_product_shop_pro_fk_shop_id_foreign` (`shop_pro_fk_shop_id`),
  KEY `shop_product_shop_pro_fk_pro_id_foreign` (`shop_pro_fk_pro_id`),
  CONSTRAINT `shop_product_shop_pro_fk_pro_id_foreign` FOREIGN KEY (`shop_pro_fk_pro_id`) REFERENCES `product` (`pro_id`),
  CONSTRAINT `shop_product_shop_pro_fk_shop_id_foreign` FOREIGN KEY (`shop_pro_fk_shop_id`) REFERENCES `shop` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of shop_product
-- ----------------------------
