<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', 'FrontEnd\HomeController@indexAction')->name('home');


// Route::resource('product', 'FrontEnd\ProductController');
// Route::resource('productattribute', 'FrontEnd\ProductAttributeController');
// Route::resource('category', 'FrontEnd\CategoryController');
// Route::resource('member', 'FrontEnd\MemberController');
// Route::resource('shop', 'FrontEnd\ShopController');
// Route::resource('shoppoduct', 'FrontEnd\ShopPoductController');
// Route::resource('productimage', 'FrontEnd\ProductImageController');

Route::group(['middleware' => 'web'], function () {
    // your routes
Auth::routes();


  Route::group(['namespace'=>"FrontEnd"],function(){
  
  Route::get('/','HomeController@indexAction');

  Route::get('/category/{slug}-{id}',"CategoryController@listAction")->where('slug', '.*?')
    ->where('id', '\d+');

// carrt + quicl view 
  Route::get('/quick-view-product', 'ProductController@quickViewProductAction');
  Route::get('/add-to-cart', 'ProductController@addToCart');
  Route::get('/cart-list', 'ProductController@cartList');
  Route::post('/checkout', 'ProductController@cartOrderAction');
  Route::get('/order-thanks', 'ProductController@orderThanksAction');

 // detail product
 
 Route::get("/product/{slug}-{id}.html","ProductController@detailAction")->where('slug', '.*?')
    ->where('id', '\d+');




  // route about 
    Route::get('/gioi-thieu', 'AboutController@indexAction');
    Route::get('/ban-do', 'AboutController@mapAction');
    Route::get('/lien-he', 'AboutController@contactAction');



    Route::get("/tin-tuc",'NewController@indexAction');
    Route::get("/tin-tuc/{slug}-{id}",'NewController@detailAction')->where('slug', '.*?')
    ->where('id', '\d+');


   Route::get('/dang-ky', 'MemberController@registerAction')->middleware("checkLogin");
   Route::get('/logout',"MemberController@logoutAction");




    Route::get('dang-nhap','MemberController@loginAction')->middleware("checkLogin");
  


});


 Route::post('/login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
 Route::post('/register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);    


Route::group(['prefix'=>'/manager','namespace'=>"BackEnd"],function(){
	
    Route::get('/product','ProductController@indexAction');
    
    Route::get('/product/add','ProductController@addAction');
    Route::post('/product/add-store','ProductController@addStoreAction');

    Route::get('/product/edit/{id}','ProductController@editAction');
    Route::post('/product/edit-store','ProductController@editStoreAction');
    // End Product


    Route::get('/product-category','ProductCategoryController@indexAction');
    Route::get('/product-category/add','ProductCategoryController@addAction');
    Route::post('/product-category/add-store','ProductCategoryController@addStoreAction');




    Route::get('/news-category','NewsCategoryController@indexAction');
    Route::get('/news-category/add','NewsCategoryController@addAction');
    Route::post('/news-category/add-store','NewsCategoryController@addStoreAction');



    Route::get('/news','NewsController@indexAction');
    
    Route::get('/news/add','NewsController@addAction');
    Route::post('/news/add-store','NewsController@addStoreAction');

    Route::get('/news/edit/{id}','NewsController@editAction');
    Route::post('/news/edit-store','NewsController@editStoreAction');  
    
     Route::get('/config-html/{id}','HtmlController@detailAction'); 
     Route::post('/config-html/update','HtmlController@storeAction');  

});

});

