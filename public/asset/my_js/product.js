
 var  Product  =  {


 	init : function(){

 		this.uploadImage(); 
 		$(document).ready(function() {
		  $('.summernote').summernote({
		  	  height: 600,                 // set editor height
			  minHeight: null,             // set minimum height of editor
			  maxHeight: null,             // set maximum height of editor
			  focus: true        
		  });
		  
		});
		
 	},

 	uploadImage : function(){

 		$(document).ready(function(){

			$(".inputImage a.upload").on("click",function(){

				var image  = $(this).parents(".inputImage").find("input");
				image.click();

				image.change(function(){

					var reader = new FileReader();
    				reader.onload = function (e) {
		             // get loaded data and render thumbnail.
		             image.parents(".itemImage").find("img").attr("src", e.target.result);	
		   		 };
		       // read the image file as a data URL.
		         reader.readAsDataURL(this.files[0]);	
				});
				return false;

			});
	});

 	}

 }