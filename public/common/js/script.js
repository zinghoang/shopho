/**
 * Look under your chair! console.log FOR EVERYONE!
 *
 * @see http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
 */
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());

/**
 * Page-specific call-backs
 * Called after dom has loaded.
 */

/**
 * Ajaxy add-to-cart
 */
Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "." : t, 
		s = n < 0 ? "-" : "", 
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


$(document).on('click', ".btn_remove_item_cart", function () {
	var pro_id = $(this).attr('data-id');
	var qty =  0;
	Product.updateQuantity(qty, pro_id);
});
$(document).on('click', ".items-count", function () {
	//$(this).parent().children('.items-count').prop('disabled', true);
	var thisBtn = $(this);
	var pro_id = $(this).parent().find('.pro_id').val();
	var qty =  $(this).parent().children('.number-sidebar').val();
	Product.updateQuantity(qty, pro_id);
});
$(document).on('change', ".number-sidebar", function () {
	var pro_id = $(this).parent().children('.pro_id').val();
	var qty =  $(this).val();
	Product.updateQuantity(qty, pro_id);
});

