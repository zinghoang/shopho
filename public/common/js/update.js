

function validate(a) {
    var b = a || window.event,
        c = b.keyCode || b.which;
    c = String.fromCharCode(c);
    var d = /[0-9]|\./;
    d.test(c) || (b.returnValue = !1, b.preventDefault && b.preventDefault())
}
window.dqdt = window.dqdt || {}, dqdt.init = function() {
    dqdt.showPopup(), dqdt.hidePopup()
}, $(window).on("load resize scroll", function(a) {
    $(window).width() < 1025 && ($(".product-box .add_to_cart").text("Mua ngay"), $("a.btn.btn-style.quick-view").css("display", "none"))
}), dqdt.showNoitice = function(a) {
    $(a).animate({
        right: "0"
    }, 500), setTimeout(function() {
        $(a).animate({
            right: "-300px"
        }, 500)
    }, 3500)
}, dqdt.showLoading = function(a) {
    var b = $(".loader").html();
    $(a).addClass("loading").append(b)
}, dqdt.hideLoading = function(a) {
    $(a).removeClass("loading"), $(a + " .loading-icon").remove()
}, dqdt.showPopup = function(a) {
    $(a).addClass("active")
}, dqdt.hidePopup = function(a) {
    $(a).removeClass("active")
}
var product = {},
    currentLinkQuickView = "",
    option1 = "",
    option2 = "";
$(document).on("click", ".quickview-close, #quick-view-product .quickview-overlay, .fancybox-overlay", function(a) {
    $("#quick-view-product").fadeOut(0), dqdt.hidePopup(".loading"), $("#quick-view-product").modal("hide")
});