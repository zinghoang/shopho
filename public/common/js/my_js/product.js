
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};




 var  Product  =  {


 	init : function(){

 		this.quickViewProduct(); 
 		this.addProductToCart();
 		
 	},


 	quickViewProduct  : function() {
      
      
 		$(document).ready(function(){
 			$(".btn_quick_view").on("click",function(){
 				
 			   element   = $(this);
 			   pro_id  = element.attr("data-id");
 			   $.ajax({
	            type: 'GET',
	            data : {pro_id:pro_id},
	            url: '/quick-view-product',
	            dataType:"json",
	            success: function (reponsive){ 
	            	if(!reponsive.error){
	            	   $('#quick-view-product .quick-view-product').html(reponsive.data);
	                   $('#quick-view-product').modal();
	                  	var e = $('#quick-view-product').find(".more-view-wrapper ul .owl-stage");
	                  	e.owlCarousel({
           				 navigation: !0,
				            items: 4,
				            margin: 10,
				            itemsDesktop: [1199, 4],
				            itemsDesktopSmall: [979, 3],
				            itemsTablet: [768, 3],
				            itemsTabletSmall: [540, 3],
				            itemsMobile: [360, 3]
				        }).css("visibility", "visible");

			              Product.addProductToCart();  


	                }         
			    }
		       });
 			   
			
 			   return false;
 			});
 		});
 		

    },


    addProductToCart : function() {

    	$(document).ready(function(){
 			$(".btn_add_to_cart").on("click",function(){
 				
 			   element   = $(this);
 			   pro_id  = element.attr("data-id");
 			   qty   = element.parent(".box-update-to-cart").find("input.quantity").val(); 
 			   console.log(qty);
 			   if( qty < 0){
 			   	qty = 1;
 			   }
 			   $.ajax({
	            type: 'GET',
	            data : {pro_id:pro_id,quantity: qty},
	            url: '/add-to-cart',
	            dataType:"json",
	            success: function (reponsive){ 
	            	if(!reponsive.error){

	            		  new jBox('Notice', {
						    content: 'Sản phẩm đã được thêm vào <a href="/cart-list" >Xem giỏ hàng </a>',
						    color: 'black',
						    attributes: {
						      x: 'right',
						      y: 'bottom'
						    }
						  });  
	            	   // $('#quick-view-product .quick-view-product').html(reponsive.data);
	                //    $('#quick-view-product').modal();
	            	}
	            }
        		});

 			   return false;
 			});
 		});
    },

    updatetoCart : function(){


    	$(document).on('click', ".items-count", function () {
			$(this).parent().children('.items-count').prop('disabled', true);
			var thisBtn = $(this);
			var pro_id = $(this).parent().find('.variantID').val();
			var qty =  $(this).parent().children('.number-sidebar').val();
			this.updateQuantity(qty, pro_id);
		});
    },


    updateQuantity : function(qty,pro_id){

    	$.ajax({
			type: "GET",
			url: "/add-to-cart",
			data: {"quantity": qty, "pro_id": pro_id,"type":"update"},
			dataType: "json",
			success: function (reponsive) {
				//Bizweb.onCartUpdateClick(cart, variantIdUpdate);

				if(reponsive.data){


					if(qty==0){
						$(".productid-"+pro_id).remove();
					}


				   list  = 	 JSON.parse(reponsive.data);

				   total_price  =  30000;
				   for (item in list) {

					 cart  = list[item];
					 total_price+= cart.subtotal;
					 element =$(".cart-tbody .item-cart.productid-"+cart.id);

					 cartPrice   = Number(cart.price);
					 cartSubtotal = Number(cart.subtotal);

					 element.find(".item-price .price").html((cartPrice).format(0,3,",") +"₫");
					 element.find(".cart-price .price").html((cartSubtotal).format(0,3,",")+"₫");

					}

					$(".totals_price.price").html((total_price).format(0,3,",")+"₫");
				 
				}
				
			},
			
		})
    }

}
